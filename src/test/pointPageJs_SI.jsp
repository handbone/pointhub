<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 포인트 조회 결제 스크립트(단일결제)
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/pc/std/pointPageJsSI.jsp
 * @author 이형우
 * @since 2018.11.13
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.13    이형우        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
$(document).ready(function(){
	PointFunc.initEvent();
});

var PointFunc = {
	initEvent:function(){
		//전액충전 버튼 클릭 이벤트
		$('.maxBtn').click(function(){
			var ttlPayAmt = Number($('#ttlPayAmt').attr('val'));	//충전 금액
			var ttlPntAmt = Number($('#ttlPntAmt').attr('val'));	//충전할 포인트
			
			var obj = $(this).parents('.pointGroup');
			var pntObj = obj.find('.pnt');
			var pntAmtObj = obj.find('.pntAmt');
			var avlPntObj = obj.find('.avlPnt');
			var pntExchRate = obj.find('.pntExchRate').val();
			
			var pnt = Number(pntObj.attr('val'));					//충전할 포인트
			var pntAmt = Number(pntAmtObj.attr('val'));				//전환된 결제금액
			var avlPnt = Number(avlPntObj.attr('val'));				//보유 포인트
			
			var modPntAmt = ttlPayAmt - ttlPntAmt;					//남은 충전 금액
			
			if(modPntAmt < (avlPnt - pnt) * pntExchRate){
				pntAmt = getCalcPnt(modPntAmt + (pnt * pntExchRate), 'Y');
				pnt = getCalcPnt(pntAmt / pntExchRate, 'N');
			}else{
				pntAmt = getCalcPnt(avlPnt * pntExchRate, 'Y');
				pnt = getCalcPnt(pntAmt / pntExchRate, 'N');
			}
			
			pntObj.attr('val', pnt);
			pntObj.val(PHUtil.setComma(pnt));
			
			pntAmtObj.attr('val', pntAmt);
			pntAmtObj.text(PHUtil.setComma(pntAmt));
			
			$('#ttlPntAmt').attr('val', getTtlPntAmt());
			$('#ttlPntAmt').text(PHUtil.setComma(getTtlPntAmt()));
			
			//충전금액과 충전할 포인트가 일치할 경우 확인버튼 활성화
			toggleConfirmBtn();
		});
		
		//초기화 버튼 클릭 이벤트
		$('.initBtn').click(function(){
			var pntObj = $(this).parents('.pointGroup').find('.pnt');
			var pntAmtObj = $(this).parents('.pointGroup').find('.pntAmt');
			
			pntObj.attr('val', 0);
			pntObj.val(0);
			
			pntAmtObj.attr('val', 0);
			pntAmtObj.text(0);
			
			$('#ttlPntAmt').attr('val', getTtlPntAmt());
			$('#ttlPntAmt').text(PHUtil.setComma(getTtlPntAmt()));
			
			//충전금액과 충전할 포인트가 일치할 경우 확인버튼 활성화
			toggleConfirmBtn();
		});
		
		//확인 버튼 클릭 이벤트
		$('#confirmBtn').click(function(){
			if(checkValidate()){
				PointFunc.pay();
			}
		});
		
		//취소 버튼 클릭 이벤트
		$('#cancelBtn').click(function(){
			var pgTrNo			= '${result.pg_tr_no}';
			var phubTrNo		= '${result.phubTrNo}';
			var payAmt			= '${result.ttlPayAmt}';
			var points			= 0;
			var wonAmt			= 0;
			var payMethod		= '${result.payMethod}';
			var retCode			= 'F999';
			var retMsg			= '사용자 취소';
			
			$('#return_pgTrNo').val(pgTrNo);
			$('#return_phubTrNo').val(phubTrNo);
			$('#return_payAmt').val(payAmt);
			$('#return_points').val(points);
			$('#return_wonAmt').val(wonAmt);
			$('#return_payMethod').val(payMethod);
			$('#return_retCode').val(retCode);
			$('#return_retMsg').val(retMsg);
			
			var url = '${result.return_url_2}';
			
			var attrObj = {'form' : '#formReturn'};
			PHFnc.doAction(url, '', attrObj);
		});
		
		initPointPage();
	},
	pay:function(){
		var pntList = new Array();
		$('.pointGroup').each(function(){
			var data = new Object() ;
			data.pntCd = $(this).find('.pntCd').val();
			data.pntTrNo = $(this).find('.pntTrNo').val();
			data.pntAmt = $(this).find('.pntAmt').attr('val');
			data.cprtAmt = $(this).find('.pntAmt').attr('val');		//서버체크를 위해 pntAmt와 동일한 값 전송
			data.dealUnit = $(this).find('.dealUnit').val();
			data.pntExchRate = $(this).find('.pntExchRate').val();
			data.avlPnt = $(this).find('.avlPnt').attr('val');
			data.pnt = $(this).find('.pnt').attr('val');
			
			if(Number(data.pntAmt) > 0){
				pntList.push(data);
			}
		});
		var pntListJson = JSON.stringify(pntList); 
		
		var ttlPntAmt = getTtlPntAmt();
		var ttlCprtAmt = getTtlPntAmt();	//서버체크를 위해 ttlPntAmt와 동일한 값 전송
		var ttlRmndAmt = Number($('#ttlPayAmt').attr('val')) - Number(ttlPntAmt);
		
		var url = '/phub/std/payTest.do';
		var params = {
			'phubTrNo'		: '${result.phubTrNo}',
			'phub_tr_no'	: '${result.phubTrNo}',
			'dealInd'		: '${result.dealInd}',
			'dealPgInd'		: '${result.dealPgInd}',
			'custId'		: '${result.custId}',
			'ttlPntAmt'		: ttlPntAmt,
			'ttlCprtAmt'	: ttlCprtAmt,
			'ttlRmndAmt'	: ttlRmndAmt,
			'shopPntRate'	: 1, 
			'pntList'		: pntListJson
 		}
		var type = 'post';
		var dataType = 'json';
		var success = function(data, textStatus, jqXHR){
			if(data.ret_code == '00'){
				PointFunc.pgRetSubmit(data);
			}
		}
		PHFnc.ajax(url, params, type, dataType, success, errCustom, true, true);
	},
	pgRetSubmit:function(data){
		var pgTrNo = data.pg_tr_no;
		var phubTrNo = data.phub_tr_no;
		var payAmt = data.pay_amt;
		var points = data.points;
		var wonAmt = data.won_amt;
		var authLimitDtm = data.auth_limit_dtm;
		var payMethod = data.pay_method;
		var retCode = data.ret_code;
		var retMsg = data.ret_msg;
		var returnUrl2 = data.return_url_2;

		$('#hdn_pgTrNo').val(pgTrNo);
		$('#hdn_phubTrNo').val(phubTrNo);
		$('#hdn_payAmt').val(payAmt);
		$('#hdn_points').val(points);
		$('#hdn_wonAmt').val(wonAmt);
		$('#hdn_authLimitDtm').val(authLimitDtm);
		$('#hdn_payMethod').val(payMethod);
		$('#hdn_retCode').val(retCode);
		$('#hdn_retMsg').val(retMsg);

		var url = returnUrl2;
		var attrObj = {'form' : '#pointForm'};
		PHFnc.doAction(url, '', attrObj);
	}
}

//초기 포인트 값 설정
var initPointPage = function(){
	var ttlPayAmt = $('#ttlPayAmt').attr('val');	//총 결제 금액
	var ttlAvlPnt = 0;								//총 소유 포인트
	
	//총 결제 금액 comma 설정하여 표시
	$('#ttlPayAmt').text(PHUtil.setComma(ttlPayAmt));
	
	//총 소유 포인트 계산
	$('.pointGroup').each(function(){
		var avlPntObj = $(this).find('.avlPnt')
		var avlPnt = avlPntObj.attr('val');
		avlPntObj.text(PHUtil.setComma(avlPnt));
		
		ttlAvlPnt += Number(avlPnt);
	});
	//총 소유 포인트 값 저장
	$('#ttlAvlPnt').attr('val', ttlAvlPnt);
	//총 소유 포인트 comma 설정하여 표시
	$('#ttlAvlPnt').text(PHUtil.setComma(ttlAvlPnt));
	//전환된 결제금액 0 표시
	$('.pntAmt').text(0);
}

//포인트 입력값 input값으로 입력 변환 
function setPntAmt(obj){
	var pntObj = obj.parents('.pointGroup').find('.pnt');
	var pntAmtObj = obj.parents('.pointGroup').find('.pntAmt');
	var pntExchRate = obj.parents('.pointGroup').find('.pntExchRate').val();
	var pnt = pntObj.val();
	
	//입력값 없으면 0처리
	if(pnt == '' || pnt.match(/\d/g) == null){
		pntObj.attr('val', 0);
		pntObj.val('0');
		pntAmtObj.attr('val', 0);
		pntAmtObj.text('0');
	}else{
		pnt = pnt.match(/\d/g);
		pnt = pnt.join("");
		if(pnt.length > 1 && pnt.substring(0,1) == 0){
			pnt = pnt.substring(1, pnt.length);
		}
		var pntAmt = pnt * pntExchRate;
		//포인트 소수점 계산
		pntAmt = getCalcPnt(pntAmt, 'Y');
		
		pntObj.attr('val', pnt);
		pntObj.val(PHUtil.setComma(pnt));
		
		pntAmtObj.attr('val', pntAmt);
		pntAmtObj.text(PHUtil.setComma(pntAmt));
	}
	//포인트 입력 시 각 포인트 확인 및 총포인트 계산, 표시
	setTtlPntAmt(pntObj, pntAmtObj);
}

//포인트 입력 시 각 포인트 확인 및 총포인트 계산, 표시
function setTtlPntAmt(pntObj, pntAmtObj){
	var isValid = true;
	var msg = '';
	var ttlPayAmt = $('#ttlPayAmt').attr('val');	//충전금액
	var ttlPntAmt = getTtlPntAmt();					//충전할 포인트
	
	//충전금액 보다 충전할 포인트가 클 경우
	if(ttlPayAmt < ttlPntAmt){
		isValid = false;
		msg = '입력하신 포인트가 충전금액을 초과 하였습니다.';
	}
	
	//보유 포인트보다 충전할 포인트가 클 경우(카드사별)
	var avlPnt = Number(pntAmtObj.parent().parent().find('.avlPnt').attr('val'));
	var pnt = Number(pntObj.attr('val'));
	
	if(avlPnt < pnt && isValid){
		isValid = false;
		msg = '입력하신 포인트보다 보유 포인트가 부족 합니다.';
	}
	
	if(!isValid){
		PHFnc.alert(msg);
		pntObj.attr('val', 0);
		pntObj.val(0);
		pntAmtObj.attr('val', 0);
		pntAmtObj.text(0);
	}
	
	//충전금액과 충전할 포인트가 일치할 경우 확인버튼 활성화
	toggleConfirmBtn();
	
	$('#ttlPntAmt').attr('val', getTtlPntAmt());
	$('#ttlPntAmt').text(PHUtil.setComma(getTtlPntAmt()));
}

//충전금액과 충전할 포인트가 일치할 경우 확인버튼 활성화
function toggleConfirmBtn(){
	var ttlPayAmt = $('#ttlPayAmt').attr('val');
	var ttlPntAmt = getTtlPntAmt();
	
	if(ttlPayAmt == ttlPntAmt){
		$('#confirmBtn').removeClass();
		$('#confirmBtn').addClass('btn_ok_on');
	}else{
		$('#confirmBtn').removeClass();
		$('#confirmBtn').addClass('btn_ok');
	}
}

// 총포인트값 산출
function getTtlPntAmt(){
	var ttlPntAmt = 0;
	$('.pntAmt').each(function(){
		var pntAmt = $(this).attr('val');
		if(pntAmt == ''){
			pntAmt = 0;
		}
		ttlPntAmt += Number(pntAmt);	  	
	});
	return ttlPntAmt;
}


function checkValidate(){
	var isValid = true;
	var msg = '';
	
	var ttlPntAmt = getTtlPntAmt();
	var ttlPayAmt = $('#ttlPayAmt').attr('val');
	var maxPerDeal = Number('${result.maxPerDeal}');
	var minPerDeal = Number('${result.minPerDeal}');
	
	if(isValid && ttlPayAmt != ttlPntAmt){
		msg = '포인트 총액과 상품금액이 일치하지 않습니다.';
		isValid = false;
	}else if(ttlPntAmt < minPerDeal){
		minPerDeal = PHUtil.setComma(minPerDeal);
		msg = '최소 포인트금액은 '+minPerDeal+'P 입니다.';
		isValid = false;
	}else if(maxPerDeal < ttlPntAmt){
		maxPerDeal = PHUtil.setComma(maxPerDeal);
		msg = '거래당 사용가능한 포인트금액은 '+maxPerDeal+'P 입니다.';
		isValid = false;
	}
	
	if(!isValid){
		PHFnc.alert(msg);
		return isValid;
	}
	
	/* $('.pointGroup').each(function(){
		var pntAmt = Number($(this).find('.pntAmt').attr('val'));
		var avlPnt = Number($(this).find('.avlPnt').attr('val'));
		
		var dealUnit = Number($(this).find('.dealUnit').val());
		var minAvlPnt = Number($(this).find('.minAvlPnt').val());
		var maxPerDeal = Number($(this).find('.maxPerDeal').val());
		
		if(pntAmt > 0){
			if(pntAmt%dealUnit > 0){
				msg = '거래단위 확인';
				isValid = false;
			}else if(avlPnt < minAvlPnt){
				msg = '최소 사용가능 포인트 확인';
				isValid = false;
			}
		}
		if(!isValid){
			PHFnc.alert(msg);
			return isValid;
		}
	}); */
	
	return isValid;
}

//포인트 소수점 계산
function getCalcPnt(pnt, isInput){
	var calcStrMode = '${result.CALC_STR_MODE}';
	
	//직접 입력
	if(isInput == 'Y'){
		if(calcStrMode == 'FLOOR'){
			pnt = Math.floor(pnt);
		}else if(calcStrMode == 'CEIL'){
			pnt = Math.ceil(pnt);
		}else if(calcStrMode == 'ROUND'){
			pnt = Math.round(pnt);
		}
	//전액충전 버튼 클릭
	}else{
		if(calcStrMode == 'FLOOR'){
			pnt = Math.ceil(pnt);
		}else if(calcStrMode == 'CEIL'){
			pnt = Math.floor(pnt);
		}else if(calcStrMode == 'ROUND'){
			pnt = Math.floor(pnt);
		}
	}
	return pnt;
}
</script>
