package com.olleh.pointHub.test.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 본인인증 값 입력까지 자동화
 * 
 * @author harry
 *
 */

public class TestSimpleControllerTest {

	private WebDriver driver;

	@Before
	public void setUp() {
		// 다운받은 ChromeDriver 위치
		System.setProperty("webdriver.chrome.driver", "D:\\home\\harry\\dev\\chromedriver\\chromedriver.exe"); //
		driver = new ChromeDriver(); // Driver 생성
	}

	/**
	 * @throws InterruptedException
	 */
	@Test
	@Ignore
	public void requestFromPgStartTest() throws InterruptedException {

		driver.get("http://localhost/pg/pgfront.do"); // URL로 접속하기

		WebElement h4Title = driver.findElement(By.tagName("H4"));
		assertThat(h4Title.getText(), is("PG사 정보"));

		WebElement btnSetBase = driver.findElement(By.id("btn_setBase"));
		assertThat(btnSetBase, is(notNullValue()));

		WebElement textReturnUrl2 = driver.findElement(By.id("returnUrl2"));
		assertThat(textReturnUrl2, is(notNullValue()));
		assertThat(textReturnUrl2.getAttribute("value").toString(), is("http://test2.do"));

		btnSetBase.click();
		assertThat(textReturnUrl2.getAttribute("value").toString(), is("http://localhost/pg/dummyRetUrl.do"));

		WebElement btnSubmit = driver.findElement(By.id("submitBtn"));
		assertThat(btnSubmit, is(notNullValue()));

		btnSubmit.click();

		String mainWindow = driver.getWindowHandle();
		String stplatWindow = ""; // 약관 윈도우
		String kmcWindow = ""; // 본인인증 윈도우

//		new WebDriverWait(driver, 5).until(ExpectedConditions.numberOfWindowsToBe(2));	
		
		Set<String> handleSet = driver.getWindowHandles();
		Iterator<String> handleIt = handleSet.iterator();
		
		assertThat(handleSet.size(), is(2));

		while (handleIt.hasNext()) {

			String childWindow = handleIt.next();

			if (mainWindow.equalsIgnoreCase(childWindow)) {

				// true일때 액션 없음
				System.out.println("001");
			} else {

				driver.switchTo().window(childWindow);
				stplatWindow = childWindow;

				// 전체약관 동의 클릭 가능할 때까지 대기하고 클릭
//				((new WebDriverWait(driver, 5))
//						.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.check_name")))).click();

				// 약관 윈도에서 본인인증 버튼 클릭
				(driver.findElement(By.xpath("//*[@id='kmcValidateBtn']"))).click();
			}
		}
		
		
//		new WebDriverWait(driver, 5).until(ExpectedConditions.numberOfWindowsToBe(3));		

		// 본인인증 윈도우 컨트롤
		Set<String> handleSet2 = driver.getWindowHandles();
		Iterator<String> handleIt2 = handleSet2.iterator();
		
		assertThat(handleSet2.size(), is(3));

		while (handleIt2.hasNext()) {

			String childWindow = handleIt2.next();

			if (mainWindow.equalsIgnoreCase(childWindow) || stplatWindow.equalsIgnoreCase(childWindow)) {

				// true일때 액션 없음

				System.out.println("002");
			} else {

				System.out.println("003");

				driver.switchTo().window(childWindow);
				kmcWindow = childWindow;

				// kt 인증버튼 대기 후 클릭
//				((new WebDriverWait(driver, 5)).until(
//						ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='container']/ul/li[2]/a"))))
//								.click();

				// 본인확인(문자)탭 대기 후 클릭
//				((new WebDriverWait(driver, 5))
//						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div/ul/li[2]/a"))))
//								.click();

				// 이름 입력박스 대기
//				((new WebDriverWait(driver, 5))
//						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='name']"))))
//								.sendKeys("김학진");

				// 나머지 인증 값 입력
				(driver.findElement(By.xpath("//*[@id='Birth']"))).sendKeys("19770714");
				(driver.findElement(By.xpath("//*[@id='man']"))).click();
				(driver.findElement(By.xpath("//*[@id='No']"))).sendKeys("01053349588");

				(driver.findElement(By.xpath("//*[@id='agree_list01']"))).click();
				(driver.findElement(By.xpath("//*[@id='agree_list02']"))).click();
				(driver.findElement(By.xpath("//*[@id='agree_list03']"))).click();
				(driver.findElement(By.xpath("//*[@id='agree_list04']"))).click();
			}
		}

		//driver.switchTo().window(mainWindow);
	}

	@After
	public void tearDown() throws InterruptedException {

		//Thread.sleep(2000);
		//driver.quit(); // Driver 종료
	}

}
