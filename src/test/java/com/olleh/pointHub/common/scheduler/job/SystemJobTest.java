package com.olleh.pointHub.common.scheduler.job;

import static org.junit.Assert.*;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import org.junit.Ignore;
import org.junit.Test;

import com.olleh.pointHub.common.log.Logger;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations="file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class SystemJobTest {

	private Logger log = new Logger(this.getClass());

	// @Autowired
	// AccsCtrnManage accsCtrnManage;
	//
	// @Autowired
	// CodeManage codeManage;
	//
	// @Autowired
	// MessageManage messageManage;
	//
	// @Autowired
	// PgCmpnInfoManage pgCmpnInfoManage;
	//
	// @Autowired
	// SysPrmtManage sysPrmtManage;
	//
	// @Autowired
	// UrlManage urlManage;

	@Ignore
	@Test
	public void test() {

		try {

			// 공통코드
			// codeManage.reset();

		} catch (Exception e) {

			e.printStackTrace();
			fail("Exception....");
		}
	}

	@Ignore
	@Test
	public void getHostServerIP() throws SocketException {

		String methodName = "SystemJobTest.getHostServerIP()";

		log.debug(methodName, "test-log : " + "start");

		Enumeration<NetworkInterface> nienum = NetworkInterface.getNetworkInterfaces();

		assertNotNull("NetworkInterface is null", nienum);

		log.debug(methodName, "test-log : " + nienum.toString());

		while (nienum.hasMoreElements()) {

			NetworkInterface ni = nienum.nextElement();
			Enumeration<InetAddress> iaenum = ni.getInetAddresses();

			while (iaenum.hasMoreElements()) {
				
				InetAddress inetAddress = iaenum.nextElement();

				log.debug(methodName, "test-log : out : " + inetAddress.getHostAddress().toString());

				if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress()
						&& inetAddress.isSiteLocalAddress()) {

					log.debug(methodName, "test-log : in : " + inetAddress.getHostAddress().toString());
				}				
				
				if (inetAddress.isLoopbackAddress()) {

					log.debug(methodName, "test-log : LoopbackAddress : " + inetAddress.getHostAddress().toString());
				}
				else if (inetAddress.isLinkLocalAddress()) {

					log.debug(methodName, "test-log : LinkLocalAddress : " + inetAddress.getHostAddress().toString());
				}
				else if (inetAddress.isSiteLocalAddress()) {

					log.debug(methodName, "test-log : SiteLocalAddress : " + inetAddress.getHostAddress().toString());
				}
				else {
					
					log.debug(methodName, "test-log : " + inetAddress.getHostAddress().toString());
				}
				
			}
		}
	}
	
	@Ignore
	@Test
	public void isHostServerIP() throws SocketException {
		
		String cmprIP = "10.200.85.175";
		//String cmprIP = "10.200.85.170";
		//String cmprIP = "127.0.0.1";
		
		String methodName = "SystemJobTest.isHostServerIP()";
		boolean b = false;

		try {
			
			Enumeration<NetworkInterface> niEnum = NetworkInterface.getNetworkInterfaces();

			breakOut :
			while (niEnum.hasMoreElements()) {

				NetworkInterface ni = niEnum.nextElement();
				Enumeration<InetAddress> iaEnum = ni.getInetAddresses();

				while (iaEnum.hasMoreElements()) {
					
					InetAddress inetAddress = iaEnum.nextElement();
					
					log.debug(methodName, "Address : " + inetAddress.getHostAddress().toString());

					if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress()
							&& inetAddress.isSiteLocalAddress()
							&& org.apache.commons.lang3.StringUtils.equals(cmprIP, inetAddress.getHostAddress().toString())) {
						
						log.debug(methodName, "result Address : " + inetAddress.getHostAddress().toString());
						
						b = true;
						break breakOut;
					}
				}
			}
		} catch (SocketException e) {

			e.printStackTrace();
			b = false;
		}
		
		assertTrue(b);
	}

}
