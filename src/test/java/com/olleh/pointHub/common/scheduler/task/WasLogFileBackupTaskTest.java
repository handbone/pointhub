package com.olleh.pointHub.common.scheduler.task;

import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

public class WasLogFileBackupTaskTest {

	@Test
	public void test() throws Exception {
		
		WasLogFileBackupTask wasLogFileBackupTask = new WasLogFileBackupTask();
		
		Map<String,Object> ret = new HashMap<String,Object>();
		
		String fileName = "";
		
		//-------------------------------------------------
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1); //현재일자에서 D-1
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				
		String dateStr = format.format(calendar.getTime());
		
				
		System.out.println("test log : " + dateStr);
		
		//-------------------------------------------------
		
		
		Map<String, String> params = new HashMap<String, String>();
		
		params.put("trgetDe", dateStr);
		
		
		
		ret = wasLogFileBackupTask.logFileBackup(params);
		
				
		
		if(StringUtils.isBlank(dateStr)){
			
			fail("dateStr is blank");
		}
		
		
		if(ret == null){
			
			fail("fileName is blank");
		}
		
		
	}

}
