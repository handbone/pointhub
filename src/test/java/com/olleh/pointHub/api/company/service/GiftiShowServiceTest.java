package com.olleh.pointHub.api.company.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.olleh.pointHub.api.paygate.service.SbankService;
import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class GiftiShowServiceTest {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	AccsCtrnManage  accsManage;
	
	@Autowired
	GiftiShowService giftishowService;
	
	@Autowired
	SbankService sbankService;
	

	@Before
	public void setUp() throws Exception {
	}


	// 기프티쇼 테스트
	//@Ignore
	@Test
	public void isAccsIpGsTest() throws BizException, Exception {
		
		// mdcode == pgSendPoId
		String mdcode = "111";	// mid_test=PO0001, KOS=PO0011, 111=PO0006
		
		//mid_test=PO0001 = 없음
		//KOS=PO0011 = 127.0.0.1, 0:0:0:0:0:0:0:1
		//111=PO0006 = 1.1.1.7, 1.1.1.4
		
		//String ipAddress = SystemUtils.getIpAddress(request);
		String ipAddress = "1.1.1.4";
		
		
		// 기프티쇼 테스트
		String cprtCmpnId = giftishowService.getCprtCmpnId(mdcode);

		
		assertThat(cprtCmpnId, is(notNullValue()));
		assertThat(cprtCmpnId, is("PO0006"));
		
		
		if (!accsManage.isAccsIp(cprtCmpnId, ipAddress)) {
			
    		fail("접근가능 IP 없음");
        }
	}

	
	// 세틀뱅크 테스트
	//@Ignore
	@Test
	public void isAccsIpSbTest() throws BizException, Exception {
		
		//String ipAddress = SystemUtils.getIpAddress(request);
		String ipAddress = "61.250.95.42";	// 세틀뱅크 테스트용 "61.250.95.42", "61.250.95.43", "127.0.0.1"
				
		
		// 세틀뱅크 테스트
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("phub_tr_no", "PH19032200107216");		// PH19040200107669, PH19032200107213, PH19032200107216 무작위 3개
		String cprtCmpnId = sbankService.getPgId(params);	// 세틀뱅크==PG0001
		
		
		assertThat(cprtCmpnId, is(notNullValue()));
		assertThat(cprtCmpnId, is("PG0001"));
		
		
		if (!accsManage.isAccsIp(cprtCmpnId, ipAddress)) {
			
    		fail("접근가능 IP 없음");
        }
		
	}
	
	
	
	@After
	public void tearDown() throws Exception {
	}
}
