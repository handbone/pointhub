package com.olleh.pointHub.api.company.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.model.AccsCtrnInfoVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class KOSControllerTest {
	
	@Autowired
	AccsCtrnManage  accsManage;
	
	@Autowired
	GiftiShowService giftiShowService;

	@Before
	public void setUp() throws Exception {
	}

	@Ignore
	@Test
	public void athnKeyTest() throws BizException, Exception {

		// 테스트용 변수
	    // KOS=PO0011	
		String kosAuth = "12345"; // 12345
				
		String athnKey = "";
		
		ArrayList<AccsCtrnInfoVO> accsCtrnInfoList = (ArrayList<AccsCtrnInfoVO>) accsManage.getAccsCtrnInfoVO(giftiShowService.getCprtCmpnId("KOS"));
		
		assertThat(accsCtrnInfoList, is(notNullValue()));
		assertThat(accsCtrnInfoList.size(), is(2));
	    
		// giftiShowService.getCprtCmpnId("KOS") --> PG제공사용처ID(PG_SEND_PO_ID)로 관계사ID(RLTN_CORP_ID) 구함
		// 관계사ID(RLTN_CORP_ID)로 List<AccsCtrnInfoVO>를 얻어와서 athnKey값을 구한다. 
		for(AccsCtrnInfoVO accsCtrnInfoVO : accsManage.getAccsCtrnInfoVO(giftiShowService.getCprtCmpnId("KOS"))){
			athnKey = accsCtrnInfoVO.getAthnKey();
		}
		
		assertThat(athnKey, is(notNullValue()));
		assertThat(athnKey, is("12345"));
		
		if(!StringUtils.equals(kosAuth, athnKey)){
			fail("IF_INFO_102, F102, invalid authorization");
		}
		
	}


	@Test
	public void athnKeyTest2() throws BizException, Exception {

		// 테스트용 변수
	    // KOS=PO0011	
		String kosAuth = "12345"; // 12345
		String cprtCmpnId = "";
		
		
		cprtCmpnId = giftiShowService.getCprtCmpnId("KOS");
		
		assertThat(cprtCmpnId, is(notNullValue()));
		assertThat(cprtCmpnId, is("PO0011"));
		
		
		if(!accsManage.isAccsAuth(cprtCmpnId, kosAuth)){
			fail("IF_INFO_102, F102, invalid authorization");
		}
	}
	
	
	@After
	public void tearDown() throws Exception {
	}
}
