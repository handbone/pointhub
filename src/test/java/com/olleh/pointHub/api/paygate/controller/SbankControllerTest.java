package com.olleh.pointHub.api.paygate.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.CoreMatchers.*;

import java.nio.charset.Charset;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;

/**
 * @author harry
 *
 * <pre>
 * 컨트롤러 테스트 /pg/pay
 * 세틀뱅크에서 오는 취소 테스트 tr_div 값을 'CA', 'MA' (일반취소/망취소) 테스트함
 * 결제 성공한 API 연동이력에서 필요한 파라메터 참고하여 셋팅
 * 
 * 최종 리턴받은 ret_code 값이 "00" 이면 테스트 성공으로 정함
 * 테스트 후 DB 롤백됨
 * 
 * 소스 중간에 타 시스템 연동이 있으므로 이클립스 톰켓서버가 실행중인 상태에서 테스트 해야됨
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
	@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/spring/root-context.xml"),
	@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml")
})
@WebAppConfiguration
@Transactional
public class SbankControllerTest {
	
	@Autowired
	WebApplicationContext context;
	
	MockMvc mockMvc;
		
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
	
	@Before
	public void setUp() throws Exception {
		
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		//mockMvc = MockMvcBuilders.standaloneSetup(testSimpleController).build();
	}

	/**
	 * 세틀뱅크 취소요청 테스트 "CA"
	 * @throws Exception
	 */
	@Test
	public void initPayFromPgTestCa() throws Exception {
		
//		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		 
//		parameters.add("phub_tr_no", "PH19061100109548");														// 포인트허브 거래번호
//		parameters.add("tr_div", "CA");																			// CA, MA
//		parameters.add("points", "300");																		// 결제 금액
//		parameters.add("pg_tr_no", "20190611165456");															// pg_tr_no
//		parameters.add("token", "KHFbRRsWnOdUQsDsf55+E7LBTjpY2LsoHCd67nB5f8Q\u003d");							// 
//		parameters.add("check_hash", "d11b30503e6f06bf85c35e12bb9a5ea0d4ae4ae91868fe35890c43465ed1c8a0");		// 
//		parameters.add("ori_pg_tr_no", "20190611165456");														// pg_tr_no

		String content = "{\"phub_tr_no\":\"PH19061800109862\""
				+ ",\"tr_div\":\"CA\""
				+ ",\"points\":\"300\""
				+ ",\"pg_tr_no\":\"20190618120348\""
				+ ",\"token\":\"CpQUpiOmr2Wfuhrqa1D04p4l9/Ap1Lf748m1vQOTf3M\u003d\""
				+ ",\"check_hash\":\"6a00e9f3e5f78212a64b26583cce59cd1ed1b3fcf2babfdcf84b7d8c08e137ed\""
				+ ",\"ori_pg_tr_no\":\"20190618120348\"}";
		
		mockMvc.perform(post("/pg/pay").contentType(contentType).content(content))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.ret_code", is("00")))
		.andDo(print());
	}
	
	
	/**
	 * 세틀뱅크 취소요청 테스트 "MA"
	 * @throws Exception
	 */
	@Test
	public void initPayFromPgTestMa() throws Exception {
		
		String content = "{\"phub_tr_no\":\"PH19061800109862\""
				+ ",\"tr_div\":\"MA\""
				+ ",\"points\":\"300\""
				+ ",\"pg_tr_no\":\"20190618120348\""
				+ ",\"token\":\"CpQUpiOmr2Wfuhrqa1D04p4l9/Ap1Lf748m1vQOTf3M\u003d\""
				+ ",\"check_hash\":\"6a00e9f3e5f78212a64b26583cce59cd1ed1b3fcf2babfdcf84b7d8c08e137ed\""
				+ ",\"ori_pg_tr_no\":\"20190618120348\"}";
		
		mockMvc.perform(post("/pg/pay").contentType(contentType).content(content))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.ret_code", is("00")))
		.andDo(print());
	}	
	
	@After
	public void tearDown() throws Exception {

	}
}
