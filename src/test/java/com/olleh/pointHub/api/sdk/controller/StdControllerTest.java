package com.olleh.pointHub.api.sdk.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.olleh.pointHub.api.provider.service.ClipService;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class StdControllerTest {
	
	@Autowired
	ClipService clipService;

	@Before
	public void setUp() throws Exception {
	}

	/**
	 * 최소 사용가능 포인트 체크
	 * 
	 * 개발 시점 테스트
	 */
	@Ignore
	@Test
	public void requestPayFromSdkTest() {

		List<Map<String, Object>> pntList = new ArrayList<Map<String, Object>>();

		pntList = JsonUtil.JsonToList(
				"[{pntCd:bccard,pntTrNo:BC190528000000026573,pntAmt:70,cprtAmt:70,dealUnit:1,pntExchRate:0.70,avlPnt:66900,pnt:100,minAvlPnt:100},{pntCd:kbpointree,pntTrNo:KB190528000000026573,pntAmt:230,cprtAmt:230,dealUnit:1,pntExchRate:1.00,avlPnt:70400,pnt:230,minAvlPnt:150}]");

		assertThat(pntList, is(notNullValue()));

		assertThat(pntList.size(), is(2));
		
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("pg_cmpn_id", "PG0001");
		paramMap.put("shop_cd", "mid_test");
		
		List<Map<String, Object>> cardlist = clipService.getBasicPointInfo(paramMap);

		int i=0;
		int tpnt = 0;
		int apnt = 0;
		
		int tmp = 0;
		
		int i_pnt = 0;
		int i_minAvlPnt = 0;
		
		String testStr = "";

		for (Map<String, Object> pnt : pntList) {
			
			for(Map<String, Object> card : cardlist){
				
				if(StringUtils.equals((String)pnt.get("pntCd"), (String)card.get("pntCd"))){
					
					i = i + 1;
					testStr = testStr + (String)pnt.get("pntCd");
					
					i_pnt = ((Double)pnt.get("pnt")).intValue();
					i_minAvlPnt = ((Integer)card.get("minAvlPnt")).intValue();
					
					if( i_pnt >= i_minAvlPnt ){
						
						tpnt = tpnt + i_pnt;
						apnt = apnt + i_minAvlPnt;
					}
					
					
					
				}
				
			}
		}
		
		assertThat(i, is(2));
		assertThat(testStr, is("bccardkbpointree"));
		
		assertThat(tpnt, is(330));	//	330
		assertThat(apnt, is(250));	//	250

	}
	
	
	/**
	 * 최소 사용가능 포인트 체크
	 * 
	 * 개발 서버에 적용한 소스로 테스트
	 * 200	하나
	 * 100	비씨
	 * 200	삼성
	 * 150	KB
	 * 200	신한
	 */
	@Test
	@Ignore
	public void requestPayFromSdkTest2() {

		List<Map<String, Object>> pntList = new ArrayList<Map<String, Object>>();
		pntList = JsonUtil.JsonToList(
				"[{pntCd:bccard,pntTrNo:BC190529000000026833,pntAmt:700,cprtAmt:700,dealUnit:1,pntExchRate:0.70,avlPnt:45700,pnt:0,minAvlPnt:100}"
				+ ",{pntCd:kbpointree,pntTrNo:KB190529000000026833,pntAmt:500,cprtAmt:500,dealUnit:1,pntExchRate:1.00,avlPnt:49800,pnt:0,minAvlPnt:150}"
				+ ",{pntCd:shinhan,pntTrNo:SH190529000000026833,pntAmt:500,cprtAmt:500,dealUnit:1,pntExchRate:1.00,avlPnt:53600,pnt:0,minAvlPnt:1500}"
				+ ",{pntCd:hanamembers,pntTrNo:HA190529000000026833,pntAmt:500,cprtAmt:500,dealUnit:1,pntExchRate:1.00,avlPnt:57000,pnt:0,minAvlPnt:200}"
				+ ",{pntCd:hyundaicard,pntTrNo:HD190529000000026833,pntAmt:500,cprtAmt:500,dealUnit:1,pntExchRate:0.67,avlPnt:57000,pnt:0,minAvlPnt:1500}"
				+ ",{pntCd:samsungcard,pntTrNo:SS190529000000026833,pntAmt:800,cprtAmt:800,dealUnit:1,pntExchRate:1.00,avlPnt:60400,pnt:0,minAvlPnt:200}]");

		assertThat(pntList, is(notNullValue()));
		assertThat(pntList.size(), is(6));
				
		Map<String,Object> pgParamMap = new HashMap<String,Object>();
		
		pgParamMap.put("pg_cmpn_id", "PG0001");
		pgParamMap.put("shop_cd", "mid_test");
		
		List<Map<String, Object>> prvdrlist = clipService.getBasicPointInfo(pgParamMap);

		
		for (Map<String, Object> pntMap : pntList) {
			
			int pnt = ((Double)pntMap.get("pnt")).intValue();	// 서버에 적용한 소스와 다름 Double 처리하는 방식이 다른거 같음
			
			if(pnt < 1){
				continue;
			}
			
			for(Map<String, Object> prvdrMap : prvdrlist){
				
				if(StringUtils.equals((String)pntMap.get("pntCd"), (String)prvdrMap.get("pntCd"))){
										
					int minAvlPnt = Integer.valueOf(StringUtil.nvl(prvdrMap.get("minAvlPnt"),"0"));
					
					if(pnt < minAvlPnt){
						
						Assert.fail((String)pntMap.get("pntCd"));
					}
				}
			}
		}

	}
	

	@After
	public void tearDown() throws Exception {
	}
}
