<%@  page language="java" contentType="text/html; charset=UTF-8" buffer="16kb"
%><%@include file="/WEB-INF/views/include/globalVar.jsp"
%><%--
 **********************************************************************************************
 * @desc : PC 공통 LAYOUT -> Footer
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/pc/include/globalVar.jsp
 * @author lys
 * @since 2018.07.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.07   lys        공통 LAYOUT -> Footer 
 * </pre>
 **********************************************************************************************
--%>

<%--
	**************************************************************************
	Footer HTML 소스 구현
	**************************************************************************
 --%>

<%-- 공통 js 
<script src="${ResRoot}/common/js/ph-const.js?${TimeStamp}"></script>
<script src="${ResRoot}/common/js/ph-event.js?${TimeStamp}"></script>
<script src="${ResRoot}/common/js/ph-fnc.js?${TimeStamp}"></script>
<script src="${ResRoot}/common/js/ph-util.js?${TimeStamp}"></script>
<script src="${ResRoot}/common/js/ph-valid.js?${TimeStamp}"></script>
<script type="text/javascript">
$(function(){
	// javascript 구현
});
</script>
<%--
<section id="pop-loading" class="popup loading" style="position:fixed;">
	<div class="bg"></div>
	<img src="${ResRoot}/mb/images/common/loading.gif" alt="로딩중"/>
</section>

<footer>
	<small>Copyright (c) 2018 KT, Inc.</small>
</footer>
--%>