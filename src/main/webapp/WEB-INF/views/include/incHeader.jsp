<%@  page language="java" contentType="text/html; charset=UTF-8"
%><%@include file="/WEB-INF/views/include/globalVar.jsp"
%><%--
 **********************************************************************************************
 * @desc : 공통 LAYOUT -> header
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/include/incHeader.jsp
 * @author lys
 * @since 2018.07.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.07   lys        공통 LAYOUT -> header 
 * </pre>
 **********************************************************************************************
--%>
<header ${param.pageType eq 'MAIN' ? 'class="main"' : '' }>
	<h1>KT 포인트 허브</h1>
	<jsp:include page="./incMenu.jsp" />
</header>