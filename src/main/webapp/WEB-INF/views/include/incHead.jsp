<%@  page language="java" contentType="text/html; charset=UTF-8"
%><%--
 **********************************************************************************************
 * @desc : 공통 LAYOUT -> head
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/include/incHead.jsp
 * @author lys
 * @since 2018.07.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.07   lys        공통 LAYOUT -> head 
 * </pre>
 **********************************************************************************************
--%>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <title>PC POINTHUB</title>
    
    <link rel="stylesheet" href="${ResRoot}/pc/css/style.css">
	<link rel="stylesheet" type="text/css" media="screen" href="${ResRoot}/common/jQueryUi/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${ResRoot}/common/jQgrid/css/ui.jqgrid.css" />	
	
	<script type="text/javascript" src="${ResRoot}/common/jQuery/jquery.min.js"></script>
	<script type="text/javascript" src="${ResRoot}/common/jQueryUi/jquery-ui.min.js"></script>
	<script type="text/javascript" src="${ResRoot}/common/jQgrid/src/jquery.jqGrid.js"></script>
	<script type="text/javascript" src="${ResRoot}/common/jQgrid/src/i18n/grid.locale-kr.js"></script>
	<script type="text/javascript" src="${ResRoot}/common/jQueryValid/jquery.validate.min.js"></script>
	<script type="text/javascript" src="${ResRoot}/common/jQueryValid/additional-methods.min.js"></script>
	<script type="text/javascript" src="${ResRoot}/common/jQueryValid/messages_ko.min.js"></script>
	<script type="text/javascript" src="${ResRoot}/common/pubJs/script.js?v=js_Ver1"></script>
    <!--[if lt IE 9]>
    <script src="${ResRoot}/common/pubJs/html5shiv.js"></script>
    <script src="${ResRoot}/common/pubJs/html5shiv-printshiv.js"></script><![endif]-->
    
    
         
	
