<%@  page language="java" contentType="text/html; charset=UTF-8"
%><%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"
%><%@taglib prefix="fn"	    uri="http://java.sun.com/jsp/jstl/functions"
%><%@taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt"
%><%@taglib prefix="spring" uri="http://www.springframework.org/tags"
%><%--
 **********************************************************************************************
 * @desc : MB 공통 전역변수 설정
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/mb/include/globalVar.jsp
 * @author lys
 * @since 2018.07.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.07   lys        MB 공통 전역변수 설정 
 * </pre>
 **********************************************************************************************
--%><%
//String  gbTimeStamp   = String.valueOf(System.currentTimeMillis());
String  gbTimeStamp   = "19032802";
pageContext.setAttribute("CRCN", "\r\n");
pageContext.setAttribute("CN", "\n");
%>
<spring:eval var="GbServerType"   expression="3"/> <%-- 1:운영 2:테스트 3:로컬 --%>
<c:set var="TimeStamp"            value="${(GbServerType ne '1') ? gbTimeStamp : '19061701'}"	scope="request"/>
<c:set var="ResRoot"              value="${pageContext.request.contextPath}/resources"			scope="request"/>
<c:set var="ViewRoot"             value="${pageContext.request.contextPath}"					scope="request"/>