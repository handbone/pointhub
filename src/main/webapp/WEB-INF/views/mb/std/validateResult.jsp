<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!-- 
   Point Hub version 1.0
  
   Copyright ⓒ 2014 kt corp. All rights reserved.
   
   This is a proprietary software of kt corp, and you may not use this file except in 
   compliance with license agreement with kt corp. Any redistribution or use of this 
   software, with or without modification shall be strictly prohibited without prior written 
   approval of kt corp, and the copyright notice above does not evidence any actual or 
   intended publication of such software. 
 -->
<%--
 **********************************************************************************************
 * @desc : KMC본인인증 결과 수신 페이지
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/mb/std/validateResult.jsp
 * @author 이형우
 * @since 2018.11.13
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.13    이형우        최초생성
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/mb/include/globalVar.jsp" flush="false" />
	
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/mb/include/incHead.jsp" flush="false" />
</head>
<body>
	<c:if test="${service_id == 'SVC_BASE'}">
		<div id="wrap">
		<!-- header 시작-->
		    <header class="index">
		        <div class="container">
		            <div class="title">포인트다모아</div>
		        </div>
		    </header>
		<!-- header 끝-->
		</div>
	</c:if>
	<c:if test="${service_id == 'SVC_FP'}">
		<div id="wrap" class="family_p">
		<!-- header 시작-->
		    <header class="index">
		        <div class="container">
		            <div class="title">패밀리 포인트<br>(5G 스마트폰 할인권)</div>
		        </div>
		    </header>
		<!-- header 끝-->
		</div>
	</c:if>

<%--
	*******************************************************************************
	** Html 구현
	*******************************************************************************
--%>
<%-- Footer --%>
<jsp:include page="/WEB-INF/views/mb/include/incFooter.jsp" flush="false" />

<script type="text/javascript">

$(document).ready(function(){
	PHFnc.progressbar(true, false);
	clsAgr();
});

function clsAgr(){
	//TODO phubTrNo 확인
	var phubTrNo =  '${phubTrNo}';
	
	var url = '/phub/std/clsAgr.do';
	var params = {
			'phubTrNo'	: phubTrNo
	}
	var type = 'post';
	var dataType = 'json';
	var success = function(data, textStatus, jqXHR){
		if(data.ret_code == '00'){
			if(data.serviceId == 'SVC_FP' && data.payYn == 'N'){				
				if(data.path == 'CL'){		  
					PHFnc.doAction("/phub/fp/couponList.do", "custId="+data.custId);															
				}else{
					PHFnc.doAction("/phub/fp/familyList.do", "phub_tr_no=${phubTrNo}");										
				} 
			}else{
				PHFnc.doAction("/phub/std/point.do", "phubTrNo=${phubTrNo}");
			}
		}else{
			PHFnc.progressbar(false, false);
			PHFnc.alert(data.ret_msg);
		}
	}
	PHFnc.ajax(url, params, type, dataType, success, errCustom, true, true);
}
</script>
</body>
</html>