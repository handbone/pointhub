<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!-- 
   Point Hub version 1.0
  
   Copyright ⓒ 2014 kt corp. All rights reserved.
   
   This is a proprietary software of kt corp, and you may not use this file except in 
   compliance with license agreement with kt corp. Any redistribution or use of this 
   software, with or without modification shall be strictly prohibited without prior written 
   approval of kt corp, and the copyright notice above does not evidence any actual or 
   intended publication of such software. 
 -->
<%--
 **********************************************************************************************
 * @desc : 포인트 조회 결제 화면(패밀리포인트 - 모바일)
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/mb/std/pointPage_FP.jsp
 * @author 이형우
 * @since 2018.11.13
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.13    이형우        최초생성
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/mb/include/globalVar.jsp" flush="false" />
	
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/mb/include/incHead.jsp" flush="false" />
</head>
<body>



<div id="wrap" class="family_p">
<!-- header 시작-->
    <header class="index">
		<div class="container">
			<form id="pointForm" method="post">
				<input type="hidden" id="hdn_pgTrNo" name="pg_tr_no">
				<input type="hidden" id="hdn_phubTrNo" name="phub_tr_no">
				<input type="hidden" id="hdn_payAmt" name="pay_amt">
				<input type="hidden" id="hdn_points" name="points">
				<input type="hidden" id="hdn_wonAmt" name="won_amt">
				<input type="hidden" id="hdn_authLimitDtm" name="auth_limit_dtm">
				<input type="hidden" id="hdn_payMethod" name="pay_method">
				<input type="hidden" id="hdn_retCode" name="ret_code">
				<input type="hidden" id="hdn_retMsg" name="ret_msg">
			</form>
			
			<input type="hidden" id="itemName" name="itemName" >
			<input type="hidden" id="itemPrice" name="itemPrice" value="0">
			<input type="hidden" id="totalPointAmtVal" name="totalPointAmtVal" value="0">
			
			<div class="title">패밀리 포인트<br>(5G 스마트폰 할인권)</div>
			<!-- <div class="script_title">충전 포인트 선택</div> -->
			<div class="script_text">사용할 포인트를 입력하시면 할인권금액으로 전환됩니다.</div>
			<div class="total">
				<ul>
					<li style="width: 100%;">
						<div class="point">
							<ul>
								<li>할인권금액</li>
								<li><span id="ttlPntAmt" val="0">0</span>P</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
        </div>
    </header>
<!-- header 끝-->

<!-- contents 시작-->
	<section class="contents" style="padding: 0px 12px 12px 12px;">
		<div class="container" style="padding: 0 12px 70px !important;">
			<div class="have_point">
				<ul>
					<li><span id="custName"><c:out value="${result.custName}"/></span>님 총 보유 포인트</li>
					<li><span id="ttlAvlPnt"></span>P</li>
				</ul>
			</div>
<c:if test="${fn:length(result.pntList) == 0}">
			<section class="point_null">
				<div class="point_null_script">
					고객님의 충전 가능 포인트가 없습니다.
				</div>
			</section>
</c:if>
<c:if test="${fn:length(result.pntList) > 0}">
			<div class="item_area">
				<div class="item_2th" style="padding: 0px;">
					<div class="tab">
						<ul>
							<li class="tb_top"></li>
							<li class="tb_top">보유<br>포인트</li>
							<li class="tb_top">사용<br>포인트</li>
							<li class="tb_top">전환<br>포인트</li>
						</ul>
					</div>
				</div>
<c:forEach var="pntMap" items="${result.pntList}">
				<div class="item_2th pointGroup">
					<div class="tab">
						<ul>
							<li class="tb_middle"><img src='<c:out value="${pntMap.imageLink}"/>' alt='<c:out value="${pntMap.pntNm}"/>'></li>
							<li class="tb_middle"><span class="avlPnt" val='<c:out value="${pntMap.avlPnt}"/>'></span><c:out value="${pntMap.dpUnit}"/></li>
							<li class="tb_middle"><input type="text" class="pnt" val="0" oninput="setPntAmt($(this));" value="0"><c:out value="${pntMap.dpUnit}"/></li>
							<li class="tb_middle"><span class="cprtAmt" val='<c:out value="0"/>'></span><c:out value="${pntMap.dpUnit}"/></li>
						</ul>
						<ul>
							<li class="tb_bottom"></li>
							<li class="tb_bottom"></li>
							<li class="tb_bottom"><div class="btn maxBtn">전액충전</div></li>
							<li class="tb_bottom"><div class="btn initBtn">초기화</div></li>
						</ul>
					</div>
					<input type="hidden" class="pntCd" value='<c:out value="${pntMap.pntCd}"/>'>
					<input type="hidden" class="pntTrNo" value='<c:out value="${pntMap.pntTrNo}"/>'>
					<input type="hidden" class="dealUnit" value='<c:out value="${pntMap.dealUnit}"/>'>
					<input type="hidden" class="minAvlPnt" value='<c:out value="${pntMap.minAvlPnt}"/>'>
					<input type="hidden" class="pntExchRate" value='<c:out value="${pntMap.pntExchRate}"/>'>
					<input type="hidden" class="pntAmt">
				</div>
	            
				<c:if test="${not empty pntMap.pntRule}">
					<div class="item_script">
						<%--
						<c:if test="${pntMap.pntExchRate < 1}">
							<div class="bullet" style="color: red;">※</div>
							<div class="script" style="color: red;">
								<c:out value="${pntMap.pntNm}"/>는 <fmt:parseNumber integerOnly="true" value="${pntMap.pntExchRate * 100}"/>%만 전환 됩니다.
							</div>
						</c:if>
						--%>
						<c:if test="${pntMap.pntCd == 'samsungcard'}">
							<div class="bullet" style="color: red;">※</div>
							<div class="script" style="color: red;"><c:out value="${pntMap.pntRule}" escapeXml="false" /></div>
						</c:if>
						<c:if test="${pntMap.pntCd != 'samsungcard'}">
							<div class="bullet">※</div>
							<div class="script"><c:out value="${pntMap.pntRule}" escapeXml="false" /></div>
						</c:if>
					</div>
				</c:if>
				
				<c:if test="${empty pntMap.pntRule}">
					<div class="item_script">
						<c:if test="${pntMap.pntExchRate < 1}">
							<div class="bullet" style="color: red;">※</div>
							<div class="script" style="color: red;">
								<c:out value="${pntMap.pntNm}"/>는 <fmt:parseNumber integerOnly="true" value="${pntMap.pntExchRate * 100}"/>%만 전환 됩니다.
							</div>
						</c:if>
					</div>
				</c:if>
				<div class="dot"><div class="dot_line"></div></div>
</c:forEach>
			</div>
</c:if>
		</div>           
	</section>
    <!-- contents 끝-->
    <!-- footer 시작-->
	<section class="footer" style="padding: 0 0 48px;">
		<div class="total_point">
			<div class="tab">
				<ul>
					<li class="tb_middle">합계</li>
					<li class="tb_middle"><span id="total_avlPnt"></span>P</li>
					<li class="tb_middle"><span id="total_pnt">0</span>P</li>
					<li class="tb_middle"><span id="total_pntAmt">0</span>P</li>
				</ul>
			</div>
			<div class="dot"><div class="dot_line"></div></div>
			<div class="item_script">
				<c:if test="${result.shopPntRate+0 > 0}">
					<div class="bullet" style="color: red;">※</div>
					<div class="script" style="color: red;">
						전환 포인트는 <c:out value="${result.pgCmpnNm}"/> 수수료 <c:out value="${result.shopPntRate}"/>%가 차감된 포인트 입니다.
					</div>
				</c:if>
			</div>
		</div>
		<div class="submet_btn" style="position: fixed; bottom: 0;">
			<ul>
				<li><a href="#!"><div id="cancelBtn" class="btn_cancel">취소</div></a></li>
				<li><a href="#!"><div id="confirmBtn" class="btn_ok">확인</div></a></li>
			</ul>
		</div>
	</section>
	<!-- footer 끝-->
	
	<!-- popup 시작-->
	<div id="myPopup" class="popup">
		<div class="popup_content2">
			<div class="popup_title"><span class="ls">패밀리 포인트(5G 스마트폰 할인권)</span><div class="close pop_close"></div></div>
			<div class="text">
				<p class="script1">5G 스마트폰 할인권(<span id="pop_ttlAmt"></span>)을 구매하여<br><span id="pop_ownerName"></span>님에게 보내시겠습니까?</p>
				<p class="script2">※ 할인권을 보내시면 다시 포인트로 전환할 수 없습니다.</p>
			</div>
			<div class="btn_area">
				<a href="#!" class="close_btn btn_cancel pop_close">취소</a>
				<a href="#!" class="btn_ok pop_ok">확인</a>
			</div>
		</div>
	</div>
	<!-- popup 끝-->
	<!-- popup 시작-->
	<div id="myPopup2" class="popup">
		<div class="popup_content2">
			<div class="popup_title"><span class="ls">패밀리 포인트(5G 스마트폰 할인권)</span><div class="close pop_close"></div></div>
			<div class="text">
				<p class="script1">총 전환 포인트 <span>20,000P</span> <b id="pop_div"></b>일 경우<br>포인트 제공처별 전환 포인트 거래단위는 <br><span id="pop_dealUnit"></span> 입니다.</p>
			</div>
			<div class="btn_area">
				<a href="#!" class="btn_ok pop_close">확인</a>
			</div>
		</div>
	</div>
	<!-- popup 끝-->
	<!-- popup 시작-->
	<div id="myPopup3" class="popup">
		<div class="popup_content2">
			<div class="popup_title"><span class="ls">패밀리 포인트(5G 스마트폰 할인권)</span><div class="close pop_close"></div></div>
			<div class="text">
				<p class="script1">가족에게 스마트폰 할인권을<br>요청 하시겠습니까?</p>
			</div>
			<div class="btn_area">
				<a href="#!" class="close_btn btn_cancel pop_close">취소</a>
				<a href="#!" class="btn_ok pop_ok">확인</a>
			</div>
		</div>
	</div>
	<!-- popup 끝-->
</div>
<%-- Footer --%>
<jsp:include page="/WEB-INF/views/mb/include/incFooter.jsp" flush="false" />

<%-- js --%>
<jsp:include page="/WEB-INF/views/pc/std/pointPageJs_FP.jsp" flush="false" />
</body>
</html>