<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!-- 
   Point Hub version 1.0
  
   Copyright ⓒ 2014 kt corp. All rights reserved.
   
   This is a proprietary software of kt corp, and you may not use this file except in 
   compliance with license agreement with kt corp. Any redistribution or use of this 
   software, with or without modification shall be strictly prohibited without prior written 
   approval of kt corp, and the copyright notice above does not evidence any actual or 
   intended publication of such software. 
 -->
<%--
 **********************************************************************************************
 * @desc : 약관동의 및 본인인증 팝업호출 화면(모바일)
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/mb/std/validateUser.jsp
 * @author 이형우
 * @since 2018.08.20
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.20    이형우        최초생성
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/mb/include/globalVar.jsp" flush="false" />
	
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/mb/include/incHead.jsp" flush="false" />
</head>
<body>
<div id="wrap">
	<!-- header 시작-->
    <header>
        <div class="container">
            <div class="title">포인트다모아</div>
            <div class="script_text">서비스 이용 위해서는<br>서비스 이용약관동의, 본인확인인증 필요합니다.</div>
        </div>
    </header>
	<!-- header 끝-->
	
	<!-- popup 시작-->
	<div id="myPopup" class="popup">
		<div class="popup_content">
			<div class="popup_title"><span class="ls" id="dtlTitl"></span><a href="#!"><div class="close btn_closePop"></div></a></div>
			<div class="clause_text" id="clauseText">
				<iframe id="ifClause"></iframe>
			</div>
			<a href="#!"><div class="close_btn btn_closePop">닫기</div></a>
		</div>
	</div>
	<!-- popup 끝-->
	
	<!-- contents 시작-->
    <section class="clause">
        <div class="container">
            <div class="clause_title">서비스 이용약관 동의</div>
            <div class="check_row">
            <ul>
                <li>
                    <div class="all_check"><a href="javascript:;"><div class="check_toggle"></div><div class="check_name">전체 약관 동의</div></a></div>
                </li>
			</ul>
		</div>
			<c:forEach var="clsMap" items="${clsList}">
				<div class="check_row">
					<ul>
						<li>
							<div class="check_type" clsId='<c:out value="${clsMap.clsId}"/>' prvdrId='<c:out value="${clsMap.prvdrId}"/>' clsVer='<c:out value="${clsMap.clsVer}"/>' clsTitl='<c:out value="${clsMap.clsTitl}${clsMap.clsSubTitl}"/>'>
								<a href="#!" class="${((clsMap.clsUrl ne null) && (clsMap.clsUrl ne '')) ? 'check_link nlink' : 'check_link wlink'}">
									<c:if test="${clsMap.mandYn == 'Y'}">
										<c:if test="${clsMap.clsId != 'PHA3'}">
											<div class="check_toggle mandY"></div>
										</c:if>
										<c:if test="${clsMap.clsId == 'PHA3'}">
											<div class="check_toggle mandY" style="height: 40px;"></div>
										</c:if>
									</c:if>
									<c:if test="${clsMap.mandYn == 'N'}">
										<div class="check_toggle mandN"></div>
									</c:if>
								
									<div class="check_name">
										<c:out value="${clsMap.clsTitl}"/>
										<c:if test="${clsMap.mandYn == 'Y' }">
											<span class="red">(필수)</span>
										</c:if>
										<c:if test="${clsMap.mandYn == 'N' }">
											<span>(선택)</span>
										</c:if>
										<c:if test="${clsMap.clsId == 'PHA3'}">
											<br>
											<c:out value="${clsMap.clsSubTitl}"/>
										</c:if>
									</div>
								</a>
								<c:if test="${(clsMap.clsUrl ne null) && (clsMap.clsUrl ne '')}">
									<a href="#!" class="alink" clsUrl="${clsMap.clsUrl}" clsTitl="${clsMap.clsTitl}">
										<div class="arrow_right"></div>
									</a>
								</c:if>
							</div>
						</li>
					</ul>
				</div>
				<c:if test="${clsMap.clsId == 'PHA1' || clsMap.clsId == 'PHA3'}">
					<div class="clauseCard_table">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<c:if test="${clsMap.clsId == 'PHA1'}">
				                <colgroup>
				               		<col width="25%"/>
				                    <col width="*"/>
				                    <col width="25%"/>
				                    <col width="25%"/>
								</colgroup>
								<tbody>
				               		<c:forEach var="cardInfoMap" items="${cardInfoList}">
				               			<c:if test="${cardInfoMap.clsId == 'PHA1'}">
					               			<c:if test="${cardInfoMap.sortOrd == '0'}">
					                			<tr>
					                				<th><c:out value="${cardInfoMap.prvdrNm}"/></th>
					                				<th><c:out value="${cardInfoMap.prps}"/></th>
					                				<th><c:out value="${cardInfoMap.item}"/></th>
					                				<th><c:out value="${cardInfoMap.holdPrd}"/></th>
					                			</tr>
					               			</c:if>
					               			<c:if test="${cardInfoMap.sortOrd == '1'}">
					                			<tr  class="card_info"
									                clsId='<c:out value="${cardInfoMap.clsId}"/>'
									                prvdrId='<c:out value="${cardInfoMap.prvdrId}"/>'
									                clsVer='<c:out value="${cardInfoMap.clsVer}"/>'
									                clsTitl='<c:out value="${cardInfoMap.clsTitl}"/>'>
					                				<td><c:out value="${cardInfoMap.prvdrNm}"/></td>
					                				<td rowspan="7"><c:out value="${cardInfoMap.prps}"/></td>
					                				<td rowspan="6"><c:out value="${cardInfoMap.item}"/></td>
					                				<td rowspan="7"><c:out value="${cardInfoMap.holdPrd}"/></td>
					                			</tr>
					               			</c:if>
					               			<c:if test="${cardInfoMap.sortOrd != '0' && cardInfoMap.sortOrd != '1'}">
					                			<tr  class="card_info"
									                clsId='<c:out value="${cardInfoMap.clsId}"/>'
									                prvdrId='<c:out value="${cardInfoMap.prvdrId}"/>'
									                clsVer='<c:out value="${cardInfoMap.clsVer}"/>'
									                clsTitl='<c:out value="${cardInfoMap.clsTitl}"/>'>
					                				<td><c:out value="${cardInfoMap.prvdrNm}"/></td>
					                				
					                				<c:if test="${cardInfoMap.sortOrd == '7'}">
					                					<td><c:out value="${cardInfoMap.item}"/></td>
					                				</c:if>					                				
					                			</tr>
					               			</c:if>
				               			</c:if>
				               		</c:forEach>
								</tbody>
			              	</c:if>
			              	<c:if test="${clsMap.clsId == 'PHA3'}">
			              		<colgroup>
									<col width="22%"/>
									<col width="20%"/>
									<col width="*"/>
									<col width="15%"/>
									<col width="20%"/>
								</colgroup>
			              		<tbody>
				               		<c:forEach var="cardInfoMap" items="${cardInfoList}">
				               			<c:if test="${cardInfoMap.clsId == 'PHA3'}">
					               			<c:if test="${cardInfoMap.sortOrd == '0'}">
					                			<tr>
					                				<th><c:out value="${cardInfoMap.prvdrNm}"/></th>
					                				<th><c:out value="${cardInfoMap.rcvrNm}"/></th>
					                				<th><c:out value="${cardInfoMap.prps}"/></th>
					                				<th><c:out value="${cardInfoMap.item}"/></th>
					                				<th><c:out value="${cardInfoMap.holdPrd}"/></th>
					                			</tr>
					               			</c:if>
					               			<c:if test="${cardInfoMap.sortOrd == '1'}">
					               				<tr  class="card_info"
									                clsId='<c:out value="${cardInfoMap.clsId}"/>'
									                prvdrId='<c:out value="${cardInfoMap.prvdrId}"/>'
									                clsVer='<c:out value="${cardInfoMap.clsVer}"/>'
									                clsTitl='<c:out value="${cardInfoMap.clsTitl}"/>'>
					                				<td><c:out value="${cardInfoMap.prvdrNm}"/></td>
					                				<td rowspan="7"><c:out value="${cardInfoMap.rcvrNm}"/> </td>
					                				<td rowspan="6"><c:out value="${cardInfoMap.prps}"/> </td>
					                				<td rowspan="7"><c:out value="${cardInfoMap.item}"/> </td>
					                				<td rowspan="7"><c:out value="${cardInfoMap.holdPrd}"/> </td>
					                			</tr>
					               			</c:if>
					               			<c:if test="${cardInfoMap.sortOrd != '0' && cardInfoMap.sortOrd != '1'}">
					                			<tr  class="card_info"
									                clsId='<c:out value="${cardInfoMap.clsId}"/>'
									                prvdrId='<c:out value="${cardInfoMap.prvdrId}"/>'
									                clsVer='<c:out value="${cardInfoMap.clsVer}"/>'
									                clsTitl='<c:out value="${cardInfoMap.clsTitl}"/>'>
					                				<td><c:out value="${cardInfoMap.prvdrNm}"/></td>
					                				
					                				<c:if test="${cardInfoMap.sortOrd == '7'}">
					                					<td><c:out value="${cardInfoMap.prps}"/></td>
					                				</c:if>					                				
					                			</tr>
					               			</c:if>
				               			</c:if>
				               		</c:forEach>
								</tbody>
							</c:if>
						</table>
			       	</div>
				</c:if>
			</c:forEach>
        </div>
    </section>		
		
    <section class="certi">
        <div class="container">
       		<div class="certi_title">본인인증</div>
        	<div id="certiBtn" class="certi_btn">
        		<a href="#!" id="kmcValidateBtn">
	            	<ul>
	                	<li><div class="certi_user"></div></li>
	                	<li id="certiBtnTxt">본인인증</li>
	            	</ul>
        		</a>
			</div>
        </div>
    </section>

    <section class="footer">
        <div class="submet_btn">
            <ul>
                <li style="width: 100%;">
                    <c:if test="${not empty return_url_2}">
                    <a href="#!"><div id="cancelBtn" class="btn_cancel">취소</div></a>
                    </c:if>
                </li>
            </ul>
        </div>
    </section>
<!-- contents 끝-->
</div>    
<form id="reqKMCISForm">
 	<input type="hidden" id="tr_cert" name="tr_cert"> 
	<input type="hidden" id="tr_url" name="tr_url">
	<input type="hidden" id="tr_add" name="tr_add">
</form>

<%-- Footer --%>
<jsp:include page="/WEB-INF/views/mb/include/incFooter.jsp" flush="false" />

<%-- js --%>
<jsp:include page="/WEB-INF/views/pc/std/validateUserJs.jsp" flush="false" />
</body>
</html>