<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" 
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%>

<%--  
 **********************************************************************************************
 * @desc : 쿠폰리스트 조회화면(Mobile)
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/test/couponList.jsp
 * @author jungukjae
 * @since 2019.04.23
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 *  XXXX.XX.XX    XXX			XXXXXXXXXXXXXXXXXXXXXX
 * </pre>
 **********************************************************************************************
--%>

<c:if test="${platform eq 'mobile'}">
<!DOCTYPE html> 
<html lang="ko">
<head> 
	<jsp:include page="/WEB-INF/views/pc/include/globalVar.jsp" flush="false" />
	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="shortcut icon" type="image/x-icon" href="${ResRoot}/mb/img/clippoint_favicon.png">
    <title>쿠폰리스트 조회(모바일)</title>
    
    <link rel="stylesheet" href="${ResRoot}/mb/css/style.css?${TimeStamp}">
	<script type="text/javascript" src="${ResRoot}/common/jQuery/jquery.min.js?v331"></script>
	<script type="text/javascript" src="${ResRoot}/common/pubJs/script.js?v=js_Ver1&${TimeStamp}"></script>

	<style type="text/css">
		#all_check {
			margin-top: 0px;
		}
		.tab #point_title {
			text-align: left;
			width: 30%;
		}
		
		.tab #point_li {
			text-align: right; 
			width: 70%;
		}
		
		.check_toggle {
	      float: left;
	      width: 20px;
	      height: 20px;
	      color: #e4e4e4;
	      margin-right: 25px;
		  margin-top: 8px;
	      background: url("${ResRoot}/pc/img/check-circle-regular.png") no-repeat;
	      background-size: 100%; 
		  }
	
		.check_toggle_on {
	      float: left;
	      width: 20px;
	      height: 20px;
	      color: #e4e4e4;
	      margin-right: 25px;
	  	  margin-top: 8px;
	  	  background: url("${ResRoot}/pc/img/check-circle-regular-red.png") no-repeat;
	      background-size: 100%; 
	      }
	   	   
		  .family_p section.voucher .details li.cell_1 { 
		    float: left;
		    width: calc(100% - 140px);
		    font-size: 16px;
		    line-height: 35px;
	  	  }
		  .family_p section.voucher .details li.cell_2 { 
		      float:right;
		      width:130px;
		      text-align: right;
	  	  }
		  .family_p section.voucher .details li.cell_2 span { 
		      line-height:35px;
		      font-size: 18px;
		      font-weight: bold;
		      color: #d71826;
		      letter-spacing: -1px;
		  }
		  .family_p section.voucher .details li.cell_3 { 
		  	  margin-left: 5%;
		      float:right;
		      width:300px;
		      text-align:right;
		      font-size:13px;
			  line-height:27px;
		  } 
		  .family_p section.voucher .details li.cell_3 span { 
		  	  text-align:right;
		      color:#000;
		      font-size:13px;
		      font-weight:bold;
		      letter-spacing: 0.2px;
		  }
		     
		  .tb_middle2 {
	          float: left;
	          text-align: center;
	          width: 37.5%;
	          padding: 10px 5px 5px 5px;
	          font-size: 16px;
	          font-weight: bold;
	          color: #d71826;
	          line-height: 21px;
	          letter-spacing: -1px; 
	       }	  
	</style>

</head>

<body>
<!-- 20190219 패밀리 포인트 - class 추가 -->
<div id="wrap" class="family_p">
 <!-- header 시작-->
    <!-- 20190219 패밀리 포인트 - text 수정 -->
    <header>
        <div class="container">
            <div class="title" style="border-bottom:0;">패밀리 포인트<br>(5G 스마트폰 할인권)</div>
        </div>
    </header>
    <!-- header 끝-->

    <!-- contents 시작-->
    <!-- 20190219 패밀리 포인트 - 할인권발행 내역 시작-->
    <section class="contents voucher">
        <div class="voucher_title">할인권발행 내역<a href="#!" class="btn btn_refresh"><span></span>새로고침</a></div>
        <div class="container">
        	<c:if test="${fn:length(couponList) != 0}">
	            <div class="item_area">
					<c:forEach var="cList" items="${couponList}">
					<div class="item">
		                <ul class="details" data-amt='${cList.cpnAmt}' data-cpn-id='${cList.pinNo}' data-amt-encrypt='${cList.cpnAmtEncrypt}'>
		                    <li class="cell_1"><div class="check_toggle"></div>
		                    	${cList.custNm}
		                    	<c:if test="${cList.ownYn == 'Y'}">
									(본인)			                    	
		                    	</c:if>
		                    </li>
		                    <li class="cell_2"><span><fmt:formatNumber value="${cList.cpnAmt}" pattern="#,###"/>원</span></li>
		                    <li class="cell_3">할인권번호 : <span>${cList.pinNo}</span></li>
		                </ul>                 
	            	</div>
					
					
	<!-- 		            <div class="item"> -->
	<%-- 			             <ul class="details" data-amt='${cList.cpnAmt}' data-cpn-id='${cList.pinNo}'> --%>
	<!-- 			             	사용자 -->
	<!-- 		                    <li class="cell_1"></li> -->
	<!-- 		                    쿠폰번호 -->
	<!-- 		                    <li class="cell_3"> -->
	<%-- 		                    <div class="check_toggle"></div>${cList.custNm}  --%>
	<!-- 		                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
	<!-- 		                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	 -->
	<%-- 		                    할인권번호 : <span>${cList.pinNo}</span></li> --%>
	<!-- 		                    가격 -->
	<%-- 		                    <li class="cell_2"><span><fmt:formatNumber value="${cList.cpnAmt}" pattern="#,###"/></span></li> --%>
	<!-- 		                </ul>                  -->
	<!-- 		            </div>  -->
			            <div class="dot"><div class="dot_line"></div></div>  
	           		</c:forEach>           
	            </div>
	    	</c:if>   
			<c:if test="${fn:length(couponList) == 0}">  
				<section class="point_null">
					<div class="point_null_script">
						할인권 발행 내역이 없습니다.
					</div>
				</section>
			</c:if>    
        </div> 
    </section>
    <!-- 20190219 패밀리 포인트 - 할인권발행 내역 끝-->
    
    <!-- footer 시작-->
    <section class="footer">
		<div class="total_point">
			<div class="tab">
				<ul>
					<li class="tb_middle2" id="point_title">총 할인액</li>
					<li class="tb_middle2" id="point_li"><span> 0원</span></li>
				</ul>
			</div>
		</div>
        <div class="submet_btn" style="position: fixed; bottom: 0;">
            <ul>
                <li><a href="#!" class="bg_blue"><div class="btn_oneclick" id="all_check">전체선택</div></a></li>
                <li><a href="#!"><div class="btn_ok">확인</div></a></li>
            </ul> 
        </div>
    </section>
    <form id="couponListForm" method="post">
    	<input type="hidden" id="coupons" name="coupons">
    </form>
    <!-- footer 끝-->
<!-- contents 끝-->
</div>

<%-- Footer --%>
<jsp:include page="/WEB-INF/views/mb/include/incFooter.jsp" flush="false" />

<%-- js --%>
<jsp:include page="/WEB-INF/views/pc/std/couponListJs.jsp" flush="false" />
</body>
</html>
</c:if>
<c:if test="${platform eq 'pc'}">
	<jsp:include page="/WEB-INF/views/pc/std/couponList.jsp" flush="false" />
</c:if>