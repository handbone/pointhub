<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!-- 
   Point Hub version 1.0
  
   Copyright ⓒ 2014 kt corp. All rights reserved.
   
   This is a proprietary software of kt corp, and you may not use this file except in 
   compliance with license agreement with kt corp. Any redistribution or use of this 
   software, with or without modification shall be strictly prohibited without prior written 
   approval of kt corp, and the copyright notice above does not evidence any actual or 
   intended publication of such software. 
 -->
<%--
 **********************************************************************************************
 * @desc : 패밀리 포인트 조르기(요청) 화면(PC)
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/pc/std/familyPoint.jsp
 * @author 이형우
 * @since 2019.04.15
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일			수 정 자		수정내용
 * ----------	--------	-----------------------------
 * 2019.04.15	이형우			최초생성
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%-- Head --%>
<jsp:include page="/WEB-INF/views/pc/include/globalVar.jsp" flush="false" />

<%-- Head --%>
<jsp:include page="/WEB-INF/views/pc/include/incHead.jsp" flush="false" />
</head>
<body>
	<div id="wrap" class="family_p">
		<!-- header 시작-->
		<header style="padding:45px 20px 40px 20px">
			<div class="container">
				<div class="title" style="border-bottom:0;">패밀리 포인트<br>(5G 스마트폰 할인권)</div>
				<div class="total multi">
					<ul>
						<li class="total_row">
							<div class="point">
								<div class="point_total">
									<ul>
										<li class="point_row">총 할인액</li>
										<li class="point_row"><span id="ttlCpnAmt"></span></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</header>
		<!-- header 끝-->
		
		<!-- contents 시작-->
		<section class="contents voucher">
			<div class="voucher_title">할인권발행 내역<a id="btnRefresh" class="btn btn_refresh"><span></span>새로고침</a></div>
			<div class="container">
				<div class="item_area" style="overflow-y: auto;">
					<c:forEach var="fpMap" items="${fpList}">
						<div class="item" phubTrNo="${fpMap.phubTrNo}" phubCpnNo="${fpMap.phubCpnNo}" ktCustId="${fpMap.ktCustId}" custNm="${fpMap.custNm}" custCtn="${fpMap.custCtn}" reqStatCd="${fpMap.reqStatCd}" cpnAmt="${fpMap.cpnAmt}" ownYn="${fpMap.ownYn}" endYn="${fpMap.endYn}">
							<ul class="details">
								<li class="cell_1">${fpMap.custNm}</li>
								<c:if test="${fpMap.ownYn == 'Y'}">
									<c:if test="${fpMap.cpnStatCd != '9000'}">
										<c:if test="${empty fpMap.phubCpnNo}">
											<li class="cell_3"></li>
											<li class="cell_2"><span>0원</span></li>
										</c:if>
										<c:if test="${not empty fpMap.phubCpnNo}">
											<li class="cell_3">할인권번호 : <span>${fpMap.pinNo}</span></li>
											<li class="cell_2"><span class="cpnAmt" cpnAmt="${fpMap.cpnAmt}"></span></li>
										</c:if>
									</c:if>
									<c:if test="${fpMap.cpnStatCd == '9000'}">
										<li class="cell_3">할인권번호 : <span>${fpMap.pinNo}</span></li>
										<li class="cell_2"><span>발행 취소</span></li>
									</c:if>
								</c:if>
								<c:if test="${fpMap.ownYn == 'N'}">
									<c:if test="${fpMap.phubCpnNo == null}">
										<c:if test="${fpMap.prgrStat == 'PR000'}">
											<li class="cell_3">미진행</li>
										</c:if>
										<c:if test="${fpMap.prgrStat == 'PR110' || fpMap.prgrStat == 'PR120' || fpMap.prgrStat == 'PR130' || fpMap.prgrStat == 'PR140'}">
											<li class="cell_3">진행중</li>
										</c:if>
										<c:if test="${fpMap.prgrStat == 'PR900'}">
											<li class="cell_3">결제실패</li>
										</c:if>
										<c:if test="${fpMap.reqStatCd == 'RE' }">
											<li class="cell_2"><a href="#!" class="btn btn_off">요청</a></li>
										</c:if>
										<c:if test="${fpMap.reqStatCd != 'RE' }">
											<li class="cell_2"><a class="btn aReqCpn">요청</a></li>
										</c:if>
									</c:if>
									<c:if test="${fpMap.phubCpnNo != null}">
										<li class="cell_3">할인권번호 : <span>${fpMap.pinNo}</span></li>
										<c:if test="${fpMap.cpnStatCd != '9000'}">
											<li class="cell_2"><span class="cpnAmt" cpnAmt="${fpMap.cpnAmt}"></span></li>
										</c:if>
										<c:if test="${fpMap.cpnStatCd == '9000'}">
											<li class="cell_2"><span>발행 취소</span></li>
										</c:if>
									</c:if>
								</c:if>
							</ul>
						</div>
						<div class="dot"><div class="dot_line"></div></div>  
					</c:forEach>
				</div>
			</div>
		</section>
		
		<section class="footer">
			<div class="submet_btn" style="position: fixed; bottom: 0;">
				<ul>
					<li style="width: 100%;"><a href="#" id="btnReqAll" class="bg_blue"><div class="btn_oneclick">일괄요청</div></a></li>
				</ul>
			</div>
		</section>
		
		<form id="fpForm">
			<input type="hidden" id="form_phubTrNo" name="phub_tr_no">
		</form>
		<!-- contents 끝-->
	</div>
	<!-- popup 시작-->
	<div id="myPopup" class="popup">
		<div class="popup_content2">
			<div class="popup_title"><span class="ls">패밀리 포인트(5G 스마트폰 할인권)</span><div class="close pop_close"></div></div>
			<div class="text">
				<p class="script1">가족 분 <span id="pop_custNm"></span>님에게 할인권 요청 메시지가 발송 되었습니다.</p>
				<p class="script2">※ 가족 분에게 메시지 확인 및 URL 접속 진행을 알려 주시면 더 빨리 할인권을 받으실 수 있습니다.</p>
			</div>
			<div class="btn_area">
				<a href="#!" class="btn_ok pop_ok">확인</a>
			</div>
		</div>
	</div>
	<!-- popup 끝-->

<%-- Footer --%>
<jsp:include page="/WEB-INF/views/pc/include/incFooter.jsp" flush="false" />

<%-- js --%>
<jsp:include page="/WEB-INF/views/pc/std/familyPointJs.jsp" flush="false" />
</body>
</html>