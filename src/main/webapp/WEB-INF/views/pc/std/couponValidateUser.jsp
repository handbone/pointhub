<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" 
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%>

<%--  
 **********************************************************************************************
 * @desc : 쿠폰리스트 조회화면(PC버전)
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/test/couponList.jsp
 * @author jungukjae
 * @since 2019.04.23
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 *  XXXX.XX.XX    XXX			XXXXXXXXXXXXXXXXXXXXXX
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html> 
<html lang="ko">
<head> 
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/pc/include/globalVar.jsp" flush="false" />
	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="shortcut icon" type="image/x-icon" href="${ResRoot}/pc/img/clippoint_favicon.png">
    <title>본인인증 이동 페이지</title>

    <link rel="stylesheet" href="${ResRoot}/pc/css/style.css?${TimeStamp}">
	<script type="text/javascript" src="${ResRoot}/common/jQuery/jquery.min.js?v331"></script>
	<script type="text/javascript" src="${ResRoot}/common/pubJs/script.js?v=js_Ver1&${TimeStamp}"></script>

	<style type="text/css">
		#all_check {
			margin-top: 0px;
		}
		
		.check_toggle {
	      float: left;
	      width: 20px;
	      height: 20px;
	      color: #e4e4e4;
	      margin-right: 25px;
		  margin-top: 8px;
	      background: url("${ResRoot}/pc/img/check-circle-regular.png") no-repeat;
	      background-size: 100%; 
		  }
	
		.check_toggle_on {
	      float: left;
	      width: 20px;
	      height: 20px;
	      color: #e4e4e4;
	      margin-right: 25px;
	  	  margin-top: 8px;
	  	  background: url("${ResRoot}/pc/img/check-circle-regular-red.png") no-repeat;
	      background-size: 100%; 
	      }
	</style>

	<script type="text/javascript">
		  function kmcValidate(){
			  getKmcSendParam();		  
		  }
		  function getKmcSendParam(){
			  var url = "/phub/getKmcSendParamCoupon.do";
				var params = {
					'serviceId' : "SVC_CP"	
				}
				var type = 'post';
				var dataType = 'json';
				var success = function(data, textStatus, jqXHR){
					if(data.ret_code == '00'){						
						$('#tr_cert').val(data.trCert);
						$('#tr_url').val(data.resultUrl);
						$('#tr_add').val(data.trAdd);
						$('#reqKMCISForm').attr('action', data.sendUrl);
						validateUserFunc.kmcPop();
					}
				} 		
				PHFnc.ajax(url, params, type, dataType, success, errCustom, true, true);  
		  }
	</script>
</head>

<body>

<!-- 20190411 패밀리 포인트 - class 추가 -->
<div id="wrap" class="family_p">

<!-- head 끝-->


<!-- header 시작-->
    <header style="padding:45px 20px 40px 20px">
	
        <div class="container">
            <div class="title" style="border-bottom:0;">본인인증 이동 페이지</div>
					
        </div>
    </header>
<!-- header 끝-->

<!-- contents 시작-->
    <!-- 20190411 패밀리 포인트 - 할인권발행 내역 시작-->
    <section class="contents voucher">
         <div class="voucher_title" align="center">  
         	            <div class="total multi">
         	            	<form id="reqKMCISForm">
	         	            	<input type="hidden" id="tr_cert" name="tr_cert">
	         	            	<input type="hidden" id="tr_url" name="tr_url">
	         	            	<input type="hidden" id="tr_add" name="tr_add">
							</form>
         	            
         	            </div>
         
        		본인인증 <span style="padding-left: 30%;"></span> <button onclick="kmcValidate();">이동</button>
         </div>
        <div class="container">  </div>

    </section>
    <!-- 20190411 패밀리 포인트 - 할인권발행 내역 끝-->


    
    
	<!-- <form id="applySalesTicek" method="post" >
		<input id="ticketListBox" class="" type="hidden"> 
	</form> -->
    <!-- footer 끝-->
	<!-- contents 끝-->
</div>
<%-- js --%>
<jsp:include page="./validateUserJs.jsp" flush="false" />
<%-- Footer --%>
<jsp:include page="/WEB-INF/views/pc/include/incFooter.jsp" flush="false" />
</body>
</html>
