<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 포인트 조회 결제 스크립트(패밀리포인트)
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/pc/std/pointPageJsFP.jsp
 * @author 이형우
 * @since 2019.02.20
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.02.20    이형우        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
$(document).ready(function(){
	PointFunc.initEvent();
});

var PointFunc = {
	initEvent:function(){
		//전액충전 버튼 클릭 이벤트
		$('.maxBtn').click(function(){
			var ttlPntAmt = Number($('#ttlPntAmt').attr('val'));	//충전할 포인트
			
			var obj = $(this).parents('.pointGroup');
			var pntObj = obj.find('.pnt');
			var pntAmtObj = obj.find('.pntAmt');
			var cprtAmtObj = obj.find('.cprtAmt');
			var avlPntObj = obj.find('.avlPnt');
			var pntExchRate = obj.find('.pntExchRate').val();
			
			var pnt = Number(pntObj.attr('val'));					//사용 포인트
			var cprtAmt = Number(cprtAmtObj.attr('val'));			//전환된 결제금액
			var avlPnt = Number(avlPntObj.attr('val'));				//보유 포인트
			
			//javascript 소수점 계산 오류 방지
			//카드 수수료율 적용
			var dealUnit = '${result.dealUnit}';
			
			var pntAmt = parseFloat(avlPnt * pntExchRate).toFixed(2);
			pntAmt = getCalcPnt(pntAmt, 'Y');
			
			//전액충전 하려는 합계 금액이 20000 미만일 경우 dealUnit2 사용
			var dealDiv = pntAmt + getTtlCprtAmt();
			if(dealDiv < 20000){
				dealUnit = '${result.dealUnit2}';
			}
			
			//전환 포인트 거래 단위 금액 계산
			var pntAmtMod = pntAmt%dealUnit;
			pntAmt = pntAmt - pntAmtMod;
			pntAmtObj.attr('val', pntAmt);
			
			//전환 포인트 거래 단위 계산 나머지 값의 포인트 전환율 복원
			var pntMod = getCalcPnt(pntAmtMod / pntExchRate, 'Y');
			
			//TODO 가맹점 수수료율 적용(1이 아닐 겅우 로직 변경)
			var shopPntRate = Number('${result.shopPntRate}');		//가맹점 수수료율
			var cprtAmt = parseFloat(pntAmt - (pntAmt * (shopPntRate/100) ) ).toFixed(2);
			cprtAmt = getCalcPnt(cprtAmt, 'Y');
			
			//사용 가능 포인트에서 전환율 복원된 거래 단위 나머지 값 계산
			pnt = avlPnt - pntMod;
			
			//사용포인트 표시
			pntObj.attr('val', pnt);
			pntObj.val(PHUtil.setComma(pnt));
			$('#total_pnt').text(PHUtil.setComma(getTtlPnt()));
			
			//전환 포인트 표시
			cprtAmtObj.attr('val', cprtAmt);
			cprtAmtObj.text(PHUtil.setComma(cprtAmt));
			
			$('#ttlPntAmt').attr('val', getTtlPntAmt());
			$('#ttlPntAmt').text(PHUtil.setComma(getTtlCprtAmt()));
			$('#total_pntAmt').text(PHUtil.setComma(getTtlCprtAmt()));
			
			//충전할 포인트 0초과일 경우 확인버튼 활성화
			toggleConfirmBtn();
		});
		
		//초기화 버튼 클릭 이벤트
		$('.initBtn').click(function(){
			initPntObj($(this));
		});
		
		//패밀리 포인트 확인 버튼 클릭 이벤트
		$('#confirmBtn').click(function(){
			if(checkPrvdrPerDeal()){
				if(checkValidate()){
					var ownerYn		= '${result.ownerYn}';
					var ownerName	= '${result.ownerName}';
					
					if(ownerYn == 'N'){
						$('#myPopup').css('display', 'block');
						$('#pop_ttlAmt').html(PHUtil.setComma(getTtlCprtAmt()));
						$('#pop_ownerName').html(ownerName);
					}else{
						var ttlCprtAmt = getTtlCprtAmt();
						if(ttlCprtAmt == 0){
							$('#myPopup3').css('display', 'block');
						}else{
							PointFunc.pay();
						}
					}
				}
			}
		});
		
		$('.pnt').focusout(function(){
			checkPrvdrPerDeal();
		});
		
		//팝업 확인 버튼 클릭 이벤트
		$('.pop_ok').click(function(){
			PointFunc.pay();
		});
		
		//팝업 닫기 버튼 클릭 이벤트
		$('.pop_close').click(function(){
			$('.popup').css('display', 'none');
		});
		
		//취소 버튼 클릭 이벤트
		$('#cancelBtn').click(function(){
			var pgTrNo			= '${result.pg_tr_no}';
			var phubTrNo		= '${result.phubTrNo}';
			var payAmt			= 0;
			var points			= 0;
			var wonAmt			= 0;
			var payMethod		= '${result.payMethod}';
			var retCode			= 'F999';
			var retMsg			= '사용자 취소';
			
			$('#return_pgTrNo').val(pgTrNo);
			$('#return_phubTrNo').val(phubTrNo);
			$('#return_payAmt').val(payAmt);
			$('#return_points').val(points);
			$('#return_wonAmt').val(wonAmt);
			$('#return_payMethod').val(payMethod);
			$('#return_retCode').val(retCode);
			$('#return_retMsg').val(retMsg);
			
			var url = '${result.return_url_2}';
			var attrObj = {'form' : '#formReturn'};
			PHFnc.doAction(url, '', attrObj);
		});
		
		
		
		initPointPage();
	},
	pay:function(){
		var minPerDeal = Number('${result.minPerDeal}');
		
		var ttlPntAmt = getTtlPntAmt();
		var ttlCprtAmt = getTtlCprtAmt();
		var ttlRmndAmt = 0;
		
		var pntList = new Array();
		$('.pointGroup').each(function(){
			var data = new Object() ;
			data.pntCd = $(this).find('.pntCd').val();
			data.pntTrNo = $(this).find('.pntTrNo').val();
			data.pntAmt = $(this).find('.pntAmt').attr('val');
			data.cprtAmt = $(this).find('.cprtAmt').attr('val');
			data.dealUnit = $(this).find('.dealUnit').val();
			data.pntExchRate = $(this).find('.pntExchRate').val();
			data.avlPnt = $(this).find('.avlPnt').attr('val');
			data.pnt = $(this).find('.pnt').attr('val');
			data.minAvlPnt = $(this).find('.minAvlPnt').val();
			
			//0원 결제 시 조회된 카드 리스트 모두 push
			if(Number(data.pntAmt) > 0){
				pntList.push(data);
			}else if(ttlCprtAmt == 0 && minPerDeal == 0){
				pntList.push(data);
			}
		});
		var pntListJson = JSON.stringify(pntList); 
		
		var url = '/phub/std/pay.do';
		var params = {
			'phubTrNo'		: '${result.phubTrNo}',
			'dealInd'		: '${result.dealInd}',
			'dealPgInd'		: '${result.dealPgInd}',
			'custId'		: '${result.custId}',
			'ttlPntAmt'		: ttlPntAmt,
			'ttlCprtAmt'	: ttlCprtAmt,
			'ttlRmndAmt'	: ttlRmndAmt,
			'shopPntRate'	: '${result.shopPntRate}',
			'pntList'		: pntListJson
 		}
		var type = 'post';
		var dataType = 'json';
		var success = function(data, textStatus, jqXHR){
			if(data.ret_code == '00'){
				PointFunc.pgRetSubmit(data);
			}
		}
		PHFnc.ajax(url, params, type, dataType, success, errCustom, true, true);
	},
	pgRetSubmit:function(data){
		var pgTrNo = data.pg_tr_no;
		var phubTrNo = data.phub_tr_no;
		var payAmt = data.pay_amt;
		var points = data.points;
		var wonAmt = data.won_amt;
		var authLimitDtm = data.auth_limit_dtm;
		var payMethod = data.pay_method;
		var retCode = data.ret_code;
		var retMsg = data.ret_msg;
		var returnUrl2 = data.return_url_2;

		$('#hdn_pgTrNo').val(pgTrNo);
		$('#hdn_phubTrNo').val(phubTrNo);
		$('#hdn_payAmt').val(payAmt);
		$('#hdn_points').val(points);
		$('#hdn_wonAmt').val(wonAmt);
		$('#hdn_authLimitDtm').val(authLimitDtm);
		$('#hdn_payMethod').val(payMethod);
		$('#hdn_retCode').val(retCode);
		$('#hdn_retMsg').val(retMsg);

		var url = returnUrl2;
		var attrObj = {'form' : '#pointForm'};
		PHFnc.doAction(url, '', attrObj);
	}
}

//초기 포인트 값 설정
var initPointPage = function(){
	var ttlAvlPnt = 0;								//총 소유 포인트
	//총 소유 포인트 계산
	$('.pointGroup').each(function(){
		var avlPntObj = $(this).find('.avlPnt')
		var avlPnt = avlPntObj.attr('val');
		avlPntObj.text(PHUtil.setComma(avlPnt));
		
		ttlAvlPnt += Number(avlPnt);
	});
	//총 소유 포인트 값 저장
	$('#ttlAvlPnt').attr('val', ttlAvlPnt);
	//총 소유 포인트 comma 설정하여 표시
	$('#ttlAvlPnt').text(PHUtil.setComma(ttlAvlPnt));
	$('#total_avlPnt').text(PHUtil.setComma(ttlAvlPnt));
	//전환된 결제금액 0 표시
	$('.cprtAmt').text(0);
	
	//요청자 구분 확인버튼 활성화 여부 동작 
	toggleConfirmBtn();
}

//포인트 입력값 input값으로 입력 변환 
function setPntAmt(obj){
	var pntObj = obj.parents('.pointGroup').find('.pnt');
	var pntAmtObj = obj.parents('.pointGroup').find('.pntAmt');
	var cprtAmtObj = obj.parents('.pointGroup').find('.cprtAmt');
	var pntExchRate = obj.parents('.pointGroup').find('.pntExchRate').val();
	var pnt = pntObj.val();
	
	//입력값 없으면 0처리
	if(pnt == '' || pnt.match(/\d/g) == null){
		pntObj.attr('val', 0);
		pntObj.val('0');
		$('#total_pnt').text(PHUtil.setComma(getTtlPnt()));
		pntAmtObj.attr('val', 0);
		cprtAmtObj.attr('val', 0);
		cprtAmtObj.text('0');
	}else{
		pnt = pnt.match(/\d/g);
		pnt = pnt.join("");
		if(pnt.length > 1 && pnt.substring(0,1) == 0){
			pnt = pnt.substring(1, pnt.length);
		}
		//javascript 소수점 계산 오류 방지
		var pntAmt = parseFloat(pnt * pntExchRate).toFixed(2);
		pntAmt = getCalcPnt(pntAmt, 'Y');
		pntAmtObj.attr('val', pntAmt);
		
		var shopPntRate = Number('${result.shopPntRate}');		//가맹점 수수료율
		var cprtAmt = parseFloat(pntAmt - (pntAmt * (shopPntRate/100) ) ).toFixed(2);
		cprtAmt = getCalcPnt(cprtAmt, 'Y');
		
		pntObj.attr('val', pnt);
		pntObj.val(PHUtil.setComma(pnt));
		$('#total_pnt').text(PHUtil.setComma(getTtlPnt()));
		
		pntAmtObj.attr('val', pntAmt);
		cprtAmtObj.attr('val', cprtAmt);
		cprtAmtObj.text(PHUtil.setComma(cprtAmt));
	}
	//포인트 입력 시 각 포인트 확인 및 총포인트 계산, 표시
	setTtlPntAmt(pntObj, pntAmtObj, cprtAmtObj);
}

//포인트 입력 시 각 포인트 확인 및 총포인트 계산, 표시
function setTtlPntAmt(pntObj, pntAmtObj, cprtAmtObj){
	var isValid = true;
	var msg = '';
	var ttlPntAmt = getTtlPntAmt();					//충전할 포인트
	
	//보유 포인트보다 충전할 포인트가 클 경우(카드사별)
	var avlPnt = Number(cprtAmtObj.parent().parent().find('.avlPnt').attr('val'));
	var pnt = Number(pntObj.attr('val'));
	
	if(avlPnt < pnt && isValid){
		isValid = false;
		msg = '입력하신 포인트보다 보유 포인트가 부족 합니다.';
	}
	
	if(!isValid){
		PHFnc.alert(msg);
		pntObj.attr('val', 0);
		pntObj.val(0);
		pntAmtObj.attr('val', 0);
		cprtAmtObj.attr('val', 0);
		cprtAmtObj.text(0);
	}
	
	//충전할 포인트 0초과일 경우 확인버튼 활성화
	toggleConfirmBtn();
	
	//사용 포인트 표시
	$('#total_pnt').text(PHUtil.setComma(getTtlPnt()));
	
	//충전할 포인트 표시
	$('#ttlPntAmt').attr('val', getTtlPntAmt());
	$('#ttlPntAmt').text(PHUtil.setComma(getTtlCprtAmt()));
	$('#total_pntAmt').text(PHUtil.setComma(getTtlCprtAmt()));
}

//충전할 포인트 0초과일 경우 확인버튼 활성화
function toggleConfirmBtn(){
	var ttlCprtAmt	= getTtlCprtAmt();
	var minPerDeal	= Number('${result.minPerDeal}');
	
	//0원 결제 가능일 경우 확인버튼 조건 없이 활성화
	if(minPerDeal > 0){
		if(ttlCprtAmt > 0){
			$('#confirmBtn').removeClass();
			$('#confirmBtn').addClass('btn_ok_on');
			$('#confirmBtn').css('background', '#5dbcfe');
		}else{
			$('#confirmBtn').removeClass();
			$('#confirmBtn').addClass('btn_ok');
			$('#confirmBtn').css('background', '#333');
		}
	}else{
		$('#confirmBtn').removeClass();
		$('#confirmBtn').addClass('btn_ok_on');
		$('#confirmBtn').css('background', '#5dbcfe');
	}
}

// 총 전환 포인트값 산출
function getTtlPntAmt(){
	var ttlPntAmt = 0;
	$('.pntAmt').each(function(){
		var pntAmt = PHUtil.nvl($(this).attr('val'));
		if(pntAmt == ''){
			pntAmt = 0;
		}
		ttlPntAmt += Number(pntAmt);
	});
	return ttlPntAmt;
}

function getTtlCprtAmt(){
	var ttlCprtAmt = 0;
	$('.cprtAmt').each(function(){
		var cprtAmt = PHUtil.nvl($(this).attr('val'));
		if(cprtAmt == ''){
			cprtAmt = 0;
		}
		ttlCprtAmt += Number(cprtAmt);
	});
	return ttlCprtAmt;
}

//총 사용 포인트값 산출
function getTtlPnt(){
	var ttlPnt = 0;
	$('.pnt').each(function(){
		var pnt = PHUtil.nvl($(this).attr('val'));
		if(pnt == ''){
			pnt = 0;
		}
		ttlPnt += Number(pnt);	  	
	});
	return ttlPnt;
}


function checkValidate(){
	var isValid = true;
	var msg = '';
	
	var ttlCprtAmt	= getTtlCprtAmt();
	var maxPerDeal	= Number('${result.maxPerDeal}');
	var minPerDeal	= Number('${result.minPerDeal}');
	
	if(ttlCprtAmt < minPerDeal){
		minPerDeal = PHUtil.setComma(minPerDeal);
		msg = '최소 포인트금액은 '+minPerDeal+'P 입니다.';
		isValid = false;
	}else if(maxPerDeal < ttlCprtAmt){
		maxPerDeal = PHUtil.setComma(maxPerDeal);
		msg = '거래당 사용가능한 포인트금액은 '+maxPerDeal+'P 입니다.';
		isValid = false;
	}
	
	if(!isValid){
		PHFnc.alert(msg);
		return isValid;
	}
	
	
	$('.pointGroup').each(function(){
		
	    var pnt = Number($(this).find('.pnt').attr('val'));
	    var pntNm = $(this).find('img').attr('alt');
		var pntAmt = Number($(this).find('.pntAmt').attr('val'));
		var avlPnt = Number($(this).find('.avlPnt').attr('val'));
		var dealUnit = Number($(this).find('.dealUnit').val());
		var minAvlPnt = Number($(this).find('.minAvlPnt').val());
		var maxPerDeal = Number($(this).find('.maxPerDeal').val());
		
        if(pntAmt > 0){
            
            if(pntAmt%dealUnit > 0){
                
                msg = '거래단위를 확인해주세요';
                isValid = false;
            }else if(avlPnt < minAvlPnt){
                
                msg = pntNm + '의 최소 사용 가능한 포인트가 부족합니다.';
                isValid = false;
            }else if(pnt < minAvlPnt){
                 
                msg = pntNm + '의 최소 사용 포인트는 '+ minAvlPnt +'포인트 이상입니다.';
                isValid = false;
            }
        }		
        
		if(!isValid){
			PHFnc.alert(msg);
			return isValid;
		}
	});
	
	return isValid;
}

function checkPrvdrPerDeal(){
	var isValid		= true;
	var dealUnit	= Number('${result.dealUnit}');
	$('#pop_div').text('초과');
	
	//합계 금액이 20000 이하일 경우 dealUnit2 사용
	var dealDiv = getTtlCprtAmt();
	if(dealDiv <= 20000){
		dealUnit = '${result.dealUnit2}';
		$('#pop_div').text('이하');
	}
	
	$('.cprtAmt').each(function(){
		var cprtAmt = Number($(this).attr('val'));
		if(cprtAmt%dealUnit != 0){
			$('#pop_dealUnit').text(PHUtil.setComma(dealUnit)+'P');
			$('#myPopup2').css('display', 'block');
			//setDealUnitPntObj($(this));
			isValid = false;
			return false;
		}
	});
	return isValid;
}

//사용하지 않음
function setDealUnitPntObj(obj){
	var pntObj = obj.parents('.pointGroup').find('.pnt');
	var cprtAmtObj = obj.parents('.pointGroup').find('.cprtAmt');
	var pntAmtObj = obj.parents('.pointGroup').find('.pntAmt');
	
	var pntExchRate = obj.parents('.pointGroup').find('.pntExchRate').val();
	var shopPntRate = Number('${result.shopPntRate}');		//가맹점 수수료율
	
	var maxPerDeal	= Number('${result.maxPerDeal}');
	var minPerDeal	= Number('${result.minPerDeal}');
	
	var cprtAmt = Number(obj.attr('val'));
	var dealUnit	= Number('${result.dealUnit}');
	
	cprtAmt -= (cprtAmt%dealUnit);
	
	var pntAmt = parseFloat((cprtAmt * (100/(100-shopPntRate)) ) ).toFixed(2);
	pntAmt = getCalcPnt(pntAmt, 'N');
	var pnt = parseFloat((pntAmt / pntExchRate)).toFixed(2);
	pnt = getCalcPnt(pnt, 'N');
	
	pntObj.attr('val', pnt);
	pntObj.val(PHUtil.setComma(pnt));
	$('#total_pnt').text(PHUtil.setComma(getTtlPnt()));
	
	pntAmtObj.attr('val', pntAmt);
	
	cprtAmtObj.attr('val', cprtAmt);
	cprtAmtObj.text(PHUtil.setComma(cprtAmt));
	
	$('#ttlPntAmt').attr('val', getTtlPntAmt());
	$('#ttlPntAmt').text(PHUtil.setComma(getTtlCprtAmt()));
	$('#total_pntAmt').text(PHUtil.setComma(getTtlCprtAmt()));
	
	//충전할 포인트 0초과일 경우 확인버튼 활성화
	toggleConfirmBtn();
}

function initPntObj(obj){
	var pntObj = obj.parents('.pointGroup').find('.pnt');
	var cprtAmtObj = obj.parents('.pointGroup').find('.cprtAmt');
	var pntAmtObj = obj.parents('.pointGroup').find('.pntAmt');
	
	pntObj.attr('val', 0);
	pntObj.val(0);
	$('#total_pnt').text(PHUtil.setComma(getTtlPnt()));
	
	cprtAmtObj.attr('val', 0);
	cprtAmtObj.text(0);
	
	pntAmtObj.attr('val', 0);
	
	$('#ttlPntAmt').attr('val', getTtlPntAmt());
	$('#ttlPntAmt').text(PHUtil.setComma(getTtlCprtAmt()));
	$('#total_pntAmt').text(PHUtil.setComma(getTtlCprtAmt()));
	
	//충전할 포인트 0초과일 경우 확인버튼 활성화
	toggleConfirmBtn();
}

//포인트 소수점 계산
function getCalcPnt(pnt, isInput){
	var calcStrMode = '${result.CALC_STR_MODE}';
	
	//직접 입력
	if(isInput == 'Y'){
		if(calcStrMode == 'FLOOR'){
			pnt = Math.floor(pnt);
		}else if(calcStrMode == 'CEIL'){
			pnt = Math.ceil(pnt);
		}else if(calcStrMode == 'ROUND'){
			pnt = Math.round(pnt);
		}
	//전액충전 버튼 클릭
	}else{
		if(calcStrMode == 'FLOOR'){
			pnt = Math.ceil(pnt);
		}else if(calcStrMode == 'CEIL'){
			pnt = Math.floor(pnt);
		}else if(calcStrMode == 'ROUND'){
			pnt = Math.floor(pnt);
		}
	}
	return pnt;
}
</script>