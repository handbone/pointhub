<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" 
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%>

<%--  
 **********************************************************************************************
 * @desc : 쿠폰리스트 조회화면(PC버전)
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/test/couponList.jsp
 * @author jungukjae
 * @since 2019.04.23
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 *  XXXX.XX.XX    XXX			XXXXXXXXXXXXXXXXXXXXXX
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html> 
<html lang="ko">
<head>
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/pc/include/globalVar.jsp" flush="false" />
	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="shortcut icon" type="image/x-icon" href="${ResRoot}/pc/img/clippoint_favicon.png">
    <title>쿠폰리스트 조회(PC)</title>

    <link rel="stylesheet" href="${ResRoot}/pc/css/style.css?${TimeStamp}">
	<script type="text/javascript" src="${ResRoot}/common/jQuery/jquery.min.js?v331"></script>
	<script type="text/javascript" src="${ResRoot}/common/pubJs/script.js?v=js_Ver1&${TimeStamp}"></script>

	<style type="text/css">
		#all_check {
			margin-top: 0px;
		}
		
		.check_toggle {
	      float: left;
	      width: 20px;
	      height: 20px;
	      color: #e4e4e4;
	      margin-right: 25px;
		  margin-top: 8px;
	      background: url("${ResRoot}/pc/img/check-circle-regular.png") no-repeat;
	      background-size: 100%; 
		  }
	
		.check_toggle_on {
	      float: left;
	      width: 20px;
	      height: 20px;
	      color: #e4e4e4;
	      margin-right: 25px;
	  	  margin-top: 8px;
	  	  background: url("${ResRoot}/pc/img/check-circle-regular-red.png") no-repeat;
	      background-size: 100%; 
	      }
	</style>


	<script type="text/javascript">

	</script>

</head>

<body>

<!-- 20190411 패밀리 포인트 - class 추가 -->
<div id="wrap" class="family_p">

<!-- head 끝-->


<!-- header 시작-->
    <header style="padding:45px 20px 40px 20px">
	
        <div class="container">
            <div class="title" style="border-bottom:0;">패밀리 포인트<br>(5G 스마트폰 할인권)</div>
			
			<!-- 20190411 패밀리 포인트 - 총 할인액 -->
            <div class="total multi">
                <ul>
                    <li class="total_row">
                        <div class="point">
                            <div class="point_total">
                                <ul>
                                    <li class="point_row">총 할인액</li>
                                    <li class="point_row" id="point_li"> 0원 </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
			
        </div>
    </header>
<!-- header 끝-->

<!-- contents 시작-->
    <!-- 20190411 패밀리 포인트 - 할인권발행 내역 시작-->
    <section class="contents voucher">
        <div class="voucher_title"><!-- <div class="check_toggle" id="all_check"></div> --> 할인권발행 내역<a href="#!" class="btn btn_refresh"><span></span>새로고침</a></div>
        <div class="container">
        	<c:if test="${fn:length(couponList) != 0}">
				<div class="item_area">
					<c:forEach var="cList" items="${couponList}">
			            <div class="item">
				             <ul class="details" data-amt='${cList.cpnAmt}' data-cpn-id='${cList.pinNo}' data-amt-encrypt='${cList.cpnAmtEncrypt}'>
				             	<!-- 사용자 -->
			                    <li class="cell_1"><div class="check_toggle"></div>
			                    	${cList.custNm}
			                    	<c:if test="${cList.ownYn == 'Y'}">
										(본인)			                    	
			                    	</c:if>
			                    </li>
			                    <!-- 쿠폰번호 -->
			                    <li class="cell_3">할인권번호 : <span>${cList.pinNo}</span></li>
			                    <!-- 가격 -->
			                    <li class="cell_2"><span><fmt:formatNumber value="${cList.cpnAmt}" pattern="#,###"/></span></li>
			                </ul>                 
			            </div> 
			            <div class="dot"><div class="dot_line"></div></div>  
	           		</c:forEach>
	            </div>
	    	</c:if>   
			<c:if test="${fn:length(couponList) == 0}">   
				<section class="point_null">
					<div class="point_null_script">
						할인권 발행 내역이 없습니다.
					</div>
				</section>
			</c:if>   
        </div>

    </section>
    <!-- 20190411 패밀리 포인트 - 할인권발행 내역 끝-->


    <!-- footer 시작-->
    <section class="footer">
        <div class="submet_btn_bottom">
            <ul>
                <li><a href="#!" class="bg_blue"><div id="all_check">전체선택</div></a></li>
                <li><a href="#!"><div class="btn_ok">확인</div></a></li>
            </ul>
        </div>
    </section>
    <form id="couponListForm" method="post">
    	<input type="hidden" id="coupons" name="coupons">
    </form>
	<!-- <form id="applySalesTicek" method="post" >
		<input id="ticketListBox" class="" type="hidden"> 
	</form> -->
    <!-- footer 끝-->
	<!-- contents 끝-->
</div>
<%-- js --%>
<%-- <jsp:include page="./couponListJs.jsp" flush="false" /> --%>
<%-- Footer --%>
<jsp:include page="/WEB-INF/views/pc/include/incFooter.jsp" flush="false" />

<%-- js --%>
<jsp:include page="/WEB-INF/views/pc/std/couponListJs.jsp" flush="false" />
</body>
</html>
