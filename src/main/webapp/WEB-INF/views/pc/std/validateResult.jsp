<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%>
<!-- 
   Point Hub version 1.0
  
   Copyright ⓒ 2014 kt corp. All rights reserved.
   
   This is a proprietary software of kt corp, and you may not use this file except in 
   compliance with license agreement with kt corp. Any redistribution or use of this 
   software, with or without modification shall be strictly prohibited without prior written 
   approval of kt corp, and the copyright notice above does not evidence any actual or 
   intended publication of such software. 
 -->
<%--
 **********************************************************************************************
 * @desc : KMC본인인증 결과 수신 페이지
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/pc/std/validateResult.jsp
 * @author 이형우
 * @since 2018.11.13
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.13    이형우        최초생성
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/pc/include/globalVar.jsp" flush="false" />
	
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/pc/include/incHead.jsp" flush="false" />
</head>
<body>


<%--
	*******************************************************************************
	** Html 구현
	*******************************************************************************
--%>
<%-- Footer --%>
<jsp:include page="/WEB-INF/views/pc/include/incFooter.jsp" flush="false" />

<script type="text/javascript">

$(document).ready(function(){
	opener.afterCert();
	window.close();
});

function closeWindow(){
	window.close();
}
</script>
</body>
</html>