<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 약관동의 및 본인인증 팝업호출 스크립트
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/pc/std/familyPointJs.jsp
 * @author 이형우
 * @since 2019.02.20
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일			수 정 자		수정내용
 * ----------	--------	-----------------------------
 * 2019.02.20	이형우			최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">

$(document).ready(function(){
	//초기 이벤트 생성
	FamilyPointFunc.initEvent();
});

var FamilyPointFunc = {
	initEvent:function(){
		//새로고침 버튼 클릭 이벤트
		$('#btnRefresh').click(function(){
			FamilyPointFunc.initFamlyList();
		});
		
		//일괄요청 버튼 클릭 이벤트
		$('#btnReqAll').click(function(){
			if(validateReqAll()){
				var fmlyList = new Array();
				var ownCustNm = '';
				var ownCustCtn = '';
				
				//요청자명 가져오기
				$('.item').each(function(){
					var custNm = $(this).attr('custNm');
					var custCtn = $(this).attr('custCtn');
					var ownYn = $(this).attr('ownYn');
					
					if(ownYn == 'Y'){
						ownCustNm = custNm;
						ownCustCtn = custCtn;
					}
				});
				
				$('.item').each(function(){
					var phubTrNo = $(this).attr('phubTrNo');
					var phubCpnNo = $(this).attr('phubCpnNo');
					var ktCustId = $(this).attr('ktCustId');
					var custNm = $(this).attr('custNm');
					var custCtn = $(this).attr('custCtn');
					
					var reqStatCd = $(this).attr('reqStatCd');
					var ownYn = $(this).attr('ownYn');
					if(reqStatCd != 'RE' && ownYn != 'Y' && !phubCpnNo){
						fmlyList.push({
							'phubTrNo'		: phubTrNo,
							'phubCpnNo'		: phubCpnNo,
							'ktCustId'		: ktCustId,
							'recvCustNm'	: custNm,
							'recvCustCtn'	: custCtn,
							'sendCustNm'	: ownCustNm,
							'sendCustCtn'	: ownCustCtn
						});
					}
				});
				FamilyPointFunc.reqCpn(fmlyList);
			}
		});
		
		//요청 버튼 클릭 이벤트
		$('.aReqCpn').click(function(){
			if(isEnd()){
				PHFnc.alert('거래가 종료되어 요청할 수 없습니다.');
			}else{
				var fmlyObj = $(this).parents('.item');
				
				var phubTrNo	= fmlyObj.attr('phubTrNo');
				var phubCpnNo	= fmlyObj.attr('phubCpnNo');
				var ktCustId	= fmlyObj.attr('ktCustId');
				var custNm		= fmlyObj.attr('custNm');
				var custCtn		= fmlyObj.attr('custCtn');
				var ownCustNm = '';
				var ownCustCtn = '';
				
				//요청자명 가져오기
				$('.item').each(function(){
					var custNm = $(this).attr('custNm');
					var custCtn = $(this).attr('custCtn');
					var ownYn = $(this).attr('ownYn');
					
					if(ownYn == 'Y'){
						ownCustNm = custNm;
						ownCustCtn = custCtn;
					}
				});
				
				var reqStatCd = fmlyObj.attr('reqStatCd');
				var ownYn = fmlyObj.attr('ownYn');
				
				var fmlyList = new Array();
				if(reqStatCd != 'RE' && ownYn != 'Y'){
					fmlyList.push({
						'phubTrNo'		: phubTrNo,
						'phubCpnNo'		: phubCpnNo,
						'ktCustId'		: ktCustId,
						'recvCustNm'	: custNm,
						'recvCustCtn'	: custCtn,
						'sendCustNm'	: ownCustNm,
						'sendCustCtn'	: ownCustCtn
					});
				}
				
				FamilyPointFunc.reqCpn(fmlyList);
			}
		});
		
		$('.btn_off').click(function(){
			if(isEnd()){
				PHFnc.alert('거래가 종료되어 요청할 수 없습니다.');
			}else{
				PHFnc.alert('가족 할인권 요청은 2회까지 가능 합니다.');
			}
		});
		
		$('.pop_ok').click(function(){
			$('.popup').css('display', 'none');
			FamilyPointFunc.initFamlyList();
		});
		
		$('.pop_close').click(function(){
			$('.popup').css('display', 'none');
			FamilyPointFunc.initFamlyList();
		});
		
		initReqAll();
		initTtlCpnAmt();
	},
	reqCpn:function(fmlyList){
		if(fmlyList.length == 0){
			var alertMsg = '요청에 실패하였습니다.';
			PHFnc.alert(alertMsg);
			FamilyPointFunc.initFamlyList();
		}else{
			var jsonList = JSON.stringify(fmlyList);
			
			var url = '/phub/fp/sendPesterMMS.do';
			var params = {
					'fmlyList'	: jsonList
			}
			var type = 'post';
			var dataType = 'json';
			var success = function(data, textStatus, jqXHR){
				if(data.ret_code == '00'){
					var custNmList = '';
					var fmlyCnt = fmlyList.length;
					fmlyList.forEach(function(fmlyMap){
						fmlyCnt -= 1;
						if(fmlyCnt == 0){
							custNmList += '"' + fmlyMap.recvCustNm + '"';
						}else{
							custNmList += '"' + fmlyMap.recvCustNm + '", ';
						}
					});
					alertReqCpn(custNmList);
				}
			}
			var errCustom = function(data, textStatus, jqXHR){
				var alertMsg = data.ret_msg;
				PHFnc.alert(alertMsg);
			}
			PHFnc.ajax(url, params, type, dataType, success, errCustom, true, true);
		}
	},
	initFamlyList:function(){
		var url = '/phub/fp/getFamilyList.do';
		var phubTrNo = $('.item').attr('phubTrNo');
		$('#form_phubTrNo').val(phubTrNo);
		
		var attrObj = {'form' : '#fpForm'};
		
		PHFnc.doAction(url, '', attrObj);
	}
}

function alertReqCpn(custNmList){
	$('#myPopup').css('display', 'block');
	$('#pop_custNm').html(custNmList);
}

function getTtlCpnAmt(){
	var ttlCpnAmt = 0;
	$('.cpnAmt').each(function(){
		var cpnAmt = $(this).attr('cpnAmt');
		if(cpnAmt == ''){
			cpnAmt = 0;
		}
		ttlCpnAmt += Number(cpnAmt);
	});
	return ttlCpnAmt;
}

function validateReqAll(){
	var isValid = true;
	var fmlyCnt = $('.item').length;
	var reCnt = 0;
	$('.item').each(function(){
		var reqStatCd = $(this).attr('reqStatCd');
		if(reqStatCd == 'RE'){
			reCnt += 1;
		}
	});
	
	if(isEnd()){
		PHFnc.alert('거래가 종료되어 요청할 수 없습니다.');
		isValid = false;
	}else if(reCnt == fmlyCnt-1){
		PHFnc.alert('가족 할인권 요청은 2회까지 가능 합니다.');
		isValid = false;
	}
	
	return isValid;
}

function initReqAll(){
	var fmlyCnt = $('.item').length;
	var reCnt = 0;
	$('.item').each(function(){
		var reqStatCd = $(this).attr('reqStatCd');
		if(reqStatCd == 'RE'){
			reCnt += 1;
		}
	});
	
	if(reCnt == fmlyCnt-1){
		$('#btnReqAll').css('opacity', 0.4);
	}else if(isEnd()){
		$('#btnReqAll').css('opacity', 0.4);
		$('.aReqCpn').css('opacity', 0.4);
	}
}

function initTtlCpnAmt(){
	$('#ttlCpnAmt').text(PHUtil.setComma(getTtlCpnAmt()) + 'P');
	$('.cpnAmt').each(function(){
		var cpnAmt = $(this).attr('cpnAmt');
		$(this).text(PHUtil.setComma(cpnAmt) + '원');
	});
}

function isEnd(){
	var isEnd = false;
	var endCnt = 0;
	$('.item').each(function(){
		var endYn = $(this).attr('endYn');
		if(endYn == 'Y'){
			endCnt += 1;
		}
	});
	
	if(endCnt > 0){
		isEnd = true;
	}
	return isEnd;
}

</script>