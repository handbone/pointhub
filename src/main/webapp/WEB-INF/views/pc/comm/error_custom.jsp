<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--
 **********************************************************************************************
 * @desc : 에러 페이지
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/pc/comm/error_custom.jsp
 * @author 이형우
 * @since 2018.08.20
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.20    이형우        최초생성
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- Head --%>
	<%@include file="/WEB-INF/views/pc/include/globalVar.jsp" %>
	
	<%-- Head --%>
	<%@include file="/WEB-INF/views/pc/include/incHead.jsp" %>
</head>

<body>
<div id="wrap">
<!-- head 끝-->


<!-- header 시작-->
    <header>
        <div class="container">
            <div class="title_error">포인트다모아</div>
        </div>
    </header>
<!-- header 끝-->

<!-- contents 시작-->
    <section class="contents">
        <div class="container">
            <section class="error">
                <div class="error_script" id="eMsg">
                	<input type="hidden" id="hdn_eMsg" value='<c:out value="${eMsg}"/>'>
                </div>
            </section>
        </div>
    </section>

    <section class="footer">
        <div class="submit_btn_bottom_one">
        	<a href="#!"><div class="btn_ok">확인</div></a>
        </div>
    </section>
<!-- contents 끝-->
</div>
<%-- Footer --%>
<%@include file="/WEB-INF/views/pc/include/incFooter.jsp" %>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		//에러 메시지 내 html 태그 -> <br> 태그로 치환
		var eMsg = '';
		var hdnMsg = $('#hdn_eMsg').val();
		var msgArray = hdnMsg.split(/<[^<>]*>/);
		for(var i=0; i<msgArray.length; i++){
			eMsg += msgArray[i];
			eMsg +='<br>';
		}
		$('#eMsg').html(eMsg);
		
		var returnUrl2 = $('#formReturn').attr('action');
		if(returnUrl2 == ''){
			if(!opener){
				$('.btn_ok').css('display', 'none');
			}
		}
		$('.btn_ok').click(function(){
			if(returnUrl2 == ''){
				close();
			}else{
				var url = PHUtil.nvl(returnUrl2, PHConst.RETURN_URL_2);
				var attrObj = {'form' : '#formReturn'};
				PHFnc.doAction(url, '', attrObj);
			}
		});
	});
</script>