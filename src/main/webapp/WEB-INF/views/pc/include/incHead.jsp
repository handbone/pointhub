<%@  page language="java" contentType="text/html; charset=UTF-8"
%><%--
 **********************************************************************************************
 * @desc : PC 공통 LAYOUT -> head
 * @FileName : /pointHub/src/main/webapp/WEB-INF/views/pc/include/incHead.jsp
 * @author lys
 * @since 2018.07.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.07   lys        공통 LAYOUT -> head 
 * </pre>
 **********************************************************************************************
--%>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="shortcut icon" type="image/x-icon" href="${ResRoot}/pc/img/clippoint_favicon.png">
    <title>포인트다모아</title>

    <link rel="stylesheet" href="${ResRoot}/pc/css/style.css?${TimeStamp}">
	<script type="text/javascript" src="${ResRoot}/common/jQuery/jquery.min.js?v331"></script>
	<script type="text/javascript" src="${ResRoot}/common/pubJs/script.js?v=js_Ver1&${TimeStamp}"></script>
    <!--[if lt IE 9]>
    <script src="${ResRoot}/common/pubJs/html5shiv.js"></script>
    <script src="${ResRoot}/common/pubJs/html5shiv-printshiv.js"></script><![endif]-->