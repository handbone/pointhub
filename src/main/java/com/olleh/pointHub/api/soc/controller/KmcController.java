/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.soc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.olleh.pointHub.api.comn.service.ComnService;
import com.olleh.pointHub.api.paygate.service.SbankService;
import com.olleh.pointHub.api.provider.service.ClipService;
import com.olleh.pointHub.api.soc.service.KmcService;
import com.olleh.pointHub.common.components.CodeManage;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.components.UtilsComponent;
import com.olleh.pointHub.common.exception.BaseException;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.spring.annotation.SessionTimeoutCheck;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>KMC 연동 클래스</pre>
 * @Class Name : KmcController
 * @author 이형우
 * @since 2018.08.01
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.09   이형우       certNum 난수 생성, 시스템 파라미터 조회
 * 2019.01.04   sci        본인인증 후 신청상태 업데이트
 * </pre>
 */

@Controller
public class KmcController {
	private Logger log = new Logger(this.getClass());

    @Autowired
    CodeManage codeManage;
    
    @Autowired
    SysPrmtManage sysPrmtManage;
    
    @Autowired
    UtilsComponent utilsComponent;

    @Autowired
    KmcService kmcService;
    
    @Autowired
	MessageManage messageManage;
    
    @Autowired
    ClipService  clipService;
    
	@Autowired
	SbankService sbankService;
	
	@Autowired
	ComnService comnService;
	
	/**
	 * <pre> KMC 파라미터 생성 AJAX Controller </pre>
	 * 
	 * @param String
	 * @param HttpServletRequest request
	 * @return JSON
	 * @throws IOException 
	 * @see <pre>
	 *      KMC본인인증 호출을 위한 파라미터 생성 컨트롤러
	 *      </pre>
	 */
    @SessionTimeoutCheck(required=true)
	@RequestMapping(value = "/phub/getKmcSendParam.do", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
	public String getKmcSendParam(@RequestParam Map<String, Object> params, HttpServletRequest request) throws BaseException, Exception {
    	String stepCd = "001";
		String methodName = "getKmcSendParam";
		log.info(methodName, "Start!");
		//log.debug(methodName, "getKmcSendParam's params=\n"+ params.toString());
		
		Map<String, Object> jsonObject = new HashMap<String, Object>();
		
		/* 본인인증(KMC) 팝업 -> 화면전환변경으로 sdk vs api 간 전달 */
		SbankDealVO sbankDealVO = SessionUtils.getSbankDealVO(request);
		try {
			
			String sendUrl   = sysPrmtManage.getSysPrmtVal("KMC_SEND_URL");
			String resultUrl = sysPrmtManage.getSysPrmtVal("KMC_RESULT_URL");
			String urlCode   = sysPrmtManage.getSysPrmtVal("KMC_URL_ID");
			
			String serviceId = sbankDealVO.getServiceId();
			//세션이 만료되어 서비스 아이디를 가져오지 못할 경우(브라우저 크로스 사이트 추적 방지 on 상태일 경우)
			if(StringUtil.nvl(serviceId).equals("")){
				throw new BizException(stepCd, messageManage.getMsgVOY("SY_INFO_901"));
			}
			
			//serviceId가 SVC_FP일 경우 패밀리포인트 url 코드 사용
			if(serviceId.equals(Constant.SERVICE_ID_SVC_FP)){
				urlCode   = sysPrmtManage.getSysPrmtVal("KMC_FP_URL_ID");
			} 
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("yyyyMMddHHmmss", Locale.KOREA );
			Date currentTime = new Date ();
			String date      = simpleDateFormat.format ( currentTime );//요청일시
			String randStr   = Integer.toString(Common.getRandom(1000000));
			
			/* 요청번호 : 서비스 호출 시 Unique한 값을 생성하여 전송 *결과 수신 시 복화화 KEY로 사용함 */
			String certNum   = randStr + date;
			
			String trCert    = kmcService.prepareKMCParams(certNum, urlCode);
			
			jsonObject.put("trCert", trCert);
			jsonObject.put("rt_url", params.get("rt_url"));
			jsonObject.put("platform", params.get("platform"));
			jsonObject.put("sendUrl", sendUrl);
			jsonObject.put("resultUrl", resultUrl);
			jsonObject.put("trAdd", "Y");
			jsonObject.put("ret_code", "00" );
			jsonObject.put("ret_msg", Constant.SUCCESS );
			
			/* SDK로부터 전송된 약관동의 목록 세션에 담기 */
			
			List<Map<String, Object>> paramCls = JsonUtil.JsonToList((String) params.get("clsList"));
			List<Map<String, Object>> clsList = new ArrayList<>();
			for (Map<String, Object> pmap : paramCls) {
				Map<String, Object> clsMap = new HashMap<String, Object>(pmap);
				clsList.add(clsMap);
			}
			
			sbankDealVO.setClsList(clsList);
								
			/*리턴 url, 플랫폼 세션에 저장*/
			sbankDealVO.setRtUrl(StringUtil.nvl(params.get("rt_url")));
			sbankDealVO.setPlatform(StringUtil.nvl(params.get("platform")));
			
			SessionUtils.setSbankDealVO(request, sbankDealVO);
			//log.debug(methodName, "clsList into dealVO=> \n"+ sbankDealVO.toString());
			
			
			log.debug(methodName, "End!");
		} catch (BizException be) {
			log.error(methodName, "[BizException]stepCd =" + be.getStepCd());
			log.error(methodName, "[BizException]errCd  =" + be.getErrCd());
			log.error(methodName, "[BizException]msg    =" + be.getErrMsg());

			jsonObject.put("ret_code", be.getErrCd());
			jsonObject.put("ret_msg", be.getErrMsg());
		}
		log.debug(methodName, "MapToJson="+JsonUtil.toJson(jsonObject));
		return utilsComponent.afterResMap(request, jsonObject, false);
	}
	
	/**
	 * <pre> KMC본인인증 결과를 수신하는 컨트롤러 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      KMC본인인증 결과를 수신하는 컨트롤러
	 *      </pre>
	 */
	@SessionTimeoutCheck(required=true)
	@RequestMapping(value = "/kmc/kmcisRes.do", method = {RequestMethod.POST})
	public ModelAndView kmcisRes(@RequestParam Map<String, Object> pmap
								, HttpServletRequest request
								, Locale locale
								, Model model) throws BaseException, Exception {
		String methodName = "kmcisRes";
		//log.info(methodName, "Start! (pmap="+JsonUtil.toJson(pmap)+")");

		Map<String,Object> params = new HashMap<String,Object>();
		
        String sLogCd  = Constant.SUCCESS_CODE;
        String sLogMsg = Constant.SUCCESS_MSG;
		
		ModelAndView mv = new ModelAndView("std/validateResult");
		HashMap<String,Object> kmcMap = new HashMap<String,Object>();

		SbankDealVO voDeal = SessionUtils.getSbankDealVO(request);
		
		/* ph_deal_req_info 업데이트용 파라미터 */
		Map<String, Object> reqmap = new HashMap<String, Object>();
		reqmap.put("deal_stat" , Constant.DEAL_STAT_0200);
		reqmap.put("user_id"   , StringUtil.nvl(voDeal.getUserId(),"admin"));
		reqmap.put("phub_tr_no", voDeal.getpHubTrNo());		

		/* 뒤로가기 반복에 의한 꼬임 방지 */
		boolean blMustUpdate = true;
		Map<String, Object> checkmap = new HashMap<String, Object>();
		checkmap.put("phub_tr_no", voDeal.getpHubTrNo());
		checkmap.put("uri"       , request.getRequestURI() );
		
		try {
			log.info(methodName, "Start! certNum="+ StringUtil.nvl(pmap.get("certNum")));
			
			/* 뒤로가기 반복 후 다시실행(F5)시 이상현상 막기 */
			if ("Y".equals(voDeal.getPayYn())) {
				if (!comnService.preventRetro(checkmap)){
					blMustUpdate = false;
					throw new BizException(methodName , messageManage.getMsgVOY("IF_INFO_116"));
				}
			}
			
			kmcMap = kmcService.getKMCValidData(request);
			
			mv.addObject("phubTrNo", voDeal.getpHubTrNo());
			mv.addObject("ret_code", kmcMap.get("ret_code"));
			mv.addObject("ret_msg" , kmcMap.get("ret_msg"));
			mv.addObject("service_id", voDeal.getServiceId());
			
			//KMC 결과 코드 메시지 값  set
			sLogCd  = StringUtil.nvl(kmcMap.get("ret_code"));
			sLogMsg = StringUtil.nvl(kmcMap.get("ret_msg"));
		} catch (BizException be) {
			log.debug(methodName, "[BizException]stepCd="+be.getStepCd());
			log.debug(methodName, "[BizException]errCd="+be.getErrCd());
			log.debug(methodName, "[BizException]msg="+be.getErrMsg());
			
			sLogCd  = be.getErrCd();
			sLogMsg = be.getErrMsg();

		} catch( Exception e) {
			log.debug(methodName, "[Exception]msg="+e.getMessage());
			
			sLogCd  = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg = messageManage.getMsgTxt("SY_ERROR_900");		
		} finally {
			params.put(Constant.RET_CODE, sLogCd);
			params.put(Constant.RET_MSG , sLogMsg);
			
			params.put("req_data"  , StringUtil.nvl(SessionUtils.getCertificationVO(request).getLogData()));
			params.put("res_data"  , JsonUtil.HashMapToJson(kmcMap));
			params.put("phub_tr_no", voDeal.getpHubTrNo());
			params.put("pg_tr_no"  , voDeal.getPgTrNo());
			
			clipService.logRequestFromOut(params, request, methodName);
			
			reqmap.put("err_cd" , sLogCd);
			reqmap.put("err_msg", sLogMsg);
			
			/* 거래시, 뒤로가기에 사용자액션이 아닐때에만 상태 업데이트한다. */
			if ("Y".equals(voDeal.getPayYn()) && blMustUpdate ){
				sbankService.updateDealReqInfoStatPre(reqmap);
			}

			if ( !Constant.SUCCESS_CODE.equals(sLogCd) ) {
				// ModelAndView 초기화
				mv.clear();
				if( !sLogCd.startsWith("I") ) {
					mv.setView(new RedirectView("/error.do"));	
				} else {
					mv.setView(new RedirectView("/error_custom.do"));
					FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
					flashMap.put("eCode", sLogCd);
					flashMap.put("eMsg" , sLogMsg );				
				}
			}
			
		}
		
		log.debug(methodName, "/kmc/kmcisRes.do's return =\n"+JsonUtil.toJson(mv));
		return mv;
	}
}