package com.olleh.pointHub.api.soc.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.CertificationVO;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.SessionUtils;

/**
 * KMC 연동 서비스
 * @Class Name : KmcService
 * @author 이형우
 * @since 2018.08.01
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.01	이형우			최초생성
 * 2018.08.10	이형우			CI 값 복호화 된 값으로 사용
 * </pre>
 */

@Service("kmcService")
public class KmcService   {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	SysPrmtManage sysPrmtManage;

	// 파라미터 유효성 검증 --------------------------------------------
	private boolean b = true;
	private String  regex = "";	
	
	/**
	 * KMC본인인증 호출 파라미터 생성
	 * 
	 * @param
	 * @return Map<String, Object>
	 * @see KMC 파라미터 생성
	 */
	public String prepareKMCParams(String certNum, String urlCode) throws BizException{
		String methodName = "prepareKMCParams";
		log.debug(methodName, "Start!");

		String stepCd = "001";
		//05. 본인인증요청 URL로 암호화 데이터 넘기기
		String tr_cert	= "";
		
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ("yyyyMMddHHmmss", Locale.KOREA );
        Date currentTime = new Date ();
        
        String cpId			= "KBZM1009";		//회원사ID
        String date = mSimpleDateFormat.format ( currentTime );//요청일시
        String certMet		= "M";		// 휴대폰 본인확인
        String birthDay	     = "";		// 생년월일
        String gender	     = "";
        String name          = "";        // 성명
        String phoneNo	     = "";	    // 휴대폰번호
        String phoneCorp     = "";
        String nation        = "";      // 내외국인 구분
    	String plusInfo      = "";	// 추가DATA정보
    	String extendVar     = "0000000000000000";                  // 확장변수
    	
    	try {
    		stepCd = "002";
    		//01. 한국모바일인증(주) 암호화 모듈 선언
    	    com.icert.comm.secu.IcertSecuManager seed  = new com.icert.comm.secu.IcertSecuManager();

    		//02. 1차 암호화 (tr_cert 데이터변수 조합 후 암호화)
    		String enc_tr_cert = "";
    		tr_cert = cpId +"/"+ urlCode +"/"+ certNum +"/"+ date +"/"+ certMet +"/"+ birthDay +"/"+ gender +"/"+ name +"/"+ phoneNo +"/"+ phoneCorp +"/"+ nation +"/"+ plusInfo +"/"+ extendVar;
    		stepCd = "003";
            enc_tr_cert = seed.getEnc(tr_cert, "");
            stepCd = "004";
    		//03. 1차 암호화 데이터에 대한 위변조 검증값 생성 (HMAC)
    		String hmacMsg = "";
            hmacMsg = seed.getMsg(enc_tr_cert);
            stepCd = "005";
    		//04. 2차 암호화 (1차 암호화 데이터, HMAC 데이터, extendVar 조합 후 암호화)
    		tr_cert = seed.getEnc(enc_tr_cert +"/"+ hmacMsg +"/"+ extendVar, "");
    		stepCd = "006";
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
		}
    	log.debug(methodName, "Finish!");
    	
		return tr_cert;
	}



	/**
	 * KMC본인인증 결과 파라미터를 복호화하는 프로세스
	 * 
	 * @param
	 * @return Map<String, Object>
	 * @throws ParseException 
	 * @see 인증결과 복호화
	 */
	public HashMap<String, Object> getKMCValidData(HttpServletRequest request) throws BizException {
		String methodName = "getKMCValidData";
		log.info(methodName, "Start!");
		
		String stepCd = "001";
		String ret_code = "00";
		String ret_msg = Constant.SUCCESS;

		//결과값 변수--------------------------------------------------------------------------------------------
		HashMap<String, Object> kmcMap = new HashMap<String, Object>();

    	String certNum		= "";			// 요청번호
        String date			= "";			// 요청일시
    	String CI	    	= "";			// 연계정보(CI)
    	String DI	    	= "";			// 중복가입확인정보(DI)
        String phoneNo		= "";			// 휴대폰번호
    	String phoneCorp	= "";			// 이동통신사
    	String birthDay		= "";			// 생년월일
    	String gender		= "";			// 성별
    	String nation		= "";			// 내국인
    	String name			= "";			// 성명
    	String M_name		= "";			// 미성년자 성명
    	String M_birthDay	= "";			// 미성년자 생년월일
    	String M_Gender		= "";			// 미성년자 성별
    	String M_nation		= "";			// 미성년자 내외국인
        String result		= "";			// 결과값

        String certMet		= "";			// 인증방법
        String ip			= "";			// ip주소
    	String plusInfo		= "";

    	String encPara		= "";
    	String encMsg1		= ""; 
    	String encMsg2		= "";
    	String msgChk       = "";		//-------------------------------------------------------------------------------------------------------

    	try {
    		stepCd = "002";
    		String rec_cert 	= request.getParameter("rec_cert");
        	String k_certNum	= request.getParameter("certNum").trim();
        	
        	com.icert.comm.secu.IcertSecuManager seed  = new com.icert.comm.secu.IcertSecuManager();

    		//07. rec_cert 정상/비정상 체크
    		if(rec_cert.length() == 8){
    			stepCd = "003";
    		// 비정상 결과값 처리 부분 -----------------------------------------------------------------------

    		// End - 비정상 결과값 처리 부분 -----------------------------------------------------------------
    		}else{
    			stepCd = "004";
    		// 정상 결과값 처리 부분 -------------------------------------------------------------------------
    			//08. 1차 복호화 (요청번호를 이용해 복호화)
    			rec_cert = seed.getDec(rec_cert, k_certNum);
    			
    			stepCd = "005";
    			int inf1 = rec_cert.indexOf("/",0);
    			int inf2 = rec_cert.indexOf("/",inf1+1);

    			encPara = rec_cert.substring(0, inf1);
    			encMsg1 = rec_cert.substring(inf1+1, inf2);

    			//09. 1차 복호화 데이터에 대한 위변조 검증값 검증
    			encMsg2 = seed.getMsg(encPara);

    			if(encMsg2.equals(encMsg1)){
    				stepCd = "006";
    				msgChk="Y";
    			}

    			if(msgChk.equals("N")){
    				stepCd = "007";
    				ret_code = "I116";
    				ret_msg = messageManage.getMsgTxt("IF_INFO_116");
    				
    				kmcMap.put("ret_code", ret_code);
    				kmcMap.put("ret_msg", ret_msg);

    				log.info(methodName, ret_msg);
    				
    				return kmcMap;
    			}else{
    				stepCd = "008";
    				//10. 2차 복호화 (요청번호를 이용해 복호화)
    				rec_cert  = seed.getDec(encPara, k_certNum);

    		        int info1  = rec_cert.indexOf("/",0);
    		        int info2  = rec_cert.indexOf("/",info1+1);
    		        int info3  = rec_cert.indexOf("/",info2+1);
    		        int info4  = rec_cert.indexOf("/",info3+1);
    		        int info5  = rec_cert.indexOf("/",info4+1);
    		        int info6  = rec_cert.indexOf("/",info5+1);
    		        int info7  = rec_cert.indexOf("/",info6+1);
    		        int info8  = rec_cert.indexOf("/",info7+1);
    				int info9  = rec_cert.indexOf("/",info8+1);
    				int info10 = rec_cert.indexOf("/",info9+1);
    				int info11 = rec_cert.indexOf("/",info10+1);
    				int info12 = rec_cert.indexOf("/",info11+1);
    				int info13 = rec_cert.indexOf("/",info12+1);
    				int info14 = rec_cert.indexOf("/",info13+1);
    				int info15 = rec_cert.indexOf("/",info14+1);
    				int info16 = rec_cert.indexOf("/",info15+1);
    				int info17 = rec_cert.indexOf("/",info16+1);
    				int info18 = rec_cert.indexOf("/",info17+1);

    		        certNum		= rec_cert.substring(0,info1);
    		        date		= rec_cert.substring(info1+1,info2);
    		        CI			= rec_cert.substring(info2+1,info3);
    		        phoneNo		= rec_cert.substring(info3+1,info4);
    		        phoneCorp	= rec_cert.substring(info4+1,info5);
    		        birthDay	= rec_cert.substring(info5+1,info6);
    		        gender		= rec_cert.substring(info6+1,info7);
    		        nation		= rec_cert.substring(info7+1,info8);
    				name		= rec_cert.substring(info8+1,info9);
    				result		= rec_cert.substring(info9+1,info10);
    				certMet		= rec_cert.substring(info10+1,info11);
    				ip			= rec_cert.substring(info11+1,info12);
    				M_name		= rec_cert.substring(info12+1,info13);
    				M_birthDay	= rec_cert.substring(info13+1,info14);
    				M_Gender	= rec_cert.substring(info14+1,info15);
    				M_nation	= rec_cert.substring(info15+1,info16);
    				plusInfo	= rec_cert.substring(info16+1,info17);
    				DI      	= rec_cert.substring(info17+1,info18);
    				
    				stepCd = "009";
    		        //07. CI, DI 복호화
    		        CI  = seed.getDec(CI, k_certNum);
    		        DI  = seed.getDec(DI, k_certNum);
    		        

    				if( certNum.length() == 0 || certNum.length() > 40){
    					stepCd = "010";
    					ret_code = "I117";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_117");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}

    				regex = "[0-9]*";
    				if( date.length() != 14 || !paramChk(regex, date) ){
    					stepCd = "011";
    					ret_code = "I118";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_118");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}

    				regex = "[A-Z]*";
    				if( certMet.length() != 1 || !paramChk(regex, certMet) ){
    					stepCd = "012";
    					ret_code = "I119";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_119");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}

    				regex = "[0-9]*";
    				if( (phoneNo.length() != 10 && phoneNo.length() != 11) || !paramChk(regex, phoneNo) ){
    					stepCd = "013";
    					ret_code = "I120";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_120");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}
    				
    				regex = "[A-Z]*";
    				if( phoneCorp.length() != 3 || !paramChk(regex, phoneCorp) ){
    					stepCd = "014";
    					ret_code = "I121";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_121");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}

    				regex = "[0-9]*";
    				if( birthDay.length() != 8 || !paramChk(regex, birthDay) ){
    					stepCd = "015";
    					ret_code = "I122";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_122");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}

    				regex = "[0-9]*";
    				if( gender.length() != 1 || !paramChk(regex, gender) ){
    					stepCd = "016";
    					ret_code = "I123";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_123");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}

    				regex = "[0-9]*";
    				if( nation.length() != 1 || !paramChk(regex, nation) ){
    					stepCd = "017";
    					ret_code = "I124";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_124");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}
    				
    				regex = "[\\sA-Za-z가-�R.,-]*";
    				if( name.length() > 60 || !paramChk(regex, name) ){
    					stepCd = "018";
    					ret_code = "I125";
    					ret_msg = messageManage.getMsgTxt("IF_INFO_125");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}
    				
    				regex = "[A-Z]*";
    				if( result.length() != 1 || !paramChk(regex, result) ){
    					stepCd = "019";
    					ret_code = "F126";
    					ret_msg = messageManage.getMsgTxt("IF_ERROR_126");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}
    				
    				regex = "[\\sA-Za-z가-?.,-]*";
    				if( M_name.length() != 0 ){
    					if( M_name.length() > 60 || !paramChk(regex, M_name) ){
    						stepCd = "020";
    						ret_code = "I127";
    						ret_msg = messageManage.getMsgTxt("IF_INFO_127");
    						
    						kmcMap.put("ret_code", ret_code);
    						kmcMap.put("ret_msg", ret_msg);
    						
    						log.info(methodName, ret_msg);
    						
    						return kmcMap;
    					}
    				}
    				
    				regex = "[0-9]*";
    				if( M_birthDay.length() != 0 ){
    					if( M_birthDay.length() != 8 || !paramChk(regex, M_birthDay) ){
    						stepCd = "021";
    						ret_code = "I128";
    						ret_msg = messageManage.getMsgTxt("IF_INFO_128");
    						
    						kmcMap.put("ret_code", ret_code);
    						kmcMap.put("ret_msg", ret_msg);
    						
    						log.info(methodName, ret_msg);
    						
    						return kmcMap;
    					}
    				}

    				regex = "[0-9]*";
    				if( M_Gender.length() != 0 ){
    					if( M_Gender.length() != 1 || !paramChk(regex, M_Gender) ){
    						stepCd = "022";
    						ret_code = "I129";
    						ret_msg = messageManage.getMsgTxt("IF_INFO_129");
    						
    						kmcMap.put("ret_code", ret_code);
    						kmcMap.put("ret_msg", ret_msg);
    						
    						log.info(methodName, ret_msg);
    						
    						return kmcMap;
    					}
    				}

    				regex = "[0-9]*";
    				if( M_nation.length() != 0 ){
    					if( M_nation.length() != 1 || !paramChk(regex, M_nation) ){
    						stepCd = "023";
    						ret_code = "I130";
    						ret_msg = messageManage.getMsgTxt("IF_INFO_130");
    						
    						kmcMap.put("ret_code", ret_code);
    						kmcMap.put("ret_msg", ret_msg);
    						
    						log.info(methodName, ret_msg);
    						
    						return kmcMap;
    					}
    				}
    				// End 파라미터 유효성 검증 --------------------------------------------
    				stepCd = "024";
    				// Start - 수신내역 유효성 검증(사설망의 사설 IP로 인해 미사용, 공용망의 경우 확인 후 사용) *********************/
    				// 1. date 값 검증
    				SimpleDateFormat formatter	= new SimpleDateFormat("yyyyMMddHHmmss",Locale.KOREAN); // 현재 서버 시각 구하기
    				String strCurrentTime	= formatter.format(new Date());
    				
    				Date toDate				= formatter.parse(strCurrentTime);
    				Date fromDate			= formatter.parse(date);
    				long timediff			= toDate.getTime()-fromDate.getTime();
    				
    				if ( timediff < -30*60*1000 || 30*60*100 < timediff  ){
    					stepCd = "025";
    					//ret_code = "F131";
    					ret_code = messageManage.getMsgCd("IF_INFO_131");
    					ret_msg  = messageManage.getMsgTxt("IF_INFO_131");
    					
    					kmcMap.put("ret_code", ret_code);
    					kmcMap.put("ret_msg", ret_msg);
    					
    					log.info(methodName, ret_msg);
    					
    					return kmcMap;
    				}
    				
    				// 2. ip 값 검증
    				String client_ip = request.getHeader("HTTP_X_FORWARDED_FOR"); // 사용자IP 구하기
    				if ( client_ip != null ){
    					if( client_ip.indexOf(",") != -1 )
    						stepCd = "026";
    						client_ip = client_ip.substring(0,client_ip.indexOf(","));
    				}
    				if ( client_ip==null || client_ip.length()==0 ){
    					stepCd = "027";
    					client_ip = request.getRemoteAddr();
    				}
    				
    				if(sysPrmtManage.getSysPrmtVal("KMC_IP_CHECK").equals("Y")){
    					if( !client_ip.equals(ip) ){
    						stepCd = "028";
    						ret_code = "F132";
    						ret_msg = messageManage.getMsgTxt("IF_ERROR_132");
    						
    						kmcMap.put("ret_code", ret_code);
    						kmcMap.put("ret_msg", ret_msg);
    						
    						log.info(methodName, ret_msg);
    						log.info(methodName, "client_ip : "+client_ip +", ip : "+ip );
    						
    						return kmcMap;
    					}
    				}
    				
    				// Masking
    				//log.info(methodName, "KMC validation certNum : "+ k_certNum);
    				if("".equals(name)){
    					log.info(methodName, "KMC validation name : "+ name);
    				}else{
    					log.info(methodName, "KMC validation name : "+ Common.getMaskCustNm(name));
    				}
    				log.info(methodName, "KMC validation phoneNo : "+ Common.getMaskHpNo(phoneNo));
    				log.info(methodName, "KMC validation phoneCorp : "+ phoneCorp);
    				log.info(methodName, "KMC validation birthDay : "+ birthDay);
    				//log.info(methodName, "KMC validation gender : "+ gender);				
    				//log.info(methodName, "KMC validation date : "+ date);
    				//log.info(methodName, "KMC validation ip : "+ ip);
    				
    				/*
    				if("".equals(M_name)){
    					//log.info(methodName, "KMC validation M_name : "+  M_name);
    				}else{
    					//log.info(methodName, "KMC validation M_name : "+  Common.getMaskCustNm(M_name));
    				}
    				*/
    				
    				//log.info(methodName, "KMC validation M_Gender : "+ M_Gender);
    				//log.info(methodName, "KMC validation M_nation : "+ M_nation);
    				log.info(methodName, "KMC validation result : "+ result);
    				//log.info(methodName, "KMC validation plusInfo : "+ plusInfo);
    				log.info(methodName, "KMC validation CI : "+ CI);
    				
    				stepCd = "029";
    				CertificationVO certificationVO = new CertificationVO();
    				certificationVO.setCustCI(CI);
    				certificationVO.setPhoneNo(phoneNo);
    				certificationVO.setPhoneCorp(phoneCorp);
    				certificationVO.setBirthDay(birthDay);
    				certificationVO.setGender(gender);
    				certificationVO.setNations(nation);
    				certificationVO.setCustName(name);
    				certificationVO.setValidResult(result);
    				certificationVO.setCustDI(DI);
    				certificationVO.setSuccYn(result);
    				
    				String ctzNo7 = this.getCtzNo(birthDay, gender);
    				
    				certificationVO.setCtzNo7(ctzNo7);
    				
    				
    				SessionUtils.setCertificationVO(request, certificationVO);
    								
    				log.info(methodName, "Finish!");
    				
    				kmcMap.put("ret_code", ret_code);
					kmcMap.put("ret_msg", ret_msg);
    			}
    		}
		} catch (Exception e) {
			//TODO KMC 데이터 수신 에러
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Certification By KMC Fail");
			throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
		}
		return kmcMap;
	}
	

	public Boolean paramChk(String patn, String param){
		Pattern pattern = Pattern.compile(patn);
		Matcher matcher = pattern.matcher(param);
		b = matcher.matches();
		return b;
	}
	
	public String getCtzNo(String pBirth, String pSex){
    	String sRet = "";   	
    	String sYy = pBirth.substring(0,2);
    	
    	if (pSex.equals("0")){
    		if (sYy.equals("19")){
    			sRet = pBirth.substring(2) + "1";
    		}else{
    			sRet = pBirth.substring(2) + "3";
    		}
    	} else{
    		if (sYy.equals("19")){
    			sRet = pBirth.substring(2) + "2";
    		}else{
    			sRet = pBirth.substring(2) + "4";
    		}
    	}
		
		return sRet;
	}

}
