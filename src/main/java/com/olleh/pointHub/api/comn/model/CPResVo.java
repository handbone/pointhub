/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.comn.model;

import java.util.Map;

import com.olleh.pointHub.common.utils.DateUtils;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>클립포인트 수신(response) 결과정보 VO</pre>
 * 
 * @Class Name : CPResVo
 * @author lys
 * @since 2018.08.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.05   lys        최초생성
 * </pre>
 */
public class CPResVo {	
    
	/** 결과 클래스 */
	private String retcode;			// 응답코드
	private String retmsg;			// 응답메시지
	private String req_point;		// 요청포인트
	private String current_point;	// 사용가능포인트
	private String hub_tr_id;		// 거래고유번호(포인트허브)
	private String optcode;			// 옵션코드(카드사의 응답코드등)
	private String caller;			// caller (caller에 따라 결과를 parsing 할때 사용)
	private String sucYn;			// 성공여부
	private Map    reqBody;			// 송신데이터

	
	/** Getter, Setter */
	public String getRetcode() {
		return StringUtil.nvl(retcode);
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}
	public String getRetmsg() {
		return StringUtil.nvl(retmsg);
	}
	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}
	public String getReq_point() {
		return StringUtil.nvl(req_point);
	}
	public void setReq_point(String req_point) {
		this.req_point = req_point;
	}
	public String getCurrent_point() {
		return StringUtil.nvl(current_point);
	}
	public void setCurrent_point(String current_point) {
		this.current_point = current_point;
	}
	public String getHub_tr_id() {
		return StringUtil.nvl(hub_tr_id);
	}
	public void setHub_tr_id(String hub_tr_id) {
		this.hub_tr_id = hub_tr_id;
	}
	public String getOptcode() {
		return optcode;
	}
	public void setOptcode(String optcode) {
		this.optcode = optcode;
	}
	public String getCaller() {
		return StringUtil.nvl(caller);
	}
	public void setCaller(String caller) {
		this.caller = caller;
	}
	public String getSucYn() {
		return StringUtil.nvl(sucYn, "N");
	}
	public void setSucYn(String sucYn) {
		this.sucYn = sucYn;
	}
	public Map getReqBody() {
		return reqBody;
	}
	public void setReqBody(Map reqBody) {
		this.reqBody = reqBody;
	}
}