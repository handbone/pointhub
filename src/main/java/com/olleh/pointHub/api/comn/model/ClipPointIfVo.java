/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.comn.model;

import java.util.Map;

/**
 * <pre>클립포인트 IF 정보 VO</pre>
 * 
 * @Class Name : ClipPointIfVo
 * @author lys
 * @since 2018.08.09
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.09   lys        최초생성
 * 2018.10.30   sci        검색일자 추가
 * </pre>
 */
public class ClipPointIfVo {
	
	
	/** 결과 클래스 */
	private String userCi;				// 유저ci
	private String pntTrNo;				// 거래고유번호 (카드사별)
	private String oriPntTrNo;			// 이전거래고유번호 (카드사별)
	private String ctn;					// 전화번호(암호화 대상)
	private String userId;				// 거래요청자
	private String pntCd;				// 카드사코드
	private String pntAmt;				// 요청포인트
	private String date;				// 요청일자
	private String time;				// 요청시간
	private String retCode;				// 응답코드
	private String retMsg;				// 응답메시지
	private String currentPoint;		// 사용가능포인트
	private String optcode;				// 옵션코드
	private Map    reqBody;				// 송신데이터
	private String phubTrNo;			// 거래고유번호 (포인트허브 거래 마스터ID)
	private String sucYn;				// 성공여부 (Y:성공, N:실패)
	private String caller;				// caller (caller에 따라 결과를 parsing 할때 사용)
	
	private String dealDt;              // 거래일시
	
	private String stDealDt;			// deal_dt 검색조건 추가
	private String endDealDt;			// deal_dt 검색조건 추가	
	
	public void setCopy(ClipPointIfVo fCard) {
		// TODO Auto-generated constructor stub
		this.userCi       = fCard.getUserCi();
		this.pntTrNo      = fCard.getPntTrNo();
		this.oriPntTrNo   = fCard.getOriPntTrNo();
		this.ctn          = fCard.getCtn();
		this.userId       = fCard.getUserId();
		this.pntCd        = fCard.getPntCd();
		this.pntAmt       = fCard.getPntAmt();
		this.date         = fCard.getDate();
		this.time         = fCard.getTime();
		this.retCode      = fCard.getRetCode();
		this.retMsg       = fCard.getRetMsg();
		this.currentPoint = fCard.getCurrentPoint();
		this.optcode      = fCard.getOptcode();
		this.reqBody      = fCard.getReqBody();
		this.phubTrNo     = fCard.getPhubTrNo();
		this.sucYn        = fCard.getSucYn();
		this.caller       = fCard.getCaller();
		this.dealDt       = fCard.getDealDt();
		this.stDealDt     = fCard.getStDealDt();
		this.endDealDt    = fCard.getEndDealDt();
	}

	/**
	 * @return the dealDt
	 */
	public String getDealDt() {
		return dealDt;
	}
	/**
	 * @param dealDt the dealDt to set
	 */
	public void setDealDt(String dealDt) {
		this.dealDt = dealDt;
	}

	/** Getter, Setter */
	public String getUserCi() {
		return userCi;
	}
	public void setUserCi(String userCi) {
		this.userCi = userCi;
	}
	public String getPntTrNo() {
		return pntTrNo;
	}
	public void setPntTrNo(String pntTrNo) {
		this.pntTrNo = pntTrNo;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPntCd() {
		return pntCd;
	}
	public void setPntCd(String pntCd) {
		this.pntCd = pntCd;
	}
	public String getPntAmt() {
		return pntAmt;
	}
	public void setPntAmt(String pntAmt) {
		this.pntAmt = pntAmt;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getRetCode() {
		return retCode;
	}
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	public String getRetMsg() {
		return retMsg;
	}
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	public String getCurrentPoint() {
		return currentPoint;
	}
	public void setCurrentPoint(String currentPoint) {
		this.currentPoint = currentPoint;
	}
	public String getOptcode() {
		return optcode;
	}
	public void setOptcode(String optcode) {
		this.optcode = optcode;
	}
	public Map getReqBody() {
		return reqBody;
	}
	public void setReqBody(Map reqBody) {
		this.reqBody = reqBody;
	}
	public String getPhubTrNo() {
		return phubTrNo;
	}
	public void setPhubTrNo(String phubTrNo) {
		this.phubTrNo = phubTrNo;
	}
	public String getSucYn() {
		return sucYn;
	}
	public void setSucYn(String sucYn) {
		this.sucYn = sucYn;
	}	
	public String getCaller() {
		return caller;
	}
	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getOriPntTrNo() {
		return oriPntTrNo;
	}
	public void setOriPntTrNo(String oriPntTrNo) {
		this.oriPntTrNo = oriPntTrNo;
	}
	public String getStDealDt() {
		return stDealDt;
	}
	public void setStDealDt(String stDealDt) {
		this.stDealDt = stDealDt;
	}
	public String getEndDealDt() {
		return endDealDt;
	}
	public void setEndDealDt(String endDealDt) {
		this.endDealDt = endDealDt;
	}
	
	@Override
	public String toString() {
		return "ClipPointIfVo [userCi=" + userCi + ", pntTrNo=" + pntTrNo + ", oriPntTrNo=" + oriPntTrNo + ", ctn="
				+ ctn + ", userId=" + userId + ", pntCd=" + pntCd + ", pntAmt=" + pntAmt + ", date=" + date + ", time="
				+ time + ", retCode=" + retCode + ", retMsg=" + retMsg + ", currentPoint=" + currentPoint + ", optcode="
				+ optcode + ", reqBody=" + reqBody + ", phubTrNo=" + phubTrNo + ", sucYn=" + sucYn + ", caller="
				+ caller + ", dealDt=" + dealDt + ", stDealDt=" + stDealDt + ", endDealDt=" + endDealDt + "]";
	}
	
}