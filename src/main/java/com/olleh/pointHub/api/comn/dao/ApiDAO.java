package com.olleh.pointHub.api.comn.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pointHub.common.dao.AbstractDAO;

/**
 * api 공통 api DAO
 * @Class Name : ApiDAO
 * @author sci
 * @since 2019.01.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.01.28   sci        최초생성
 * </pre>
 */
@Repository("apiDAO")
public class ApiDAO extends AbstractDAO {
	/**
	 * <pre>포인트 거래번호 채번</pre>
	 * 
	 * @param  형식만
	 * @return lmap
	 * @see
	 */
	public Map<String,Object> getPntRootId(Map<String, Object> params) {
		return selectOne("mybatis.common.getPntRootId", params);
	}
	
	/**
	 * <pre>포인트 거래번호 접두사 조회</pre>
	 * 
	 * @param  prvdr_id (포인트제공처ID)
	 * @return String (ph_pnt_prvdr.pnt_prfx)
	 * @see
	 */
	public Map<String,Object> getPntTrNoPrfx(String pntCd) {
		return selectOne("mybatis.common.getPntTrNoPrfx", pntCd);
	}
	
	/**
	 * <pre>거래상태 조회</pre>
	 * 
	 * @param  phub_tr_no, uri(거래번호,uri)
	 * @return String (ph_deal_req_info.deal_stat)
	 * @see
	 */
	public Map<String,Object> getDealStatCd(Map<String,Object> params) {
		return selectOne("mybatis.common.getDealStatCd", params);
	}	
	
}
