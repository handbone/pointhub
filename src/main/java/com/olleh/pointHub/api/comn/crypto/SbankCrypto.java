/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.comn.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.CryptoUtil;
import com.olleh.pointHub.common.utils.StringUtil;


/**
 * 세틀뱅크 암복호화 처리
 * @Class Name : SbankCrypto
 * @author lys
 * @since 2018.08.03
 * @version 1.0
 * @see <pre>
 *  1. 알고리즘 : AES/CBC/PKCS5Padding
 * 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.03   lys        최초생성
 * </pre>
 */
@Component
@DependsOn(value={"sysPrmtManage"})
public class SbankCrypto {
	private Logger log = new Logger(SbankCrypto.class);
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	/*
	 * 암호화키,iv
	 */
//	private String secretKey;
//	private String iv;
//
//    
//	@PostConstruct
//	public void start() {
//		// 시스템파라미터 정보 메모리 로드
//		secretKey = sysPrmtManage.getSysPrmtVal("PH_SB_KEY");
//		iv        = sysPrmtManage.getSysPrmtVal("PH_SB_KEY_IV");
//		
//		log.debug("start", "SbankCrypto!");	
//	}     
	
	
	/**
	 * <pre> 세틀뱅크 암호화 </pre>
	 * 
	 * @param String dat
	 * @return String
	 * @see
	 */
	public String encAES(String str) {
		String encStr = "";
		String secretKey = sysPrmtManage.getSysPrmtVal("PH_SB_KEY");
		String iv        = sysPrmtManage.getSysPrmtVal("PH_SB_KEY_IV");		
		
		try {
			encStr = CryptoUtil.encAES(StringUtil.nvl(str), secretKey, iv);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException
				| IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			log.debug("encAES", "exception msg="+StringUtil.nvl(e.getMessage()));
		}
		
		return encStr;
	}	
	
	/**
	 * <pre> 세틀뱅크 복호화 </pre>
	 * 
	 * @param String dat
	 * @return String
	 * @see
	 */	
	public String decAES(String str) {
		String decStr = "";
		String secretKey = sysPrmtManage.getSysPrmtVal("PH_SB_KEY");
		String iv        = sysPrmtManage.getSysPrmtVal("PH_SB_KEY_IV");
		
		try {
			decStr = CryptoUtil.decAES(StringUtil.nvl(str), secretKey, iv);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException
				| IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			log.debug("decAES", "exception msg="+StringUtil.nvl(e.getMessage()));
			decStr = "";
		}
		
		return decStr;
	}
	
	/**
	 * SHA-256으로 해시한다.
	 * 
	 * @param msg
	 * @return
	 * @see
	 */
	public String sha256(String str) {
		String encStr = "";
		try {
			encStr = CryptoUtil.sha256(StringUtil.nvl(str));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			log.debug("sha256", "exception msg="+StringUtil.nvl(e.getMessage()));
			encStr = "";
		}
		
		return encStr;
	}	
}
