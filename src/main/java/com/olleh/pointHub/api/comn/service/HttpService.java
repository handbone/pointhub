/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.comn.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import org.springframework.web.util.UriComponentsBuilder;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * Http 통신 관련 Service 
 * @Class Name : HttpService
 * @author lys
 * @since 2018.07.24
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.24   lys        최초생성
 * </pre>
 */
public class HttpService {
	public Logger log = new Logger(this.getClass());
	
	private static final int CONNECTION_TIMEOUT = 10000;
	private static final int READ_TIMEOUT = 20000;
	
	@Autowired
	MessageManage messageManage;

	@Autowired
	SysPrmtManage sysPrmtManage;
	
	/**
	 * <pre> post방식의 http(s)통신을 한다. </pre>
	 * 
	 * @param String baseUrl - 도메인까지 포함한 전체 url
	 * @param Map<String,Object> headers - 헤더영역 정보
	 * @param Map<String, Object> params - body 정보
	 * @return ResponseEntity<Map>
	 * @see <pre>
	 *      1. 도메인까지 포함한 전체 url 로 http(s) 통신을 하고 결과값을 리턴 한다.
	 *      2. 현재는 json 방식의 통신만 구현되어 있다.
	 *      </pre>
	 */
    public ResultVo sendHttpPost(String baseUrl, Map<String,Object> headers, Map<String, Object> params) throws Exception {
    	String methodName = "sendHttpPost";
    	//log.debug(methodName, "Start!");
    	
    	// 로그기록여부
    	String logWriteYN = StringUtil.nvl(params.get(Constant.API_LOG_WRITE_YN), "Y"); 	
    	
    	// 결과객체
    	ResultVo resultVo = new ResultVo();
    	resultVo.setSucYn("N");
    	
    	try {
            //String baseUrl = getApiServerUrl() + "/plugins/restapi/v1/users/{id}";
        	// set Headers
    		/***************************************************************
    		 * 1. Header 정보 셋팅
    		 ***************************************************************/
        	String authorization          = StringUtil.nvl(headers.get("authorization"));
        	String appType                = StringUtil.nvl(headers.get("appType"), "json");
        	Map<String, Object> bodyParam = params;
        	
        	// get Headers
        	//HttpEntity<?> httpEntity = getHttpEntity(authorization, appType, bodyParam);
        	HttpEntity httpEntity = getHttpEntity(authorization, appType, bodyParam);
        	//log.debug(methodName, "httpEntity="+JsonUtil.toJson(httpEntity));
        	//log.debug(methodName, "ReqeustHeader["+httpEntity.getHeaders()+"]");
        	//log.debug(methodName, "RequestBody["+httpEntity.getBody()+"]");
        	

    		/***************************************************************
    		 * 2. RestTemplate 객체 생성
    		 ***************************************************************/        	
        	RestTemplate restTemplate = getRestTempalte();
            
        	
    		/***************************************************************
    		 * 3. http, https 통신
    		 ***************************************************************/        	
        	URL wasUrl = new URL(baseUrl);
        	resultVo.setReqBody(bodyParam);
        	
        	// 송신데이터 로그 기록
        	//log.debug(methodName, "[request]URL="+baseUrl);
        	//log.debug(methodName, "[request]Body Data="+JsonUtil.toJson(httpEntity.getBody()));
        	
        	// call
        	ResponseEntity<Map> responseEntity = restTemplate.exchange(baseUrl, HttpMethod.POST, httpEntity, Map.class);
        	//log.debug(methodName, "StatusCode="+responseEntity.getStatusCode().toString());
        	

    		/***************************************************************
    		 * 2. Retul값 처리
    		 ***************************************************************/
    		HttpHeaders header = responseEntity.getHeaders();
    		Map body           = responseEntity.getBody(); 
    		String stat        = responseEntity.getStatusCode().toString();
    		int intCd          = responseEntity.getStatusCodeValue();
    		
    		
    		/***************************************************************
    		 * 2-1. Result값  log 기록
    		 ***************************************************************/
    		//log.debug(methodName, "[response]HttpHeaders="+JsonUtil.toJson(header));
    		if( !"N".equals(logWriteYN) ) {
    			//log.debug(methodName, "[response]Map="+JsonUtil.toJson(body));
    		}    		
    		//log.debug(methodName, "[response]HttpStatus="+stat);
    		//log.debug(methodName, "[response]intCd="+intCd);    		
        	
    		
    		/***************************************************************
    		 * 2-2. 리턴객체에 수신데이터 및 상태값 셋팅
    		 ***************************************************************/    		
    		resultVo.setCaller("SbankHttpService.sendHttpPost");
    		resultVo.setRstBody(body);
    		resultVo.setRstCd(String.valueOf(intCd));
    		resultVo.setRstMsg(String.valueOf(stat));    		
    		
    		// 성공결과 셋팅 : 벤더별로 성공결과를 셋팅해야 한다.
    		if( (200 == intCd) && "200".equals(stat) ) {
    			resultVo.setSucYn("Y");
    		} else {
    			log.info(methodName, "invalid stat&intCd!");    			
    		}
    		
    		// finish log
    		//log.debug(methodName, "Success!");
    		
    	} catch (MalformedURLException e) {
    		//
    		log.error(methodName, "[MalformedURLException]url fail!");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_902"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(HttpStatusCodeException e) {
    		//
    		log.error(methodName, "Ezfarm Server HttpStatusCodeException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_907"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(ResourceAccessException e) {
    		//
    		log.error(methodName, "Ezfarm Server ResourceAccessException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_903"));
    		resultVo.setRstMsg(e.getMessage());

    	} catch(UnknownHttpStatusCodeException e) {
    		//
    		log.error(methodName, "Ezfarm Server UnknownHttpStatusCodeException======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_904"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(RestClientException e) {
    		//
    		log.error(methodName, "Ezfarm Server RestClientException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_905"));
    		resultVo.setRstMsg(e.getMessage());    		
    		
    	} catch (Exception e) {
    		//
    		log.error(methodName, "[Exception]fail!");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_900"));
    		resultVo.setRstMsg(e.getMessage());
    	}
    	
    	
		/***************************************************************
		 * 3. 리턴!
		 ***************************************************************/
    	//log.debug(methodName, "End!");
    	    	
        return resultVo;
    }
    
    /**
     * <pre>Get 방식의 http(s)통신을 한다.
     * -> 헤더 정의</pre>
     * 
     * @param builderUrl
     * @param headers
     * @param resultVo
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	protected ResultVo sendHttpGet(String baseUrl, HttpHeaders headers, ResultVo resultVo, Map<String, Object> params) throws Exception {
    	String methodName = "sendHttpGet";
    	log.debug(methodName, "Start!");
    	
    	// 로그기록여부
    	String logWriteYN = StringUtil.nvl(params.get(Constant.API_LOG_WRITE_YN), "Y");    	
    	
    	// 결과객체
    	resultVo.setSucYn("N");
    	
    	try {
    		// 1. 헤더 세팅
    		HttpEntity httpEntity = new HttpEntity<>(headers);
    		
    		// 2. 파라미터 세팅
    		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl);
    		try {
    			Map<String, Object> param = (Map<String, Object>) resultVo.getReqBody();
    			for (Map.Entry<String, ?> entry : param.entrySet()) {
    				uriBuilder.queryParam(entry.getKey(), StringUtil.nvl(entry.getValue()));
    			}
    		} catch (Exception e) {
    			log.error(methodName, e.getMessage());
    		}
    		
    		// 3. RestTemplate 객체 생성
    		RestTemplate restTemplate = getRestTempalte();
    		
    		log.debug(methodName, "[request]URL="+baseUrl);
        	log.debug(methodName, "[request]Body Data="+JsonUtil.toJson(resultVo.getReqBody()));
    		
        	// 4. HTTP(s) 연동
        	ResponseEntity<String> responseEntity = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, httpEntity, String.class);
        	log.debug(methodName, "StatusCode="+responseEntity.getStatusCode().toString());
        	
        	// 5. 결과 값 세팅
    		resultVo.setRstBody(responseEntity.getBody());
    		resultVo.setRstCd(String.valueOf(responseEntity.getStatusCodeValue()));
    		resultVo.setRstMsg(String.valueOf(responseEntity.getStatusCode().toString()));
    		
    		//log.debug(methodName, "[response]HttpHeaders="+ JsonUtil.toJson(responseEntity.getHeaders()));
    		if( !"N".equals(logWriteYN) ) {
    			log.debug(methodName, "[response]Json="+ resultVo.getRstBody());
    		}    		
    		//log.debug(methodName, "[response]intCd="+ resultVo.getRstCd());
    		//log.debug(methodName, "[response]HttpStatus="+ resultVo.getRstMsg());
    		
    		// 6. 성공 결과 세팅
    		if(responseEntity.getStatusCodeValue() == HttpURLConnection.HTTP_OK) {
    			resultVo.setSucYn("Y");
    		} else {
    			log.info(methodName, "invalid stat&intCd!");    			
    		}
    		
    		// finish log
    		log.debug(methodName, "Success!");
    		
    	} catch(HttpStatusCodeException e) {
    		//
    		log.error(methodName, "Ezfarm Server HttpStatusCodeException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_907"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(ResourceAccessException e) {
    		//
    		log.error(methodName, "Ezfarm Server ResourceAccessException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_903"));
    		resultVo.setRstMsg(e.getMessage());

    	} catch(UnknownHttpStatusCodeException e) {
    		//
    		log.error(methodName, "Ezfarm Server UnknownHttpStatusCodeException======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_904"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(RestClientException e) {
    		//
    		log.error(methodName, "Ezfarm Server RestClientException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_905"));
    		resultVo.setRstMsg(e.getMessage());    		
    		
    	} catch (Exception e) {
    		//
    		log.error(methodName, "[Exception]fail!");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_900"));
    		resultVo.setRstMsg(e.getMessage());
    	}
    	
    	return resultVo;
    }
    
    /**
     * <pre>Post 방식의 http(s)통신을 한다</pre>
     * - MultiValueMap 타입은 수신쪽의 RequestBody 타입으로 받을 경우 Array타입으로 전달됨
     * - FormData or MultiPart 전송의 경우는 상관없이 RequestParam으로 <String, Object> 타입으로 전달받을수 있음
     * - 기프티쇼 연동 타입으로 이후 Application/json 으로 진행될 경우 sendHttpPost(String baseUrl, Map<String,Object> headers, Map<String, Object> params) 메서도 호출 바람
     * 
     * @param baseUrl
     * @param headers
     * @param resultVo
     * @return json
     * @throws Exception
     */
	@SuppressWarnings("unchecked")
	protected ResultVo sendHttpPost(String baseUrl, HttpHeaders headers, ResultVo resultVo, Map<String, Object> params) throws Exception {
    	String methodName = "sendHttpPost";
    	log.debug(methodName, "Start!");
    	
    	// 로그기록여부
    	String logWriteYN = StringUtil.nvl(params.get(Constant.API_LOG_WRITE_YN), "Y");    	
    	
    	// 결과객체
    	resultVo.setSucYn("N");
    	
    	try {
    		// 1. 파라미터 정의
    		MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<String, Object>();
    		try {
    			Map<String, Object> param = (Map<String, Object>) resultVo.getReqBody();
    			for (Map.Entry<String, ?> entry : param.entrySet()) {
    				requestBody.add(entry.getKey(), StringUtil.nvl(entry.getValue()));
    			}
    		} catch (Exception e) {
    			log.error(methodName, e.getMessage());
    		}
    		
    		// 2. 파라미터 & 헤더 세팅
    		HttpEntity<?> httpEntity = new HttpEntity<MultiValueMap<String, Object>>(requestBody, headers);
    		
    		// 3. RestTemplate 객체 생성
    		RestTemplate restTemplate = getRestTempalte();
    		
    		log.debug(methodName, "[request]URL="+baseUrl);
        	log.debug(methodName, "[request]Body Data="+JsonUtil.toJson(httpEntity.getBody()));
    		
        	// 4. HTTP 연동
        	ResponseEntity<String> responseEntity = restTemplate.exchange(baseUrl, HttpMethod.POST, httpEntity, String.class);
        	log.debug(methodName, "StatusCode="+responseEntity.getStatusCode().toString());
        	
        	// 5. 결과 값 세팅
    		resultVo.setRstBody(responseEntity.getBody());
    		resultVo.setRstCd(String.valueOf(responseEntity.getStatusCodeValue()));
    		resultVo.setRstMsg(String.valueOf(responseEntity.getStatusCode().toString()));
    		
    		//log.debug(methodName, "[response]HttpHeaders="+ JsonUtil.toJson(responseEntity.getHeaders()));
    		if( !"N".equals(logWriteYN) ) {
    			log.debug(methodName, "[response]Json="+ resultVo.getRstBody());
    		}
    		//log.debug(methodName, "[response]intCd="+ resultVo.getRstCd());
    		//log.debug(methodName, "[response]HttpStatus="+ resultVo.getRstMsg());
    		
    		// 6. 성공 결과 세팅
    		if(responseEntity.getStatusCodeValue() == HttpURLConnection.HTTP_OK) {
    			resultVo.setSucYn("Y");
    		} else {
    			log.info(methodName, "invalid stat&intCd!");    			
    		}
    		
    		// finish log
    		log.debug(methodName, "Success!");
    		
    	} catch(HttpStatusCodeException e) {
    		//
    		log.error(methodName, "Ezfarm Server HttpStatusCodeException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_907"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(ResourceAccessException e) {
    		//
    		log.error(methodName, "Ezfarm Server ResourceAccessException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_903"));
    		resultVo.setRstMsg(e.getMessage());

    	} catch(UnknownHttpStatusCodeException e) {
    		//
    		log.error(methodName, "Ezfarm Server UnknownHttpStatusCodeException======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_904"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(RestClientException e) {
    		//
    		log.error(methodName, "Ezfarm Server RestClientException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_905"));
    		resultVo.setRstMsg(e.getMessage());    		
    		
    	} catch (Exception e) {
    		//
    		log.error(methodName, "[Exception]fail!");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_900"));
    		resultVo.setRstMsg(e.getMessage());
    	}
    	
    	return resultVo;
    }
    
    /**
     * <pre>Post 방식의 http(s)통신을 한다. -> XML 반환</pre>
     * - MultiValueMap 타입은 수신쪽의 RequestBody 타입으로 받을 경우 Array타입으로 전달됨
     * - FormData or MultiPart 전송의 경우는 상관없이 RequestParam으로 <String, Object> 타입으로 전달받을수 있음
     * - 기프티쇼 연동 타입으로 이후 Application/json 으로 진행될 경우 sendHttpPost(String baseUrl, Map<String,Object> headers, Map<String, Object> params) 메서도 호출 바람
     * - 현재 사용하고 있지 않음
     * 
     * @param sendUrl
     * @param headers
     * @param resultVo
     * @return json
     * @throws Exception
     */
    protected ResultVo sendHttpPostToXml(String baseUrl, HttpHeaders headers, Map<String, Object> params, ResultVo resultVo) throws Exception {
    	String methodName = "sendHttpPostToXml";
    	log.debug(methodName, "Start!");
    	
    	// 로그기록여부
    	String logWriteYN = StringUtil.nvl(params.get(Constant.API_LOG_WRITE_YN), "Y");    	
    	
    	// 결과객체
    	resultVo.setSucYn("N");
    	
    	try {
    		// 1. 파라미터 정의
    		MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<String, Object>();
    		try {
    			for (Map.Entry<String, ?> entry : params.entrySet()) {
    				requestBody.add(entry.getKey(), StringUtil.nvl(entry.getValue()));
    			}
    		} catch (Exception e) {
    			log.error(methodName, e.getMessage());
    		}
    		
    		// 2. 헤더 및 파라미터 세팅
    		HttpEntity<?> httpEntity = new HttpEntity<MultiValueMap<String, Object>>(requestBody, headers);
    		
    		// 3. RestTemplate 객체 생성
    		RestTemplate restTemplate = getRestTempalte();
    		
    		log.debug(methodName, "[request]URL="+baseUrl);
        	log.debug(methodName, "[request]Body Data="+JsonUtil.toJson(httpEntity.getBody()));
    		
        	// 4. HTTP 연동
        	ResponseEntity<String> responseEntity = restTemplate.exchange(baseUrl, HttpMethod.POST, httpEntity, String.class);
        	log.debug(methodName, "StatusCode="+responseEntity.getStatusCode().toString());
        	
        	// 5. 결과 값 세팅
    		resultVo.setRstBody(responseEntity.getBody());
    		resultVo.setRstCd(String.valueOf(responseEntity.getStatusCodeValue()));
    		resultVo.setRstMsg(String.valueOf(responseEntity.getStatusCode().toString()));
    		
    		//log.debug(methodName, "[response]HttpHeaders="+ JsonUtil.toJson(responseEntity.getHeaders()));
    		if( !"N".equals(logWriteYN) ) {
    			log.debug(methodName, "[response]XML="+ responseEntity.getBody());
    		}
    		//log.debug(methodName, "[response]intCd="+ resultVo.getRstCd());
    		//log.debug(methodName, "[response]HttpStatus="+ resultVo.getRstMsg());
    		
    		// 6. 성공 결과 세팅
    		if(responseEntity.getStatusCodeValue() == HttpURLConnection.HTTP_OK) {
    			resultVo.setSucYn("Y");
    		} else {
    			log.info(methodName, "invalid stat&intCd!");    			
    		}
    		
    		// finish log
    		log.debug(methodName, "Success!");
    		
    	} catch(HttpStatusCodeException e) {
    		//
    		log.error(methodName, "Ezfarm Server HttpStatusCodeException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_907"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(ResourceAccessException e) {
    		//
    		log.error(methodName, "Ezfarm Server ResourceAccessException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_903"));
    		resultVo.setRstMsg(e.getMessage());

    	} catch(UnknownHttpStatusCodeException e) {
    		//
    		log.error(methodName, "Ezfarm Server UnknownHttpStatusCodeException======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_904"));
    		resultVo.setRstMsg(e.getMessage());
    		
    	} catch(RestClientException e) {
    		//
    		log.error(methodName, "Ezfarm Server RestClientException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_905"));
    		resultVo.setRstMsg(e.getMessage());    		
    		
    	} catch (Exception e) {
    		//
    		log.error(methodName, "[Exception]fail!");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_900"));
    		resultVo.setRstMsg(e.getMessage());
    	}
    	
		return resultVo;
    }
    
    /**
     * <pre>SOAP http(s)통신을 한다.</pre>
     * 
     * @param sendUrl
     * @param resultVo
     * @return xml
     * @throws Exception
     */
    protected ResultVo sendHttpPostSoapXml(String sendUrl, ResultVo resultVo, Map<String, Object> params) throws Exception {
    	String methodName = "sendHttpPostSoapXml";
    	log.debug(methodName, "Start!");
    	
    	// 로그기록여부
    	String logWriteYN = StringUtil.nvl(params.get(Constant.API_LOG_WRITE_YN), "Y");
    	
    	// 결과객체
    	resultVo.setSucYn("N");
    	
    	HttpURLConnection conn = null;
    	DataOutputStream dos = null;
    	BufferedReader br = null;
    	try {
    		String body = resultVo.getReqBody().toString();
    		StringBuffer sb = new StringBuffer();
    		
    		URL url = new URL(sendUrl);
    		conn = (HttpURLConnection) url.openConnection();
    		conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE);
    		conn.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(body.length()));
    		conn.setRequestMethod(RequestMethod.POST.toString());
    		conn.setConnectTimeout(CONNECTION_TIMEOUT);
    		conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.connect();
		
			dos = new DataOutputStream(conn.getOutputStream());
			dos.write(body.getBytes());
			dos.flush();
			
			int responseCode = conn.getResponseCode();
			String responseMessage = conn.getResponseMessage();
			log.debug(methodName, "[response]Code: "+ responseCode);
    		if( !"N".equals(logWriteYN) ) {
    			//log.debug(methodName, "[response]Message: "+ responseMessage);
    		}
			
			br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line.trim());
			}
			dos.close();
			br.close();
    		
    		resultVo.setRstBody(sb.toString());
    		resultVo.setRstCd(String.valueOf(responseCode));
    		resultVo.setRstMsg(String.valueOf(responseMessage));
    		
    		// 성공결과 셋팅 : 벤더별로 성공결과를 셋팅해야 한다.
    		if(responseCode == HttpURLConnection.HTTP_OK) {
    			resultVo.setSucYn("Y");
    		} else {
    			log.info(methodName, "invalid stat&intCd!");    			
    		}
    		
    		// finish log
    		log.info(methodName, "Success!");
			
    	} catch (MalformedURLException me) {
    		log.error(methodName, "Ezfarm Server MalformedURLException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(me.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_902"));
    		resultVo.setRstMsg(me.getMessage());
    	} catch (IOException ie) {
    		log.error(methodName, "Ezfarm Server IOException ======");
    		log.error(methodName, "Message: " + StringUtil.nvl(ie.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_908"));
    		resultVo.setRstMsg(ie.getMessage());
    	} catch (Exception e) {
    		log.error(methodName, "[Exception]fail!");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		resultVo.setRstCd(messageManage.getMsgCd("IF_ERROR_905"));
    		resultVo.setRstMsg(e.getMessage());
    	} finally {
    		try {
	    		if (dos != null) {
	    			dos.close();
	    		}
    		} catch (Exception e) {log.debug(methodName, "dos Exception: " + StringUtil.nvl(e.getMessage()));}
    		try {
    			if (br != null) {
    				br.close();
    			}
    		} catch (Exception e) {log.debug(methodName, "br Exception: " + StringUtil.nvl(e.getMessage()));}
    		try {
	    		if (conn != null) {
	    			conn.disconnect();
	    		}
    		} catch (Exception e) {log.debug(methodName, "conn Exception: " + StringUtil.nvl(e.getMessage()));}
    	}
    	
    	return resultVo;
    }
    
    /**
     * Secret키와 Content-type을 설정
     * @param String authorization(SysPrmtManage.REST_API_SECRETKEY)
     * @param appType (json)
     * @param params
     * @return HttpEntity<?>
     * @see
     */
    private HttpEntity getHttpEntity(String authorization, String appType, Map<String, Object> params) {
        // set Header 
        HttpHeaders headers = new HttpHeaders();
        //Charset utf8 = Charset.forName("UTF-8");
        //headers.setContentType(new MediaType("application", appType, utf8));
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set(HttpHeaders.CACHE_CONTROL, "no-cache");
        if( !"".equals(StringUtil.nvl(authorization)) ) headers.set("Authorization", authorization);	// PG사 연도에만 필요하므로, null이 아닐때만 추가한다.
        
        // 파라미터 null 체크
        return new HttpEntity(params, headers);
//        if ( Common.isNull(params) || params.isEmpty())
//            return new HttpEntity<Object>(headers);
//        else
//            //return new HttpEntity<MultiValueMap<String, Object>>(parameters, headers);
//        	return new HttpEntity<Map<String, Object>>(params, headers);
    }
    
	/**
     * <pre>http 통신을 위한 스프링 restTemplate 객체 생성</pre>
     * @param
     * @return RestTemplate
     * @see
     */
    private RestTemplate getRestTempalte() {
        // set httpClient
        HttpClient httpClient = HttpClientBuilder.create()
        		.setMaxConnTotal(100) 	// connection pool 적용 (최대 오픈되는 커넥션 수를 제한한다)
        		.setMaxConnPerRoute(5) 	// connection pool 적용 (setMaxConnPerRoute : IP,포트 1쌍에 대해 수행 할 연결 수를 제한한다)
        		.build(); 
        
    	// set HttpComponentsClientHttpRequestFactory
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setReadTimeout(READ_TIMEOUT);  			// 읽기시간초과, ms : 20초
        factory.setConnectTimeout(CONNECTION_TIMEOUT);	// 연결시간초과, ms : 10초
        factory.setHttpClient(httpClient); 				// 동기실행에 사용될 HttpClient 세팅         
        
        // set RestTemplate
        RestTemplate restTemplate = new RestTemplate(factory);        
        
        // return
        return restTemplate;
    }
    
    
}