/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.comn.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pointHub.common.dao.AbstractDAO;
import com.olleh.pointHub.common.log.Logger;

/**
 * api 공통 log DAO
 * @Class Name : LogDAO
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Repository("logDAO")
public class LogDAO extends AbstractDAO {
	private Logger log = new Logger(this.getClass());
	
		
	/**
	 * <pre>I/F 로그를 기록 한다.</pre>
	 * 
	 * @param 액션URI, 발신처, 수신처, 포인트허브거래번호, 송수신값, 응답코드, 응답메시지, 등록자ID, 등록일시
	 * @return 공통코드
	 * @see
	 */
	public int insertAdmApiIfLog(Map<String, Object> params) {
		return insert("mybatis.api.log.insertAdmApiIfLog", params);
	}
	
	
	/**
	 * <pre>I/F 로그 결과 갱신</pre>
	 * 
	 * @param 액션URI, 발신처, 수신처, 포인트허브거래번호, 송수신값, 응답코드, 응답메시지, 등록자ID, 등록일시
	 * @return 공통코드
	 * @see
	 */
	public int updateAdmApiIfLog(Map<String, Object> params) {
		return update("mybatis.api.log.updateAdmApiIfLog", params);
	}	
}