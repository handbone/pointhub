/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.comn.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.api.comn.dao.ApiDAO;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.PntPrvdrMsgManage;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.PntPrvdrMsgVO;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.StringUtil;


/**
 * api 공통 service
 * @Class Name : ComnService
 * @author lys
 * @since 2018.11.07
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.07    lys        최초생성
 * </pre>
 */
@Service
public class ComnService {
	private Logger log = new Logger(this.getClass());

	@Autowired
	PntPrvdrMsgManage pntPrvdrMsgManage;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	ApiDAO apiDAO;

	/**
	 * <pre> 포인트거래번호 어근찾기 </pre>
	 * 
	 * @param  String pntFrfx : 거래번호 접두사
	 * @return String 
	 * @see <pre>
	 *       
	 *      </pre>
	 */
	public String getPntTrNoRoot(Map<String,Object> params) {
		//Map<String,Object> params = new HashMap<String,Object>();
		Map<String,Object> retmap = new HashMap<String,Object>();
		retmap = apiDAO.getPntRootId(params);
		log.debug("getPntTrNoRoot", retmap.toString());
		return (String) retmap.get("pnt_tr_id");
	}
	
	/**
	 * <pre> 포인트거래번호 접두사찾기 </pre>
	 * 
	 * @param  String pntCd   : 카드사코드
	 * @return String pntFrfx : 거래번호 접두사
	 * @see <pre>
	 *         getPntTrNoPrfx 결과값 & getPntTrNoRoot결합하여 포인트거래번호 생성
	 *         참조: getPntTrNoRoot
	 *      </pre>
	 */
	public String getPntTrNoPrfx(String pntCd) {
		Map<String,Object> params = new HashMap<String,Object>();
		Map<String,Object> retmap = new HashMap<String,Object>();
		retmap = apiDAO.getPntTrNoPrfx(pntCd);
		log.debug("getPntTrNoPrfx", retmap.toString());
		return (String) retmap.get("pnt_prfx");
	}	
	
	/**
	 * <pre> 클립포인트 에러코드 변환 처리 </pre>
	 * 
	 * @param String pntCd : 카드사코드
	 * @param String msgId : 클립포인트 결과코드
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. 클립포인트 결과코드를 포인트허브 결과코드로 변환시켜 준다.
	 *      </pre>
	 */
	public String convClipPointErrCd(String pntCd, String msgId) {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "errorProc";
		String rtnCd      = "";
		String prvdrId    = "CP0001";		// 클립포인트ID
		
		try {
			/***************************************************************
			 * 1. mapper 테이블에 매핑되어 있는 포인트허브 메시지ID를 리턴
			 * test
			 ***************************************************************/
			PntPrvdrMsgVO pntMsgIdVO = pntPrvdrMsgManage.getPntMsgIdVO(prvdrId, pntCd, msgId); 
			rtnCd = StringUtil.nvl(pntMsgIdVO.getRfrnVal1());
			
		} catch (Exception e) {
			rtnCd = "";
		}
		
		/***************************************************************
		 * 2. 리턴 
		 ***************************************************************/
		//log.debug(methodName, "rtnCd="+rtnCd+"(pntCd="+pntCd+", msgId="+msgId+")");
		return rtnCd;
	}
	
	/**
	 * <pre> 사용자의 뒤로가기 방지 </pre>
	 * 
	 * @param String  phub_tr_no:거래번호
	 * @param String  uri
	 * @return Map<String, Object>
	 * @see <pre>
	 *  (1) /kmc/kmcisRes.do  : SbankDealVO.getPayYn() = "Y" 인 경우에 한하여 ph_deal_req_info의 상태값이 "0100" 이 아니면 false 리턴
	 *  (2) /phub/std/point.do: ph_deal_req_info의 상태값이 "0200", "0300"이 아니면 false 리턴
	 *  (3) /phub/std/pay.do  : ph_deal_req_info의 상태값이 "0300" 이 아니면 false 리턴
	 *  </pre>
	 */
	public boolean preventRetro(Map<String, Object> param) {
		boolean retBln = false;
		String  methodName  = "preventRetro";
		String  sDealStat   = "";
		String  sUri = StringUtil.nvl(param.get("uri"));
		Map<String,Object> map = new HashMap<String,Object>();
		
		try {
			if (StringUtil.nvl(param.get("phub_tr_no")).equals("") || sUri.equals("")) {
				return retBln;
			}
			
			map = apiDAO.getDealStatCd(param);
			
			if (Common.isNull(map)){
				return retBln;
			}

			sDealStat = StringUtil.nvl(map.get("deal_stat"));
			
			switch (sUri) {
				case "/kmc/kmcisRes.do":
					if(Constant.DEAL_STAT_0100.equals(sDealStat)) {
						retBln = true;
					}
					break;
				case "/phub/std/point.do":
					if(Constant.DEAL_STAT_0200.equals(sDealStat)||Constant.DEAL_STAT_0300.equals(sDealStat)) {
						retBln = true;
					}
					break;
				case "/phub/std/pay.do":
					if(Constant.DEAL_STAT_0300.equals(sDealStat)) {
						retBln = true;
					}
					break;
				case "/pg/pay":
					if(Constant.DEAL_STAT_1000.equals(sDealStat)) {
						retBln = true;
					}
					break;
				default :					
					break;
			}
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		
		return retBln;
	}
	
}