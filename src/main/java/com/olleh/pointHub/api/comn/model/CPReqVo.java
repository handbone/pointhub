/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.comn.model;

import java.util.List;
import java.util.Map;

import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>클립포인트 송신(request) 정보 VO</pre>
 * 
 * @Class Name : CPReqVo
 * @author lys
 * @since 2018.08.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.05   lys        최초생성
 * </pre>
 */
public class CPReqVo {
	
	
	/** 결과 클래스 */
	private String userCi;						// 유저ci
	private String hubTrId;						// 거래고유번호
	private String ctn;							// 전화번호(암호화 대상)
	private String userId;						// 거래요청자
	private String date;						// 요청일자
	private String time;						// 요청시간
	private List<Object> pntList;				// 카드사정보 List()
	private String pntTrNo;						// 거래고유번호 (카드사별)
	private String pntCd;						// 카드사코드
	private String pntAmt;						// 요청포인트
	private String caller;						// caller (caller에 따라 결과를 parsing 할때 사용)
	//private Object tmpData;						// 기타 정의되지 않은 추가정보를 담는다.

	
	/** Getter, Setter */
	public String getUserCi() {
		return StringUtil.nvl(userCi);
	}
	public void setUserCi(String userCi) {
		this.userCi = userCi;
	}
	public String getHubTrId() {
		return StringUtil.nvl(hubTrId);
	}
	public void setHubTrId(String hubTrId) {
		this.hubTrId = hubTrId;
	}
	public String getCtn() {
		return StringUtil.nvl(ctn);
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getUserId() {
		return StringUtil.nvl(userId);
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDate() {
		return StringUtil.nvl(date);
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return StringUtil.nvl(time);
	}
	public void setTime(String time) {
		this.time = time;
	}
	public List<Object> getPntList() {
		return pntList;
	}
	public void setPntList(List<Object> pntList) {
		this.pntList = pntList;
	}
	public String getPntTrNo() {
		return StringUtil.nvl(pntTrNo);
	}
	public void setPntTrNo(String pntTrNo) {
		this.pntTrNo = pntTrNo;
	}
	public String getPntCd() {
		return StringUtil.nvl(pntCd);
	}
	public void setPntCd(String pntCd) {
		this.pntCd = pntCd;
	}
	public String getPntAmt() {
		return StringUtil.nvl(pntAmt);
	}
	public void setPntAmt(String pntAmt) {
		this.pntAmt = pntAmt;
	}
	public String getCaller() {
		return StringUtil.nvl(caller);
	}
	public void setCaller(String caller) {
		this.caller = caller;
	}
}