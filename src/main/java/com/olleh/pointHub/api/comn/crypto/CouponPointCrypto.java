/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.comn.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.CryptoUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 쿠폰 포인트 암호화 처리
 * @Class Name : CouponPointCrypto
 * @author khj
 * @since 2019.05.17
 * @version 1.0
 * @see <pre>
 *  1. AES128 암호화
 * 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.05.17   khj        최초생성
 * 2019.05.23   khj		  AES256 -> AES128 변경
 * </pre>
 */
@Component
@DependsOn(value={"sysPrmtManage"})
public class CouponPointCrypto {
private Logger log = new Logger(CouponPointCrypto.class);
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	/**
	 * <pre> 쿠폰 포인트 암호화 AES128 </pre>
	 * 
	 * @param String str
	 * @return String
	 * @see
	 */
	public String encAES(String str) {
		String encStr = "";
		String secretKey = sysPrmtManage.getSysPrmtVal("CL_CP_KEY");
		String iv		 = sysPrmtManage.getSysPrmtVal("CL_CP_KEY_IV");
		
		try {
			encStr = CryptoUtil.encAES(StringUtil.nvl(str), secretKey, iv);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException
				| IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
			log.debug("encAES", "exception msg="+StringUtil.nvl(e.getMessage()));
		} 
		return encStr;
	}	
	
	/**
	 * <pre> 쿠폰 포인트 복호화 AES128 </pre>
	 * 
	 * @param String str
	 * @return String
	 * @see
	 */	
	public String decAES(String str) {
		String decStr = "";
		String secretKey = sysPrmtManage.getSysPrmtVal("CL_CP_KEY");
		String iv		 = sysPrmtManage.getSysPrmtVal("CL_CP_KEY_IV");
		
		try {
			decStr = CryptoUtil.decAES(StringUtil.nvl(str), secretKey, iv);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException
				| IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
			log.debug("decAES", "exception msg="+StringUtil.nvl(e.getMessage()));
		}
		return decStr;
	}
	
}
