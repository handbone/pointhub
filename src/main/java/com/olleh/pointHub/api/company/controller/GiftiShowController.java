package com.olleh.pointHub.api.company.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.api.paygate.service.SbankService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>기프티쇼(mhous) 연동 콘트롤러</pre>
 * @Class Name : GiftishowController
 * @author : lys
 * @since : 2019.02.11
 * @version : 1.0
 * @see <pre> 
 * << 개정이력(Modification Information) >> 
 *    수정일          수정자         수정내용 
 * -----------  ----------- ---------------- 
 * 2019. 02. 11 lys  최초 생성
 * </pre>
 */

/*
 * 1.쿠폰 상태변경 요청 chgCpnStatus ("/fp/giftishow/chgCpnStatus")
 * tradeType : 01(핀상태변경), 02(유효기간변경)
 * tradeCode : 02(교환), 03(반품), 04(관리폐기), 07(구매취소)
 * ^^ @TODO 사용 또는 취소 가능 체크 필요
 * */

@RestController
public class GiftiShowController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	GiftiShowService giftiShowService;
	
	@Autowired
	SbankService sbankService;

	/**
     * <pre>기프티쇼 실시간 승인 통보 요청 처리 API</pre>
     * @see <pre>파라미터 상세정의는 외부연동규약서 참조</pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description 기프티쇼 실시간 승인 통보 요청 처리
     */
	@RequestMapping(value = "/fp/giftishow/chgCpnStatus", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public Map<String, Object> chgCpnStatus(@RequestParam Map<String, Object> params, HttpServletRequest request) {
		String methodName = "chgCpnStatus";
		String stepCd = "001";
		log.info(methodName, "Init Param From GiftiShow:: \n" + params.toString());
		
		Map<String, Object> resultMap  = new HashMap<String,Object>();
		Map<String, Object> serviceMap = new HashMap<String,Object>();
		Map<String, Object> returnMap  = new HashMap<String,Object>();
		Map<String, Object> orgParams  = params;
		
		try {
			serviceMap = giftiShowService.chgCpnService(params, request);
			resultMap  = new HashMap(serviceMap);
		}catch (BizException be){
			log.error(methodName, "BizException code="+StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg="+StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step="+StringUtil.nvl(be.getStepCd()));
			
			resultMap.put(Constant.RET_CODE, be.getErrCd());
			resultMap.put(Constant.RET_MSG , be.getErrMsg());
		}catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception stepCd="+StringUtil.nvl(stepCd));
			log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));
			
			resultMap.put(Constant.RET_CODE, Constant.FAIL_CODE);
			resultMap.put(Constant.RET_MSG , Constant.FAIL_MESSAGE);
		} finally {
			//연동규격에 맞는 응답코드 변환
			resultMap.put("phubTrNo", StringUtil.nvl(params.get("tr_id")));
			String resCode = StringUtil.nvl(resultMap.get(Constant.RET_CODE)).equals(Constant.SUCCESS_CODE) ? "1000" : "9999";
			returnMap.put("resCode", resCode);
			returnMap.put("resMsg", resultMap.get(Constant.RET_MSG));
			
			giftiShowService.logRequestFromGS(orgParams, returnMap, resultMap, request, methodName);
			
		}
		
		log.info(methodName, "end! >>> " + JsonUtil.toJson(returnMap));
		return returnMap;
	}
}
