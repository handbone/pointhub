package com.olleh.pointHub.api.company.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface KOSService {
	public void logRequestFromKOS(Map<String, Object> params, Map<String, Object> sendMap, 
			HttpServletRequest request, String pMethod, String phubTrNo, String pgTrNo);
}
