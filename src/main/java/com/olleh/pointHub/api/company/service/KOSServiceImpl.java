package com.olleh.pointHub.api.company.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

@Service
public class KOSServiceImpl implements KOSService{
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	LogService logService;
	/**
     * <pre>insert 기프티쇼 인터페이스 로그</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return void
     * @description insert 기프티쇼 인터페이스 로그
     */
	@Override
	public void logRequestFromKOS(Map<String, Object> params, Map<String, Object> sendMap, 
			HttpServletRequest request, String pMethod, String phubTrNo, String pgTrNo) {
		Map<String, Object> logMap = new HashMap<String, Object>();
		String methodName = "logRequestFromKOS";
		
		try {
			logMap.put("actn_uri"		, request.getRequestURI());
			logMap.put("send_plc"		, Constant.PLC_KOS);
			logMap.put("rcv_plc"		, Constant.PLC_PH);
			logMap.put("phub_tr_no"		, phubTrNo);
			logMap.put("pg_deal_no"		, pgTrNo);
			logMap.put("req_data"		, JsonUtil.MapToJson(sendMap));
			logMap.put("res_data"		, JsonUtil.MapToJson(params));
			logMap.put("rply_cd"		, StringUtil.nvl(sendMap.get(Constant.RET_CODE),"00"));
			logMap.put("rply_msg"		, StringUtil.nvl(sendMap.get(Constant.RET_MSG), "200"));
			logMap.put("rgst_user_id"	, pMethod);
			
			logService.insertAdmApiIfLog(logMap);
		} catch (Exception e) {
			log.error(methodName, JsonUtil.MapToJson(logMap));
			log.error(methodName, StringUtil.nvl(e.getMessage()));
		}
	}
}
