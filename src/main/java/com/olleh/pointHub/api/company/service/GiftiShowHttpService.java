package com.olleh.pointHub.api.company.service;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.api.comn.service.HttpService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.CryptoUtil;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 기프티쇼 내부 Http(s) 연동 클래스
 * 
 * @author CREDIF
 */
@Service("GiftiShowHttpService")
public class GiftiShowHttpService extends HttpService {
	private static final String SEND_PLC = "PH";
	private static final String RECV_PLC = "GS";
	
	private static final String GS_DOMAIN_KEY = "GS_DOMAIN";
	private static final String GS_MDCODE_KEY = "GS_MDCODE";
	private static final String GS_COUPON_URI = "/media/request.asp";
	private static final String GS_RESEND_URI = "/media/Resend.asp";
	private static final String GS_STATUS_URI = "/media/coupon_status.asp";
	
	private static final String PARAM_MDCODE = "MDCODE";		//매체코드
	private static final String PARAM_ENC_FLAG = "enc_flag";	//암호화여부
	private static final String PARAM_GUBUN = "gubun";			//구분
	
	private static final String RES_RESPONSE = "response";
	private static final String RES_RESULT = "result";
	private static final String RES_CODE = "code";
	private static final String RES_REASON = "reason";
	private static final String RES_VALUE = "value";
	private static final String RES_STATUS_CODE = "StatusCode";
	private static final String RES_STATUS_TEXT = "StatusText";
	
	/** 제목 Parameter key */
	public static final String PARAM_TITLE = "TITLE";
	/** 전송메시지 Parameter key */
	public static final String PARAM_MSG = "MSG";
	/** 발신번호 Parameter key */
	public static final String PARAM_CALLBACK = "CALLBACK";
	/** 수신번호 Parameter key */
	public static final String PARAM_PHONE_NO = "phone_no";
	/** 거래아이디 Parameter key */
	public static final String PARAM_TR_ID = "tr_id";
	/** 핀번호 Parameter key */
	public static final String PARAM_PIN_NO = "pin_no";
	
	/** 쿠폰발행 성공 코드 */
	public static final String GS_SUCS_COUPON_CD = "1000";
	/** 기프티쇼 성공 기본 코드 */
	public static final String GS_SUCS_CD = "0";
	
	@Autowired
    SysPrmtManage sysPrmtManage;
	
	@Autowired
    LogService logService;
	
	/**
	 * <pre>쿠폰 발송 API
	 * - TITLE: SMS 제목 [EUC-KR]
	 * - MSG: SMS 내용 [EUC-KR]
	 * - CALLBACK: 호출번호
	 * - goods_id: 상품아이디
	 * - phone_no: 수신자번호
	 * - tr_id: 클라이언트유니크키 (20)</pre>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public ResultVo sendHttpPostGiftiShowRequest(Map<String, Object> params) throws Exception {
		String methodName = getClass().getName() +"."+ "sendHttpPostGiftiShowRequest";
		log.debug(methodName, "Start!");
		
		ResultVo resultVo = null;
		
		String trId = params.get(PARAM_TR_ID).toString();
		
		resultVo = sendHttpPostGiftiShowCoupon(params);
		String pinNo = resultVo.getPinNo();
		
		params.clear();
		params.put(PARAM_TR_ID, trId);
		
		resultVo = sendHttpPostGiftiShowReSend(params);
		if (resultVo != null && resultVo.getRstCd().equals(GS_SUCS_CD)) {
			resultVo.setPinNo(pinNo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>쿠폰 발행 연동 API
	 * - TITLE: SMS 제목 [EUC-KR]
	 * - MSG: SMS 내용 [EUC-KR]
	 * - CALLBACK: 호출번호
	 * - goods_id: 상품아이디
	 * - phone_no: 수신자번호
	 * - tr_id: 클라이언트유니크키 (20)</pre>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public ResultVo sendHttpPostGiftiShowCoupon(Map<String, Object> inParams) throws BizException, Exception {
		String methodName = getClass().getName() +"."+ "sendHttpPostGiftiShowCoupon";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal(GS_DOMAIN_KEY) + GS_COUPON_URI;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		Map<String, Object> params = setParams(inParams);
		try {
			// 1. 파라미터 정의 & 인코딩, 암호화
			params.put(PARAM_ENC_FLAG, "Y");
			params.put(PARAM_GUBUN, "Y");
			for (Map.Entry<String, ?> entry : params.entrySet()) {
				String key = entry.getKey();
				String value = "";
				if (key.equals(PARAM_TITLE) || key.equals(PARAM_MSG)) {
					//EUC-KR 인코딩
					value = urlEncodeString(StringUtil.nvl(entry.getValue().toString()), "EUC-KR");
				} else if (key.equals(PARAM_CALLBACK) || key.equals(PARAM_PHONE_NO)) {
					//AES-256 암호화
					value = encrytoAES256(StringUtil.nvl(entry.getValue()));
				} else {
					value = StringUtil.nvl(entry.getValue());
				}
				params.put(key, value);
			}
			resultVo.setReqBody(params);
			
			// 2. 헤더 정의
			HttpHeaders headers = getHeaders();
			
			// 3. 통신
			resultVo = sendHttpPostToXml(sendUrl, headers, params, resultVo);
			if (!resultVo.getSucYn().equals("Y")) {
				throw new BizException(resultVo.getRstMsg());
			}
			
			// 4. 결과 세팅
			JSONObject response = XML.toJSONObject(new StringReader(StringUtil.nvl(resultVo.getRstBody()))).getJSONObject(RES_RESPONSE);
			JSONObject result = response.getJSONObject(RES_RESULT);
			String code = StringUtil.nvl(result.get(RES_CODE));
			resultVo.setRstCd(code);
			if (code.equals(GS_SUCS_COUPON_CD)) {
				// 성공 시
				resultVo.setPinNo(StringUtil.nvl(response.getJSONObject(RES_VALUE).get(PARAM_PIN_NO)));
				log.debug(methodName, "pin_no : "+ resultVo.getPinNo());
			} else {
				// 실패 시
				resultVo.setSucYn("N");
				resultVo.setRstMsg(StringUtil.nvl(result.get(RES_REASON)));
			}
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get(PARAM_TR_ID)), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>재전송 API
	 * - MDCODE: 매체코드 -> 정의됨
	 * - tr_id: 클라이언트유니크키</pre>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public ResultVo sendHttpPostGiftiShowReSend(Map<String, Object> inParams) throws BizException, Exception {
		String methodName = getClass().getName() +"."+ "sendHttpPostGiftiShowReSend";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal(GS_DOMAIN_KEY) + GS_RESEND_URI;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		Map<String, Object> params = setParams(inParams);
		try {
			// 1. 파라미터 정의
			resultVo.setReqBody(params);
			
			// 2. 헤더 정의
			HttpHeaders headers = getHeaders();
    		
    		// 3. 통신
    		resultVo = sendHttpPostToXml(sendUrl, headers, params, resultVo);
			if (!resultVo.getSucYn().equals("Y")) {
				throw new BizException(resultVo.getRstMsg());
			}
			
			// 4. 결과 세팅
			JSONObject response = XML.toJSONObject(new StringReader(StringUtil.nvl(resultVo.getRstBody()))).getJSONObject(RES_RESPONSE);
			JSONObject result = response.getJSONObject(RES_RESULT);
			String code = StringUtil.nvl(result.get(RES_CODE));
			resultVo.setRstCd(code);
			if (!code.equals(GS_SUCS_CD)) {
				// 실패 시
				resultVo.setSucYn("N");
				resultVo.setRstMsg(StringUtil.nvl(result.get(RES_REASON)));
			}
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get(PARAM_TR_ID)), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>쿠폰유효성체크 API
	 * - MDCODE: 매체코드 -> 정의됨
	 * - tr_id: 클라이언트유니크키</pre>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public ResultVo sendHttpPostGiftiShowStatus(Map<String, Object> inParams) throws BizException, Exception {
		String methodName = getClass().getName() +"."+ "sendHttpPostGiftiShowStatus";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal(GS_DOMAIN_KEY) + GS_STATUS_URI;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		Map<String, Object> params = setParams(inParams);
		try {
			// 1. 파라미터 정의
			resultVo.setReqBody(params);
			
			// 2. 헤더 정의
			HttpHeaders headers = getHeaders();
    		
    		// 3. 통신
    		resultVo = sendHttpPostToXml(sendUrl, headers, params, resultVo);
			if (!resultVo.getSucYn().equals("Y")) {
				throw new BizException(resultVo.getRstMsg());
			}
			
			// 4. 결과 세팅
			JSONObject response = XML.toJSONObject(new StringReader(StringUtil.nvl(resultVo.getRstBody()))).getJSONObject(RES_RESPONSE);
			JSONObject result = response.getJSONObject(RES_RESULT);
			String code = StringUtil.nvl(result.get(RES_STATUS_CODE));
			resultVo.setRstCd(code);
			if (!code.equals(GS_SUCS_CD)) {
				// 실패 시
				resultVo.setSucYn("N");
				resultVo.setRstMsg(StringUtil.nvl(result.get(RES_STATUS_TEXT)));
			}
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get(PARAM_TR_ID)), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>기프티쇼 전문 송/수신 Log기록</pre>
	 * 
	 * @param sendUrl
	 * @param phubTrNo
	 * @param rgstUserId
	 * @param resultVo
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void insertAdmApiIfLog(String sendUrl, String phubTrNo, String rgstUserId, ResultVo resultVo) {
		String methodName = getClass().getName() +"."+ "insertAdmApiIfLog";
		
		// declare
	    Map<String, Object> logMap = new HashMap<String, Object>();
	    
	    try {
	    	Map tmpReq = (Map)resultVo.getReqBody();
	    	if (tmpReq == null) {
	    		tmpReq = new HashMap();
	    	}
		    logMap.put("actn_uri",     sendUrl);
		    logMap.put("send_plc",     SEND_PLC);
		    logMap.put("rcv_plc",      RECV_PLC);
		    logMap.put("phub_tr_no",   phubTrNo);
		    logMap.put("req_data",     JsonUtil.MapToJson(tmpReq));
		    logMap.put("res_data",     resultVo.getRstBody().toString());
		    logMap.put("rply_cd",      resultVo.getRstCd());
		    logMap.put("rply_msg",     resultVo.getRstMsg());
		    logMap.put("rgst_user_id", "GiftiShowHttpService");
		    
		    // insert log
		    logService.insertAdmApiIfLog(logMap);
	    } catch(Exception e) {
	    	log.error(methodName, JsonUtil.MapToJson(logMap));
	    	log.printStackTracePH(methodName, e);
	    }
	}
	
	/**
	 * 공통 헤더 정의
	 * 
	 * @return
	 */
	private HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
		headers.set(HttpHeaders.CACHE_CONTROL, "no-cache");
		return headers;
	}
	
	/**
	 * 공통 파라미터 세팅
	 * 
	 * @param params
	 * @return
	 */
	private Map<String, Object> setParams(Map<String, Object> params) {
		params.put(PARAM_MDCODE, sysPrmtManage.getSysPrmtVal(GS_MDCODE_KEY));
		return params;
	}
	
	/**
	 * <pre>문자열 인코딩</pre>
	 * 
	 * @param s
	 * @param enc
	 * @return
	 */
	private String urlEncodeString(String s, String enc) {
		String methodName = getClass().getName() +"."+ "urlEncode";
		String ret = "";
		try {
			ret = URLEncoder.encode(s, enc);
		} catch (UnsupportedEncodingException e) {
			log.error(methodName, e.getMessage());
		}
		return ret;
	}
	
	/**
	 * <pre>AES256 암호화</pre>
	 * 
	 * @param str
	 */
	private String encrytoAES256(String str) {
		String methodName = getClass().getName() +"."+ "encrytoAES256";
		String ret = "";
		try {
			String gsKey = sysPrmtManage.getSysPrmtVal("GS_AES_KEY");
			ret = CryptoUtil.encAES(StringUtil.nvl(str), gsKey);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e) {
			log.debug(methodName, "exception msg=" + StringUtil.nvl(e.getMessage()));
		}
		return ret;
	}
}
