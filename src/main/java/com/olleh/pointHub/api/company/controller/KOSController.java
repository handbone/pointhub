package com.olleh.pointHub.api.company.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.olleh.pointHub.api.company.dao.GiftiShowDao;
import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.api.company.service.KOSService;
import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;

/**
 * <pre>KOS 연동 콘트롤러</pre>
 * @Class Name : KOSController
 * @author : lys
 * @since : 2019.02.11
 * @version : 1.0
 * @see <pre> 
 * << 개정이력(Modification Information) >> 
 * 수정일              수정자      수정내용 
 * -----------  ----------- ---------------- 
 * 2019. 02. 11      lys       최초 생성
 * </pre>
 */

@RestController
public class KOSController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	GiftiShowService giftiShowService;
	
	@Autowired
	MessageManage messageManager;
	
	@Autowired
	KOSService kosService;
	
	@Autowired
	GiftiShowDao giftiShowDao;
	
	@Autowired
	AccsCtrnManage  accsManage;
	
	/**
     * <pre>KOS로부터 쿠폰 유효성 체크 요청 처리</pre>
     * @see <pre>파라미터 상세정의는 외부연동규약서 참조</pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description KOS로부터 쿠폰 유효성 체크 요청 처리
     * KOS -> 포인트허브 쿠폰 유효성 체크 -> 기프티쇼 I/F -> KOS로 결과 전송
     */
	@RequestMapping(value = "/fp/kos/cpnvalidity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public Map<String, Object> checkCouponValid(@RequestBody Map<String, Object> params, HttpServletRequest request) {
		String       methodName = "checkCouponValid";
		String stepCd = "001";
		log.info(methodName, "Init Param From KOS:: \n" + params.toString());
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String,Object>();
		Map<String, Object> orgParams = params;
		
		String kosAuth  =  StringUtil.nvl(request.getHeader("Authorization"));
		String ktCustId = StringUtil.nvl(params.get("kt_cust_id"));
		String pinNo = StringUtil.nvl(params.get("pin_no"));
		
		String phubTrNo = "";
		String pgTrNo = "";
		
		paramMap.put("ktCustId", ktCustId);
		paramMap.put("pinNo", pinNo);
		
		try {
			
			stepCd = "002";
			//인증키 검증
			// giftiShowService.getCprtCmpnId("KOS") --> PG제공사용처ID(PG_SEND_PO_ID)로 관계사ID(RLTN_CORP_ID) 구함
			if(!accsManage.isAccsAuth(giftiShowService.getCprtCmpnId("KOS"), kosAuth)){
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_102"));
			}
			
			
			stepCd = "003";
			//IP 검증
			if (!accsManage.isAccsIp(giftiShowService.getCprtCmpnId("KOS"), SystemUtils.getIpAddress(request))) {
	    		throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_148"));
	        }
			
			//포인트허브 쿠폰 유효성 체크
			stepCd = "004";
			Map<String, Object> cpnInfoMap = giftiShowService.getPhCpnInfo(paramMap, Constant.PLC_KOS);
			phubTrNo		= StringUtil.nvl(cpnInfoMap.get("phub_tr_no"));
			String cpnAmt	= StringUtil.nvl(cpnInfoMap.get("cpn_amt"));
			
			//IF 로그 쌓기 위한 resulMap 구성
			paramMap.clear();
			paramMap.put("phubTrNo", phubTrNo);
			pgTrNo = giftiShowDao.getPgTrNo(paramMap);
			if(pgTrNo.equals("")){
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_104"));
			}
			
			//기프티쇼 쿠폰 유효성 체크
			stepCd = "005";
			Map<String, Object> gsParamMap = new HashMap<String, Object>();
			gsParamMap.put("phubTrNo", phubTrNo);
			gsParamMap.put("pinNo", pinNo);
			
			Map<String, Object> gsCpnInfoMap = giftiShowService.getGsCpnInfo(gsParamMap);
			
			String cnsmPriceAmt = StringUtil.nvl(gsCpnInfoMap.get("cnsmPriceAmt"));
			//cnsmPriceAmt 쿠폰가격 기프티쇼 쿠폰 금액과 크로스체크
			if(!cnsmPriceAmt.equals(cpnAmt)){
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_104"));
			}
			resultMap.put("cpn_amt", cnsmPriceAmt);
			
			//pinStatusCd 01(발행), 03(반품), 06(재발행)
			String pinStatusCd = StringUtil.nvl(gsCpnInfoMap.get("pinStatusCd"));
			String validYn = pinStatusCd.equals("01") || pinStatusCd.equals("03") || pinStatusCd.equals("06") ? "Y" : "N";
			resultMap.put("valid_yn", validYn);
			
			resultMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			resultMap.put(Constant.RET_MSG , Constant.SUCCESS_MSG);
			
		} catch (BizException be) {
			log.error(methodName, "BizException code="+StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg="+StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step="+StringUtil.nvl(be.getStepCd()));
			
			if(be.getParmObj() != null){
				Map tmpMap = (Map)be.getParmObj();
				phubTrNo = StringUtil.nvl(tmpMap.get("phub_tr_no"));
			}
			
			resultMap.put(Constant.RET_CODE, be.getErrCd());
			resultMap.put(Constant.RET_MSG , be.getErrMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception stepCd="+StringUtil.nvl(stepCd));
			log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));
			
			resultMap.put(Constant.RET_CODE, messageManager.getMsgCd("SY_ERROR_900"));
			resultMap.put(Constant.RET_MSG , messageManager.getMsgTxt("SY_ERROR_900"));
		}finally{
			kosService.logRequestFromKOS(orgParams, resultMap, request, methodName, phubTrNo, pgTrNo);
		}
		
		log.info(methodName, "end! >>> " + JsonUtil.toJson(resultMap));
		return resultMap;
	}	
}
