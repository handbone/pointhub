package com.olleh.pointHub.api.company.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.olleh.pointHub.common.exception.BizException;

public interface GiftiShowService {
	
	/* 쿠폰 사용/발행 취소 상태 업데이트 */
	public int updateCpnInfo(Map<String, Object> params) throws Exception;

	/* 쿠폰 가족지원 종료여부 상태 업데이트 */
	public int updateFmlySupot(Map<String, Object> params) throws Exception;
	
	/* 쿠폰 가족지원 종료여부 상태 업데이트(Sbank용) */
	public int updateFmlySupotSbank(Map<String, Object> params) throws Exception;
	
	/* 쿠폰 상태 업데이트 분기 처리 */
	public Map<String, Object> chgCpnService(Map<String, Object> params, HttpServletRequest request) throws Exception;
	
	public void logRequestFromGS(Map<String, Object> params, Map<String, Object> sendMap, Map<String, Object> resultMap,
			HttpServletRequest request, String pMethod);
	
	public Map<String, Object> getPhCpnInfo(Map<String, Object> params, String srcInd) throws Exception;
	
	public Map<String, Object> getGsCpnInfo(Map<String, Object> params) throws Exception;
	
	public String getOwnerPhubTrNo(Map<String, Object> params);
	
	public Map<String, Object> useCpn(Map<String, Object> params) throws Exception, BizException;
	
	public Map<String, Object> cancelCpnUse(Map<String, Object> params) throws Exception, BizException;
	
	public Map<String, Object> cancelCpnIsue(Map<String, Object> params, HttpServletRequest request) throws Exception, BizException;
	
	public Map<String, Object> cancelCpnUpdate(Map<String, Object> params, HttpServletRequest request) throws Exception, BizException;
	
	public Map<String, Object> cancelCms(Map<String, Object> params, Map<String, Object> cmsParams, HttpServletRequest request) throws Exception, BizException;

	public String getCprtCmpnId(String pgSendPoId) throws Exception, BizException;
}
