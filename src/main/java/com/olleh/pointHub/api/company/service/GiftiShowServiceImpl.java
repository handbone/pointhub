package com.olleh.pointHub.api.company.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pointHub.api.cms.service.CmsService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.api.company.dao.GiftiShowDao;
import com.olleh.pointHub.api.paygate.dao.SbankDao;
import com.olleh.pointHub.api.paygate.service.SbankHttpService;
import com.olleh.pointHub.api.paygate.service.SbankService;
import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;

@Service
public class GiftiShowServiceImpl implements GiftiShowService{
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	GiftiShowDao giftiShowDao;
	
	@Autowired
	SbankDao sbankDao;
	
	@Autowired
	SbankService sbankService;
	
	@Autowired
	SbankHttpService sbankHttpService;
	
	@Autowired
	MessageManage messageManager;
	
	@Autowired
	LogService logService;
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	AccsCtrnManage accsCtrnManage;
	
	@Autowired
	GiftiShowRestHttpService gsRestHttpService;
	
	@Autowired
	CmsService cmsService;
	
	@Autowired
	GiftiShowService giftishowService;
	
	@Autowired
	AccsCtrnManage  accsManage;
	
	/**
     * <pre>쿠폰 정보 업데이트</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return int
     * @description 쿠폰 정보 업데이트
     */
	@Override
	public int updateCpnInfo(Map<String, Object> params) throws Exception {
		int cnt = giftiShowDao.updateCpnInfo(params);
		if(cnt > 0){
			giftiShowDao.insertCpnHist(params);
		}
		return cnt;
	}

	/**
     * <pre>가족 지원 사용여부 컬럼 업데이트</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return int
     * @description 가족 지원 사용여부 컬럼 업데이트
     */
	@Override
	public int updateFmlySupot(Map<String, Object> params) throws Exception {
		int cnt = giftiShowDao.updateFmlySupot(params);
		return cnt;
	}
	
	/**
     * <pre>가족 지원 사용여부 컬럼 업데이트(Sbank용)</pre>
     * @see <pre></pre>
     * @author cisohn
     * @since  2019.03.18
     * @param  Map 
     * @return int
     * @description 가족 지원 사용여부 컬럼 업데이트
     */
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int updateFmlySupotSbank(Map<String, Object> params) throws Exception {
		int cnt = giftiShowDao.updateFmlySupot(params);
		return cnt;
	}
	
	/**
     * <pre>포인트허브 쿠폰 번호로 가족 지원 테이블에서 요청자 포인트허브 거래번호 조회</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.03.12
     * @param  Map 
     * @return String
     * @description 포인트허브 쿠폰 번호로 가족 지원 테이블에서 요청자 포인트허브 거래번호 조회
     */
	@Override
	public String getOwnerPhubTrNo(Map<String, Object> params) {
		return giftiShowDao.getOwnerPhubTrNo(params);
	}
	
	/**
     * <pre>쿠폰 상태 변경 처리 메소드</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description tradeCode 값에 따라 분기
     */
	@Override
	public Map<String, Object> chgCpnService(Map<String, Object> params, HttpServletRequest request) throws Exception {
		String methodName = "chgCpnService";
		String stepCd = "001";
		
		log.debug(methodName, "Init Param From chgCpnStatus:: \n" + params.toString());
		
		Map<String, Object> paramMap = new HashMap<String,Object>();
		Map<String, Object> cpnInfoMap = new HashMap<String,Object>();
		Map<String, Object> resultMap = new HashMap<String,Object>();
		
		resultMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		resultMap.put(Constant.RET_MSG, Constant.SUCCESS_MSG);
		
		//파라미터 값 변수 선언
		String mdcode = StringUtil.nvl(params.get("mdcode"));
		String trId = StringUtil.nvl(params.get("tr_id"));
		String tradeType = StringUtil.nvl(params.get("trade_type"));
		String tradeCode = StringUtil.nvl(params.get("trade_code"));
		String tradeDate = StringUtil.nvl(params.get("trade_date"));
		String tradeAmt = StringUtil.nvl(params.get("trade_amt"));
		String branchCode = StringUtil.nvl(params.get("branch_code"));
		String branchName = StringUtil.nvl(params.get("branch_name"));
		String pinNo = StringUtil.nvl(params.get("pin_no"));
		String useStartDate = StringUtil.nvl(params.get("use_start_date"));
		String useEndDate = StringUtil.nvl(params.get("use_end_date"));
		
		stepCd = "002";
		//필수 파라미터 검증
		if(StringUtil.isNull(mdcode)
				|| StringUtil.isNull(trId)
				|| StringUtil.isNull(tradeType)
				|| StringUtil.isNull(tradeCode)
				|| StringUtil.isNull(tradeDate)
				|| StringUtil.isNull(pinNo)){
			throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_103"));
		}
		
		//MDCODE 검증
		stepCd = "003";
		String phMdcode = StringUtil.nvl(sysPrmtManage.getSysPrmtVal("GS_MDCODE"));
		if(!mdcode.equals(phMdcode)){
			throw new BizException(stepCd, messageManager.getMsgVOY("BZ_ERROR_156"));
		}
		
		//IP 검증 로직
		stepCd = "004";
		if (!accsManage.isAccsIp(this.getCprtCmpnId(mdcode), SystemUtils.getIpAddress(request))) {
			
    		throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_148"));
        }
		
		paramMap.put("phubTrNo", trId);
		paramMap.put("pinNo", pinNo);
		
		String pgTrNo = giftiShowDao.getPgTrNo(paramMap);
		if(pgTrNo.equals("")){
			throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_104"));
		}
		
		//쿠폰 유효성 검증(phubTrNo, pinNo 쿠폰상태 조회)
		stepCd = "005";
		cpnInfoMap = this.getPhCpnInfo(paramMap, Constant.PLC_GS);
		
		String phubCpnNo = StringUtil.nvl(cpnInfoMap.get("phub_cpn_no"));
		String cpnStatCd = StringUtil.nvl(cpnInfoMap.get("cpn_stat_cd"));
		String custCtn = StringUtil.nvl(cpnInfoMap.get("cust_ctn"));
		
		paramMap.put("phubCpnNo", phubCpnNo);
		paramMap.put("cpnStatCd", cpnStatCd);
		paramMap.put("custCtn", custCtn);
		
		/*
		 * tradeType : 01(핀상태변경), 02(유효기간변경), A01(CMS 핀상태변경)
		 * tradeCode : 02(교환), 03(반품), 04(관리폐기), 07(구매취소), A01(발행취소), A02(사용취소)
		 * */
		stepCd = "006";
		if("01".equals(tradeType)){
			/*
			 * tradeCode
			 * 02 : 쿠폰 사용 API 호출
			 * 03 : 쿠폰 사용 취소 API 호출
			 * 07 : 구매취소 API 호출
			 * 
			 * 취소 경로 구분
			 * CPN_CNCL_IND_Y : 자동취소
			 * */
			if("02".equals(tradeCode)){
				resultMap = giftishowService.useCpn(paramMap);
			}else if("03".equals(tradeCode)){
				paramMap.put("cpnCnclInd", Constant.CPN_CNCL_IND_Y);
				paramMap.put("cnclInd", Constant.CPN_CNCL_IND_Y);
				resultMap = giftishowService.cancelCpnUse(paramMap);
			}else if("07".equals(tradeCode)){
				paramMap.put("cpnCnclInd", Constant.CPN_CNCL_IND_Y);
				paramMap.put("cnclInd", Constant.CPN_CNCL_IND_Y);
				paramMap.put("pgTrNo", pgTrNo);
				// TODO
				// 거래구분 null등록으로 인한 소스 추가 - 2019.03.21. lys
				paramMap.put("tr_div", "CA");
				resultMap = giftishowService.cancelCpnIsue(paramMap, request);
			}else{
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_103"));
			}
		}else if("A01".equals(tradeType)){
			/*
			 * tradeCode
			 * A01 : 발행취소
			 * A02 : 사용취소
			 * 
			 * 취소 경로 구분
			 * CPN_CNCL_IND_A : ONM 취소
			 * */
			paramMap.put("cpnCnclInd", StringUtil.nvl(params.get("cncl_ind")));
			paramMap.put("cnclInd", StringUtil.nvl(params.get("cncl_ind")));
			paramMap.put("tr_div", "CA");
			if("A01".equals(tradeCode)){
				resultMap = giftishowService.cancelCms(paramMap, params, request);
			}else if("A02".equals(tradeCode)){
				resultMap = giftishowService.cancelCpnUse(paramMap);
			}else{
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_103"));
			}
		}else{
			throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_103"));
		}
		//IF 로그 쌓기 위한 resulMap 구성
		resultMap.put("phubTrNo", trId);
		resultMap.put("pgTrNo", pgTrNo);
		
		return resultMap;
	}
	
	/**
     * <pre>기프티쇼로부터의 쿠폰 사용 처리</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description 기프티쇼로부터의 쿠폰 사용 처리
     */
	@Override
	@Transactional(rollbackFor={Exception.class, BizException.class})
	public Map<String, Object> useCpn(Map<String, Object> params) throws Exception, BizException {
		String       methodName = "useCpn";
		String stepCd = "001";
		log.debug(methodName, "Init Param From chgCpnStatus:: \n" + params.toString());
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		resultMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		resultMap.put(Constant.RET_MSG, Constant.SUCCESS_MSG);
		
		try {
			stepCd = "002";
			paramMap.put("phubTrNo", params.get("phubTrNo"));
			paramMap.put("pinNo", params.get("pinNo"));
			paramMap.put("cpnStatCd", Constant.CPN_STAT_4000);
			paramMap.put("chngTypCd", Constant.CHNG_TYP_CD_USE);
			
			int cnt = this.updateCpnInfo(paramMap);
			if(cnt == 0){
				throw new BizException(stepCd, messageManager.getMsgVOY("BZ_ERROR_158"));
			}
			
			stepCd = "003";
			paramMap.clear();
			paramMap.put("custCtn", params.get("custCtn"));
			paramMap.put("phubCpnNo", params.get("phubCpnNo"));
			String ownerPhubTrNo = this.getOwnerPhubTrNo(paramMap);
			if(ownerPhubTrNo.equals("")){
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_104"));
			}
			
			paramMap.clear();
			paramMap.put("endYn", "Y");
			paramMap.put("custCtn", params.get("custCtn"));
			paramMap.put("phubTrNo", ownerPhubTrNo);
			this.updateFmlySupot(paramMap);		//쿠폰 상태변경시 가족지원 테이블 수정하기
		} catch (BizException be) {
			log.error(methodName, "BizException code="+StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg="+StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step="+StringUtil.nvl(be.getStepCd()));
			
			resultMap.put(Constant.RET_CODE, be.getErrCd());
			resultMap.put(Constant.RET_MSG , be.getErrMsg());
			
			throw new BizException(stepCd, be.getErrCd(), be.getErrMsg());
		} catch (Exception e){
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception stepCd="+StringUtil.nvl(stepCd));
			log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));
			
			resultMap.put(Constant.RET_CODE, Constant.FAIL_CODE);
			resultMap.put(Constant.RET_MSG , Constant.FAIL_MESSAGE);
			
			throw new BizException(stepCd, messageManager.getMsgVOY("SY_ERROR_900"));
		}
		return resultMap;
	}
	
	/**
     * <pre>기프티쇼로부터의 쿠폰 사용 취소 처리</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description 기프티쇼로부터의 쿠폰 사용 취소 처리
     */
	@Override
	@Transactional(rollbackFor={Exception.class, BizException.class})
	public Map<String, Object> cancelCpnUse(Map<String, Object> params) throws Exception, BizException {
		String methodName = "cancelCpnUse";
		String stepCd = "001";
		log.debug(methodName, "Init Param From chgCpnStatus:: \n" + params.toString());
		
		Map<String, Object> paramMap = new HashMap<String,Object>();
		Map<String, Object> resultMap = new HashMap<String,Object>();
		
		resultMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		resultMap.put(Constant.RET_MSG, Constant.SUCCESS_MSG);
		
		try {
			stepCd = "002";
			paramMap.put("phubTrNo", params.get("phubTrNo"));
			paramMap.put("pinNo", params.get("pinNo"));
			paramMap.put("cpnCnclInd", params.get("cpnCnclInd"));
			paramMap.put("cpnStatCd", Constant.CPN_STAT_2100);
			paramMap.put("chngTypCd", Constant.CHNG_TYP_CD_CANCEL);
			
			int cnt = this.updateCpnInfo(paramMap);
			
			if(cnt == 0){
				throw new BizException(stepCd, messageManager.getMsgVOY("BZ_ERROR_158"));
			}
			
			stepCd = "003";
			paramMap.clear();
			paramMap.put("custCtn", params.get("custCtn"));
			paramMap.put("phubCpnNo", params.get("phubCpnNo"));
			String ownerPhubTrNo = this.getOwnerPhubTrNo(paramMap);
			if(ownerPhubTrNo.equals("")){
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_104"));
			}
			
			paramMap.clear();
			paramMap.put("endYn", "N");
			paramMap.put("custCtn", params.get("custCtn"));
			paramMap.put("phubTrNo", ownerPhubTrNo);
			this.updateFmlySupot(paramMap);		//쿠폰 상태변경시 가족지원 테이블 수정하기
			
		} catch (BizException be) {
			log.error(methodName, "BizException code="+StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg="+StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step="+StringUtil.nvl(be.getStepCd()));
			
			resultMap.put(Constant.RET_CODE, be.getErrCd());
			resultMap.put(Constant.RET_MSG , be.getErrMsg());
			
			throw new BizException(stepCd, be.getErrCd(), be.getErrMsg());
		} catch (Exception e){
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception stepCd="+StringUtil.nvl(stepCd));
			log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));
			
			resultMap.put(Constant.RET_CODE, Constant.FAIL_CODE);
			resultMap.put(Constant.RET_MSG , Constant.FAIL_MESSAGE);
			
			throw new BizException(stepCd, messageManager.getMsgVOY("SY_ERROR_900"));
		}
		return resultMap;
	}
	
	/**
     * <pre>기프티쇼로부터의 쿠폰 구매 취소 처리</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description 기프티쇼로부터의 쿠폰 구매 취소 처리
     */
	@Override
	public Map<String, Object> cancelCpnIsue(Map<String, Object> params, HttpServletRequest request) throws Exception, BizException {
		String methodName = "cancelCpnIsue";
		String stepCd = "001";
		log.debug(methodName, "Init Param From chgCpnStatus:: \n" + params.toString());
		
		Map<String, Object> paramMap = new HashMap<String,Object>();
		Map<String, Object> resultMap = new HashMap<String,Object>();
		
		resultMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		resultMap.put(Constant.RET_MSG, Constant.SUCCESS_MSG);
		
		try {
			stepCd = "002";
			String phubTrNo = StringUtil.nvl(params.get("phubTrNo"));
			String pgTrNo = StringUtil.nvl(params.get("pgTrNo"));
			
			Map<String, Object> fpParamMap = new HashMap<String, Object>();
			fpParamMap.put("pg_tr_no", pgTrNo);
			fpParamMap.put("phub_tr_no", phubTrNo);
			fpParamMap.put("cncl_ind", StringUtil.nvl(params.get("cnclInd")));
	
			// 거래구분 null등록으로 인한 소스 추가 - 2019.03.21. lys
			fpParamMap.put("tr_div", StringUtil.nvl(params.get("tr_div")));
			
			resultMap = sbankService.requestCancelFromFp(fpParamMap, request);
			
			log.debug(methodName, "++++++++++++ => \n"+ resultMap.toString());
			
			// CLiP 연동처리 실패시 Exception 발생시켜 rollback 처리
			if(!resultMap.get("ret_code").equals("00")){
				throw new BizException(stepCd, messageManager.getMsgVOY("BZ_FATAL_160"));
			}
			
			
			// 쿠폰 정보 수정 및 변경이력 저장
			stepCd = "003";
			giftishowService.cancelCpnUpdate(params, request);
			
			
			stepCd = "004";
			//PG 결제 취소 처리를 위한 요청(/point/ClipPointCancelNoti.do)
			String uri = "/point/ClipPointCancelNoti.do";
			String domain = sysPrmtManage.getSysPrmtVal("SB_CONN_IP") + uri;
			Map<String, Object> SBParam = new HashMap<String, Object>();
			SBParam.put("pg_tr_no", pgTrNo);
			SBParam.put("phub_tr_no", phubTrNo);
			
			stepCd = "005";
			Map<String, Object> etcMap = new HashMap<String, Object>();
			etcMap.put("authorization", sbankDao.selectReqDataToPg(SBParam).get("authorization"));
			etcMap.put("pgDealNo"  , pgTrNo);
			etcMap.put("phub_tr_no", phubTrNo);
			
			ResultVo resultVo = sbankHttpService.sendHttpPostSB(domain, SBParam, etcMap);
			
			if(resultVo.getSucYn().equals("N")){
				resultMap.put(Constant.RET_CODE, resultVo.getRstCd());
				resultMap.put(Constant.RET_MSG, resultVo.getRstMsg());
				return resultMap;
			}
		} catch (BizException be) {
			log.error(methodName, "BizException code="+StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg="+StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step="+StringUtil.nvl(be.getStepCd()));
			
			resultMap.put(Constant.RET_CODE, be.getErrCd());
			resultMap.put(Constant.RET_MSG , be.getErrMsg());
			
			throw new BizException(stepCd, be.getErrCd(), be.getErrMsg());
		} catch (Exception e){
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception stepCd="+StringUtil.nvl(stepCd));
			log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));
			
			resultMap.put(Constant.RET_CODE, Constant.FAIL_CODE);
			resultMap.put(Constant.RET_MSG , Constant.FAIL_MESSAGE);
			
			throw new BizException(stepCd, messageManager.getMsgVOY("SY_ERROR_900"));
		}
		
		return resultMap;
	}
	
	
	
	/**
     * <pre>쿠폰구매취소를 위한 포인트취소&쿠폰정보 수정</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.03.26
     * @param  Map 
     * @return Map
     * @description Sbank통신 오류에 의한 DB작업 롤백 회피를 위함.
     */
	@Override
	@Transactional(rollbackFor={Exception.class, BizException.class})
	public Map<String, Object> cancelCpnUpdate(Map<String, Object> params, HttpServletRequest request)
			throws Exception, BizException {
		String methodName = "cancelCpnUpdate";
		String stepCd = "001";
		log.debug(methodName, "Init Param From cancelCpnIsue:: \n" + params.toString());
		
		Map<String, Object> paramMap = new HashMap<String,Object>();
		Map<String, Object> resultMap = new HashMap<String,Object>();
		
		resultMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		resultMap.put(Constant.RET_MSG, Constant.SUCCESS_MSG);
		
		String sRetCode = Constant.SUCCESS_CODE;
		String sRetMsg  = Constant.SUCCESS_MSG;
		
		stepCd = "002";
		String phubTrNo = StringUtil.nvl(params.get("phubTrNo"));
		String pinNo = StringUtil.nvl(params.get("pinNo"));
		String pgTrNo = StringUtil.nvl(params.get("pgTrNo"));
		String cpnCnclInd = StringUtil.nvl(params.get("cpnCnclInd"));
		
		try {
		
			paramMap.put("phubTrNo", phubTrNo);
			paramMap.put("pinNo", pinNo);
			paramMap.put("cpnCnclInd", cpnCnclInd);
			paramMap.put("cpnStatCd", Constant.CPN_STAT_9000);
			paramMap.put("chngTypCd", Constant.CHNG_TYP_CD_REFUND);
			
			int cnt = this.updateCpnInfo(paramMap);
			
			if(cnt == 0){
				throw new BizException(stepCd, messageManager.getMsgVOY("BZ_ERROR_158"));
			}
			
			paramMap.clear();
			paramMap.put("custCtn", params.get("custCtn"));
			paramMap.put("phubCpnNo", params.get("phubCpnNo"));
			String ownerPhubTrNo = this.getOwnerPhubTrNo(paramMap);
			if(ownerPhubTrNo.equals("")){
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_104"));
			}
			
			paramMap.clear();
			paramMap.put("endYn", "Y");
			paramMap.put("custCtn", params.get("custCtn"));
			paramMap.put("phubTrNo", ownerPhubTrNo);
			this.updateFmlySupot(paramMap);		//쿠폰 상태변경시 가족지원 테이블 수정하기
		
		} catch(BizException be) {
			sRetCode  = StringUtil.nvl(be.getErrCd());
			sRetMsg   = StringUtil.nvl(be.getErrMsg());
			log.error(methodName, "BizException code=" + sRetCode);
			log.error(methodName, "BizException msg =" + sRetMsg);
			log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));
			throw new BizException(stepCd, sRetCode, sRetMsg);
		} catch(Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			resultMap.put(Constant.RET_CODE, messageManager.getMsgCd("SY_ERROR_900"));
			resultMap.put(Constant.RET_MSG , StringUtil.nvl(e.getMessage()));
			throw new BizException(stepCd, messageManager.getMsgVOY("SY_ERROR_900"));
		}
		
		return resultMap;
	}

	/**
     * <pre>CMS부터의 쿠폰 구매 취소 처리</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description 기프티쇼로부터의 쿠폰 구매 취소 처리
     */
	@Override
	public Map<String, Object> cancelCms(Map<String, Object> params, Map<String, Object> cmsParams, HttpServletRequest request) throws Exception, BizException {
		String methodName = "cancelCms";
		String stepCd = "001";
		log.debug(methodName, "Init Param From chgCpnStatus:: \n" + params.toString());
		
		Map<String, Object> paramMap = new HashMap<String,Object>();
		Map<String, Object> resultMap = new HashMap<String,Object>();
		
		resultMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		resultMap.put(Constant.RET_MSG, Constant.SUCCESS_MSG);
		
		try {
			String phubTrNo = StringUtil.nvl(cmsParams.get("phub_tr_no"));
			String pgTrNo = StringUtil.nvl(cmsParams.get("pg_tr_no"));
			
			//내부 취소 처리(if_yn Y일 경우 CLiP 연동 처리)
			log.debug(stepCd, "Cms Admin Cancel Start!");
			
			stepCd = "002";
			cmsParams.put("ori_pg_tr_no"  , cmsParams.get("pg_tr_no"));
			cmsParams.put("ori_phub_tr_no", cmsParams.get("phub_tr_no"));
			cmsParams.put("user_id", StringUtil.nvl(cmsParams.get("user_id"),"admin"));
			cmsParams.put("if_yn", StringUtil.nvl(cmsParams.get("if_yn"),"Y"));
			cmsParams.put("cncl_ind", StringUtil.nvl(params.get("cnclInd")));
			
			log.debug(methodName, "cms/cancel Init:: \n" + cmsParams.toString());
			resultMap = cmsService.requestCancelFromCms(cmsParams);
			
			//CLiP 연동처리 실패시 Exception 발생시켜 rollback 처리
			if(!resultMap.get("ret_code").equals("00")){
				throw new BizException(stepCd, messageManager.getMsgVOY("BZ_FATAL_160"));
			}
			
			stepCd = "003";
			// 쿠폰 정보 수정 및 변경이력 저장
			giftishowService.cancelCpnUpdate(params, request);
			
			//if_yn 값으로 연동 취소 여부 결정
			if("Y".equals(cmsParams.get("if_yn"))){
				//PG 결제 취소 처리를 위한 요청(/point/ClipPointCancelNoti.do)
				String uri = "/point/ClipPointCancelNoti.do";
				String domain = sysPrmtManage.getSysPrmtVal("SB_CONN_IP") + uri;
				Map<String, Object> SBParam = new HashMap<String, Object>();
				SBParam.put("pg_tr_no", pgTrNo);
				SBParam.put("phub_tr_no", phubTrNo);
				
				stepCd = "004";
				Map<String, Object> etcMap = new HashMap<String, Object>();
				etcMap.put("authorization", sbankDao.selectReqDataToPg(SBParam).get("authorization"));
				etcMap.put("pgDealNo"  , pgTrNo);
				etcMap.put("phub_tr_no", phubTrNo);
				
				ResultVo resultVo = sbankHttpService.sendHttpPostSB(domain, SBParam, etcMap);
				
				if(resultVo.getSucYn().equals("N")){
					resultMap.put(Constant.RET_CODE, resultVo.getRstCd());
					resultMap.put(Constant.RET_MSG, resultVo.getRstMsg());
					return resultMap;
				}
			}
		} catch (BizException be) {
			log.error(methodName, "BizException code="+StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg="+StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step="+StringUtil.nvl(be.getStepCd()));
			
			resultMap.put(Constant.RET_CODE, be.getErrCd());
			resultMap.put(Constant.RET_MSG , be.getErrMsg());
			
			throw new BizException(stepCd, be.getErrCd(), be.getErrMsg());
		} catch (Exception e){
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception stepCd="+StringUtil.nvl(stepCd));
			log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));
			
			resultMap.put(Constant.RET_CODE, Constant.FAIL_CODE);
			resultMap.put(Constant.RET_MSG , Constant.FAIL_MESSAGE);
			
			throw new BizException(stepCd, messageManager.getMsgVOY("SY_ERROR_900"));
		}
		
		return resultMap;
	}


	/**
     * <pre>기프티쇼 인터페이스 로그</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return void
     * @description 기프티쇼 인터페이스 로그
     */
	@Override
	public void logRequestFromGS(Map<String, Object> params, Map<String, Object> sendMap, Map<String, Object> resultMap,
			HttpServletRequest request, String pMethod) {
		Map<String, Object> logMap = new HashMap<String, Object>();
		String methodName = "logRequestFromGS";
		
		try {
			logMap.put("actn_uri"		, request.getRequestURI());
			logMap.put("send_plc"		, Constant.PLC_GS);
			logMap.put("rcv_plc"		, Constant.PLC_PH);
			logMap.put("phub_tr_no"		, StringUtil.nvl(resultMap.get("phubTrNo")));
			logMap.put("pg_deal_no"		, StringUtil.nvl(resultMap.get("pgTrNo")));
			logMap.put("req_data"		, JsonUtil.MapToJson(sendMap));
			logMap.put("res_data"		, JsonUtil.MapToJson(params));
			logMap.put("rply_cd"		, StringUtil.nvl(resultMap.get(Constant.RET_CODE)));
			logMap.put("rply_msg"		, StringUtil.nvl(resultMap.get(Constant.RET_MSG)));
			logMap.put("rgst_user_id"	, pMethod);
			
			logService.insertAdmApiIfLog(logMap);
		} catch (Exception e) {
			log.error(methodName, JsonUtil.MapToJson(logMap));
			log.error(methodName, StringUtil.nvl(e.getMessage()));
		}
	}

	/**
     * <pre>쿠폰 유효성 체크(mhows)</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description 쿠폰 유효성 체크(mhows)
     */
	@Override
	public Map<String, Object> getGsCpnInfo(Map<String, Object> params) throws Exception {
		Map<String, Object> gsParamMap = new HashMap<String, Object>();
		gsParamMap.put("tr_id", StringUtil.nvl(params.get("phubTrNo")));
		gsParamMap.put("pin_no", StringUtil.nvl(params.get("pinNo")));
		
		ResultVo resultVo = gsRestHttpService.sendHttpPostGiftiShowCoupon(gsParamMap);
		Map<String, Object> resultMap = resultVo.getMap();
		
		return resultMap;
	}

	/**
     * <pre>쿠폰 유효성 체크(포인트허브)</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description 쿠폰 유효성 체크(포인트허브)
     */
	@Override
	public Map<String, Object> getPhCpnInfo(Map<String, Object> params, String srcInd) throws Exception {
		String methodName = "getPhCpnInfo";
		String stepCd = "001";
		
		log.debug(methodName, "checkPhCpn params :: \n" + params.toString());
		log.debug(methodName, "srcInd :: \n" + srcInd);
		
		Map<String, Object> cpnInfoMap = new HashMap<String, Object>();
		
		/*
		 * 호출 구분 코드 값에 따라 로직 분기
		 * KOS, GS(기프티쇼)
		 * */
		if(srcInd.equals(Constant.PLC_KOS)){
			stepCd = "002";
			cpnInfoMap = giftiShowDao.getCpnInfoKos(params);
			
			if(cpnInfoMap == null){
				throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_104"));
			}else{
				String cpnStatCd = StringUtil.nvl(cpnInfoMap.get("cpn_stat_cd"));
				if(!(cpnStatCd.equals(Constant.CPN_STAT_2000) || cpnStatCd.equals(Constant.CPN_STAT_2100))){
					throw new BizException(stepCd, messageManager.getMsgVOY("IF_INFO_104"), cpnInfoMap);
				}
			}
		}else if(srcInd.equals(Constant.PLC_GS)){
			stepCd = "003";
			cpnInfoMap = giftiShowDao.getCpnInfo(params);
			
			if(cpnInfoMap == null){
				throw new BizException(stepCd, messageManager.getMsgVOY("BZ_ERROR_154"));
			}else{
				String phubCpnNo = StringUtil.nvl(cpnInfoMap.get("phub_cpn_no"));
				String cpnStatCd = StringUtil.nvl(cpnInfoMap.get("cpn_stat_cd"));
				String custCtn = StringUtil.nvl(cpnInfoMap.get("cust_ctn"));
				
				if(StringUtil.isNull(cpnStatCd) || StringUtil.isNull(custCtn) || StringUtil.isNull(phubCpnNo)){
					throw new BizException(stepCd, messageManager.getMsgVOY("BZ_ERROR_155"));
				}
			}
		}
		return cpnInfoMap;
	}
	
	/**
     * <pre>포인트 사용처 ID 조회</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  String 
     * @return String
     * @description 포인트 사용처 ID 조회
     */
	public String getCprtCmpnId(String pgSendPoId){
		Map<String, Object> resultMap = giftiShowDao.getCprtCmpnId(pgSendPoId);
		String cprtCmpnId = StringUtil.nvl(resultMap.get("cprt_cmpn_id"));
		
		return cprtCmpnId;
	}

}
