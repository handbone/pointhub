package com.olleh.pointHub.api.company.service;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;

import com.olleh.pointHub.api.comn.service.HttpService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

@Service("SHUBHttpService")
public class SHUBHttpService extends HttpService {
	private static final String SEND_PLC = "PH";
	private static final String RECV_PLC = "SH";
	
	private static final String SHUB_REQUEST_URL = "/vas/VasManager";
	private static final String SHUB_REQUEST_2102_URL = "/MESG";
	
	private static final String RES_RTN_CD = "returnCode";			//결과코드 (1:성공,0:실패)
	private static final String RES_RTN_DC = "returnDesc";			//결과설명
	private static final String RES_ERR_CD = "errorcode";			//오류코드
	private static final String RES_ERR_DC = "errordescription";	//오류설명
	private static final String RES_SVC_LIST = "svcNoList";
//	private static final String RES_SVC_INFO = "svcNoInfo";
	private static final String RES_RT = "RT";
	private static final String RES_RT_MSG = "RT_MSG";
	
	private static final String CUST_DIV_CD = "01"; 				//고객구분(1 : 전화번호 + 고객명 + 생년월일)
	
	/** 모바일번호, 서비스번호 */
	public static final String PARAM_SVC_NO = "svcNo";
	/** 고객식별번호 (생년월일+주민번호7번째숫자) */
	public static final String PARAM_CUST_IDFY_NO = "custIdfyNo";
	/** 고객명 */
	public static final String PARAM_CUST_NM = "custNm";
	/** KOS 고객아이디 */
	public static final String PARAM_CUST_ID = "custId";
	/** 결합할인유형명 */
	public static final String PARAM_COMB_DC_TYPE_NM = "combDcTypeNm";
	/** 서비스구분코드 */
	public static final String PARAM_SVC_CONT_DIV_CD = "svcContDivCd";
	/** MMSC 트랜젝션 아이디 */
	public static final String PARAM_TR_ID = "TR_ID";
	/** 메시지 제목 */
	public static final String PARAM_SUBJECT = "SUBJECT";
	/** 인코딩된 문자열 */
	public static final String PARAM_ENC_CONTENT = "ENC_CONTENT";
	/** 수신번호 */
	public static final String PARAM_RCV_CTN = "RCV_CTN";
	
	/** SHUB 성공 기본 코드 */
	public static final String SHUB_SUCS_CD = "1";
	/** 포인트허브 성공 기본 코드 */
	public static final String PHUB_SUCS_CD = "00";
	
	@Autowired
    SysPrmtManage sysPrmtManage;
	
	@Autowired
    LogService logService;
	
	/**
	 * <pre>SHUB 무선결합 가족회선 조회 API 연동 [OIF_769: CombIndivCircuitInfoRetv]
	 * - svcNo: 모바일번호
	 * - custIdfyNo: 고객식별번호 (생년월일+주민번호7번째숫자)
	 * - custNm: 고객명</pre>
	 * 
	 * @param params
	 * @return resultVo
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ResultVo sendHttpPostRequest(Map<String, Object> params) throws BizException, Exception {
		String methodName = "sendHttpPostRequest";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal("SHUB_DOMAIN") + SHUB_REQUEST_URL;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		try {
			String username = sysPrmtManage.getSysPrmtVal("SHUB_USERNAME");
			String password = sysPrmtManage.getSysPrmtVal("SHUB_PASSWORD");
			String svcNo = StringUtil.nvl(params.get(PARAM_SVC_NO));
			String custIdfyNo = StringUtil.nvl(params.get(PARAM_CUST_IDFY_NO));
			String custNm = StringUtil.nvl(params.get(PARAM_CUST_NM));
			
			// 1. SOAP 전문 작성 - Request
			StringBuffer requestBody = new StringBuffer();
			requestBody.append("<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\"");
				requestBody.append(" xmlns:oas=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"");
				requestBody.append(" xmlns:sdp=\"http://kt.com/sdp\">");
				requestBody.append("<env:Header>");
					requestBody.append("<oas:Security>");
						requestBody.append("<oas:UsernameToken>");
							requestBody.append("<oas:Username>");
							requestBody.append(username);
							requestBody.append("</oas:Username>");
							requestBody.append("<oas:Password>");
							requestBody.append(password);
							requestBody.append("</oas:Password>");
						requestBody.append("</oas:UsernameToken>");
					requestBody.append("</oas:Security>");
				requestBody.append("</env:Header>");
				requestBody.append("<env:Body>");
					requestBody.append("<sdp:CombIndivCircuitInfoRetv>");
						requestBody.append("<sdp:svcNo>").append(svcNo).append("</sdp:svcNo>");
						requestBody.append("<sdp:custIdfyNo>");
						requestBody.append(custIdfyNo);
						requestBody.append("</sdp:custIdfyNo>");
						requestBody.append("<sdp:custNm>");
						requestBody.append(custNm);
						requestBody.append("</sdp:custNm>");
						requestBody.append("<sdp:custDivCd>");
						requestBody.append(CUST_DIV_CD);	
						requestBody.append("</sdp:custDivCd>");						
					requestBody.append("</sdp:CombIndivCircuitInfoRetv>");
				requestBody.append("</env:Body>");
			requestBody.append("</env:Envelope>");
			resultVo.setReqBody(requestBody.toString());
			log.debug(methodName, "sendUrl: "+ sendUrl);
			//log.debug(methodName, "requestBody: "+ requestBody.toString());
			
			// 2. 웹서비스 통신
			resultVo = sendHttpPostSoapXml(sendUrl, resultVo, params);
			
			// 3. SOAP XML 파싱
			Node svcNoList = null;
			Map<String, Object> responseInfo = new HashMap<String, Object>();
			MessageFactory factory = MessageFactory.newInstance();
			MimeHeaders headers = new MimeHeaders();
			headers.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML_VALUE);
			SOAPMessage message = factory.createMessage(headers, new ByteArrayInputStream(StringUtil.nvl(resultVo.getRstBody()).getBytes(StandardCharsets.UTF_8)));
			Node responseNode = message.getSOAPBody().getFirstChild(); // sdp:CombIndivCircuitInfoRetv
			// 3-1. Response 상태 파싱
			String errorCode = "";
			for (int i=0; i<responseNode.getChildNodes().getLength(); i++) {
				Node node = responseNode.getChildNodes().item(i);
				if (node.getLocalName().equals(RES_SVC_LIST)) {
					svcNoList = node;
					continue;
				}
				if (node.getLocalName().equals(RES_RTN_CD)) {
					// 성공코드가 아닐경우
					if (!node.getTextContent().equals(SHUB_SUCS_CD)) {
						resultVo.setSucYn("N");
					}
				}
				if (node.getLocalName().equals(RES_ERR_CD)) {
					errorCode = node.getTextContent();
				}
				responseInfo.put(node.getLocalName(), node.getTextContent());
			}
			// KT고객이나 결합대상이 아닐경우 - 성공으로 처리
			if (resultVo.getSucYn().equals("N") && errorCode.equals("0002")) {
				resultVo.setSucYn("Y");
			}
			// 3-2. Response 상태 세팅
			resultVo.setMap(responseInfo);
			if (resultVo.getSucYn().equals("Y")) {
				if (errorCode.equals("0002")) {
					resultVo.setRstCd(StringUtil.nvl(responseInfo.get(RES_ERR_CD)));
					resultVo.setRstMsg(StringUtil.nvl(responseInfo.get(RES_ERR_DC)));
				} else {
					resultVo.setRstCd(StringUtil.nvl(responseInfo.get(RES_RTN_CD)));
					resultVo.setRstMsg(StringUtil.nvl(responseInfo.get(RES_RTN_DC)));
				}
			} else {
				resultVo.setRstCd(StringUtil.nvl(responseInfo.get(RES_ERR_CD)));
				resultVo.setRstMsg(StringUtil.nvl(responseInfo.get(RES_ERR_DC)));
			}
			// 3-3. 결합 정보 파싱 및 세팅
			if (resultVo.getSucYn().equals("Y") && svcNoList != null) {
				List<Map> list = new ArrayList<Map>();
				for (int i=0; i<svcNoList.getChildNodes().getLength(); i++) {
					Node svcNoInfo = svcNoList.getChildNodes().item(i);
					Map<String, Object> map = new HashMap<String, Object>();
					for (int j=0; j<svcNoInfo.getChildNodes().getLength(); j++) {
						Node item = svcNoInfo.getChildNodes().item(j);
						map.put(item.getLocalName(), item.getTextContent());
					}
					list.add(map);
				}
				resultVo.setList(list);
			}
			
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get("phub_tr_no")), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>SHUB KT가입자MMS발송 API 연동 [OIF_2102: MessageMTMMSReportNoNetCharge]
	 * ※ TB 테스트 용</pre>
	 * 
	 * @return string
	 * @throws Exception
	 */
	public ResultVo sendHttpPostRequestOIF2102(Map<String, Object> params) throws Exception {
		String methodName = "sendHttpPostRequestOIF2102";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal("SHUB2102_DOMAIN") + SHUB_REQUEST_2102_URL;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		try {
			String username = sysPrmtManage.getSysPrmtVal("SHUB_USERNAME");
			String password = sysPrmtManage.getSysPrmtVal("SHUB_PASSWORD");
			String vasId = sysPrmtManage.getSysPrmtVal("SHUB_VASID");
			String callCtn = sysPrmtManage.getSysPrmtVal("SHUB_CALL_CTN");
			String callBackCtn = sysPrmtManage.getSysPrmtVal("SHUB_CALLBACK_CTN");
			String trId = StringUtil.nvl(params.get(PARAM_TR_ID));
			String subject = StringUtil.nvl(params.get(PARAM_SUBJECT));
			String encContent = StringUtil.nvl(params.get(PARAM_ENC_CONTENT));
			String rcvCtn = StringUtil.nvl(params.get(PARAM_RCV_CTN));
			
			// 개행문자 치환
			encContent = encContent.replaceAll("<br>", "\n").replaceAll("<br />", "\n");
			
			// 1. SOAP 전문 작성 - Request
			StringBuilder requestBody = new StringBuilder();
			requestBody.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			requestBody.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"");
				requestBody.append(" xmlns:oas=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"");
				requestBody.append(" xmlns:sdp=\"http://kt.com/sdp\">");
				requestBody.append("<soapenv:Header>");
					requestBody.append("<oas:Security>");
						requestBody.append("<oas:UsernameToken>");
							requestBody.append("<oas:Username>");
							requestBody.append(username);
							requestBody.append("</oas:Username>");
							requestBody.append("<oas:Password>");
							requestBody.append(password);
							requestBody.append("</oas:Password>");
						requestBody.append("</oas:UsernameToken>");
					requestBody.append("</oas:Security>");
				requestBody.append("</soapenv:Header>");
				requestBody.append("<soapenv:Body>");
					requestBody.append("<sdp:MessageMTMMSReportNoNetCharge>");
						requestBody.append("<TRID>");
						requestBody.append(trId);
						requestBody.append("</TRID>");
						requestBody.append("<VASID>");
						requestBody.append(vasId);
						requestBody.append("</VASID>");
						requestBody.append("<CALLBACK_CTN>");
						requestBody.append(callBackCtn);
						requestBody.append("</CALLBACK_CTN>");
						requestBody.append("<PUSH>Push</PUSH>");
						requestBody.append("<ISCONVERTED>false</ISCONVERTED>");
						requestBody.append("<SUBJECT>");
						requestBody.append(subject);
						requestBody.append("</SUBJECT>");
						requestBody.append("<ENC_CONTENT>");
							requestBody.append("<![CDATA[");
							requestBody.append("Content-Type: text/plain; charset=\"euc-kr\" ").append("\n\n");
							requestBody.append(encContent);
//							requestBody.append("\n\n");
							requestBody.append("]]>");
						requestBody.append("</ENC_CONTENT>");
						requestBody.append("<PFORM_TYPE>1</PFORM_TYPE>");
						requestBody.append("<SERVICE_TYPE>A</SERVICE_TYPE>");
						requestBody.append("<CALL_CTN>");
						requestBody.append(callCtn);
						requestBody.append("</CALL_CTN>");
						requestBody.append("<RCV_CTN>");
						requestBody.append(rcvCtn);
						requestBody.append("</RCV_CTN>");
						requestBody.append("<READNOTI>false</READNOTI>");
						requestBody.append("<RECEIVENOTI>false</RECEIVENOTI>");
					requestBody.append("</sdp:MessageMTMMSReportNoNetCharge>");
				requestBody.append("</soapenv:Body>");
			requestBody.append("</soapenv:Envelope>");
			resultVo.setReqBody(requestBody.toString());
			//log.debug(methodName, "requestBody: "+ requestBody.toString());
			// 2. 웹서비스 통신
			resultVo = sendHttpPostSoapXml(sendUrl, resultVo, params);

			// 3. SOAP XML 파싱
			Map<String, Object> responseInfo = new HashMap<String, Object>();
			MessageFactory factory = MessageFactory.newInstance();
			MimeHeaders headers = new MimeHeaders();
			headers.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML_VALUE);
			SOAPMessage message = factory.createMessage(headers, new ByteArrayInputStream(StringUtil.nvl(resultVo.getRstBody()).getBytes(StandardCharsets.UTF_8)));
			Node responseNode = message.getSOAPBody().getFirstChild();
			// 3-1. Response 상태 파싱
			for (int i=0; i<responseNode.getChildNodes().getLength(); i++) {
				Node node = responseNode.getChildNodes().item(i);
				if (node.getLocalName().equals(RES_RT)) {
					// 성공코드가 아닐경우
					if (!node.getTextContent().equals(SHUB_SUCS_CD)) {
						resultVo.setSucYn("N");
					}
				}
				responseInfo.put(node.getLocalName(), node.getTextContent());
			}
			// 3-2. Response 상태 세팅
			resultVo.setMap(responseInfo);
			resultVo.setRstCd((resultVo.getSucYn().equals("Y")) 
					? StringUtil.nvl(responseInfo.get(RES_RT))
					: StringUtil.nvl(responseInfo.get("ERROR_CODE"))
			);
			resultVo.setRstMsg((resultVo.getSucYn().equals("Y")) 
					? StringUtil.nvl(responseInfo.get(RES_RT_MSG))
					: StringUtil.nvl(responseInfo.get("ERROR_DESCRIPTION"))
			);
			
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
			log.debug(methodName, "responseBody: "+ StringUtil.nvl(resultVo.getRstBody()));
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get("phub_tr_no")), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>기프티쇼 전문 송/수신 Log기록</pre>
	 * 
	 * @param sendUrl
	 * @param phubTrNo
	 * @param rgstUserId
	 * @param resultVo
	 */
	private void insertAdmApiIfLog(String sendUrl, String phubTrNo, String rgstUserId, ResultVo resultVo) {
		String methodName = getClass().getName() +"."+ "insertAdmApiIfLog";
		
		// declare
	    Map<String, Object> logMap = new HashMap<String, Object>();
	    
	    try {
	    	// 연동이 성공일 경우 포인트허브 성공 공통코드로 세팅
	    	if (resultVo.getSucYn().equals("Y")) {
	    		resultVo.setRstCd(PHUB_SUCS_CD);
	    	}
		    logMap.put("actn_uri",     sendUrl);
		    logMap.put("send_plc",     SEND_PLC);
		    logMap.put("rcv_plc",      RECV_PLC);
		    logMap.put("phub_tr_no",   phubTrNo);
		    logMap.put("req_data",     resultVo.getReqBody().toString());
		    logMap.put("res_data",     StringUtil.nvl(resultVo.getRstBody()).toString());
		    logMap.put("rply_cd",      resultVo.getRstCd());
		    logMap.put("rply_msg",     resultVo.getRstMsg());
		    logMap.put("rgst_user_id", "SHUBHttpService");
		    
		    // insert log
		    logService.insertAdmApiIfLog(logMap);
	    } catch(Exception e) {
	    	log.error(methodName, JsonUtil.MapToJson(logMap));
	    	log.printStackTracePH(methodName, e);
	    }
	}
	
	/**
	 * Node 정보를 문자열로 반환
	 * 
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getNodeString(Node node) {
		String methodName = getClass().getName() +"."+ "getNodeString";
		StringWriter sw = new StringWriter();
		try {
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(node), new StreamResult(sw));
		} catch (Exception e) {
			log.error(methodName, e.getMessage());
		}
		return sw.toString();
	}
}
