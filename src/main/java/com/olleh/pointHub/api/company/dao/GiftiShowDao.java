package com.olleh.pointHub.api.company.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pointHub.common.dao.AbstractDAO;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.StringUtil;

@Repository
public class GiftiShowDao extends AbstractDAO{
	private Logger log = new Logger(this.getClass());
	
	public Map<String, Object> getCpnInfo(Map<String, Object> params){
		return selectOne("mybatis.family.getCpnInfo", params);
	}

	public int updateCpnInfo(Map<String, Object> params){
		return update("mybatis.family.updateCpnInfo", params);
	}
	
	public int insertCpnHist(Map<String, Object> params){
		return insert("mybatis.family.insertCpnHist", params);
	}
	
	public int updateFmlySupot(Map<String, Object> params){
		return update("mybatis.family.updateFmlySupot", params);
	}
	
	public String getPgTrNo(Map<String, Object> params){
		Map<String, Object> map = selectOne("mybatis.family.getPgTrNo", params);
		String pgTrNo = "";
		if(map!=null){
			pgTrNo = StringUtil.nvl(map.get("pg_deal_no")); 
		}
		return pgTrNo;
	}
	
	public Map<String, Object> getCpnInfoKos(Map<String, Object> params){
		return selectOne("mybatis.family.getCpnInfoKos", params);
	}
	
	public Map<String, Object> getCprtCmpnId(String pgSendPoId){
		return selectOne("mybatis.api.getCprtCmpnId", pgSendPoId);
	}
	
	
	public String getOwnerPhubTrNo(Map<String, Object> params){
		Map<String, Object> map = selectOne("mybatis.family.getOwnerPhubTrNo", params);
		String ownerPhubTrNo = "";
		if(map != null){
			ownerPhubTrNo = StringUtil.nvl(map.get("phub_tr_no"));
		}
		return ownerPhubTrNo;
	}
}
