package com.olleh.pointHub.api.company.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.olleh.pointHub.api.comn.service.HttpService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.exception.IfException;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.CryptoUtil;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 기프티쇼 외부 Http(s) 연동 클래스
 * 
 * @author CREDIF
 */
@Service("GiftiShowRestHttpService")
public class GiftiShowRestHttpService extends HttpService {
	private static final String SEND_PLC = "PH";
	private static final String RECV_PLC = "GS";
	
	private static final String GS_COUPON_URL = "/coupon";
	private static final String GS_COUPON_SEND_URL = "/coupon/send";
	private static final String GS_COUPON_RESEND_URL = "/coupon/resend";
	private static final String GS_COUPON_CANCEL_URL = "/coupon/cancel";
	
	private static final String HEADER_API_CODE = "api_code";						//요청할API번호
	private static final String HEADER_CUSTOM_AUTH_CODE = "custom_auth_code";		//매체코드
	private static final String HEADER_CUSTOM_AUTH_TOKEN = "custom_auth_token";		//암호화된매체코드값
	private static final String HEADER_CUSTOM_ENC_FLAG = "custom_enc_flag";			//파라미터/응답 정보의 암호화 여부
	
	private static final String PARAM_OPERATION_TYPE = "operation_type";
	private static final String PARAM_GUBUN = "gubun";
	
	private static final String RES_CODE = "resCode";	//결과코드
	private static final String RES_MSG = "resMsg";		//코드메시지
	private static final String RES_PIN_NO = "pinNo";	//핀번호
//	private static final String RES_TR_ID = "trId";		//거래아이디
//	private static final String RES_CTR_ID = "ctrId";	//구매번호
	private static final String RES_COUPON_INFO_LIST = "couponInfoList";
	
	public static final String RES_PIN_STAT_CD = "pinStatusCd";
	public static final String RES_PIN_STAT_NM = "pinStatusNm";
	
	/** 전송메시지 */
	public static final String PARAM_MSG = "msg";
	/** 전송메시지 */
	public static final String PARAM_TITLE = "title";
	/** 발신번호 */
	public static final String PARAM_SENDER = "sender";
	/** 수신번호 */
	public static final String PARAM_RECEIVER = "receiver";
	/** 상품아이디 */
	public static final String PARAM_GOODS_ID = "goods_id";
	/** 거래아이디 */
	public static final String PARAM_TR_ID = "tr_id";
	/** 핀번호 */
	public static final String PARAM_PIN_NO = "pin_no";

	/** 기프티쇼 성공 기본 코드 */
	public static final String GS_SUCS_CD = "0000";
	public static final String GS_SUCS_COUPON_CD = "COUPONS.2000";
	
	/** 포인트허브 성공 기본 코드 */
	public static final String PHUB_SUCS_CD = "00";
	
	/** 기프티쇼 개행문자 */
	public static final String CR_LF = "\r\n";
	
	
	@Autowired
    SysPrmtManage sysPrmtManage;
	
	@Autowired
    LogService logService;
	
	/**
	 * <pre>단일 쿠폰 상세 정보 (0451) GET
	 * - tr_id : 거래아이디 ※둘중 하나는 필수
	 * - pin_no : 핀번호    ※둘중 하나는 필수</pre>
	 * 
	 * @param params
	 * @return resultVo
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResultVo sendHttpPostGiftiShowCoupon(Map<String, Object> params) throws BizException, Exception {
		String methodName = getClass().getName() +"."+ "sendHttpPostGiftiShowCoupon";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal("GS_DOMAIN") + GS_COUPON_URL;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		try {
			// 0. 파라미터 세팅
			resultVo.setReqBody(params);
			
			// 1. 헤더 정의
			HttpHeaders headers = getHeaders();
			headers.add(HEADER_API_CODE, "0451");
			
			// 2. 통신
			resultVo = sendHttpGet(sendUrl, headers, resultVo, params);
			
			// 3. 결과 세팅
			Map response = JsonUtil.JsonToMap(StringUtil.nvl(resultVo.getRstBody()));
			String resCode = StringUtil.nvl(response.get(RES_CODE));
			resultVo.setRstCd(resCode);
			resultVo.setRstMsg(StringUtil.nvl(response.get(RES_MSG)));
			if (!resCode.equals(GS_SUCS_CD)) {
				// 실패 시
				resultVo.setSucYn("N");
			} else {
				// 성공 시 쿠폰 정보 세팅
				List<Map> list = (List<Map>)response.get(RES_COUPON_INFO_LIST);
				resultVo.setMap(list.get(0));
				resultVo.setList(list);
			}
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get(PARAM_TR_ID)), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>쿠폰 발송 요청 (0455) POST
	 * - msg: 전송메시지
	 * - title: 제목
	 * - sender: 발신번호
	 * - receiver: 수신번호
	 * - goods_id: 상품아이디
	 * - tr_id: 거래아이디</pre>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ResultVo sendHttpPostGiftiShowCouponSend(Map<String, Object> inParams) throws BizException, Exception {
		String methodName = "sendHttpPostGiftiShowCouponSend";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal("GS_DOMAIN") + GS_COUPON_SEND_URL;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		Map<String, Object> params = inParams;
		try {
			// 0. 파라미터 세팅
			params.put(PARAM_OPERATION_TYPE, "REAL");
			params.put(PARAM_GUBUN, "Y");
			
			// encoder
			params = this.uRLEncoderData(params);
			
			// set
			resultVo.setReqBody(params);
			
			// 1. 헤더 정의
			HttpHeaders headers = getHeaders();
			headers.setContentType(new MediaType(MediaType.MULTIPART_FORM_DATA, StandardCharsets.UTF_8));
			headers.add(HEADER_API_CODE, "0455");
			
			// 2. 통신
			resultVo = sendHttpPost(sendUrl, headers, resultVo, params);
			
			// 3. 결과 세팅
			Map response = JsonUtil.JsonToMap(StringUtil.nvl(resultVo.getRstBody()));
			String resCode = StringUtil.nvl(response.get(RES_CODE));
			resultVo.setRstCd(resCode);
			resultVo.setRstMsg(StringUtil.nvl(response.get(RES_MSG)));
			if (!resCode.equals(GS_SUCS_COUPON_CD)) {
				// 실패 시
				resultVo.setSucYn("N");
			} else {
				// 성공 시 핀번호 & Map 세팅
				resultVo.setPinNo(response.get(RES_PIN_NO).toString());
				resultVo.setMap(response);
				log.debug(methodName, "pinNo: "+ resultVo.getPinNo());
			}
			//log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			//log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get(PARAM_TR_ID)), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>쿠폰 재전송 (0454) POST
	 * - tr_id : 거래아이디 ※둘중 하나는 필수
	 * - pin_no : 핀번호    ※둘중 하나는 필수</pre>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ResultVo sendHttpPostGiftiShowCouponReSend(Map<String, Object> params) throws BizException, Exception {
		String methodName = getClass().getName() +"."+ "sendHttpPostGiftiShowCouponReSend";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal("GS_DOMAIN") + GS_COUPON_RESEND_URL;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		
		try {
			// 0. 파라미터 세팅
			resultVo.setReqBody(params);
			
			// 1. 헤더 정의
			HttpHeaders headers = getHeaders();
			headers.setContentType(new MediaType(MediaType.MULTIPART_FORM_DATA, StandardCharsets.UTF_8));
			headers.add(HEADER_API_CODE, "0454");
			
			// 2. 통신
			resultVo = sendHttpPost(sendUrl, headers, resultVo, params);
			
			// 3. 결과 세팅
			Map response = JsonUtil.JsonToMap(StringUtil.nvl(resultVo.getRstBody()));
			String resCode = StringUtil.nvl(response.get(RES_CODE));
			resultVo.setRstCd(resCode);
			resultVo.setRstMsg(StringUtil.nvl(response.get(RES_MSG)));
			if (!resCode.equals(GS_SUCS_CD)) {
				// 실패 시
				resultVo.setSucYn("N");
			}
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get(PARAM_TR_ID)), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>쿠폰 취소 (0452) POST
	 * - tr_id : 거래아이디 ※둘중 하나는 필수
	 * - pin_no : 핀번호    ※둘중 하나는 필수</pre>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public ResultVo sendHttpPostGiftiShowCouponCancel(Map<String, Object> params) throws BizException, Exception {
		String methodName = getClass().getName() +"."+ "sendHttpPostGiftiShowCouponCancel";
		log.debug(methodName, "Start!");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal("GS_DOMAIN") + GS_COUPON_CANCEL_URL;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		try {
			// 0. 파라미터 세팅
			resultVo.setReqBody(params);
			
			// 1. 헤더 정의
			HttpHeaders headers = getHeaders();
			headers.setContentType(new MediaType(MediaType.MULTIPART_FORM_DATA, StandardCharsets.UTF_8));
			headers.add(HEADER_API_CODE, "0452");
			
			// 2. 통신
			resultVo = sendHttpPost(sendUrl, headers, resultVo, params);
			
			// 3. 결과 세팅
			JsonObject response = new JsonParser().parse(StringUtil.nvl(resultVo.getRstBody())).getAsJsonObject();
			String resCode = StringUtil.nvl(response.get(RES_CODE));
			resultVo.setRstCd(resCode);
			resultVo.setRstMsg(StringUtil.nvl(response.get(RES_MSG)));
			if (!resCode.equals(GS_SUCS_CD)) {
				// 실패 시
				resultVo.setSucYn("N");
			}
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			insertAdmApiIfLog(sendUrl, StringUtil.nvl(params.get(PARAM_TR_ID)), methodName, resultVo);
		}
		
		return resultVo;
	}
	
	/**
	 * <pre>기프티쇼 전문 송/수신 Log기록</pre>
	 * 
	 * @param sendUrl
	 * @param phubTrNo
	 * @param rgstUserId
	 * @param resultVo
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void insertAdmApiIfLog(String sendUrl, String phubTrNo, String rgstUserId, ResultVo resultVo) {
		String methodName = getClass().getName() +"."+ "insertAdmApiIfLog";
		
		// declare
	    Map<String, Object> logMap = new HashMap<String, Object>();
	    
	    try {
	    	Map tmpReq = (Map)resultVo.getReqBody();
	    	if (tmpReq == null) {
	    		tmpReq = new HashMap();
	    	}
	    	// 연동이 성공일 경우 포인트허브 성공 공통코드로 세팅
	    	if (resultVo.getSucYn().equals("Y")) {
	    		resultVo.setRstCd(PHUB_SUCS_CD);
	    	}
		    logMap.put("actn_uri",     sendUrl);
		    logMap.put("send_plc",     SEND_PLC);
		    logMap.put("rcv_plc",      RECV_PLC);
		    logMap.put("phub_tr_no",   phubTrNo);
		    logMap.put("req_data",     JsonUtil.MapToJson(tmpReq));
		    logMap.put("res_data",     StringUtil.nvl(resultVo.getRstBody()).toString());
		    logMap.put("rply_cd",      resultVo.getRstCd());
		    logMap.put("rply_msg",     resultVo.getRstMsg());
		    logMap.put("rgst_user_id", "GiftiShowRest");
		    
		    // insert log
		    logService.insertAdmApiIfLog(logMap);
	    } catch(Exception e) {
	    	log.error(methodName, JsonUtil.MapToJson(logMap));
	    	log.printStackTracePH(methodName, e);
	    }
	}
	
	/**
	 * 공통 헤더 정의
	 * 
	 * @return
	 */
	private HttpHeaders getHeaders() {
		String mdCode = sysPrmtManage.getSysPrmtVal("GS_MDCODE");
		HttpHeaders headers = new HttpHeaders();
		headers.add(HEADER_CUSTOM_AUTH_CODE, mdCode);
		headers.add(HEADER_CUSTOM_AUTH_TOKEN, encrytoAES256(mdCode));
		headers.add(HEADER_CUSTOM_ENC_FLAG, "N");
		headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
		return headers;
	}
	
	/**
	 * <pre>AES256 암호화</pre>
	 * 
	 * @param str
	 */
	private String encrytoAES256(String str) {
		String methodName = getClass().getName() +"."+ "encrytoAES256";
		String ret = "";
		try {
			String gsKey = sysPrmtManage.getSysPrmtVal("GS_AES_KEY");
			ret = CryptoUtil.encAES(StringUtil.nvl(str), gsKey);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e) {
			log.debug(methodName, "exception msg=" + StringUtil.nvl(e.getMessage()));
		}
		return ret;
	}

	/**
	 * <pre>AES256 복호화</pre>
	 * 
	 * @param str
	 * @return
	 */
	@SuppressWarnings("unused")
	private String descryptoAES256(String str) {
		String methodName = getClass().getName() +"."+ "descryptoAES256";
		String ret = "";
		try {
			String gsKey = sysPrmtManage.getSysPrmtVal("GS_AES_KEY");
			ret = CryptoUtil.decAES(StringUtil.nvl(str), gsKey);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e) {
			log.debug(methodName, "exception msg=" + StringUtil.nvl(e.getMessage()));
		}
		return ret;
	}
	
	/**
	 * <pre> 기프티쇼 URLEncoder 처리 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @throws UnsupportedEncodingException 
	 * @see <pre>
	 *      1. 기프티쇼 연동시 저의된 항목의 데이터를 URLEncoder 처리 한다.
	 *      2. URLEncoder 항목은 항목은 MMS제목, MMS메시지 항목밖에 없다.
	 *      </pre>
	 */		
	private Map<String, Object> uRLEncoderData(Map<String, Object> params) throws IfException, UnsupportedEncodingException {
		// declare
		String stepCd = "001";
		Map<String, Object> rtnMap = params;
		// MMS제목, 메시지 URLEncoder
		stepCd = "002";
		String title = StringUtil.nvl(params.get("title")).replace("<br />", CR_LF);
		title        = URLEncoder.encode(title, StandardCharsets.UTF_8.toString());
		String msg = StringUtil.nvl(params.get("msg")).replace("<br />", CR_LF);
		msg        = URLEncoder.encode(msg, StandardCharsets.UTF_8.toString());
		//String msg   = URLEncoder.encode("KT 스마트폰 구매시\r\n할인가능 합니다.\r\n▷가족요청내역 조회 현황\r\n(아래 링크 클릭)\r\nhttps://tb.pointhub.clipwallet.co.kr/phub/fp/presents/PH19022600106404", "UTF-8");
		// put
		rtnMap.put("title", title);
		rtnMap.put("msg", msg);
		//rtnMap.put("receiver", "01094124543");		// test소스
		
		return rtnMap;
	}	
}
