/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */

package com.olleh.pointHub.api.cms.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.olleh.pointHub.api.cms.service.CmsService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.PgCmpnInfoManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;


/**
 * <pre> ONM 연동 콘트롤러 </pre>
 * @Class Name : CmsController
 * @author : sci
 * @since :  2018.10.12
 * @version : 1.0
 * @see
 * <pre>
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    ---------------------------
 *  2018. 10. 12.     cisohn        최초 생성
 *
 * </pre>
 */

@RestController
public class CmsController {

	private Logger log = new Logger(this.getClass());

	@Autowired
	CmsService service;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	PgCmpnInfoManage pgManage;
	
	@Autowired
	SysPrmtManage   sysPrmtManage;	
	
    /**
     * <pre> CMS프로그램 참조 </pre>
     * @author cisohn
     * @since  2018.10.12
     * @param  Map (phub_tr_no , pg_tr_no, tr_div, if_yn, user_id)
     * @return Map
     * @see <pre> CMS로부터의 결제 요청 처리
     *  : 거래구분 "tr_div": "PA"[결제], "CA"[취소일반], "MA"[망취소]
     *  : 외부연동 인터페이스 여부 "if_yn":"Y" or "N"
     *  </pre>
     */	
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/cms/cancel.do", method = RequestMethod.POST ,produces = "application/json; charset=utf-8")
	public Map<String,Object> requestPayFromCms(@RequestParam Map<String, Object> params, HttpServletRequest request) throws  Exception {
		String methodName = "requestPayFromCms";
        Map<String, Object> resultmap = new HashMap<String, Object>();
        Map<String, Object> map       = new HashMap<String,Object>();
        Map<String, Object> dealret   = new HashMap<String,Object>();
        Map<String, Object> implmap   = new HashMap<String,Object>();
        Map<String, Object> statmap   = new HashMap<String,Object>();
        Map<String, Object> exmap     = new HashMap<String,Object>();
        
        String sVoAuth = "";
        String stepCd = "001";
        
        try {

            /********************************************************************
             * 1.IP 체크
             ********************************************************************/
        	String sIp    =  SystemUtils.getIpAddress(request);
        	String sAllowedIp = sysPrmtManage.getSysPrmtVal("ONM_CONN_IP");
        	
        	log.debug("[requestPayFromCms]"+ stepCd, sIp + " / "+ sAllowedIp);
        	
        	if(!sIp.equals(sAllowedIp)){
        		throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_148"));
        	}
        	
        	log.debug(stepCd, "Cms Admin Cancel Start!");
    		log.debug(methodName, "cms/pay Init:: \n" + params.toString());

    		params.put("ori_pg_tr_no"  , params.get("pg_tr_no"));
    		params.put("ori_phub_tr_no", params.get("phub_tr_no"));
    		params.put("user_id"       , StringUtil.nvl(params.get("user_id"),"admin"));
    		params.put("if_yn"         , StringUtil.nvl(params.get("if_yn"),"Y"));
			
            /********************************************************************
             * 2.취소실행
             ********************************************************************/ 
        	implmap = service.requestCancelFromCms(params);
        	
	        resultmap.put("pay_amt", implmap.get("pay_amt"));
	        resultmap.put("points" , implmap.get("points"));
	        resultmap.put("tr_dtm" , implmap.get("tr_dtm"));
	        resultmap.put(Constant.RET_CODE, implmap.get(Constant.RET_CODE));
	        resultmap.put(Constant.RET_MSG , implmap.get(Constant.RET_MSG ));
	        
	        
	        statmap.put("phub_tr_no", implmap.get("phub_tr_no"));
	        statmap.put("err_cd"    , implmap.get(Constant.RET_CODE));
	        statmap.put("err_msg"   , implmap.get(Constant.RET_MSG));
	        statmap.put("deal_stat" , implmap.get("deal_stat"));
	        
        	log.debug(methodName, "cms/cancel.do Last:: \n" + resultmap.toString());
        	
		} catch( BizException be) {
			log.error(methodName, "BizException code=" + StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg =" + StringUtil.nvl(be.getErrMsg()));	
			log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));	
						
			
			resultmap.put("ret_code", be.getErrCd());
			resultmap.put("ret_msg" , be.getErrMsg());
			
			log.debug("cms/cancel.do BizException::", resultmap.toString());
			
			exmap = (Map<String,Object>)be.getParmObj();
			if(!Common.isNull(exmap)){
				if(!"".equals(exmap.get("phub_tr_no"))){
					statmap.put("phub_tr_no", exmap.get("phub_tr_no"));
					statmap.put("deal_dt", exmap.get("deal_dt"));
				}
			}
			
			statmap.put("err_cd"   , be.getErrCd());
			statmap.put("err_msg"  , be.getErrMsg());
			statmap.put("deal_stat", Constant.DEAL_STAT_3000);

		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			resultmap.put("ret_code", messageManage.getMsgCd("SY_ERROR_900"));
			resultmap.put("ret_msg" , messageManage.getMsgTxt("SY_ERROR_900"));		
			
        } finally {
        	
        	/* 로그남기기 */
        	service.logRequestFromCms(params, resultmap, request, methodName);
        }

        
        log.debug(methodName, "resultmap="+ JsonUtil.toJson(resultmap));
        
		return resultmap;
	}	
	
}
