package com.olleh.pointHub.api.cms.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.olleh.pointHub.common.exception.BizException;

public interface CmsService {

	/* 취소처리(포인트허브 내부 only) */
	public Map<String,Object> requestCancelFromCms(Map<String, Object> params) throws BizException, Exception;
	
	/* 신청정보 수정 */
	public int updateDealReqInfoStat(Map<String, Object> params);

	/* CMS용 로그 */
	public void logRequestFromCms(Map<String,Object> params, Map<String,Object> resmap,HttpServletRequest req , String pMethod );
}
