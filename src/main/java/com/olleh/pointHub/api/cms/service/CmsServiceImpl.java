package com.olleh.pointHub.api.cms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pointHub.api.cms.dao.CmsDao;
import com.olleh.pointHub.api.comn.model.ClipPointIfVo;
import com.olleh.pointHub.api.comn.service.ComnService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.api.paygate.service.SbankHttpService;
import com.olleh.pointHub.api.provider.service.ClipPointService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;


/**
 * <pre>ONM 으로부터 거래 취소처리를 위한 서비스</pre>
 * @Class Name : CmsServiceImpl
 * @author : cisohn
 * @since 2018.10.15
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자     수정내용
 * ----------  --------  -----------------
 * 2018.10.15   cisohn      최초생성
 * 2019.01.28   cisohn      포인트거래번호 생성로직 변경됨
 * </pre>
 */

@Service
public class CmsServiceImpl implements CmsService {

	private Logger log = new Logger(this.getClass());

	@Autowired
	private SbankHttpService httpservice;
	
	@Autowired
	private ClipPointService clipservice;
	
	@Resource(name="cmsDao")
	private CmsDao dao;

	@Autowired
	MessageManage	messageManage;
	
	@Autowired
	SysPrmtManage	sysPrmtManage;		
	
	@Autowired
	ComnService	comnService;
	
	@Autowired
	CmsService	cmsService;
	
	@Autowired
	LogService	logService;
	
    private String SEND_PLC = Constant.PLC_PH;
    private String RECV_PLC = Constant.PLC_PH;	
	
	/**
	 * <pre> ONM으로부터의 취소처리 </pre>
	 * 
	 * @param  Map<String,Object>
	 * @param  HttpServletRequest request
	 * @return Map<String,Object>
	 * @throws BizException, Exception 
	 */
	@SuppressWarnings("unchecked")
	@Transactional(rollbackFor={BizException.class, Exception.class})
	@Override
	public Map<String,Object> requestCancelFromCms(Map<String, Object> param) throws BizException, Exception {
		String methodName = "requestCancelFromCms";
		String stepCd = "001";
		
		Map<String,Object> params    = new HashMap<String,Object>(param);
		
		Map<String,Object> retmap    = new HashMap<String,Object>();
		Map<String,Object> orimap    = new HashMap<String,Object>();
		Map<String,Object> dealdd    = new HashMap<String,Object>();
		Map<String,Object> statparam = new HashMap<String,Object>();
		
		String sIfYn     = StringUtil.nvl(params.get("if_yn"),"N");
		String sCnclYn   = StringUtil.nvl(params.get("cncl_yn"),"A");
		String sCnclInd  = StringUtil.nvl(params.get("cncl_ind"),"A");
		String sTrDiv    = StringUtil.nvl(params.get("tr_div"),"-");
		
		
		String sPhubTrNo = "";
		String sDealDate = "";
		String sDealTime = "";
		String sServiceId= "";
		
		int iDeal  = 0;
		int iDealD = 0;
		
		// 취소건으로서 거래요청건이 생성됬는지 여부(오류메시지 저장여부 판단용)
		String isKeyChanged = "N";		
			
		String sLogCd  = messageManage.getMsgCd("SY_INFO_00");
		String sLogMsg = messageManage.getMsgTxt("SY_INFO_00");
		
		retmap.put(Constant.RET_CODE , sLogCd);
		retmap.put(Constant.RET_MSG  , sLogMsg);
		
		try {
		
			/***********************************************************  
			 * 0.거래구분(CA,MA,PA) 필수요소 체크
			 ***********************************************************/	
			if("-".equals(sTrDiv)) {
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_103"));
			}
			
			/***********************************************************  
			 * 1.Return값 셋팅
			 ***********************************************************/		
			retmap = dao.selectResultDataToPg(params);		
			
			sServiceId = StringUtil.nvl(retmap.get("service_id"),"SVC_BASE");
			
			stepCd = "002";
			
			/***********************************************************
			 * 2.취소대상 건 존재유무 체크
			 ***********************************************************/
			orimap = dao.checkCancelByPhubTrNo(params);
	
			if ("0".equals(StringUtil.nvl(orimap.get("cnt")))){
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}
	
			/***********************************************************
			 * 3-1.이미 취소되었는지 체크(취소플래그가 N 이외에 다수임)
			 ***********************************************************/	
			stepCd = "003";
			params.put("cncl_yn", "N");
			
			orimap = dao.checkCancelByPhubTrNo(params);
			
			if (!"0".equals(StringUtil.nvl(orimap.get("cnt")))){
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_156"));
			}		
			
			log.debug(methodName, "service 파람 1: \n"+ params.toString());
			
			/***********************************************************
			 * 4.거래요청 정보 등록(ph_deal_req_info, ph_deal_req_dtl)
			 *   포인트거래번호 생성로직 변경(19.01.28)
			 ***********************************************************/		
			stepCd = "004";
			String sPntRootId = comnService.getPntTrNoRoot(params);
			
			iDeal  = dao.insertDealCancelReqInfo(params);
			
			// 변경됨(19.01.28)
			params.put("pnt_root_id", sPntRootId);
			
			iDealD = dao.insertDealCancelReqDtl(params);
			
			log.debug(methodName, "service 파람 2: \n"+ params.toString());		
			
			sPhubTrNo = (String) params.get("phub_tr_no");
			
			/* 새로운 거래번호를 리턴값에 셋팅한다.(for update) */
			retmap.put("phub_tr_no", sPhubTrNo);
			statparam.put("phub_tr_no", sPhubTrNo);
			
			isKeyChanged = "Y";
			
			stepCd = "005";
			/*********************************************************** 
			 * 5.클립포인트 연동처리(포인트 가감처리)
			 ***********************************************************/
			/*---------------------------------------------------------- 
			   성공/실패 정보를 ph_deal_req_dtl 테이블에 적용한다.
			   성공건에 대한 취소처리: INSERT
			   실패건에 대한 에러코드 저장: update "sucs_yn", "err_cd", "err_msg"
			  ** Cnm에 의한 취소 중 클립포인트와 연계없이 취소처리하는 용도로 if_yn 을 사용한다.
			 -----------------------------------------------------------*/		
			List<ClipPointIfVo> cliplist = dao.selectCancelDataToClip(params);
			List<ClipPointIfVo> retList  = new ArrayList<ClipPointIfVo>();
			ResultVo resultVo = new ResultVo();
			
		  	String inDate   = new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
		  	String inTime   = new java.text.SimpleDateFormat("HHmmss").format(new java.util.Date());
			
			if(sIfYn.equalsIgnoreCase("Y")) {
			
				Map<String,Object> tMap = new HashMap<String,Object>();
				tMap.put("pgDealNo"  , params.get("pg_tr_no"));	
				tMap.put("service_id", sServiceId);
				
				resultVo = clipservice.cancelpoint(cliplist, tMap );
				
				log.debug(methodName, "$$$$$$$$$$$$$$=> \n"+ JsonUtil.toJson(resultVo));
		
				retList = (List<ClipPointIfVo>)resultVo.getRstBody();
				
			} else {
				/* CNM에 의한 연계없는 취소처리 */
				for(ClipPointIfVo vo : cliplist) {
					ClipPointIfVo setvo = new ClipPointIfVo();
					setvo.setPntTrNo(vo.getPntTrNo());
					setvo.setSucYn("Y");
					setvo.setRetCode("0000");
					setvo.setRetMsg("no sync");
					retList.add(setvo);
				}
				resultVo.setSucYn("Y");
				resultVo.setRstDate(inDate);
				resultVo.setRstTime(inTime);
			}	
					
			for(ClipPointIfVo vo : retList) {
				dao.updateCancelPntResult(vo);
			}
			
			stepCd = "006";		
			/*----------------------------------------------------------------------
			  resultVo.getSucYn() N: 부분실패(1건이라도 차감성공건 존재)
			                    , F: 전체실패(차감성공건 0건)
			                    , Y: 전체성공
			 ----------------------------------------------------------------------*/
			String resultVoSucYn = resultVo.getSucYn();
			
			/*----------------------------------------------------------------------
			  정확한 거래일자 표기 
			 ---------------------------------------------------------------------*/  	
			sDealDate = StringUtil.nvl(resultVo.getRstDate(), inDate);
			sDealTime = StringUtil.nvl(resultVo.getRstTime(), inTime);
			
			dealdd.put("phub_tr_no", sPhubTrNo);
			dealdd.put("deal_dt"   , sDealDate + sDealTime);
			retmap.put("deal_dt"   , sDealDate + sDealTime);
			
			if(!resultVoSucYn.equals("Y")) {
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_111"), retmap);
			}
			
			dao.updateDealReqInfoDealDt(dealdd);
			dao.updateDealReqDtlDealDt(dealdd);
			
			/*********************************************************** 
			 * 7.거래원장&상세 저장(ph_deal_info, ph_deal_dtl)
			 ***********************************************************/
			stepCd = "007";
			iDeal  = dao.insertDealInfo(params);
			iDealD = dao.insertDealDtl(params);
			
			/***********************************************************  
			 * 8.이전 거래정보 수정(ph_deal_info.cncl_yn='Y') [취소여부Y]
			 ***********************************************************/
			stepCd = "008";
			params.put("cncl_yn" , sCnclYn );
			params.put("cncl_ind", sCnclInd);
			
			dao.updateOriDealInfo(params);
			
			retmap.put("tr_dtm", sDealDate+sDealTime);
	
			stepCd = "009";
			/***********************************************************  
			 * 9.거래일자 저장하기(pg_deal_req_info & pg_deal_info)
			 ***********************************************************/
			if(retmap.get(Constant.RET_CODE).equals("00")){
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("phub_tr_no", params.get("phub_tr_no"));						
				map.put("user_id"   , params.get("cust_id"));
				map.put("deal_dt"   , sDealDate + sDealTime);
				map.put("err_cd"    , retmap.get(Constant.RET_CODE));
				map.put("err_msg"   , retmap.get(Constant.RET_MSG));
				map.put("deal_stat" , "2000");
				
				dao.updateDealReqInfoStat(map);
			}

		} catch (BizException be) {
			log.error(methodName, "BizException code=" + StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg =" + StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));
			sLogCd  = be.getErrCd();
			sLogMsg = be.getErrMsg();
			
			retmap.put(Constant.RET_CODE, be.getErrCd());
			retmap.put(Constant.RET_MSG , be.getErrMsg());
			
			statparam.put("err_cd"   , sLogCd);
			statparam.put("err_msg"  , sLogMsg);
			statparam.put("deal_stat", Constant.DEAL_STAT_3000);
			
		} catch (Exception e) {
			sLogCd  = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg = messageManage.getMsgTxt("SY_ERROR_900");
			retmap.put(Constant.RET_CODE, sLogCd);
			retmap.put(Constant.RET_MSG , sLogMsg);				
		} finally {
			// 오류발생시 & 취소요청건이 생성된 경우에 한하여 업데이트
			if (!retmap.get(Constant.RET_CODE).equals(Constant.SUCCESS_CODE) && "Y".equals(isKeyChanged)) {
				statparam.put(Constant.RET_CODE, sLogCd);
				statparam.put(Constant.RET_MSG , sLogMsg);
				cmsService.updateDealReqInfoStat(statparam);
			}
		}
		
		return retmap;
		
	}
	
	
	/**
     * @author cisohn
     * @since  2018.11.09
     * @param  Map 
     * @return void
     * @description 외부로부터의 요청정보 로깅
     */	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	@Override
	public void logRequestFromCms(Map<String,Object> params, Map<String,Object> resmap,HttpServletRequest req , String pMethod ) {
		Map<String, Object> logMap  = new HashMap<String,Object>();
		Map<String, Object> sendmap = new HashMap<String,Object>(resmap);
		String methodName = "logRequestFromCms";
		
		log.debug(methodName, "logRequestFromCms's params => \n"+ params.toString());		
		
		try {
			
			logMap.put("actn_uri"      , req.getRequestURI());
			logMap.put("send_plc"	   , SEND_PLC);
			logMap.put("rcv_plc"	   , RECV_PLC);
			logMap.put("phub_tr_no"	   , StringUtil.nvl(params.get("phub_tr_no")));
			logMap.put("pg_deal_no"	   , StringUtil.nvl(params.get("pg_tr_no")));
			logMap.put("req_data"	   , JsonUtil.MapToJson(params));
			logMap.put("res_data"	   , JsonUtil.MapToJson(sendmap));
			logMap.put("rply_cd"	   , StringUtil.nvl(sendmap.get(Constant.RET_CODE),"00"));
			logMap.put("rply_msg"	   , StringUtil.nvl(sendmap.get(Constant.RET_MSG), "200"));
			logMap.put("rgst_user_id"  , pMethod);
			
			logService.insertAdmApiIfLog(logMap);
		
		} catch (Exception e) {
			log.error(methodName, JsonUtil.MapToJson(logMap));
			log.error(methodName, StringUtil.nvl(e.getMessage()));
		}
	}	
	
	
	/**
	 * <pre> 거래정보 취소처리결과 저장메소드 </pre>
	 * 
	 * @param  Map<String,Object>
	 * @param  HttpServletRequest request
	 * @return String
	 */
	@Override
	public int updateDealReqInfoStat(Map<String, Object> params){
		return dao.updateDealReqInfoStat(params);
	}
	
}
