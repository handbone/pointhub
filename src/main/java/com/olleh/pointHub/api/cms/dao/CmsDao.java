package com.olleh.pointHub.api.cms.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pointHub.api.comn.model.ClipPointIfVo;
import com.olleh.pointHub.common.dao.AbstractDAO;

/**
 * <pre>ONM 연동 거래취소 지원 DAO</pre>
 * @Class Name : CmsDao
 * @author : cisohn
 * @since :  2018.10.15
 * @version : 1.0
 * @see
 * 
 * <pre>
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 10. 15.     cisohn        최초 생성
 *
 * </pre>
 * 
 */

@Repository
public class CmsDao extends AbstractDAO {

	
    /**
     * <pre> 포인트결제 취소를 위한 이전자료 유무체크</pre> 
     * @param  Map
     * @return Map
     */	
	public Map<String,Object> checkCancelByPhubTrNo(Map<String,Object> params){
		return selectOne("mybatis.cms.checkCancelByPhubTrNo",params);
	}

    /**
     * <pre> 거래정보 체크 (결제) </pre>
     * @param  Map
     * @return Map
     */
	public Map<String,Object> checkPgPayDealInfo(Map<String,Object> params){
		return selectOne("mybatis.cms.checkPgPayDealInfo", params);
	}		
	
    /**
     * <pre> 포인트 거래취소요청정보 상세 등록</pre> 
     * @param map
     * @return int
     */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int insertDealCancelReqDtl(Map<String, Object> params) {
		return insert("mybatis.cms.insertDealCancelReqDtl", params);
	}	
    /**
     * <pre> 포인트 거래취소요청정보 등록</pre> 
     * @param map
     * @return int
     */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int insertDealCancelReqInfo(Map<String, Object> params) {
		return insert("mybatis.cms.insertDealCancelReqInfo", params);
	}	
    /**
     * <pre> 포인트거래 완료 후 처리(거래상세 저장) </pre> 
     * @param map
     * @return int
     */
	public int insertDealDtl(Map<String, Object> params) {
		return insert("mybatis.cms.insertDealDtl", params);
	}	
	   /**
     * <pre> 포인트거래 완료 후 처리(거래마스타 저장)</pre>
     * @param map
     * @return int
     */
	public int insertDealInfo(Map<String, Object> params) {
		return insert("mybatis.cms.insertDealInfo", params);
	}

    /**
     * <pre> 포인트결제 취소를 위한 클립연동 데이타 말기</pre> 
     * @param  Map
     * @return List
     */	
	public List<ClipPointIfVo> selectCancelDataToClip(Map<String,Object> params){
		return selectList("mybatis.cms.selectCancelDataToClip",params);
	}	

    /**
     * <pre> 포인트 가감처리 후 처리결과 PG사에 보낼 데이타 말기</pre> 
     * @param  Map
     * @return Map
     */
	public Map<String,Object> selectResultDataToPg(Map<String,Object> params){
		return selectOne("mybatis.cms.selectResultDataToPg", params);
	}

    /**
     * <pre> 클립포인트 결제요청 결과 업데이트</pre> 
     * @param map
     * @return int
     */
	public int updateCancelPntResult(ClipPointIfVo vo) {
		return update("mybatis.cms.updateCancelPntResult", vo);
	}

	
    /**
     * <pre> 요청정보 상세 수정 </pre>
     * @param map
     * @return int
     */		
	public int updateDealReqDtlDealDt(Map<String, Object> params) {
		return update("mybatis.cms.updateDealReqDtlDealDt", params);
	}	

    /**
     * <pre> 결제취소시 일대사(pg)기준일을 위해 클립포인트 거래일시로 맞춘다.</pre>
     * @param map
     * @return int
     */
	public int updateDealReqInfoDealDt(Map<String, Object> params) {
		return update("mybatis.cms.updateDealReqInfoDealDt", params);
	}	

    /**
     * <pre> 포인트 가감 신청(TO CLIP POINT) 성공 후</pre> 
     * @param map
     * @return int
     */
	public int updateDealReqInfoStat(Map<String, Object> params) {
		return update("mybatis.cms.updateDealReqInfoStat", params);
	}

    /**
     * <pre> 취소처리 후 원장정보 수정</pre>
     * @param map	
     * @return int
     */	
	public int updateOriDealInfo(Map<String,Object> params) {
		return update("mybatis.cms.updateOriDealInfo", params);
	}	
	
}
