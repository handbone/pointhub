package com.olleh.pointHub.api.sdk.dao;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.dao.AbstractDAO;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>패밀리포인트 서비스를 위한 Dao</pre>
 * @Class Name : FamilyPointDao
 * @author : cisohn
 * @since :  2018.07.30
 * @version : 1.0
 * @see
 * 
 * <pre>
 * << 개정이력(Modification Information) >>   
 *      수정일        수정자         수정내용
 *  ------------  -----------    -------------
 *  2019.02.12.     cisohn        최초 생성
 *
 * </pre>
 * 
 */

@Repository
public class FamilyPointDao extends AbstractDAO {
	/**
     * <pre>패밀리포인트 최소거래금액가져오기</pre> 
     * @param  Map
     * @return List
     */	
	@SuppressWarnings("unchecked")
	public Map<String,Object> getCountFlmySupotByTrNo(Map<String,Object> params){
		
		return selectOne("mybatis.family.getCountFlmySupotByTrNo",params);
	}
	
	/**
     * <pre>패밀리포인트 개통자 체크</pre> 
     * @param  Map
     * @return List
     */	
	@SuppressWarnings("unchecked")
	public String checkCountOwner(Map<String,Object> params){
		Map<String,Object> map = selectOne("mybatis.family.checkCountOwner",params);
		String lsCnt = StringUtil.nvl(map.get("cnt"));
		return lsCnt;
	}
	
	/**
     * <pre>패밀리포인트 피요청자 거래전 체크</pre> 
     * @param  Map
     * @return List
     */	
	@SuppressWarnings("unchecked")
	public String checkFmlySupotMustBeZero(Map<String,Object> params){
		Map<String,Object> map = selectOne("mybatis.family.checkFmlySupotMustBeZero",params);
		String lsCnt = StringUtil.nvl(map.get("cnt"));
		return lsCnt;
	}	
	
 
	/**
     * <pre>패밀리포인트 개통자의 피요청자경로로 접근 막기</pre> 
     * @param  Map
     * @return List
     */	
	@SuppressWarnings("unchecked")
	public String checkOwnerMustBeOne(Map<String,Object> params){
		Map<String,Object> map = selectOne("mybatis.family.checkOwnerMustBeOne",params);
		String lsCnt = StringUtil.nvl(map.get("cnt"));
		return lsCnt;
	}		
	
    /**
     * <pre> 피요청자 결제시 개통자정보 조회하기</pre> 
     * @param  Map
     * @return List
     */	
	@SuppressWarnings("unchecked")
	public Map<String,Object> getOwnerInfo(Map<String,Object> params){
		return selectOne("mybatis.family.selectOwnerInfo",params);
	}
	
    /**
     * <pre> 피요청자 결제시 중복지원 방지</pre> 
     * @param  Map
     * @return List
     */	
	@SuppressWarnings("unchecked")
	public Map<String,Object> getCountFlmySupot(Map<String,Object> params){
		return selectOne("mybatis.family.getCountFlmySupot",params);
	}	
	
    /**
     * <pre> 패밀리포인트 지원현황 조회(가족리스트)</pre> 
     * @param  Map
     * @return List
     */	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getFamilyPresents(Map<String,Object> params){
		return selectList("mybatis.family.selectFamilyPresents",params);
	}

    /**
     * <pre> 패밀리포인트 지원 등록</pre>
     * @param map
     * @return int
     */
	public int insertFmlySupot(Map<String, Object> params) {
		return insert("mybatis.family.insertFmlySupot", params);
	}
	
    /**
     * <pre> 패밀리포인트 지원 갱신</pre>
     * @param map
     * @return int
     */
	public int updateFmlySupot(Map<String, Object> params) {
		return update("mybatis.family.updateFmlySupot", params);
	}	
	
    /**
     * <pre> 가족지원테이블목록 조회(PH거래번호별) </pre>
     * @param map
     * @return List
     */
	public List<Map<String,Object>> getFpFmlySupotListByPhubTrNo(Map<String,Object> params){
		return selectList("mybatis.family.getFpFmlySupotListByPhubTrNo", params);
	}
	
	
    /**
     * <pre> MMS발송이력 등록 </pre>
     * @param map
     * @return int
     */
	public int insertPhMmsSendHist(Map<String, Object> params) {
		params.put("rgstUserId", Constant.MDFY_USER_ID_ADMIN);
		params.put("mdfyUserId", Constant.MDFY_USER_ID_ADMIN);
		return insert("mybatis.family.insertPhMmsSendHist", params);
	}
	
    /**
     * <pre> MMS발송이력 상태 갱신(결과갱신) </pre>
     * @param map
     * @return int
     */
	public int updatePhMmsSendHist(Map<String, Object> params) {
		params.put("mdfyUserId", Constant.MDFY_USER_ID_ADMIN);		
		return update("mybatis.family.updatePhMmsSendHist", params);
	}
	
    /**
     * <pre> 쿠폰정보마스터 등록 </pre>
     * @param map
     * @return int
     */
	public int insertPhCpnIsueInfo(Map<String, Object> params) {
		params.put("rgstUserId", Constant.MDFY_USER_ID_ADMIN);
		params.put("mdfyUserId", Constant.MDFY_USER_ID_ADMIN);		
		return insert("mybatis.family.insertPhCpnIsueInfo", params);
	}
	
    /**
     * <pre> 쿠폰정보마스터 이력 등록 </pre>
     * @param map
     * @return int
     */
	public int insertCpnHist(Map<String, Object> params) {
		// TODO		
		// history 등록일자 기록 필요 - 2019-02-21. lys
		return insert("mybatis.family.insertCpnHist", params);
	}
	
    /**
     * <pre> 기프티쇼쿠폰정보 등록 </pre>
     * @param map
     * @return int
     */
	public int insertFpGiftiShowInfo(Map<String, Object> params) {
		params.put("rgstUserId", Constant.MDFY_USER_ID_ADMIN);
		params.put("mdfyUserId", Constant.MDFY_USER_ID_ADMIN);		
		return insert("mybatis.family.insertFpGiftiShowInfo", params);
	}
	
	public String getReqStatCd(Map<String, Object> params){
		Map<String, Object> map = selectOne("mybatis.family.getReqStatCd", params);
		String reqStatCd = StringUtil.nvl(map.get("req_stat_cd"));
		return reqStatCd;
	}
	
	public int updateReqStatCd(Map<String, Object> params){
		return update("mybatis.family.updateReqStatCd", params);
	}
	
	
    /**
     * <pre> MMS메시지양식 조회(메시지ID별) </pre>
     * @param map
     * @return map
     */
	public Map getMmsFormInfoByMsgId(Map<String,Object> params){
		return selectOne("mybatis.family.getMmsFormInfoByMsgId", params);
	}
	
	
    /**
     * <pre> 기프티쇼상품정보 조회(상품금액으로 상품 조회) </pre>
     * @param map
     * @return map
     */
	public Map getGiftiShowGoodsInfoByAmt(String cnsmPriceAmt){
		return selectOne("mybatis.family.getGiftiShowGoodsInfoByAmt", cnsmPriceAmt);
	}

    /**
     * <pre>  쿠폰리스트 조회(포인트허브 거래번호로 조회) </pre>
     * @param map
     * @return map
     */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> couponList(Map<String, Object> params) { 
		return selectList("mybatis.family.selectCouponList", params);
	}	    	
}

 














