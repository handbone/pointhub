/**
 * Copyright (c) 2018 KT, Inc.
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.sdk.service;

import java.util.Map;

import com.olleh.pointHub.common.exception.BizException;

/**
 * <pre>Standard 서비스를 위한 인터페이스 </pre>
 * @Class Name : StdService
 * @author : cisohn
 * @since  : 2019.03.06
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자     수정내용
 * ----------  --------  -----------------
 * 2019.03.06   cisohn      최초생성
 * </pre>
 */

public interface StdService {
	
	/* Clip회원체크 및 약관동의사항 저장 */
	public Map<String,Object> joinClipMember(Map<String,Object> params);
	
	/* 패밀리포인트 기능처리 */
	public Map<String,Object> familyPoint(Map<String,Object> params) throws BizException;
}
