package com.olleh.pointHub.api.sdk.controller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.olleh.pointHub.api.comn.crypto.SbankCrypto;
import com.olleh.pointHub.api.comn.service.ComnService;
import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.api.company.service.SHUBHttpService;
import com.olleh.pointHub.api.paygate.service.SbankService;
import com.olleh.pointHub.api.provider.service.ClipPointService;
import com.olleh.pointHub.api.provider.service.ClipService;
import com.olleh.pointHub.api.sdk.service.FamilyPointService;
import com.olleh.pointHub.api.sdk.service.StdService;
import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.components.UtilsComponent;
import com.olleh.pointHub.common.exception.BaseException;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.CertificationVO;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.spring.annotation.CertificationCheck;
import com.olleh.pointHub.common.spring.annotation.SessionTimeoutCheck;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.DateUtils;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;

/**
 * <pre>거래요청 접수 Controller </pre>
 * @Class Name : ClipController
 * @author : InseokSeo
 * @since : 2018.07.30
 * @version : 1.0
 * @see <pre> 
 * << 개정이력(Modification Information) >> 
 * 수정일                수정자             수정내용 
 * -----------  ----------- ---------------- 
 * 2018. 07. 30 InseokSeo  최초 생성
 * 2018. 08. 06 InseokSeo  암호모듈 적용 
 * 2018. 08. 10 InseokSeo  identification.do 수정
 * 2019. 01. 04 ci sohn    PG로부터 인입시 요청정보 등록
 * </pre>
 */

@RestController
public class StdController {
	private Logger log = new Logger(this.getClass());

	@Autowired
	ClipService clipService;

	@Autowired
	SbankService sbankService;
	
	@Autowired
	GiftiShowService gsService;
	
	@Autowired
	FamilyPointService fmlyService;
	
	@Autowired
	UtilsComponent utilsComponent;

	@Autowired
	MessageManage messageManage;
	
	@Autowired
	AccsCtrnManage accsManage;
	
	@Autowired
	SysPrmtManage sysManage;
	
	@Autowired
	SbankCrypto sbankCrypto;
	
	@Autowired
	ClipPointService clipPointService;	
	
	@Autowired
	StdService stdService;	
	
	@Autowired
	ComnService comnService;
	
	/**
	 * <pre>SDK 첫화면 로드 response</pre>
	 * 
	 * @param Map<String,Object>
	 * @param HttpServletRequest
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/pg/identification.do", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public ModelAndView identification(@RequestParam Map<String, Object> params, HttpServletRequest request)
			throws BaseException, Exception {

		String methodName = "identification";
		String stepCd     = "001";	
		String phubTrNo   = "";
		String token      = "";
		String sEncToken  = "";

		SbankDealVO 		sbankDealVO = new SbankDealVO();
		ModelAndView 		mv 			= new ModelAndView();
		Map<String, Object> map 		= new HashMap<String, Object>();
		Map<String, Object> initmap 	= new HashMap<String, Object>();
		
		/* 요청정보 카피 후 변수 채워서 ph_deal_req_info 등록 */
		Map<String, Object> reqmap = new HashMap<String, Object>(params);
		
        String sLogCd  = Constant.SUCCESS_CODE;
        String sLogMsg = Constant.SUCCESS_MSG;

        log.debug(methodName, "Init Param From PG:: \n"+ JsonUtil.MapToJson(params));
        log.debug(methodName, "User-Agent::"+ JsonUtil.toJson(request.getHeader("User-Agent")));
        
		try {
			
			/* 0.PHUB 거래번호 생성 */
			stepCd = "002";
			initmap   = clipService.getPhubTrNo();
			phubTrNo  = (String) initmap.get("phub_tr_no");
		    token     = (String) initmap.get("token");
			sEncToken = sbankCrypto.encAES(token);

			/* 1.필수파라미터 체크 */
			if (StringUtil.nvl(params.get("pg_tr_no")).equals("")
					|| StringUtil.nvl(params.get("tr_div")).equals("")
					|| StringUtil.nvl(params.get("shop_cd")).equals("")
					|| StringUtil.nvl(params.get("pg_cd")).equals("")
					){
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_116"));
			}
			
			
			stepCd = "003";
			if ("CO".equals(StringUtil.nvl(params.get("pay_method")))){
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_103"));
			}
			
			stepCd = "003-2";						
			/* 4.check registered shop : 미등록 가맹점인 경우 PH_DEAL_REQ_INFO에 등록하지 않는다. - 2019.05.20. harry */
			map = clipService.getPgCprtInfo(params);
			
			/* redirect to error_custom.jsp */
			if (!StringUtil.nvl(map.get("ret_code")).equals(messageManage.getMsgCd("SY_INFO_00"))) {
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_113"));
			} else {
				sbankDealVO.setRchrPayInd(StringUtil.nvl(map.get("rchr_pay_ind")));
				sbankDealVO.setCprtCmpnId(StringUtil.nvl(map.get("cprt_cmpn_id")));
			}
			
			
			/* 2.insert deal information */
			reqmap.put("user_id"   , StringUtil.nvl(params.get("user_id"),"admin"));
			reqmap.put("phub_tr_no", phubTrNo);
			reqmap.put("deal_stat" , Constant.DEAL_STAT_0100);
			reqmap.put("service_id", Constant.SERVICE_ID_SVC_BASE);
			
			int icnt = sbankService.insertDealReqInfoPre(reqmap);
			
			/* 3.set deal information */
			/********************************************************************
			 * pay_method : 단일/복합 [단일:"SI", 복합:"CO", 소진형 : "CU"]
			 * pay_amt    : pay_method가 "SI"(단일), "CO"(복합) 인 경우만 전달 
			 ********************************************************************/
			sbankDealVO.setpHubTrNo(phubTrNo);
			sbankDealVO.setToken(sEncToken);
			sbankDealVO.setPgTrNo(StringUtil.nvl(params.get("pg_tr_no")));
			sbankDealVO.setPgCd(StringUtil.nvl(params.get("pg_cd")));
			sbankDealVO.setUserId(StringUtil.nvl(params.get("user_id"),"admin"));
			sbankDealVO.setShopCd(StringUtil.nvl(params.get("shop_cd")));
			sbankDealVO.setShopName(StringUtil.nvl(params.get("shop_name")));
			sbankDealVO.setShopBizNo(StringUtil.nvl(params.get("shop_biz_no")));
			sbankDealVO.setGoodsName(StringUtil.nvl(params.get("goods_name")));
			sbankDealVO.setPayAmt(StringUtil.nvl(params.get("pay_amt"),"0"));
			sbankDealVO.setPayMethod(StringUtil.nvl(params.get("pay_method")));
			sbankDealVO.setTrDiv(params.get("tr_div").toString());
			sbankDealVO.setReturnUrl1(StringUtil.nvl(params.get("return_url_1")));
			sbankDealVO.setReturnUrl2(StringUtil.nvl(params.get("return_url_2")));
			sbankDealVO.setShopPntRate(StringUtil.nvl(params.get("shop_point_rate"),"0"));
			sbankDealVO.setShopPntName(StringUtil.nvl(params.get("shop_point_name")));
			
			sbankDealVO.setServiceId(Constant.SERVICE_ID_SVC_BASE);			

			// SVC_BASE에는 무의미함
			sbankDealVO.setAddParam("");
			sbankDealVO.setOwnerYn("N");

		
			/* 5.세션 등록 */
			SessionUtils.setSbankDealVO(request, sbankDealVO);
			
			log.debug(methodName, "sbankDealVO==> "+ sbankDealVO.toString());
			
			stepCd = "004";			
			Map<String, Object> sendMap = new HashMap<String, Object>();

			// for logging(adm_api_if_log)
			sendMap.put("phub_tr_no", phubTrNo);
			sendMap.put("pg_tr_no"  , params.get("pg_tr_no"));
			sendMap.put("service_id", Constant.SERVICE_ID_SVC_BASE);

			// search clause
			Map<String, Object> result = clipService.getClaUses(sendMap);
			
			// view Name Setting
			String sViewName = "std/validateUser";
			
			mv.setViewName(sViewName);
			mv.addObject("phubTrNo"    , sbankDealVO.getpHubTrNo());
			mv.addObject("goodsNm"     , sbankDealVO.getGoodsName());
			mv.addObject("ttlPayAmt"   , sbankDealVO.getPayAmt());
			mv.addObject("dealInd"     , sbankDealVO.getTrDiv());
			mv.addObject("payMethod"   , sbankDealVO.getPayMethod());
			mv.addObject("pgCmpnNm"    , sbankDealVO.getShopName());
			mv.addObject("pg_tr_no"    , sbankDealVO.getPgTrNo());
			mv.addObject("return_url_2", sbankDealVO.getReturnUrl2());
			mv.addObject("clsList"     , result.get("clsList"));
			mv.addObject("cardInfoList", result.get("cardInfoList"));
			
			mv.addObject("shopPntRate", sbankDealVO.getShopPntRate());
			mv.addObject("shopPntName", sbankDealVO.getShopPntName());
			mv.addObject("rchrPayInd" , sbankDealVO.getRchrPayInd());
			
		} catch (BizException be) {
			log.error(methodName, "[BizException]stepCd=" + be.getStepCd());
			log.error(methodName, "[BizException]errCd=" + be.getErrCd());
			log.error(methodName, "[BizException]msg=" + be.getErrMsg());
			
			mv.setView(new RedirectView("/error_custom.do"));
			
			FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
			flashMap.put("eCode", be.getErrCd());
			flashMap.put("eMsg" , be.getErrMsg());

			sLogCd  = be.getErrCd();
			sLogMsg = be.getErrMsg();
			
		} catch( Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			
			mv.setView(new RedirectView("/error.do"));
			
			FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
			flashMap.put("eCode", messageManage.getMsgCd("SY_ERROR_900"));
			flashMap.put("eMsg" , e.getMessage());

			sLogCd = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg = e.getMessage();
			
		} finally {
			params.put("req_data", JsonUtil.MapToJson(params));
			params.put("res_data", "");			
            params.put("phub_tr_no", phubTrNo);
			params.put(Constant.RET_CODE, sLogCd);
			params.put(Constant.RET_MSG , sLogMsg);
			
			clipService.logRequestFromOut(params, request, methodName);
			
			reqmap.put("err_cd" , sLogCd);
			reqmap.put("err_msg", sLogMsg);
			sbankService.updateDealReqInfoStatPre(reqmap);
			
		}
		
		// device 구분 추가 (PC/MB)
		mv.addObject("deviceInd", StringUtil.nvl(SystemUtils.getCurrentDeviceInd(request)));
		
		return mv;
	}
	
	
	/**
	 * <pre>본인인증 후 처리작업</pre>
	 * 
	 * @param  Map
	 * @param  HttpServletRequest
	 * @return String
	 */
	@SessionTimeoutCheck(required = true)
	@CertificationCheck(required = true)
	@RequestMapping(value = "/phub/std/clsAgr.do", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public String doWorkAfterValid(@RequestParam Map<String, Object> params, HttpServletRequest request) throws BaseException, Exception {

		String methodName = "doWorkAfterValid";
		String stepCd ="001";
		String sPrgr  = Constant.CPN_REQ_PRGRS_PR120;
		String sRetCode = Constant.SUCCESS_CODE;
		String sRetMsg  = Constant.SUCCESS_MSG;
		
		log.debug(methodName, "Start!");
		//log.debug(methodName, "clsAgr.do's Param:: => \n" + params.toString());
		
		Map<String, Object> retMap   = new HashMap<String, Object>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> reqmap   = new HashMap<String, Object>();

		CertificationVO voCert = SessionUtils.getCertificationVO(request);		
		SbankDealVO     voDeal = SessionUtils.getSbankDealVO(request);
		String       serviceId = voDeal.getServiceId();		
		String sCertPath = StringUtil.nvl(voDeal.getCertPath());
		
		try {
			/* 모든 서비스(_BASE,_FP) 공통 */
			retMap = stdService.joinClipMember(params);
			
			if (!Constant.SUCCESS_CODE.equals(retMap.get(Constant.RET_CODE))) {
				throw new BizException(stepCd, StringUtil.nvl(retMap.get(Constant.RET_CODE)), StringUtil.nvl(retMap.get(Constant.RET_MSG)));
			}
			
			// 회원가입 처리 성공 후
			/*
			if(Constant.SUCCESS_CODE.equals(retMap.get(Constant.RET_CODE))){
				if (Constant.SERVICE_ID_SVC_FP.equals(voDeal.getServiceId()) ) {
					retMap = stdService.familyPoint(params);
					
					if (!Constant.SUCCESS_CODE.equals(retMap.get(Constant.RET_CODE))) {
						throw new BizException(stepCd, StringUtil.nvl(retMap.get(Constant.RET_CODE)), StringUtil.nvl(retMap.get(Constant.RET_MSG)));
					}					
					
				}
			} */
			if(Constant.SUCCESS_CODE.equals(retMap.get(Constant.RET_CODE))){
				
				retMap.put("certPath", StringUtil.nvl(voDeal.getCertPath()));
				
				if (Constant.SERVICE_ID_SVC_FP.equals(voDeal.getServiceId()) ) {
					if ("PH".equals(sCertPath)){
						retMap = stdService.familyPoint(params);
						
						if (!Constant.SUCCESS_CODE.equals(retMap.get(Constant.RET_CODE))) {
							throw new BizException(stepCd, StringUtil.nvl(retMap.get(Constant.RET_CODE)), StringUtil.nvl(retMap.get(Constant.RET_MSG)));
						}
					} else if ("CL".equals(sCertPath)) {
						retMap.put("custId", voCert.getCustId());
						retMap.put("serviceId", Constant.SERVICE_ID_SVC_FP);
						retMap.put("payYn", "N");
						retMap.put("path", "CL");
						log.debug(methodName, "$$$$$$$$$$$$$$$$$$  쿠폰리스트 조회 $$$$$$$$$$$$$$$$$");
					}
					
				}
			}			
			
		} catch (BizException be)  {
			log.debug(methodName, "BizEEEEEEEEEEEEEception!!!");
			sRetCode = be.getErrCd();
			sRetMsg  = be.getErrMsg();
		} catch (Exception e) {
			log.debug(methodName, "Pure EEEEEEEEEEEEEception!!!");
			throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));
		} finally {
			
			// 포인트조회 성공시 진행상태 저장 [개통자의경우|피요청자의 경우 분기]
			Map<String,Object> fmap = new HashMap<String,Object>();
			if (Constant.SERVICE_ID_SVC_FP.equals(serviceId) && "Y".equals(voDeal.getPayYn())){
				if ("Y".equals(voDeal.getOwnerYn())){
					// 개통자의 경우 자기 거래번호
					fmap.put("phubTrNo", voDeal.getpHubTrNo());
				} else {
					// 피요청자(=가족)의 경우 개통자 거래번호 사용
					fmap.put("phubTrNo", voDeal.getOwnerTrNo());
				}
				if(sRetCode.equals(Constant.SUCCESS_CODE)) {
					// 본인인증 완료 "PR120"
					fmap.put("prgr_stat", Constant.CPN_REQ_PRGRS_PR120);
				} else {
					// 본인인증 실패시 PR900
					fmap.put("prgr_stat", Constant.CPN_REQ_PRGRS_PR900);
				}

				fmap.put("custCtn", voCert.getPhoneNo());
				gsService.updateFmlySupot(fmap);
			}
			
			reqmap.put("err_cd"   , sRetCode);
			reqmap.put("err_msg"  , sRetMsg);
			reqmap.put("phub_tr_no", voDeal.getpHubTrNo());
			
			if ("Y".equals(voDeal.getPayYn())) {
				sbankService.updateDealReqInfoStatPre(reqmap);
			}
		}
		
		retMap.put(Constant.RET_CODE, sRetCode);
		retMap.put(Constant.RET_MSG , sRetMsg );
		
		return utilsComponent.afterResMap(request, retMap, false);

	}

	
	/**
	 * <pre> 클립포인트 조회 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      1. 레퍼럴 체크
	 *      2. 세션타임아웃 체크
	 *      3. 인증체크
	 *      </pre>
	 */
    @SuppressWarnings("finally")
	@SessionTimeoutCheck(required=true)
    @CertificationCheck(required=true)
	@RequestMapping(value = "/phub/std/point.do", method = {RequestMethod.POST})
	public ModelAndView getPoint(@RequestParam Map<String, Object> params, HttpServletRequest request) throws BaseException, Exception {
		//메소드명, 로그내용
		String methodName = "getPoint";
		log.debug(methodName, "Start!");
		log.debug(methodName, "params="+JsonUtil.MapToJson(params));

		ModelAndView mv = new ModelAndView();

        String sLogCd  = Constant.SUCCESS_CODE;
        String sLogMsg = Constant.SUCCESS_MSG;

		/* ph_deal_req_info 업데이트용 파라미터 */
		Map<String, Object> reqmap = new HashMap<String, Object>();
		
		// 포인트 결제화면
		String stepCd   = "001";
		
		//포인트 조회 페이지 payMethod 값에 따라 분기
		SbankDealVO sbankDealVO = SessionUtils.getSbankDealVO(request);		
		String payMethod = sbankDealVO.getPayMethod();
		String pointPage = "";
		String serviceId = sbankDealVO.getServiceId();
		
		// 본인인증 결과 정보
		CertificationVO certVO = SessionUtils.getCertificationVO(request);
		
		/* 뒤로가기 반복에 의한 꼬임 방지 */
		boolean blMustUpdate = true;
		Map<String, Object> checkmap = new HashMap<String, Object>();
		checkmap.put("phub_tr_no", sbankDealVO.getpHubTrNo());
		checkmap.put("uri"       , request.getRequestURI() );		
		
		try {
			/* 뒤로가기 반복 후 다시실행(F5)시 이상현상 막기 */
			if (!comnService.preventRetro(checkmap)){
				blMustUpdate = false;
				throw new BizException(methodName , messageManage.getMsgVOY("IF_INFO_116"));
			}
			
			reqmap.put("deal_stat" , Constant.DEAL_STAT_0300);
			reqmap.put("user_id"   , StringUtil.nvl(sbankDealVO.getUserId(),"admin"));
			reqmap.put("phub_tr_no", sbankDealVO.getpHubTrNo());
			
			/* Return view choose */
			switch (payMethod) {
			case "SI":
				pointPage = "std/pointPage_SI";
				break;
			case "CO":
				pointPage = "std/pointPage_CO";
				break;
			case "CU":
				pointPage = "std/pointPage_CU";
				
				if(Constant.SERVICE_ID_SVC_FP.equals(serviceId)){
					pointPage = "std/pointPage_FP";
				}
				
				break;
			default:
				break;
			}
			
            mv.setViewName(pointPage);
			
			/***************************************************************
			 * 1. 유효성 체크
			 ***************************************************************/		
			// 포인트허브 거래번호
			String phubTrNo = StringUtil.nvl(params.get("phubTrNo"));
			
			
			// 1-1 : PG사 정보 조회 및 거래번호 유효성 체크
			stepCd = "002";
			if( !phubTrNo.equals(sbankDealVO.getpHubTrNo()) ) {
				// 거래번호가 유효하지 않습니다.
				log.debug(methodName, "invalid phubTrNo="+sbankDealVO.getpHubTrNo()+ "/" + phubTrNo);
				throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_149"));
			}
			
		
			/***************************************************************
			 * 2. 포인트 조회(고객 접속환경 추출하여 포인트제공처 필터)
			 ***************************************************************/
			stepCd = "003";
			
			String sUserEnv = StringUtil.nvl(SystemUtils.getCurrentDeviceInd(request),"PC");
			params.put("osInd", sUserEnv);
			
			params.put("shop_cd"   , sbankDealVO.getShopCd());
			params.put("pg_cmpn_id", sbankDealVO.getPgCd());
			
			Map<String, Object> pointMap = clipPointService.getPoint(params);
			
			if( Common.isNull(pointMap) || StringUtil.nvl(pointMap.get(Constant.SUCCESS_YN)).equals(Constant.FAIL)) {
				// 포인트조회 실패
				log.error(methodName, "["+stepCd+"] Search Point Fail");
				throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_139"));
			}

			
			/***************************************************************
			 * 3. 리턴정보 셋팅
			 ***************************************************************/
			stepCd = "004";
			Map<String, Object> rtnObj  = new HashMap<String, Object>();
				
			// 셋팅
			stepCd = "005";
			rtnObj.put("pg_tr_no",     sbankDealVO.getPgTrNo());		// PG거래번호	
			rtnObj.put("phubTrNo",     phubTrNo);						// 포인트허브거래번호
			rtnObj.put("goodsNm",      sbankDealVO.getGoodsName());		// 상품명
			rtnObj.put("ttlPayAmt",    sbankDealVO.getPayAmt());		// 총결제금액
			rtnObj.put("dealInd",      sbankDealVO.getTrDiv());			// 거래구분
			rtnObj.put("pgCmpnNm",     sbankDealVO.getShopName());		// 사용처명
			rtnObj.put("custName",     certVO.getCustName());			// 고객명
			rtnObj.put("return_url_2", sbankDealVO.getReturnUrl2());	// PG ReturnUrl2
			
			if(Constant.SERVICE_ID_SVC_FP.equals(serviceId)) {
	    		Map<String,Object> tmap = new HashMap<String,Object>();
	    		tmap.put("phub_tr_no", sbankDealVO.getpHubTrNo());
	    		tmap.put("owner_yn"  , sbankDealVO.getOwnerYn());
	    		
	    		int fpMin = fmlyService.getFpMinPerDeal(tmap);

				rtnObj.put("minPerDeal", fpMin);	//거래당 최소포인트 금액
				
				rtnObj.put("maxPerDeal",sysManage.getSysPrmtVal("FP_MAX_PER_DEAL"));	//거래당 사용가능 최대포인트 금액
				rtnObj.put("dealUnit"  ,sysManage.getSysPrmtVal("FP_PRVDR_DEAL_UNIT"));	//포인트제공처별 거래단위(20001원 이상)
				rtnObj.put("dealUnit2" ,sysManage.getSysPrmtVal("FP_PRVDR_DEAL_UNIT2"));//포인트제공처별 거래단위(1000원~2만원까지)
			} else {
				rtnObj.put("maxPerDeal",sysManage.getSysPrmtVal("MAX_PER_DEAL"));	//거래당 사용가능 최대포인트 금액
				rtnObj.put("minPerDeal",sysManage.getSysPrmtVal("MIN_PER_DEAL"));	//거래당 최소포인트 금액
			}
			
			rtnObj.put("shopPntRate", sbankDealVO.getShopPntRate());
			rtnObj.put("shopPntName", sbankDealVO.getShopPntName());
			rtnObj.put("payMethod"  , sbankDealVO.getPayMethod());
			rtnObj.put("rchrPayInd" , sbankDealVO.getRchrPayInd());

			rtnObj.put("payYn"     , sbankDealVO.getPayYn());
			rtnObj.put("ownerYn"   , sbankDealVO.getOwnerYn());
			rtnObj.put("ownerName" , sbankDealVO.getOwnerName());
			
			// 카드사 포인트 전환금액(pntAmt), shopPntRate 적용시 처리방식 정의
			rtnObj.put("CALC_STR_MODE", Constant.CALC_STR_MODE);
			
			if( (Constant.SUCCESS).equals(pointMap.get(Constant.SUCCESS_YN)) ) {
				rtnObj.put("pntList", pointMap.get("data"));	// 성공 : 카드리스트
			} else {
				rtnObj.put("pntList", "");						// 실패 : 카드리스트
			}
			//log.debug(methodName, "rtnObj===> \n" + JsonUtil.MapToJson(rtnObj));
			
			// mv 셋팅
			mv.addObject("result", rtnObj);
			
		} catch( BizException be) {
			// 포인트조회 에러
			log.error(methodName, "[BizException]stepCd="+be.getStepCd());
			log.error(methodName, "[BizException]errCd="+be.getErrCd());
			log.error(methodName, "[BizException]msg="+be.getErrMsg());

			sLogCd = be.getErrCd();
			sLogMsg = be.getErrMsg();
			
			// 에러페이지로 이동
			// mv.setView(new RedirectView("/error.do" + "?eCode="+be.getErrCd()));
			
		} catch( Exception e) {
			// 에러 페이지로 이동
			log.printStackTracePH(methodName, e);
			log.error(methodName, "[Exception]stepCd="+StringUtil.nvl(stepCd));
			log.error(methodName, "[Exception]msg="+StringUtil.nvl(e.getMessage()));
			
			sLogCd  = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg = messageManage.getMsgTxt("SY_ERROR_900");
			
			// mv.setView(new RedirectView("/error.do" + "?eCode="+messageManage.getMsgCd("SY_ERROR_900")));
		} finally {
			
			reqmap.put("err_cd"   , sLogCd);
			reqmap.put("err_msg"  , sLogMsg);
			reqmap.put("deal_stat", Constant.DEAL_STAT_0300);
			
			// 포인트조회 성공시 진행상태 저장 [개통자의경우|피요청자의 경우 분기]
			/* 거래시, 뒤로가기에 사용자액션이 아닐때에만 상태 업데이트한다. */
			Map<String,Object> fmap = new HashMap<String,Object>();
			if (Constant.SERVICE_ID_SVC_FP.equals(serviceId) && "Y".equals(sbankDealVO.getPayYn()) && blMustUpdate){
				if ("Y".equals(sbankDealVO.getOwnerYn())){
					// 개통자의 경우 자기 거래번호
					fmap.put("phubTrNo", sbankDealVO.getpHubTrNo());
				} else {
					// 피요청자(=가족)의 경우 개통자 거래번호 사용
					fmap.put("phubTrNo", sbankDealVO.getOwnerTrNo());
				}
				if(sLogCd.equals(Constant.SUCCESS_CODE)) {
					// 포인트 조회 완료 "PR130"
					fmap.put("prgr_stat", Constant.CPN_REQ_PRGRS_PR130);
				} else {
					// 포인트 조회 실패시 PR900
					fmap.put("prgr_stat", Constant.CPN_REQ_PRGRS_PR900);
				}
				
				fmap.put("custCtn", certVO.getPhoneNo());
				gsService.updateFmlySupot(fmap);
			}			
			
			/* 뒤로가기에 사용자액션이 아닐때에만 상태 업데이트한다. */
			if (blMustUpdate) {
				sbankService.updateDealReqInfoStatPre(reqmap);
			}
			
			if ( !Constant.SUCCESS_CODE.equals(sLogCd) ) {
				if( !sLogCd.startsWith("I") ) {
					mv.setView(new RedirectView("/error.do" + "?eCode="+ sLogCd ));	
				} else {
					mv.setView(new RedirectView("/error_custom.do"));
					FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
					flashMap.put("eCode", sLogCd);
					flashMap.put("eMsg" , sLogMsg );				
				}
			}
			
			log.debug(methodName, "mv size="+JsonUtil.toJson(mv).length());
		}
		
		return mv;
	}
    
    /**
     * <pre>결제요청 처리(requestPayFromSdk)</pre>
     * @see <pre>신청포인트 유효성 체크 ! 리턴 > 요청정보저장 ></pre> 
     * @param params
     * @param map
     * @param ModelAndView
     * @return
     */
	@SessionTimeoutCheck(required=true)
	@CertificationCheck(required=true)	
	@RequestMapping(value="/phub/std/pay.do", method=RequestMethod.POST ,produces = "application/json; charset=utf-8") 
	public Map<String,Object> requestPayFromSdk( @RequestParam Map<String,Object> params, ModelAndView mv, HttpServletRequest request) 
			throws Exception {
		String methodName = "requestPayFromSdk";
		String stepCd = "001";
		Map<String,Object> validMap  = new HashMap<String,Object>();
		Map<String,Object> resultMap = new HashMap<String,Object>();
		Map<String,Object> saveMap   = new HashMap<String,Object>();
		Map<String,Object> pgMap     = new HashMap<String,Object>();
		Map<String,Object> statmap   = new HashMap<String,Object>();
		
		List<Map<String,Object>> pntList = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> plist   = new ArrayList<Map<String,Object>>();
		
        String sLogCd  = Constant.SUCCESS_CODE;
        String sLogMsg = Constant.SUCCESS_MSG;
		
		/* 결제정보 Main */
		SbankDealVO     voDeal = SessionUtils.getSbankDealVO(request);
		CertificationVO voCert = SessionUtils.getCertificationVO(request);
		
		String sServiceId = voDeal.getServiceId();
		String sTrDiv = voDeal.getTrDiv();
		
		/* 뒤로가기 반복에 의한 꼬임 방지 */
		boolean blMustUpdate = true;
		Map<String, Object> checkmap = new HashMap<String, Object>();
		checkmap.put("phub_tr_no", voDeal.getpHubTrNo());
		checkmap.put("uri"       , request.getRequestURI() );
		
		try {
			
			/***************************************************************
			 * 0.뒤로가기 반복에 의한 꼬임 방지
			 ***************************************************************/		
			if (!comnService.preventRetro(checkmap)){
				blMustUpdate = false;
				throw new BizException(methodName , messageManage.getMsgVOY("IF_INFO_116"));
			}
			
			/***************************************************************
			 * 1.중복거래 (Key중복) 체크 - phub_tr_no 로 체크
			 ***************************************************************/
			statmap.put("phub_tr_no" , voDeal.getpHubTrNo());
			
			/* PG로부터 인입시 등록하므로 중복체크 로직 제거됨 */
			
			stepCd = "002";
			/***************************************************************
			 * 2.거래정보 담기 (신청정보 저장용)
			 ***************************************************************/
			params.put("phub_tr_no"   , StringUtil.nvl(voDeal.getpHubTrNo()));
			params.put("token"        , StringUtil.nvl(voDeal.getToken()));
			params.put("pg_deal_no"   , StringUtil.nvl(voDeal.getPgTrNo()));
			params.put("goods_nm"     , StringUtil.nvl(voDeal.getGoodsName()));
			params.put("pg_send_po_id", StringUtil.nvl(voDeal.getShopCd()));
			params.put("cprt_cmpn_id" , "");
			params.put("pg_cmpn_id"   , StringUtil.nvl(voDeal.getPgCd()));
			params.put("deal_ind"     , StringUtil.nvl(voDeal.getTrDiv(),"PA"));
			// pay_method : 단일/복합 [단일:"SI", 복합:"CO", 소진형 : "CU"]
			params.put("pay_method"   , StringUtil.nvl(voDeal.getPayMethod()));
			params.put("deal_stat"    , Constant.DEAL_STAT_1000);
			params.put("cust_id"      , StringUtil.nvl(voCert.getCustId()));
			params.put("cust_ci"      , StringUtil.nvl(voCert.getCustCI()));
			params.put("cust_ctn"     , StringUtil.nvl(voCert.getPhoneNo()));
			params.put("ttl_pnt"      , 0);
			params.put("ttl_pay_amt"  , Integer.parseInt(voDeal.getPayAmt()));
			params.put("ttl_pnt_amt"  , params.get("ttlPntAmt"));
			params.put("ttl_rmnd_amt" , params.get("ttlRmndAmt"));
			params.put("return_url_1" , voDeal.getReturnUrl1());
			params.put("return_url_2" , voDeal.getReturnUrl2());
			
			// 단일/복합 [단일:"SI", 복합:"CO", 소진형 : "CU"]
			String sPayMethod   = StringUtil.nvl(params.get("pay_method"),"SI");
			String sShopPntRate = StringUtil.nvl(params.get("shopPntRate"),"1");    
			
			stepCd = "003";
			/***************************************************************
			 * 3.전환율/금액 검증(수정가능)
			 *   ■ 복합결제&소진형 거래는 가맹점 수수료율 적용하여 검증
			 *   ■ 단순결제(SI)는 기존대로..
			 ***************************************************************/
			pntList = JsonUtil.JsonToList((String)params.get("pntList"));
			//log.debug(stepCd, "1109 ==> \n"+ pntList.toString());
			
			if ("SI".equals(sPayMethod)){
				if(!checkAmount(pntList).get("ret_code").equals("00")){
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_154"));
				}
			} else if ("CU".equals(sPayMethod)||"CO".equals(sPayMethod)) {
				
				if (!sShopPntRate.equals(voDeal.getShopPntRate())){
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_154"));
				}
				
				Map<String,Object> cmap = new HashMap<String,Object>();
				cmap.put("list", pntList);
				cmap.put("shop_pnt_rate", sShopPntRate);
				cmap.put("ttl_pnt_amt"  , params.get("ttlPntAmt"));
				
				String checkResult = checkAmount4CU(cmap,request);
				if(!checkResult.equals("00")){
					
					throw new BizException(stepCd, messageManage.getMsgVOY(checkResult));
				}
			}
			
			stepCd = "004";
			/***************************************************************
			 * 4.포인트 신청내역 담기(ph_deal_req_dtl)  
			 ***************************************************************/
			// 원천 상품금액
			int iPayAmt    = Integer.valueOf(StringUtil.nvl(voDeal.getPayAmt(),"0"));
						
			// 사용자가 선택한 포인트 총금액 (SUM(포인트x전환율))
			int iTtlPntAmt = Integer.valueOf(StringUtil.nvl(params.get("ttlPntAmt"),"0"));

			// 총결제금액 충당금액중 포인트 이외의 결제수단으로 지불할 금액
			int iRemainAmt = Integer.valueOf(StringUtil.nvl(params.get("ttlRmndAmt"),"0"));
	
			// 단순결제 
			int iSumPntAmt    = 0;
			// 소진 & 복합형
			int iSumCprtAmt   = 0;
			
			for(int i=0;i<pntList.size();i++) {
				Map<String,Object> map     = (Map<String,Object>) pntList.get(i);
				Map<String,Object> listmap = (Map<String,Object>) pntList.get(i);
				
				int cprtAmt = Integer.valueOf(StringUtil.nvl(map.get("cprtAmt"),"0"));
				int pntAmt  = Integer.valueOf(StringUtil.nvl(map.get("pntAmt"),"0"));
				
				listmap.put("cust_id"  , voCert.getCustId());
				listmap.put("deal_ind" , voDeal.getTrDiv());
				listmap.put("pnt_tr_no", map.get("pntTrNo"));
				listmap.put("pnt_cd"   , map.get("pntCd"));
				listmap.put("pnt"      , map.get("pnt"));
				listmap.put("pnt_amt"  , map.get("pntAmt"));
				listmap.put("avl_pnt"  , map.get("avlPnt"));
				listmap.put("deal_unit", map.get("dealUnit"));
				listmap.put("pnt_exch_rate", map.get("pntExchRate"));
				
				iSumPntAmt  += pntAmt;
				iSumCprtAmt += cprtAmt;

				plist.add(new HashMap<String, Object>(listmap));
			}
			//log.debug(methodName, "[결제금액/가맹점율적용SUM]iPayAmt/iSumCprtAmt=> "+ iPayAmt +" / "+ iSumCprtAmt);
			//log.debug(methodName, "[총포인트금액/포인트금액SUM]iTtlPntAmt/iSumPntAmt=> "+ iTtlPntAmt+ " / "+  iSumPntAmt);
									
			stepCd = "005";
			/***************************************************************
			 * 5-0.각 포인트 제공처별 최소 결제금액 확인
			 * 클라이언트에서 넘어온 실제 사용 포인트와 서버의 최소사용 가능 포인트를 비교하여
			 * 사용 포인트가 최소사용 가능 포인트보다 작으면 익셉션 발생
			 * 사용 포인트(pnt)는 클라이언트값 
			 * 최소사용 가능 포인트(minAvlPnt)는 서버값
			***************************************************************/
			Map<String,Object> pgParamMap = new HashMap<String,Object>();
			pgParamMap.put("pg_cmpn_id", voDeal.getPgCd());
			pgParamMap.put("shop_cd", voDeal.getShopCd());
			
			List<Map<String, Object>> prvdrlist = clipService.getBasicPointInfo(pgParamMap);
			
			for (Map<String, Object> pntMap : pntList) {
				
				int pnt = Integer.valueOf(StringUtil.nvl(pntMap.get("pnt"),"0"));
				
				if(pnt < 1){
					continue;
				}
			
				for(Map<String, Object> prvdrMap : prvdrlist){
					
					if(StringUtils.equals((String)pntMap.get("pntCd"), (String)prvdrMap.get("pntCd"))){
											
						int minAvlPnt = Integer.valueOf(StringUtil.nvl(prvdrMap.get("minAvlPnt"),"0"));
						
						if(pnt < minAvlPnt){
							throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_182"));
						}
					}
				}
			}
			
			
			/***************************************************************
			 * 5-1.결제유형에 상관없이 각각의 포인트 금액의 합과 SUM금액 일치 여부 확인
			 * 5-2.복합결제의 경우 잔여금액 + 포인트금액 SUM = pay_amt(총결제금액=상품금액) 비교
			***************************************************************/
			stepCd = "0051";
			// 5-1
			if (iTtlPntAmt != iSumPntAmt){
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_154"));
			}
			
			stepCd = "0052";
			// 5-2
			if (sPayMethod.equals("CO")){
				if (iPayAmt != (iSumPntAmt + iRemainAmt) ) {
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_154"));
				}
			}
			
			stepCd = "0053";
			/*********************************************************************
			 * 상품ID 검증 
			 ********************************************************************/
			if( voDeal.getServiceId().equals(Constant.SERVICE_ID_SVC_FP) 
					&& !StringUtil.nvl(params.get("ttlPntAmt")).equals("0")) {
				Map<String,Object> tmpMap = new HashMap<String,Object>(); 
				tmpMap   = fmlyService.getGiftiShowGoodsInfoByAmt(StringUtil.nvl(params.get("ttl_pnt_amt"),"0"));
				
				if (Common.isNull(tmpMap) || tmpMap.isEmpty()){
					// 요청하신 금액의 단말할인권은 구매하실 수 없습니다.
					/* 1,000원~20,000원까지는 1,000원 단위로 쿠폰 발행 ,나머지 구간은 5,000원 단위로 쿠폰 발행 */
					throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_167") );
				}
			}
			
			stepCd = "006";
			/***************************************************************
			 * 6.총결제 포인트 거래당 최소 최대값(Sum기준) 체크
			 *   [기준]포인트전환율 & 가맹점수수료율 적용금액의 합계와 비교
			 ***************************************************************/
			String sMaxPoint = sysManage.getSysPrmtVal("MAX_PER_DEAL");
			String sMinPoint = sysManage.getSysPrmtVal("MIN_PER_DEAL");
			
			
			if (Integer.valueOf(sMaxPoint) < iSumCprtAmt ) {
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_155"));
			}

			// 패밀리포인트의 경우 가족이 있는 개통자는 0 원 결제가 가능하다.
			if(voDeal.getServiceId().equals(Constant.SERVICE_ID_SVC_FP)){
				Map<String,Object> tmap = new HashMap<String,Object>();
				tmap.put("phub_tr_no", voDeal.getpHubTrNo());
				tmap.put("owner_yn"  , voDeal.getOwnerYn());
				sMinPoint = fmlyService.getFpMinPerDeal(tmap) + "";	    		
			}
			
			if (Integer.valueOf(sMinPoint) > iSumCprtAmt ) {
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_164"));
			}
			
			
			params.put("list", plist );
			
			stepCd = "007";
			/***************************************************************
			 * 7.유효성 통과 후 신청정보 저장(ph_deal_req_info & dtl)
			 ***************************************************************/
			saveMap = sbankService.saveDealReq(params);
			
			stepCd = "008";
			/***************************************************************
			 * 8.PG(SettleBank)사로 전송
			 ***************************************************************/		
			params.put(Constant.RET_CODE, (String)saveMap.get(Constant.RET_CODE));
			params.put(Constant.RET_MSG , (String)saveMap.get(Constant.RET_MSG));
					
			params.put("stDealDt" , DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[0]);
			params.put("endDealDt", DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[1]);
			
			pgMap = sbankService.sendToPgPay(params);
			
			stepCd = "009";
			/***************************************************************
			 * 9.PG전송결과 화면(Ajax)에 내려주기
			 ***************************************************************/		
			resultMap.put(Constant.RET_CODE  , (String) pgMap.get(Constant.RET_CODE));
			resultMap.put(Constant.RET_MSG   , (String) pgMap.get(Constant.RET_MSG));
			resultMap.put("return_url_2"     , (String) params.get("return_url_2"));
			
			Map<String,Object> sendmap = (Map<String, Object>) pgMap.get("sendmap");
			
			resultMap.put("pg_tr_no"    , sendmap.get("pg_tr_no"));
			resultMap.put("phub_tr_no"  , sendmap.get("phub_tr_no"));
			resultMap.put("points"      , StringUtil.nvl(sendmap.get("points"),"0"));
			resultMap.put("pay_amt"     , StringUtil.nvl(sendmap.get("pay_amt"),"0"));
			resultMap.put("won_amt"     , params.get("ttlRmndAmt"));
			resultMap.put("pay_method"  , StringUtil.nvl(voDeal.getPayMethod()));
			resultMap.put("auth_limit_dtm", sendmap.get("auth_limit_dtm") );

			/***************************************************************
			 * 10.포인트조회 성공시 진행상태 저장 [개통자의경우|피요청자의 경우 분기]
			 ***************************************************************/
			/*
			Map<String,Object> fmap = new HashMap<String,Object>();
			if (Constant.SERVICE_ID_SVC_FP.equals(voDeal.getServiceId())
				&& "Y".equals(voDeal.getPayYn())){
				if ("Y".equals(voDeal.getOwnerYn())){
					// 개통자의 경우 자기 거래번호
					fmap.put("phubTrNo", voDeal.getpHubTrNo());
				} else {
					// 피요청자(=가족)의 경우 개통자 거래번호 사용
					fmap.put("phubTrNo", voDeal.getOwnerTrNo());
				}
				fmap.put("prgr_stat", "PR140");
				fmap.put("custCtn", voCert.getPhoneNo());
				gsService.updateFmlySupot(fmap);
			}*/
			
			
			log.debug("requestPayFromSdk", "Last Result::: \n"+ resultMap.toString());
			
		} catch( BizException be) {
			log.error(methodName, "BizException code="+StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg="+StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step="+StringUtil.nvl(be.getStepCd()));
			
			sLogCd  = be.getErrCd();
			sLogMsg = be.getErrMsg();
					
			resultMap.put("phub_tr_no" , StringUtil.nvl(voDeal.getpHubTrNo()));
			resultMap.put("pg_tr_no"   , StringUtil.nvl(voDeal.getPgTrNo()));
			resultMap.put("pay_amt"    , Integer.parseInt(voDeal.getPayAmt()));
			resultMap.put("won_amt"    , 0);
			resultMap.put("pay_method" , StringUtil.nvl(voDeal.getPayMethod()));
			resultMap.put("auth_limit_dtm"  , "");
			resultMap.put("return_url_2" , voDeal.getReturnUrl2());
			resultMap.put("points" , "0");
			resultMap.put(Constant.RET_CODE, sLogCd);
			resultMap.put(Constant.RET_MSG , sLogMsg);

			statmap.put("deal_stat" , Constant.DEAL_STAT_3000);
			statmap.put("err_cd"    , be.getErrCd());
			statmap.put("err_msg"   , be.getErrMsg());
			
		} catch(Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception stepCd="+StringUtil.nvl(stepCd));
			log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));
			
			sLogCd = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg= messageManage.getMsgTxt("SY_ERROR_900");
			
			resultMap.put("ret_code", sLogCd);
			resultMap.put("ret_msg" , sLogMsg);
			
			statmap.put("deal_stat" , Constant.DEAL_STAT_3000);
			statmap.put("err_cd"    , messageManage.getMsgCd("SY_ERROR_900"));
			statmap.put("err_msg"   , StringUtil.nvl(e.getMessage()));
			
		} finally {
			log.debug(methodName, "finally!");
			
			/* blMustUpdate 뒤로가기 반복 후 재조회 오류 회피 조건 추가 */
			if (!Constant.SUCCESS_CODE.equals(sLogCd) && blMustUpdate ) {
				sbankService.updateDealReqInfoStat(statmap);
			}
			
			Map<String,Object> fmap = new HashMap<String,Object>();
			if (Constant.SERVICE_ID_SVC_FP.equals(sServiceId) && "Y".equals(voDeal.getPayYn()) && blMustUpdate){
				if ("Y".equals(voDeal.getOwnerYn())){
					// 개통자의 경우 자기 거래번호
					fmap.put("phubTrNo", voDeal.getpHubTrNo());
				} else {
					// 피요청자(=가족)의 경우 개통자 거래번호 사용
					fmap.put("phubTrNo", voDeal.getOwnerTrNo());
				}
				if (sLogCd.equals(Constant.SUCCESS_CODE)) {
					fmap.put("prgr_stat", Constant.CPN_REQ_PRGRS_PR140);
				}else{
					fmap.put("prgr_stat", Constant.CPN_REQ_PRGRS_PR900);
				}
				
				fmap.put("custCtn", voCert.getPhoneNo());
				gsService.updateFmlySupot(fmap);
			}
		}
		
		return resultMap;
	
	}
	
	
    /**
     * <pre> 전환금액 체크 메소드</pre>
     * @author cisohn
     * @since  2018.11.02
     * @param  Map 
     * @return Map
     * @description
     * @see <pre>파라미터 상세정의는 외부연동규약서 참조</pre>
     */		
	public HashMap<String,String> checkAmount(List<Map<String, Object>> params){
        HashMap<String, String> returnMap = new HashMap<String, String>();
        returnMap.put("ret_code", "00");
        
        List<Map<String,Object>> list = sbankService.getPrvdrExchRate(params);
        
        if (params.size() != list.size()) {
        	returnMap.put("ret_code", "IF_INFO_154");
        	return returnMap;
        }
        
    	for(int i=0; i < params.size(); i++){
    		Map<String,Object> map = (Map<String,Object>) params.get(i);
    		int inPnt      = Integer.valueOf(StringUtil.nvl(map.get("pnt"),"0"));
    		int inPntAmt   = Integer.valueOf(StringUtil.nvl(map.get("pntAmt"),"0"));
    		String inPntCd = (String)map.get("pntCd");
    		
            for(int j=0; j < list.size();j++){            	
            	Map<String,Object> imap = (Map<String,Object>) list.get(j);
            	String dbPntCd = (String)imap.get("pnt_cd");
            	double dbRate  = Double.parseDouble(StringUtil.nvl(imap.get("exch_rate"),"1"));
            	double dblCal  = 0;
            	
            	if(dbPntCd.equals(inPntCd)) {
            		dblCal = inPnt * dbRate;
            		
            		int compareVal = Integer.parseInt(Common.calcMath(Constant.CALC_STR_MODE, dblCal, 0));
            		
            		if (compareVal != inPntAmt) {
                    	returnMap.put("ret_code", "IF_INFO_154");
                    	return returnMap;
            		}
            	}
            }
    	}
        
		return returnMap;
		
	}

	
    /**
     * <pre> 소진형 거래에서 전환금액 체크 메소드</pre>
     * @author cisohn
     * @since  2018.11.30
     * @param  Map 
     * @return Map["ret_code"]
     * @description
     * @see <pre>파라미터 상세정의는 외부연동규약서 참조</pre>
     */		
	@SuppressWarnings("unchecked")
	public String checkAmount4CU(Map<String, Object> params, HttpServletRequest request){
        HashMap<String, String> returnMap = new HashMap<String, String>();
        String lsReturn = "00";
        
        SbankDealVO dealvo = SessionUtils.getSbankDealVO(request);
        
        List<Map<String,Object>> pList = (List<Map<String, Object>>) params.get("list");
        List<Map<String,Object>> list  = sbankService.getPrvdrExchRate(pList);
        
        double iRate    = Integer.parseInt((String) params.get("shop_pnt_rate"));
        int    iTotAmt  = Integer.parseInt((String) params.get("ttl_pnt_amt"));
        double shopRate = (double)(iRate/100);
        //log.debug("checkAmount4CU shopRate=>", ""+shopRate);
        //log.debug("checkAmount4CU list size =>", ""+ pList.size()+"/"+list.size());
        
        if (pList.size() != list.size()) {
        	log.debug("checkAmount4CU", "step=1");
        	return "IF_INFO_154";
        }
        
        int    iSum    = 0;
        int dealUnit   = Integer.valueOf(StringUtil.nvl(sysManage.getSysPrmtVal("FP_PRVDR_DEAL_UNIT")));
        
        // 1,000원~20,000원까지는 1,000원 단위로 쿠폰 발행 ,나머지 구간은 5,000원 단위로 쿠폰 발행(2019.04.23)
		if (iTotAmt >= 1000 && iTotAmt <= 20000){
			dealUnit = Integer.valueOf(StringUtil.nvl(sysManage.getSysPrmtVal("FP_PRVDR_DEAL_UNIT2")));
		}
        
    	for(int i=0; i < pList.size(); i++) {
    		Map<String,Object> map = (Map<String,Object>) pList.get(i);
    		int inPnt      = Integer.valueOf(StringUtil.nvl(map.get("pnt"),"0"));
    		int inPntAmt   = Integer.valueOf(StringUtil.nvl(map.get("pntAmt"),"0"));
    		int cprtAmt    = Integer.valueOf(StringUtil.nvl(map.get("cprtAmt"),"0"));
    		
    		String inPntCd = (String)map.get("pntCd");
            for(int j=0; j < list.size();j++) {            	
            	Map<String,Object> imap = (Map<String,Object>) list.get(j);
            	String dbPntCd = (String)imap.get("pnt_cd");
            	double dbRate  = Double.parseDouble(StringUtil.nvl(imap.get("exch_rate"),"1"));
            	double dblCal  = 0;
            	
            	if(dbPntCd.equals(inPntCd)) {
            		// 전환율 적용
            		dblCal = inPnt * dbRate;

            		int compareVal = Integer.parseInt(Common.calcMath(Constant.CALC_STR_MODE, dblCal, 0));
            		//log.debug("", "^1차계산결과^" + compareVal);
            		
            		// 1차 검증=db에 저장될 값(=inPntAmt)
            		if (compareVal != inPntAmt) {
            			log.debug("checkAmount4CU", "step=2");
                    	return "IF_INFO_154";
            		}
            		
            		// 패밀리포인트거래시 최소,거래단위(최대는 루프 외곽에서 체크)
            		// 1,000원~20,000원까지는 1,000원 단위로 쿠폰 발행 ,나머지 구간은 5,000원 단위로 쿠폰 발행(2019.04.23)
            		// int dealUnit = Integer.valueOf(StringUtil.nvl(sysManage.getSysPrmtVal("FP_PRVDR_DEAL_UNIT"),"1"));
            		
            		if(dealvo.getServiceId().equals(Constant.SERVICE_ID_SVC_FP)){
            			if (inPntAmt % dealUnit != 0){
            				// 결제단위금액 오류
            				log.debug("checkAmount4CU", "step=3");
            				return "BZ_ERROR_162";
            			}
            		}
            		
            		if(dealvo.getServiceId().equals(Constant.SERVICE_ID_SVC_BASE)){
	            		// 2차 검증 (순전히 체크만)
	            		// 가맹점 수수료율 적용(할인될 금액 구하기)
	            		dblCal = compareVal * shopRate;
	            		double dblComp = compareVal - dblCal;
	            		compareVal = Integer.parseInt(Common.calcMath(Constant.CALC_STR_MODE, dblComp, 0));
	            		if (compareVal != cprtAmt) {
	            			log.debug("checkAmount4CU", "step=4");
	                    	//returnMap.put(Constant.RET_CODE, messageManage.getMsgCd("IF_INFO_154"));
	                    	return "IF_INFO_154";
	            		}
            		}
            		
            	}
            }
            iSum = iSum + inPntAmt;
    	}
    	
    	if(dealvo.getServiceId().equals(Constant.SERVICE_ID_SVC_FP)){
    		int fpMax = Integer.valueOf(StringUtil.nvl(sysManage.getSysPrmtVal("FP_MAX_PER_DEAL"),"0"));

    		Map<String,Object> tmap = new HashMap<String,Object>();
    		tmap.put("phub_tr_no", dealvo.getpHubTrNo());
    		tmap.put("owner_yn"  , dealvo.getOwnerYn());
    		
    		int fpMin = fmlyService.getFpMinPerDeal(tmap);
    		
    		if(iSum > fpMax) {
    			// 쿠폰 최대 결제금액을 초과하셨습니다.
    			log.debug("checkAmount4CU", "step=5");
            	//returnMap.put(Constant.RET_CODE, messageManage.getMsgCd("BZ_INFO_183"));	// 메시지 오등록으로 인한 신규 메시지ID로 변경(BZ_ERROR_163 -> BZ_INFO_183) - 2019.06.11.lys
            	return "BZ_INFO_183";
    		}

    		if(iSum < fpMin) {
        		// 쿠폰 최소 결제금액에 못미치는 금액입니다.
    			log.debug("checkAmount4CU", "step=6");
    			//returnMap.put(Constant.RET_CODE, messageManage.getMsgCd("BZ_ERROR_164"));
            	return "BZ_ERROR_164";
    		}
    		
    	}
    	
		return lsReturn;
	}	    
}
