package com.olleh.pointHub.api.sdk.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.olleh.pointHub.api.comn.crypto.CouponPointCrypto;
import com.olleh.pointHub.api.company.service.SHUBHttpService;
import com.olleh.pointHub.api.sdk.service.FamilyPointService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.components.UtilsComponent;
import com.olleh.pointHub.common.exception.BaseException;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.CertificationVO;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.spring.annotation.CertificationCheck;
import com.olleh.pointHub.common.spring.annotation.SessionClear;
import com.olleh.pointHub.common.spring.annotation.SessionTimeoutCheck;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;

/**
 * <pre>
 * 패밀리포인트 서비스 SDK 콘트롤러
 * </pre>
 * 
 * @Class Name : FamilyPointController
 * @author : sci
 * @since : 2019.02.11
 * @version : 1.0
 * @see
 * 
 * 		<pre>
 *  
 * << 개정이력(Modification Information) >> 
 * 수정일                수정자             수정내용 
 * -----------  ----------- ---------------- 
 * 2019. 02. 11 lys  최초 생성
 *      </pre>
 */

@RestController
public class FamilyPointController {
	private Logger log = new Logger(this.getClass());

	private String convergedUri = "/phub/fp/comnCertify.do";

	@Autowired
	FamilyPointService familyService;

	@Autowired
	MessageManage messageManage;

	@Autowired
	SHUBHttpService shubService;

	@Autowired
	SysPrmtManage sysManage;
	
	@Autowired
	CouponPointCrypto couponPointCrypto;
	
	@Autowired
    UtilsComponent utilsComponent;

	/**
	 * <pre>
	 * SDK 첫화면 로드 response
	 * </pre>
	 * 
	 * @param Map<String,Object>
	 * @param HttpServletRequest
	 * @return ModelAndView
	 * @throws IOException
	 */
	@RequestMapping(value = "/pg/fp/identification.do", method = RequestMethod.POST)
	@SessionClear
	public ModelAndView familyPointInit(@RequestParam Map<String, Object> params, HttpServletRequest request)
			throws IOException {
		String methodName = "familyPointInit";

		log.debug(methodName, "familyPointInit Param From PG:: \n" + JsonUtil.MapToJson(params));
		ModelAndView mv = new ModelAndView();

		mv.setView(new RedirectView(convergedUri));

		FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
		params.put("path", "PG");
		params.put("from_uri", request.getRequestURI());
		flashMap.put("params", params);

		return mv;

	}

	/**
	 * <pre>
	 * 개통자 MMS수신 후 약관조회화면으로 전환요청
	 * </pre>
	 * 
	 * @author cisohn
	 * @param Map<String,Object>
	 * @param HttpServletRequest
	 * @return ModelAndView
	 * 
	 *         hwlee Get방식 변경 sbankDealVO 값 세팅하여 session에 저장
	 * 
	 */
	@RequestMapping(value = "/phub/fp/presents/{phubTrNo}", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	@SessionClear
	public ModelAndView getPresentList(@PathVariable String phubTrNo, HttpServletRequest request) throws IOException {
		String methodName = "getPresentList";
		Map<String, Object> params = new HashMap<String, Object>();
		SbankDealVO sbankDealVO = new SbankDealVO();

		sbankDealVO.setpHubTrNo(phubTrNo);
		sbankDealVO.setServiceId(Constant.SERVICE_ID_SVC_FP);
		sbankDealVO.setPayYn("N");
		sbankDealVO.setCertPath("PH");

		/********************************************************************************************************
		 * 신규사용자의 개통자 전용 경로를 통해 들어온 경우 본인인증 후 고객등록시 cprt_cmpn_id 널 오류 발생에 대비하여
		 * 현재(2019.04.17) 가맹점이 유일하게 한 개인 점을 활용<?>하여 디폴트 값으로 대체한 후 뒷단에서 지원현황 사항을
		 * 조회하는 것을 막는다.
		 *******************************************************************************************************/
		sbankDealVO.setCprtCmpnId(sysManage.getSysPrmtVal("FP_DEF_CPRT_CMPN_ID"));

		SessionUtils.setSbankDealVO(request, sbankDealVO);
		log.debug(methodName, "sbankDealVO==> " + sbankDealVO.toString());

		ModelAndView mv = new ModelAndView();

		FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
		params.put("path", "PH");
		params.put("phub_tr_no", phubTrNo);
		params.put("from_uri", request.getRequestURI());
		flashMap.put("params", params);

		mv.setView(new RedirectView(convergedUri));

		return mv;
	}

	/**
	 * <pre>
	 * 약관조회화면으로 전환요청(쿠폰리스트 조회)
	 * </pre>
	 * 
	 * @param String
	 * @param HttpServletRequest
	 * @return ModelAndView
	 * @throws IOException
	 * @description 쿠폰리스트 조회이전 CL(쿠폰리스트) 약관조회화면을 호출하는 Controller로 Redirect 방식으로
	 *              이동
	 * 
	 */
	@RequestMapping(value = "/phub/fp/cpnList.do", method = {RequestMethod.POST, RequestMethod.GET}, produces = "text/plain;charset=UTF-8")
	@SessionClear
	public ModelAndView getCouponList(@RequestParam Map<String, Object> rParams, HttpServletRequest request) throws IOException {

		String 	methodName 	= "getCouponList";
		String	rt_url 		= (String) rParams.get("rt_url");
		String	platform 	= (String) rParams.get("platform");
		Map<String, Object> params = new HashMap<String, Object>();
		SbankDealVO sbankDealVO = new SbankDealVO();
		

		// sbankDealVO.setpHubTrNo(callSite);
		sbankDealVO.setServiceId(Constant.SERVICE_ID_SVC_FP);
		sbankDealVO.setPayYn("N");
		sbankDealVO.setCertPath("CL");

		/********************************************************************************************************
		 * 신규사용자의 개통자 전용 경로를 통해 들어온 경우 본인인증 후 고객등록시 cprt_cmpn_id 널 오류 발생에 대비하여
		 * 현재(2019.04.17) 가맹점이 유일하게 한 개인 점을 활용<?>하여 디폴트 값으로 대체한 후 뒷단에서 지원현황 사항을
		 * 조회하는 것을 막는다.
		 *******************************************************************************************************/
		sbankDealVO.setCprtCmpnId(sysManage.getSysPrmtVal("FP_DEF_CPRT_CMPN_ID"));

		SessionUtils.setSbankDealVO(request, sbankDealVO);
		log.debug(methodName, "sbankDealVO==> " + sbankDealVO.toString());

		ModelAndView mv = new ModelAndView();

		FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
		params.put("path", 		"CL");
		params.put("from_uri", 	request.getRequestURI());
		params.put("rt_url", 	rt_url);
		params.put("platform", 	platform);
		
		flashMap.put("params", params);

		mv.setView(new RedirectView(convergedUri));
		return mv;
	}
	
	static final String platform_mobile = "mobile";
	static final String platform_pc = "pc";
	
	String getDeviceInd(String deviceInd, String platform) {
		String ret = deviceInd;
		
		ret = platform == null || StringUtils.isEmpty(platform) ?
			  deviceInd :
			  (platform.toLowerCase().equals(platform_mobile) ? "MB" : "PC");
		
		return ret;
	}

	/**
	 * <pre>
	 * 개통자 조르기화면 진입(from MMS)
	 * </pre>
	 * 
	 * @param Map<String,Object>
	 * @param HttpServletRequest
	 * @return ModelAndView
	 * @throws BizException
	 * @description 두가지 경로로 들어옴(mms문자링크와 pg사 웹펭이지) mms문자링크는 단순한 가족지원목록조회,
	 *              pg사로부터는 결제를 위한 intro
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/phub/fp/comnCertify.do", method = RequestMethod.GET)
	public ModelAndView validateUser(@RequestParam Map<String, Object> params, HttpServletRequest request)
			throws ServletException, IOException, BizException {

		String methodName = "validateUser";
		String stepCd = "001";
		ModelAndView mv = new ModelAndView();
		Map<String, Object> retmap = new HashMap<String, Object>();
		String sViewName = "std/validateUser_FP";

		SbankDealVO voDeal = SessionUtils.getSbankDealVO(request);
		CertificationVO voCert = SessionUtils.getCertificationVO(request);

		// flash얻기
		Map<String, ?> fmap = RequestContextUtils.getInputFlashMap(request);

		Map<String, Object> map = new HashMap<String, Object>();

		String sRetCode = Constant.SUCCESS_CODE;
		String sRetMsg = Constant.SUCCESS_MSG;

		try {

			/* 본인인증 화면이나 인증 후 뒤로가기로 찾아온 경우 */
			if (Common.isNull(fmap)) {
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_116"));
			}

			map = (Map<String, Object>) fmap.get("params");

			// "PG"(개통자,피요청자) or "PH"(비거래)
			String sPath = StringUtil.nvl(map.get("path"));

			mv.setViewName(sViewName);

			/* sPath["PH":포인트허브,"PG":세틀뱅크 "CL : 패밀리포인트 쿠폰목록조회"] */
			if ("PH".equals(sPath)) {

				retmap = familyService.getPresentList(map, request);

				// 약관내용
				mv.addObject("clsList", retmap.get("clsList"));

				// 제3자 정보제공동의 리스트(카드사)
				mv.addObject("cardInfoList", retmap.get("cardInfoList"));

				log.debug(methodName, "retMap 후 => \n" + retmap.toString());
				log.debug(methodName, "voDeal => \n" + voDeal.toString());

				mv.addObject("ownerYn", voDeal.getOwnerYn());
				mv.addObject("payYn", voDeal.getPayYn());
				mv.addObject("serviceId", voDeal.getServiceId());
				mv.addObject("phubTrNo", voDeal.getpHubTrNo());

				mv.addObject("deviceInd", StringUtil.nvl(SystemUtils.getCurrentDeviceInd(request)));

			} else if ("CL".equals(sPath)) {

				log.debug(methodName, " voDeal.getCertPath ==>" + voDeal.getCertPath());

				// 이름 지정 필요
				sViewName = "std/validateUser_CL";
				mv.setViewName(sViewName);

				retmap = familyService.getPresentList(map, request);

				// 약관내용
				mv.addObject("clsList", retmap.get("clsList"));

				// 제3자 정보제공동의 리스트(카드사)
				mv.addObject("cardInfoList", retmap.get("cardInfoList"));

				log.debug(methodName, "retMap 후 => \n" + retmap.toString());
				log.debug(methodName, "voDeal => \n" + voDeal.toString());

				String platform 	= StringUtil.nvl(map.get("platform"));
				String deviceInd 	= StringUtil.nvl(SystemUtils.getCurrentDeviceInd(request));
				
				mv.addObject("rt_url", 		map.get("rt_url"));
				mv.addObject("platform", 	platform);
				mv.addObject("ownerYn", 	voDeal.getOwnerYn());
				mv.addObject("payYn", 		voDeal.getPayYn());
				mv.addObject("serviceId", 	voDeal.getServiceId());
				mv.addObject("path", 		sPath);
				// mv.addObject("phubTrNo" , voDeal.getpHubTrNo());
				
				mv.addObject("deviceInd", this.getDeviceInd(deviceInd, platform));

			} else if ("PG".equals(sPath)) {

				retmap = familyService.familyPointInit(map, request);

				voDeal = SessionUtils.getSbankDealVO(request);
				// log.debug(methodName, "familyPointInit 거래정보 셋팅 후 => \n"+
				// retmap.toString());

				if (!(messageManage.getMsgCd("SY_INFO_00")).equals(retmap.get(Constant.RET_CODE))) {

					log.debug(methodName, "familyPointInit retmap Error!!");
					throw new BizException(stepCd, (String) retmap.get(Constant.RET_CODE),
							(String) retmap.get(Constant.RET_MSG));

				} else {
					/* model & view setting */
					mv.addObject("phubTrNo", voDeal.getpHubTrNo());
					mv.addObject("goodsNm", voDeal.getGoodsName());
					mv.addObject("ttlPayAmt", voDeal.getPayAmt());
					mv.addObject("dealInd", voDeal.getTrDiv());
					mv.addObject("payMethod", voDeal.getPayMethod());
					mv.addObject("pgCmpnNm", voDeal.getShopName());
					mv.addObject("pg_tr_no", voDeal.getPgTrNo());
					mv.addObject("return_url_2", voDeal.getReturnUrl2());
					mv.addObject("shopPntRate", voDeal.getShopPntRate());
					mv.addObject("shopPntName", voDeal.getShopPntName());
					mv.addObject("rchrPayInd", voDeal.getRchrPayInd());

					// 약관내용
					mv.addObject("clsList", retmap.get("clsList"));

					// 제3자 정보제공동의 리스트(카드사)
					mv.addObject("cardInfoList", retmap.get("cardInfoList"));

					mv.addObject("ownerYn", voDeal.getOwnerYn());
					mv.addObject("serviceId", voDeal.getServiceId());

					mv.addObject("deviceInd", StringUtil.nvl(SystemUtils.getCurrentDeviceInd(request)));

				}
			}

		} catch (BizException be) {
			sRetCode = be.getErrCd();
			sRetMsg = be.getErrMsg();
		} catch (Exception e) {
			throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));
		} finally {
			if (!Constant.SUCCESS_CODE.equals(sRetCode)) {
				mv.setView(new RedirectView("/error_custom.do"));
				FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
				flashMap.put("eCode", sRetCode);
				flashMap.put("eMsg", sRetMsg);
			}
		}

		return mv;
	}

	/**
	 * <pre>
	 * 개통자 패밀리지원현황 조회
	 * </pre>
	 * 
	 * @author cisohn
	 * @param Map<String,Object>
	 *            [phub_tr_no:포인트허브 거래번호]
	 * @param HttpServletRequest
	 * @return ModelAndView
	 */
	/***********************************************************************************
	 * 개통자 쿠폰 결제 후 (/pg/pay) response 변수 중 return_url (예)
	 * http://dddkd/phub/fp/familyList.do?trno=PH190101000009
	 ***********************************************************************************/
	@SessionTimeoutCheck
	@CertificationCheck(required=true)
	@RequestMapping(value = "/phub/fp/familyList.do", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public ModelAndView initFamilyList(@RequestParam Map<String, Object> params, HttpServletRequest request) {
		String methodName = "initFamilyList";
		String stepCd = "001";
		ModelAndView mv = new ModelAndView();
		List<Map<String, Object>> fmlyList = new ArrayList<Map<String, Object>>();

		String sViewName = "std/familyPoint";

		log.debug(methodName, "initFamilyList Param From PG:: \n" + JsonUtil.MapToJson(params));
		log.debug(methodName, "User-Agent::" + JsonUtil.toJson(request.getHeader("User-Agent")));

		SbankDealVO dealVo = SessionUtils.getSbankDealVO(request);
		String sPhubTrNo = StringUtil.nvl(dealVo.getpHubTrNo());
		String pPhubTrNo = StringUtil.nvl(params.get("phub_tr_no"));

		// 패밀리 리스트 return_url_2 값 여부로 에러페이지 확인버튼 유무 결정
		dealVo.setReturnUrl2("");
		SessionUtils.setSbankDealVO(request, dealVo);

		if (!sPhubTrNo.equals(pPhubTrNo)) {
			// 세션만료
			String retCode = "SY_INFO_901";
			mv.setView(new RedirectView("/error_custom.do"));
			FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
			flashMap.put("eCode", messageManage.getMsgCd(retCode));
			flashMap.put("eMsg", messageManage.getMsgTxt(retCode));
		} else {
			Map<String, Object> retmap = new HashMap<String, Object>();

			retmap = familyService.checkCountOwner(params);

			String sRetCode = StringUtil.nvl(retmap.get(Constant.RET_CODE));

			if (sRetCode.equals(Constant.SUCCESS_CODE)) {
				fmlyList = familyService.getFamilyPresents(params);
				mv.setViewName(sViewName);
				mv.addObject("fpList", fmlyList);
			} else {
				mv.setView(new RedirectView("/error_custom.do"));
				FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
				flashMap.put("eCode", messageManage.getMsgCd(sRetCode));
				flashMap.put("eMsg", messageManage.getMsgTxt(sRetCode));
			}
		}
		return mv;
	}

	/**
	 * <pre>
	 * 개통자 패밀리지원현황 조회(새로고침)
	 * </pre>
	 * 
	 * @author cisohn
	 * @param Map<String,Object>
	 *            [phub_tr_no:포인트허브 거래번호]
	 * @param HttpServletRequest
	 * @return ModelAndView
	 */
	@SessionTimeoutCheck
	@CertificationCheck(required=true)
	@RequestMapping(value = "/phub/fp/getFamilyList.do", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public ModelAndView getFamilyList(@RequestParam Map<String, Object> params, HttpServletRequest request) {
		String methodName = "getFamilyList";
		String stepCd = "001";
		ModelAndView mv = new ModelAndView();

		String sViewName = "std/familyPoint";

		log.debug(methodName, "getFamilyList Param From SDK:: \n" + JsonUtil.MapToJson(params));
		log.debug(methodName, "User-Agent::" + JsonUtil.toJson(request.getHeader("User-Agent")));

		SbankDealVO dealVo = SessionUtils.getSbankDealVO(request);
		String sPhubTrNo = StringUtil.nvl(dealVo.getpHubTrNo());
		String pPhubTrNo = StringUtil.nvl(params.get("phub_tr_no"));

		if (!sPhubTrNo.equals(pPhubTrNo)) {
			String retCode = "SY_INFO_901";
			mv.setView(new RedirectView("/error_custom.do"));
			FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
			flashMap.put("eCode", messageManage.getMsgCd(retCode));
			flashMap.put("eMsg", messageManage.getMsgTxt(retCode));
		} else {
			List<Map<String, Object>> fmlyList = familyService.getFamilyPresents(params);

			mv.setViewName(sViewName);
			mv.addObject("fpList", fmlyList);
		}
		return mv;
	}

	/**
	 * <pre>
	 * 조르기 문자 발송 처리
	 * </pre>
	 * 
	 * @see
	 * 
	 * 		<pre></pre>
	 * 
	 * @author hwlee
	 * @since 2019.02.25
	 * @param Map
	 * @return Map
	 * @description 1. 2회 초과하였는지 FP_FMLY_SUPOT 테이블 조회하여 체크 2. FP_FMLY_SUPOT 테이블의
	 *              REQ_STAT_CD 상태 update 3. PH_MMS_SEND_HIST 테이블에 insert 4.
	 *              S-HUB 연동하여 피요청자에게 MMS 발송 요청 5. 결과 PH_MMS_SEND_HIST 테이블
	 *              update
	 */
	@CertificationCheck(required=true)
	@RequestMapping(value = "/phub/fp/sendPesterMMS.do", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public Map<String, Object> sendPesterMMS(@RequestParam Map<String, Object> params, HttpServletRequest request) {
		String methodName = "sendPesterMMS";
		String stepCd = "001";

		Map<String, Object> tmpMap = new HashMap<String, Object>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();

		resultMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		resultMap.put(Constant.RET_MSG, Constant.SUCCESS_MSG);

		log.debug(methodName, "sendPesterMMS Param From SDK:: \n" + JsonUtil.MapToJson(params));

		List<Map<String, Object>> fmlyList = new ArrayList<Map<String, Object>>();
		try {
			fmlyList = JsonUtil.JsonToList((String) params.get("fmlyList"));
			familyService.sendPesterMMSService(fmlyList);
		} catch (BizException be) {
			log.error(methodName, "BizException code=" + StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg=" + StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));

			resultMap.put(Constant.RET_CODE, be.getErrCd());
			resultMap.put(Constant.RET_MSG, be.getErrMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception stepCd=" + StringUtil.nvl(stepCd));
			log.error(methodName, "Exception e=" + StringUtil.nvl(e.getMessage()));

			resultMap.put(Constant.RET_CODE, Constant.FAIL_CODE);
			resultMap.put(Constant.RET_MSG, Constant.FAIL_MESSAGE);
		}
		return resultMap;
	}

	/**
	 * <pre> 쿠폰리스트 조회 </pre>
	 * 
	 * @author jungukjae
	 * @since 2019.04.23
	 * @param Map<String,Object> [phub_tr_no:포인트허브 거래번호]
	 * @param HttpServletRequest
	 * @return map
	 * @throws BizException
	 * @description 포인트허브거래번호를 파라미터로 쿠폰리스트 조회
	 * 
	 */
//	@RequestMapping(value = "/phub/fp/cpnList.do", method = RequestMethod.POST)
//	@SessionClear
//	public ModelAndView getCouponList(@RequestParam Map<String, Object> rParams, HttpServletRequest request) throws IOException {
	
	@SessionTimeoutCheck(required = true)
	@CertificationCheck(required = true)
	@RequestMapping(value = "/phub/fp/couponList.do", method = RequestMethod.POST)
	public ModelAndView couponList(@RequestParam Map<String, Object> params, HttpServletRequest request) throws Exception {
		String methodName = "couponList";
		String stepCd = "001";	// step 코드 (익셉션 발생시 사용)
		String indCd1 = "CPN_REQ_STAT";
		String indCd2 = "CPN_REQ_PRGRS";
		String prgrStat = Constant.CPN_REQ_PRGRS_PR200;
		ModelAndView mv = new ModelAndView();
		try {
			// Session에 있는 ret url, platform 확인
			stepCd = "002";
			String rt_url 	= SessionUtils.getSbankDealVO(request).getRtUrl();
			String platform = SessionUtils.getSbankDealVO(request).getPlatform();
			log.debug(methodName, "[rt_url ->" + rt_url + "]");
			log.debug(methodName, "[platform ->" + platform + "]");
			
			/*
				190520: session cust_id 와 parameter cust_id 비교 후 동일하지 않다면 throw(invalid parameter)
				190523: 바로 session cust_id 꺼내 쓰기로 변경(사파리 브라우저 reload 이슈)
			*/
			stepCd = "003";	
			CertificationVO certificationVo = SessionUtils.getCertificationVO(request);
			String sessionCustId = certificationVo.getCustId();
			/*String parameterCustId = StringUtil.nvl((String) params.get("custId"));
			if (!sessionCustId.equals(parameterCustId)) {
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_103"));
			}*/
			
			params.put("custId", 	sessionCustId);
			params.put("indCd1", 	indCd1);
			params.put("indCd2", 	indCd2);
			params.put("prgrStat", 	prgrStat);
			
			stepCd = "004";	
			List<Map<String, Object>> couponList = familyService.couponList(params);
			
			// coupon point 암호화 Map에 추가
			stepCd = "005";
			for (Map<String, Object> coupon : couponList) {
				String cpnAmt = String.valueOf(coupon.get("cpnAmt"));
				String cpnAmtEncrypt = couponPointCrypto.encAES(cpnAmt);
				coupon.put("cpnAmtEncrypt", cpnAmtEncrypt);
			}
			
			mv.addObject("rt_url", 		rt_url);
			mv.addObject("platform", 	platform);
			mv.addObject("couponList", 	couponList);
			mv.addObject("serviceId", 	Constant.SERVICE_ID_SVC_FP);
			mv.addObject("COUPON_MAX_COUNT", sysManage.getSysPrmtVal("SEL_CPN_MAX"));
			mv.setViewName("std/couponList");
		/*} catch (BizException be) {
			log.error(methodName, "[BizException]stepCd=" + be.getStepCd());
			log.error(methodName, "[BizException]errCd=" + be.getErrCd());
			log.error(methodName, "[BizException]msg=" + be.getErrMsg());
			
			mv.setView(new RedirectView("/error_custom.do"));
			
			FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
			flashMap.put("eCode", be.getErrCd());
			flashMap.put("eMsg" , be.getErrMsg());*/
		} catch( Exception e) {
			log.error(methodName, "Exception stepCd=" + stepCd);
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			mv.setView(new RedirectView("/error.do"));
			FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
			flashMap.put("eCode", messageManage.getMsgCd("SY_ERROR_900"));
			flashMap.put("eMsg" , e.getMessage());
		} 
		return mv;
	}
	
}
