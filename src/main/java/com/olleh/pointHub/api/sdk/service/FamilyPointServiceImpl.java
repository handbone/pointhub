/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */

package com.olleh.pointHub.api.sdk.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pointHub.api.comn.crypto.SbankCrypto;
import com.olleh.pointHub.api.company.service.GiftiShowRestHttpService;
import com.olleh.pointHub.api.company.service.SHUBHttpService;
import com.olleh.pointHub.api.paygate.service.SbankService;
import com.olleh.pointHub.api.provider.service.ClipService;
import com.olleh.pointHub.api.sdk.dao.FamilyPointDao;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.CertificationVO;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.utils.Amount;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;

/**
 * @Class Name : FamilyPointServiceImpl
 * @author : cisohn
 * @since 2018.07.31
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자     수정내용
 * ----------  --------  -----------------
 * 2019.02.12   cisohn      최초생성
 * </pre>
 */

@Service
public class FamilyPointServiceImpl implements FamilyPointService {

	private Logger log = new Logger(this.getClass());
	
	@Resource(name="familyPointDao")
	private FamilyPointDao dao;

	@Autowired
	ClipService clipService;
	
	@Autowired
	SbankCrypto sbankCrypto;
	
	@Autowired
	SbankService sbankService;
	
	@Autowired
	SHUBHttpService shubService;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	GiftiShowRestHttpService giftiShowRestHttpService;
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
    /**
     * @description 패밀리포인트 쿠폰 결제화면 초기화(약관조회,본인인증진입점)
     * @param  Map HttpServletRequest,
     * @return List
     */	
	@SuppressWarnings({ "unused", "finally" })
	@Override
	public Map<String,Object> familyPointInit(Map<String, Object> params, HttpServletRequest request) {
		String methodName = "familyPointInit";
		String stepCd     = "001";
		
		Map<String,Object> retmap  = new HashMap<String,Object>();
		Map<String,Object> map     = new HashMap<String,Object>();
		Map<String,Object> trnomap = new HashMap<String,Object>();
		Map<String,Object> clsmap  = new HashMap<String,Object>();
		
		Map<String,Object> reqmap  = new HashMap<String,Object>(params);
		
		SbankDealVO 	   sbankDealVO = new SbankDealVO();
		String sPhubTrNo = "";
		String sToken = "";
		
        String sLogCd  = Constant.SUCCESS_CODE;
        String sLogMsg = Constant.SUCCESS_MSG;		
		
		try {
			/* 0.PHUB 거래번호 생성(개통자/피요청자) */
			trnomap   = clipService.getPhubTrNo();
			sPhubTrNo = (String) trnomap.get("phub_tr_no");
			sToken    = (String) trnomap.get("token");
			sToken    = sbankCrypto.encAES(sToken);
			
			/* 1.필수파라미터 체크 */
			if (StringUtil.nvl(params.get("pg_tr_no")).equals("")
					|| StringUtil.nvl(params.get("tr_div")).equals("")
					|| StringUtil.nvl(params.get("shop_cd")).equals("")
					|| StringUtil.nvl(params.get("pg_cd")).equals("")
					){
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_116"));
			}
			//log.debug(methodName, "$$$$ step:: "+ stepCd +" $$$$ \n*********** "+ sPhubTrNo + "\n*********** "+ sToken);
			
			/* 2.pre-insert deal information */
			stepCd = "002";
			reqmap.put("user_id"   , StringUtil.nvl(params.get("user_id"),"admin"));
			reqmap.put("phub_tr_no", sPhubTrNo);
			reqmap.put("deal_stat" , Constant.DEAL_STAT_0100);
			
			/* 3.check registered shop : 미등록 가맹점인 경우 PH_DEAL_REQ_INFO에 등록하지 않는다. - 2019.05.17. lys   */
			stepCd = "003";
			map = clipService.getPgCprtInfo(params);
			
			if (!StringUtil.nvl(map.get("ret_code")).equals(messageManage.getMsgCd("SY_INFO_00"))) {
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_113"));
			} else {
				reqmap.put("rchr_pay_ind", map.get("rchr_pay_ind"));
				reqmap.put("cprt_cmpn_id", map.get("cprt_cmpn_id"));
			}			
			
			// 연동규격에 없으나 FP전용 프로그램이므로 ..
			reqmap.put("service_id", Constant.SERVICE_ID_SVC_FP);
			
			// INSERT PH_DEAL_REQ_INFO
			sbankService.insertDealReqInfoPre(reqmap);
			
			/* 4.거래구분 체크(복합배제) */
			stepCd = "004";
			if (!"CU".equals(StringUtil.nvl(params.get("pay_method")))){
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_103"));
			}
			
			/* 5.서비스ID 체크 */
			stepCd = "005";
			if (! Constant.SERVICE_ID_SVC_FP.equals(StringUtil.nvl(params.get("service_id")))){
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_103"));
			}
			
			/* 6.세션 등록(with DealVO) */
			stepCd = "006";
			reqmap.put("token", sToken);
			this.setDealVo(reqmap);
			
			/* 7.약관조회 결과 담기  */
			sbankDealVO = SessionUtils.getSbankDealVO(request);
			
			stepCd = "007";
			clsmap.put("phub_tr_no", sPhubTrNo);
			clsmap.put("pg_tr_no"  , params.get("pg_tr_no"));
			clsmap.put("service_id", Constant.SERVICE_ID_SVC_FP);
			clsmap.put("owner_yn"  , sbankDealVO.getOwnerYn());

			Map<String, Object> result = clipService.getClaUses(clsmap);
			
			retmap.put("clsList"     , result.get("clsList"));
			retmap.put("cardInfoList", result.get("cardInfoList"));
			
			retmap.put(Constant.RET_CODE , sLogCd);
			retmap.put(Constant.RET_MSG  , sLogMsg);			
			
		} catch (BizException be) {
			log.error(methodName, "[FP BizException]stepCd =" + be.getStepCd());
			log.error(methodName, "[FP BizException]stepCd =" + be.getStepCd());
			log.error(methodName, "[FP BizException]errCd  =" + be.getErrCd());
			log.error(methodName, "[FP BizException]msg    =" + be.getErrMsg());

			sLogCd  = be.getErrCd();
			sLogMsg = be.getErrMsg();
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			sLogCd  = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg = e.getMessage();
			
		} finally {
			
			/* 6.에러코드 저장 (updateDealReqInfoStatPre) */
			reqmap.put("err_cd" , sLogCd);
			reqmap.put("err_msg", sLogMsg);
			sbankService.updateDealReqInfoStatPre(reqmap);
			
			/* 7.인터페이스 로그 저장 (logRequestFromOut) */
			params.put("req_data", JsonUtil.MapToJson(params));
			params.put("res_data", "");			
            params.put("phub_tr_no", sPhubTrNo);
			params.put(Constant.RET_CODE, sLogCd);
			params.put(Constant.RET_MSG , sLogMsg);
			
			clipService.logRequestFromOut(params, request, methodName);			
			
		}
		
		retmap.put(Constant.RET_CODE , sLogCd);
		retmap.put(Constant.RET_MSG  , sLogMsg);
		
		return retmap;
		
	}	
	
	
	private void setDealVo(Map<String, Object> params) throws BizException {
		HttpServletRequest request = SystemUtils.getCurrentRequest();
		SbankDealVO  dealVo = new SbankDealVO();
		
		// 쿠폰조회와 구별하여 거래임을 나타냄
		dealVo.setCertPath("PH");
		
		dealVo.setpHubTrNo((String) params.get("phub_tr_no"));
		dealVo.setToken((String) params.get("token"));
		dealVo.setPgTrNo((String)params.get("pg_tr_no"));
		dealVo.setPgCd((String)params.get("pg_cd"));
		dealVo.setUserId((String)params.get("user_id"));
		dealVo.setShopCd((String)params.get("shop_cd"));
		dealVo.setShopName((String)params.get("shop_name"));
		dealVo.setShopBizNo((String)params.get("shop_biz_no"));
		dealVo.setGoodsName((String)params.get("goods_name"));
		dealVo.setPayAmt((String)params.get("pay_amt"));
		dealVo.setPayMethod((String)params.get("pay_method"));
		dealVo.setTrDiv((String)params.get("tr_div"));
		dealVo.setReturnUrl1((String)params.get("return_url_1"));
		dealVo.setReturnUrl2((String)params.get("return_url_2"));

		//dealVo.setShopPntRate((String)params.get("shop_point_rate"));
		dealVo.setShopPntRate(StringUtil.nvl(params.get("shop_point_rate"),"0"));
		
		dealVo.setShopPntName((String)params.get("shop_point_name"));
		dealVo.setRchrPayInd((String)params.get("rchr_pay_ind"));
		dealVo.setCprtCmpnId((String)params.get("cprt_cmpn_id"));
		
		dealVo.setServiceId((String)params.get("service_id"));
		dealVo.setAddParam((String)params.get("add_param"));

		/************************************************************* 
		 * 피요청자,개통자 여부 체크
		 * 피요청자인 경우 전달된 거래번호 검증 후 진행(개통자정볼 조회셋팅) 
		 *************************************************************/
		String sParam, sOwnerPhubTrNo , sTrNo;
		Map<String,Object> map = new HashMap<String,Object>();
		if(!StringUtil.nvl(params.get("add_param"),"").equals("")){
		   sParam = (String)params.get("add_param");
		   map    = JsonUtil.JsonToMap(sParam);
		   sOwnerPhubTrNo = (String) map.get("phubTrno");
		   
		   /* ownerYn이 N 인경우 owner정보 셋팅 */
		   Map<String,Object> reqmap = new HashMap<String,Object>();
		   Map<String,Object> resmap = new HashMap<String,Object>();
		   reqmap.put("phub_tr_no", sOwnerPhubTrNo);
		   resmap = dao.getOwnerInfo(reqmap);
		   String lsOwnerTrNo="",lsOwnerName="", lsOwnerCtn="";
		   
		   if (Common.isNull(resmap)) {
			   throw new BizException("001", messageManage.getMsgVOY("BZ_INFO_149"));
		   } else {
			   lsOwnerTrNo = (String) resmap.get("owner_tr_no");
			   lsOwnerName = (String) resmap.get("owner_name");
			   lsOwnerCtn  = (String) resmap.get("owner_ctn");
			   dealVo.setOwnerName(lsOwnerName);
			   dealVo.setOwnerTrNo(lsOwnerTrNo);
			   dealVo.setOwnerCtn(lsOwnerCtn);
		   }
		   
		   dealVo.setOwnerYn("N");
		   
        }else{
        	dealVo.setOwnerYn("Y");
        }
			
		SessionUtils.setSbankDealVO(request, dealVo);
	}
	
	
	@Override
	public List<Map<String, Object>> getFamilyPresents(Map<String, Object> params) {
		return dao.getFamilyPresents(params);
	}

    /**
     * @description SHUB를 통하여 KT고객인증 및 가족묶음 멤버 가져오기
     * @param       Map
     * @return      String("N" or "Y")
     */	
	@Override
	public String certifyKT(Map<String, Object> params) {
		String methodName = "certifyKT";
		String stepCd  = "001";
		String sReturn = "";
		ResultVo rv = new ResultVo();
		
		try {
			
			log.debug(methodName, params.toString());
			
			rv = shubService.sendHttpPostRequest(params);
			sReturn = rv.getSucYn();
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception=" + StringUtil.nvl(e.getMessage()));
		}
		
		return sReturn;
	}
	
    /**
     * @description SHUB를 통하여 KT고객인증 및 가족묶음 멤버 가져오기
     *              API 통해 가져와서 DB에 넣는다. 
     * @param       Map(ctn,ssno+1,name,phub_tr_no)
     *                 (전화번호,주민6자리+성(1,2,3,4),포인트허브거래번호)
     * @return      ResultVo
     */	
	@Override
	public Map<String,Object> getApiKtFamily(Map<String, Object> params) {
		String   methodName = "getApiKtFamily";
		ResultVo rv         = new ResultVo();
		String   sBlnKt     = "N";
		Map<String,Object> retmap = new HashMap<String,Object>();
		Map<String,Object> reqmap = new HashMap<String,Object>();
		
        String sLogCd  = Constant.SUCCESS_CODE;
        String sLogMsg = Constant.SUCCESS_MSG;
		
		try {
			//log.debug(methodName, params.toString());
			
			// SHUB API호출
			rv = shubService.sendHttpPostRequest(params);
			
			// "Y"면 fp_fmly_supot insert
			sBlnKt = rv.getSucYn();
			
			if("Y".equals(sBlnKt)) {
				
				List<Map<String,Object>> list   = rv.getList();
				List<Map<String,Object>> toList = new ArrayList<Map<String,Object>>();
				String lsOwnerCtn = (String) params.get(SHUBHttpService.PARAM_SVC_NO);
				String lsOwnerYn = "N";
			
				int mecnt = 0;
				for (Map<String,Object> map : list){
					Map<String,Object> tmap = new HashMap<String,Object>();
					tmap.put("phub_tr_no", params.get("phub_tr_no"));
					tmap.put("cust_ctn"  , map.get(SHUBHttpService.PARAM_SVC_NO));
					tmap.put("cust_nm"   , map.get(SHUBHttpService.PARAM_CUST_NM));
					tmap.put("kt_cust_id", map.get(SHUBHttpService.PARAM_CUST_ID));
					
					if (StringUtil.nvl(tmap.get("cust_ctn")).equals(lsOwnerCtn)){
						tmap.put("own_yn", "Y");
						tmap.put("prgr_stat"  , Constant.CPN_REQ_PRGRS_PR120);
						mecnt++;
					} else {
						tmap.put("own_yn", "N");
						tmap.put("prgr_stat"  , Constant.CPN_REQ_PRGRS_PR000);
					}
					
					//쿠폰요청상태(초기값:NON [미요청])
					//지원상태(초기값: 본인인증 완료부터 시작[본인/가족 모두])
					tmap.put("req_stat_cd", "NON");
					
					toList.add(tmap);
					
				}
				
				// 본인이 리스트에 없을시 오류
				if(mecnt==0){
					retmap.put(Constant.RET_CODE, messageManage.getMsgCd ("BZ_ERROR_153"));
			        retmap.put(Constant.RET_MSG , messageManage.getMsgTxt("BZ_ERROR_153"));
			        return retmap;
				}
				
				reqmap.put("list", toList);
				dao.insertFmlySupot(reqmap);
				
			} else {
		        sLogCd  = rv.getRstCd();
		        sLogMsg = rv.getRstMsg();
		        
		        if(sLogCd.equals("0001") || sLogCd.equals("0003")) {
					sLogCd  = messageManage.getMsgCd("BZ_ERROR_153");
					sLogMsg = messageManage.getMsgTxt("BZ_ERROR_153");
		        }
		        
			}
			//log.debug(methodName, "☆☆☆☆☆☆☆☆=>  \n"+ rv.toString());
			
		} catch (Exception e) {
			sLogCd  = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg = messageManage.getMsgTxt("SY_ERROR_900");
		}
		
        retmap.put(Constant.RET_CODE, sLogCd);
        retmap.put(Constant.RET_MSG , sLogMsg);
		
		return retmap;
	}
	
	
	/**
     * @description 쿠폰 발행 전에 사전 정보 조회
     * @param  Map,
     * @return ResultVo
     * @description
     * 		1. 쿠폰발행을 위한 기본정보를 조회 한다.
     *      2. input parameter
     *         - service_id : [필수]서비스ID(일반서비스, 패밀리서비스 등)
     *         - phub_tr_no : [필수]포인트허브 거래번호
     *         - add_param  : [필수]추가파라미터
     *         - cust_ctn   : [필수]고객 후대전화
     *         - pay_amt    : [필수]쿠폰발행금액
     *         
     *      3. return
     *         resultVo.setSucYn("Y") : [String]결과(Y:성공, N:실패)
     *         resultVo.setMap()      : [Map]쿠폰발행 기초 정보
     *         resultVo.setRstCd	  : [String]실패코드
     *         resultVo.setRstMsg     : [String]실패메시지
     */	
	@Override
	public ResultVo getPreInfoB4CouponIssue(Map<String, Object> params) {
		//메소드명, 로그내용
		String methodName = "getPreInfoB4CouponIssue";
		log.debug(methodName, "Start!");
		//log.debug(methodName, "params=" + params.toString());
		
		String stepCd = "000";
		
		// declare
		ResultVo resultVo             = new ResultVo();
		Map<String,Object> retmap     = new HashMap<String,Object>();	// 리턴정보
		Map<String,Object> tmpMap     = new HashMap<String,Object>();	// temp맵
		
		// get
		String serviceId     = StringUtil.nvl(params.get("service_id"));	// 서비스ID(일반서비스, 패밀리서비스 등)
		String phubTrNo      = StringUtil.nvl(params.get("phub_tr_no"));	// 포인트허브 거래번호(구매자용)
		String ownerPhubTrNo = phubTrNo;									// 포인트허브 거래번호(오너용)
		String addParam      = StringUtil.nvl(params.get("add_param"));		// 추가파라미터
		String custCtn       = StringUtil.nvl(params.get("cust_ctn"));		// 구매고객 휴대번호
		String payAmt        = StringUtil.nvl(params.get("pay_amt"));		// 쿠폰발행금액
		//pg_tr_no
		
		
		// set local variable
		String successYn   = "N";	// 성공여부(Y/N)
    	String isOwnYn     = "N";
    	String familyYn    = "N";
    	String ownCustCtn  = "";
    	String ownKtCustId = "";
    	String ownCustNm   = "";
    	String ktCustId    = "";
    	String custNm      = "";
    	String eRstCd      = "";
    	String eRstMsg     = "";
		
    	try {
	        /*********************************************************************
	         * 패밀리포인트 서비스인 경우만
	         ********************************************************************/    		
	        if( serviceId.equals(Constant.SERVICE_ID_SVC_FP) ) {
	        	
		        /*********************************************************************
		         * 가족지원테이블 조회
		         * - 개통자에게는 가족지원요청(조르기) 가능하도록 리스트 조회팝업으로 유도
		         * - OWN_YN=Y이고 fp_fmly_supot테이블의 count > 1 인 경우만 조르기화면으로 이동 가능
		         * - 피요청자인 경우 요청자의 포인트허브거래번호로 조회를 한다.
		         *   (요청자 포인트허브거래번호는 ph_deal_req_info.add_param에 기록되어 있다.)
		         ********************************************************************/
	        	//log.debug(methodName, "bef ownerPhubTrNo=" + ownerPhubTrNo);
	        	
	        	// add_param이 ""인경우 요청자로 인지 한다.
	        	if( !"".equals(addParam) ) {
	        		tmpMap.clear();
	        		tmpMap.put("add_param", addParam);
	        		tmpMap.put("key",       "phubTrno");
	        		ownerPhubTrNo = StringUtil.nvl(this.getAddParamElmtVal(tmpMap), ownerPhubTrNo);
	        	}
	        	//log.debug(methodName, "aft ownerPhubTrNo=" + ownerPhubTrNo);
	        	
	        	// 가족지원 테이블 조회
	        	tmpMap.clear();
	        	tmpMap.put("phubTrNo", ownerPhubTrNo);
	        	List<Map<String, Object>> fmlyList = this.getFpFmlySupotListByPhubTrNo(tmpMap);
	        	//log.debug(methodName, "getFpFmlySupotListByPhubTrNo=" + JsonUtil.toJson(fmlyList));
	        	
	        	// 패밀리목록의 오너여부 체크(OWN_YN=Y AND CPN_NO IS NULL)
	        	if( !fmlyList.isEmpty() && (fmlyList.size() > 0) ) {
		        	for(Map fmlyMap : fmlyList) {
		        		if( "Y".equals(StringUtil.nvl(fmlyMap.get("own_yn"))) ) {
		        			ownKtCustId = StringUtil.nvl(fmlyMap.get("kt_cust_id"));	// 오너 KT고객ID
		        			ownCustNm   = StringUtil.nvl(fmlyMap.get("cust_nm"));		// 오너 고객명
		        			ownCustCtn  = StringUtil.nvl(fmlyMap.get("cust_ctn"));		// 오너 휴대전화 번호
		        			
		        			// 오너의 휴대번호와 나의휴대번호가 일치하면 내가 오너이다.
		        			if( ownCustCtn.equals(custCtn) ) {
			        			isOwnYn = "Y";		// 오너여부
		        			}
		        		}
		        		
		        		// 나의 KT고객ID 추출
	        			if( custCtn.equals(StringUtil.nvl(fmlyMap.get("cust_ctn"))) ) {
	        				ktCustId = StringUtil.nvl(fmlyMap.get("kt_cust_id"));	// 구매고객 KT고객ID
	        				custNm   = StringUtil.nvl(fmlyMap.get("cust_nm"));		// 구매고객명
	        			}
		        	}		        		
	        	}
	        	
	        	
		        /*********************************************************************
		         * retmap setting
		         ********************************************************************/	        	
	        	// 오너이고, 가족이 있으면 조르기 화면으로 이동 가능
	        	if( "Y".equals(isOwnYn) && (fmlyList.size() > 1) ) {
	        		familyYn = "Y";
	        		//retmap.put("return_url", "https://tb.pointhub.clipwallet.co.kr/phub/fp/familyList.do");
	        		retmap.put("return_url", sysPrmtManage.getSysPrmtVal("PH_DOMAIN") + "/phub/fp/familyList.do");
	        	}
	        	
	        	// set etc
	        	retmap.put("ownYn",         isOwnYn);
	        	retmap.put("familyYn",      familyYn);
	        	retmap.put("ownKtCustId",   ownKtCustId);
	        	retmap.put("ownCustNm",     ownCustNm);
	        	retmap.put("ownCustCtn",    ownCustCtn);
	        	retmap.put("ownerPhubTrNo", ownerPhubTrNo);
	        	retmap.put("ktCustId",      ktCustId);
	        	retmap.put("cust_id",       StringUtil.nvl(params.get("cust_id")));
	        	retmap.put("custNm",        custNm);
	        	retmap.put("cust_ctn",      custCtn);
	        	retmap.put("phub_tr_no",    phubTrNo);
	        	retmap.put("pay_amt",       payAmt);
	        	retmap.put("add_param",     addParam);
	        	
		        /*********************************************************************
		         * 성공
		         ********************************************************************/
				successYn = "Y";
				log.debug(methodName, "finish!");	        	
	        }
					
		} catch (Exception e) {
			// write log
			log.printStackTracePH(methodName, e);
        	log.error(methodName, e.getMessage());
        	log.error(methodName, "Exception stepCd=" + stepCd);
        	
        	// 실패
        	successYn = "N";
        	eRstCd  = messageManage.getMsgCd("SY_ERROR_900");
        	eRstMsg = messageManage.getMsgTxt("SY_ERROR_900");
        	
		}
		
		// set resultVo
		resultVo.setSucYn(successYn);
		resultVo.setMap(retmap);
		resultVo.setRstCd(eRstCd);
		resultVo.setRstMsg(eRstMsg);
		
		// 리턴
		//log.debug(methodName, "resultVo="+resultVo.toString());
		return resultVo;
	}
	
	
    /**
     * @description 포인트허브 결제성공 후 쿠폰 발행
     * @param       Map HttpServletRequest, HttpServletRequest
     * @return      Map
     */	
	@SuppressWarnings("unchecked")
	@Override
	public ResultVo issueCoupon(Map<String, Object> params) {
		//메소드명, 로그내용
		String methodName = "issueCoupon";
		log.debug(methodName, "Start!");
		log.debug(methodName, "params=" + params.toString());
		
		// declare
		ResultVo resultVo             = new ResultVo();
		Map<String,Object> retmap     = new HashMap<String,Object>();	// 리턴정보
		Map<String,Object> mmsFormMap = new HashMap<String,Object>();	// mms양식 정보
		Map<String,Object> tmpMap     = new HashMap<String,Object>();	// temp맵
		Map cpnMap2                   = new HashMap();					// 쿠폰상태조회 맵
		
		// pg/pay에서 호출되는 경우 세션사용 불가 - 2019-02-21. lys
		String stepCd = "000";
		
		// TODO : param
		// get
		//String addParam      = StringUtil.nvl(params.get("add_param"));			// add_param
		String familyYn      = StringUtil.nvl(params.get("familyYn"));			// 결합가입자여부(Y/N)
		String ownYn         = StringUtil.nvl(params.get("ownYn"));				// 오너여부(Y/N)
		String ownKtCustId   = StringUtil.nvl(params.get("ownKtCustId"));		// 오너KT고객ID
		String ownCustNm     = StringUtil.nvl(params.get("ownCustNm"));			// 오너KT고객명
		String ownCustCtn    = StringUtil.nvl(params.get("ownCustCtn"));		// 오너 휴대번호
		String ownerPhubTrNo = StringUtil.nvl(params.get("ownerPhubTrNo"));		// 오너 포인트허브거래번호
		String phubTrNo      = StringUtil.nvl(params.get("phub_tr_no"));		// 포인트허브 거래번호
		String custCtn       = StringUtil.nvl(params.get("cust_ctn"));			// 쿠폰구매고객 휴대번호
		String payAmt        = StringUtil.nvl(params.get("pay_amt"));			// 쿠폰금액
		String custId        = StringUtil.nvl(params.get("cust_id"));			// 쿠폰구매고객 포인트허브고객ID
		String custNm        = StringUtil.nvl(params.get("custNm"));			// 쿠폰구매고객명
		String ktCustId      = StringUtil.nvl(params.get("ktCustId"));	    	// 쿠폰구매고객 KT고객ID
		
		// set method variable
		String successYn  = "N";	// 성공여부(Y/N)
		String eRstCd     = "";		// 리턴코드
		String eRstMsg    = "";		// 리턴메시지
		String cpnIsueYn  = "N";	// 쿠폰발행여부
		String mmsMsgId   = "";		// 메시지양식ID
		String pinNo      = "";		// 핀번호(쿠폰번호)
		String phubCpnNo  = "";		// 포인트허브 쿠폰번호
		String efctStrtDd = "";		// 쿠폰 유효시작일자(YYYYMMDD)
		String efctEndDd  = "";		// 쿠폰 유효종료일자(YYYYMMDD)
		String ctrId      = "";		// 기프티쇼 거래번호
		
		// 기프티쇼상품ID를 상품테이블에서 쿠폰금액으로 조회
		// G00000102473 / KT 5G 단말 10,000원 할인권 / 10,000
		stepCd = "001";
		tmpMap.clear();
		String goodsId  = "";
		
		try {
			
			/*********************************************************************
	         * 1. 상품ID 찾기 
	         ********************************************************************/
			tmpMap = this.getGiftiShowGoodsInfoByAmt(payAmt);
			
			if (Common.isNull(tmpMap) || tmpMap.isEmpty()){
				// 요청하신 금액의 단말할인권은 구매하실 수 없습니다.
				throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_167"));
			}
			
			goodsId  = StringUtil.nvl(tmpMap.get("goods_cd"));
			
	        /*********************************************************************
	         * 2. MMS 메시지 양식 조회 
	         *   - 대상별로 양식이 달라진다.
			 * MMS_ID_FP_ISU_ALON : 쿠폰발송 MMS (비패밀리고객(KT단독고객))
			 * MMS_ID_FP_ISU_FMLY : 쿠폰발송 MMS (패밀리용)
			 * MMS_ID_FP_ISU_GIFT : 쿠폰발송 선물 MMS
	         ********************************************************************/
			// 유형 분기
			stepCd = "002";
			if( "Y".equals(ownYn) ) {
				if( "Y".equals(familyYn) ) {
					// 쿠폰발송 MMS (패밀리용)
					mmsMsgId = sysPrmtManage.getSysPrmtVal(Constant.MMS_ID_FP_ISU_FMLY);
				} else {
					// 쿠폰발송 MMS (비패밀리고객(KT단독고객))
					mmsMsgId = sysPrmtManage.getSysPrmtVal(Constant.MMS_ID_FP_ISU_ALON);
				}
			} else {
				// 쿠폰 선물 MMS
				mmsMsgId = sysPrmtManage.getSysPrmtVal(Constant.MMS_ID_FP_ISU_GIFT);
			}
			
			stepCd = "003";
			/*********************************************************************
	         * 3. 메시지양식 조회 
	         ********************************************************************/
			tmpMap = new HashMap<String, Object>();
			tmpMap.put("phubTrNo",  phubTrNo);
			tmpMap.put("ownerPhubTrNo",  ownerPhubTrNo);
			tmpMap.put("custNm",    custNm);
			tmpMap.put("ownCustNm", ownCustNm);
			tmpMap.put("cpnAmt",    payAmt);
			tmpMap.put("smsMsgId",  mmsMsgId);
			//log.debug(methodName, "getMmsMsgInfo params=" + tmpMap.toString());
			mmsFormMap = this.getMmsMsgInfo(tmpMap);
			
			
	        /*********************************************************************
	         * 4. sendHttpPostGiftiShowCouponSend
	         *   - 쿠폰 발송 요청 (0455) POST
	         ********************************************************************/			
			//// MMS 발송문구는 요청자와 피요청자가 다르다.
			stepCd = "004";
			Map cpnMap1 = new HashMap();
			cpnMap1.put("msg",      mmsFormMap.get("msg_body"));				// 전송메시지
			cpnMap1.put("title",    mmsFormMap.get("msg_titl"));				// 제목
			cpnMap1.put("sender",   custCtn);									// 구매자의 휴대번호를 셋팅한다.
			cpnMap1.put("receiver", StringUtil.nvl(params.get("ownCustCtn")));	// 오너의 수신번호
			cpnMap1.put("goods_id", goodsId);									// 기프티쇼 상품아이디
			cpnMap1.put("tr_id",    StringUtil.nvl(params.get("phub_tr_no")));	// 거래아이디(포인트허브 거래ID)
			cpnMap1.put("goods_id", goodsId);
			
			// call API
			stepCd = "041";
			ResultVo resultVo1 = giftiShowRestHttpService.sendHttpPostGiftiShowCouponSend(cpnMap1);
			if( "Y".equals(StringUtil.nvl(resultVo1.getSucYn(), "N")) ) {
				// 전문 결과값 수신
				stepCd = "042";
				Map cpnResultMap1 = resultVo1.getMap();
				
				// response sample data
				/**
				핀수신 (gubun - Y)  
				{  "resCode": "COUPONS.2000",  "resMsg": "구매등록성공",  
				"pinNo": "90012345678",  "trId": "GT2016000002",  "ctrId": "C6033113460677410001"  }
				*/
				pinNo = StringUtil.nvl(cpnResultMap1.get("pinNo"));		// 기프티쇼 핀번호(쿠폰번호)
				ctrId = StringUtil.nvl(cpnResultMap1.get("ctrId"));		// 기프티쇼 구매번호
				cpnIsueYn = "Y";										// 쿠폰발행 성공
				
			} else {
				// 쿠폰생성에 실패하였습니다.(기프티쇼 생성 실패!)
				log.error(methodName, "sendHttpPostGiftiShowCouponSend(0455) Fail!=" + messageManage.getMsgTxt("BZ_ERROR_175"));
				throw new BizException(stepCd, messageManage.getMsgCd("BZ_ERROR_175"), StringUtil.nvl(resultVo1.getRstMsg()));
			}
			
			
	        /*********************************************************************
	         * 5. sendHttpPostGiftiShowCoupon
	         *   - 단일 쿠폰 상세 정보 (0451) GET
	         ********************************************************************/
			stepCd = "005";
			cpnMap2.put("tr_id",  StringUtil.nvl(params.get("phub_tr_no")));	// 거래아이디(포인트허브 거래ID)
			cpnMap2.put("pin_no", pinNo);										// 기프티쇼 핀번호(쿠폰번호)
			
			// call API
			stepCd = "051";
			ResultVo resultVo2 = giftiShowRestHttpService.sendHttpPostGiftiShowCoupon(cpnMap2);
			if( "Y".equals(StringUtil.nvl(resultVo2.getSucYn(), "N")) ) {
				// 전문 결과값 수신
				stepCd = "052";
				Map cpnResultMap2        = resultVo2.getMap();
				log.debug(methodName, "cpnResultMap2=" + JsonUtil.toJson(cpnResultMap2));

				// response sample data
				/**
				{  "resCode": "0000",  
				   "resMsg": "정상처리",  
				   "couponInfoList": [  
				           {  "brandNm": "스타벅스",  
				        	  "cnsmPriceAmt": "4100",  
				        	  "correcDtm": "20170628",  
				        	  "goodsCd": "G00000008062",  
				        	  "goodsNm": "아이스 카페아메리카노 Tall",  
				        	  "mmsBrandThumImg": "20170209_091803677.jpg",  
				        	  "pinNo": "900348036023",  
				        	  "pinStatusCd": "01",  
				        	  "pinStatusNm": "발행",  
				        	  "recverTelNo": "01067171507",  
				        	  "sellPriceAmt": "3947",  
				        	  "sendBasicCd": "2017062800000005368001",  
				        	  "sendRstCd": "1000",  
				        	  "sendRstMsg": "Success",  
				        	  "sendStatusCd": "발송완료",  
				        	  "senderTelNo": "15886474",  
				        	  "validPrdEndDt": "20170928235959"  }  ]  } 
				*/
				boolean isCpnIssue = true;
				
				// 쿠폰상태체크
				stepCd = "053";
				if( !"01".equals(StringUtil.nvl(cpnResultMap2.get("pinStatusCd"))) ) {
					// 쿠폰생성에 실패하였습니다.(기프티쇼 쿠폰상태 이상(StringUtil.nvl(cpnResultMap2.get("pinStatusCd"))))
					log.debug(methodName, "pinStatusCd=" + StringUtil.nvl(cpnResultMap2.get("pinStatusCd")));
					isCpnIssue = false;
				}
				// 쿠폰금액체크
				stepCd = "054";
				if( !StringUtil.nvl(cpnResultMap2.get("cnsmPriceAmt"), "0").equals(StringUtil.nvl(params.get("pay_amt"), "0"))  ) {
					// 쿠폰생성에 실패하였습니다.(기프티쇼 쿠폰금액 이상(StringUtil.nvl(cpnResultMap2.get("pinStatusCd"))))
					log.debug(methodName, "cnsmPriceAmt=" + StringUtil.nvl(cpnResultMap2.get("cnsmPriceAmt")));
					log.debug(methodName, "pay_amt=" + StringUtil.nvl(params.get("pay_amt"), "0"));
					isCpnIssue = false;
				}
				
				// 핀번호 체크
				stepCd = "055";
				pinNo  = StringUtil.nvl(cpnResultMap2.get("pinNo"));
				if( "".equals(pinNo)  ) {
					// 쿠폰생성에 실패하였습니다.(기프티쇼 핀번호 이상)
					log.debug(methodName, "pinNo=" + StringUtil.nvl(cpnResultMap2.get("pinNo")));
					isCpnIssue = false;
				}
								
				// 유효성체크 실패 체크
				if( !isCpnIssue ) {
					// 쿠폰유효성체크 실패 : 쿠폰발행에 실패하였습니다.(기프티쇼 쿠폰정보 이상)
					stepCd = "056";
					throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_169"), cpnResultMap2);
				} else {
					// 유효기간
					efctStrtDd = StringUtil.nvl(cpnResultMap2.get("sendBasicCd")).substring(0, 8);
					efctEndDd  = StringUtil.nvl(cpnResultMap2.get("validPrdEndDt")).substring(0, 8);					
				}
				
			} else {
				// TODO
				// 쿠폰생성에 실패하였습니다.(기프티쇼 상세정보 조회 실패!)
				log.error(methodName, "sendHttpPostGiftiShowCoupon(0451) Fail!=" + messageManage.getMsgTxt("BZ_ERROR_168"));
				throw new BizException(stepCd, messageManage.getMsgCd("BZ_ERROR_168"), StringUtil.nvl(resultVo2.getRstMsg()));
			}

			
	        /*********************************************************************
	         * 6. insert
	         *   - PH_CPN_ISUE_INFO, PH_CPN_CHNG_HIST, FP_GIFTI_SHOW_INFO
	         ********************************************************************/
			// 6.1. PH_CPN_ISUE_INFO
			stepCd = "006";
			Map cpnIsueMap = new HashMap();
			cpnIsueMap.put("pinNo",        pinNo);						// 핀번호
			cpnIsueMap.put("cpnAmt",       payAmt);						// 쿠폰금액
			cpnIsueMap.put("cpnTypId",     Constant.CPN_TYP_ID_FP001);	// 쿠폰종류(FP001 : 패밀리포인트쿠폰01)
			cpnIsueMap.put("phubTrNo",     phubTrNo);					// 포인트허브거래번호
			cpnIsueMap.put("ktCustId",     ktCustId);					// KT고객ID
			cpnIsueMap.put("ownKtCustId",  ownKtCustId);				// 쿠폰소유자 KT고객ID
			cpnIsueMap.put("custCtn",      custCtn);					// 고객휴대번호(쿠폰을 구매한 고객의 휴대번호를 셋팅해야 한다)
			cpnIsueMap.put("efctStrtDd",   efctStrtDd);					// 유효시작일자
			cpnIsueMap.put("efctEndDd",    efctEndDd);					// 유효종료일자
			cpnIsueMap.put("cpnStatCd",    Constant.CPN_STAT_2000);		// 쿠폰상태코드(2000	미사용, 2100	사용취소(미사용), 4000	사용완료, 9000	발행취소(환불))
			cpnIsueMap.put("custId",       custId);						// 포인트허브 고객ID
			// insert
			dao.insertPhCpnIsueInfo(cpnIsueMap);
			phubCpnNo = StringUtil.nvl(cpnIsueMap.get("phubCpnNo"));	// 포인트허브 쿠폰번호
			
			
			// 6.2. PH_CPN_CHNG_HIST
			stepCd = "062";
			Map cpnHistMap = new HashMap();
			cpnHistMap.put("chngTypCd", Constant.CHNG_TYP_CD_ISSUE);	// 변경유형(ISSUE:발행, USE:쿠폰사용, CANCEL:쿠폰사용취소, REFUND:쿠폰발행취소(환불), ETC:기타)
			cpnHistMap.put("phubTrNo",  phubTrNo);						// 포인트허브 거래번호
			cpnHistMap.put("pinNo",     pinNo);							// 핀번호
			// insert
			dao.insertCpnHist(cpnHistMap);
			
			
			// 6.3. FP_GIFTI_SHOW_INFO
			stepCd = "063";
			Map giftInfoMap = new HashMap();
			giftInfoMap.put("phubCpnNo", phubCpnNo);	// 포인트허브 쿠폰번호
			giftInfoMap.put("ctrId",     ctrId);		// 기프티쇼 구매번호
			giftInfoMap.put("goodsId",   goodsId);		// 기프티쇼 상품ID
			giftInfoMap.put("phubTrNo",  phubTrNo);		// 포인트허브 거래번호
			// insert
			dao.insertFpGiftiShowInfo(giftInfoMap);
			
			
	        /*********************************************************************
	         * 7. update
	         *   - FP_FMLY_SUPOT
			 * Caller 에서 처리(2019.03.28)
	         ********************************************************************/
			stepCd = "007";
			/*
			Map fmlSupMap = new HashMap();
			fmlSupMap.put("phubTrNo",    ownerPhubTrNo);	// 오너의 포인트허브 거래번호
			fmlSupMap.put("custCtn",     custCtn);			// 고객 휴대번호(쿠폰을 구매한 고객의 휴대번호를 셋팅해야 한다)
			fmlSupMap.put("phub_cpn_no", phubCpnNo);		// 포인트허브 쿠폰번호
			fmlSupMap.put("prgr_stat"  , "PR200");		    // 상태 저장
			
			dao.updateFmlySupot(fmlSupMap);
			*/
			
	        /*********************************************************************
	         * 8. sendHttpPostGiftiShowCouponReSend
	         *   - 쿠폰 재전송 (0454) POST
	         ********************************************************************/			
			// 8.1. call API
			stepCd = "008";
			tmpMap = new HashMap<String, Object>();
			ResultVo resultVo4 = giftiShowRestHttpService.sendHttpPostGiftiShowCouponReSend(cpnMap2);
			// response sample data
			/**
			{  "resCode": "0000",  "resMsg": "정상처리"  }
			*/
			stepCd = "081";
			if( "Y".equals(StringUtil.nvl(resultVo4.getSucYn(), "N")) ) {
				// 성공
				tmpMap.put("rstCd",  Constant.SUCCESS_CODE);
				tmpMap.put("rstMsg", Constant.SUCCESS_MSG);
			} else {
				// 실패
				tmpMap.put("rstCd",  StringUtil.nvl(resultVo4.getRstCd()));
				tmpMap.put("rstMsg", StringUtil.nvl(resultVo4.getRstMsg()));
				
				// TODO
				// 쿠폰 MMS발송에 실패하였습니다.(기프티쇼 MMS발송 실패!)
				log.error(methodName, "sendHttpPostGiftiShowCouponReSend(0454) Fail!=" + messageManage.getMsgTxt("BZ_ERROR_176"));
				throw new BizException(stepCd, messageManage.getMsgCd("BZ_ERROR_176"), StringUtil.nvl(resultVo4.getRstMsg()));				
			}
			
			// 8.2. MMS전송이력 등록
			stepCd = "082";
			tmpMap.put("mmsFormMap"		, mmsFormMap);
			tmpMap.put("phubTrNo"		, phubTrNo);
			tmpMap.put("phubCpnNo"		, phubCpnNo);
			
			/*
			 * own(Y) 구매 시 send, recv 요청자
			 * own(N) 구매 시 send 피요청자, recv 요청자
			 * */
			if( "Y".equals(ownYn) ) {
				tmpMap.put("recvCustNm"		, ownCustNm);
				tmpMap.put("recvCustCtn"	, ownCustCtn);
				tmpMap.put("sendCustNm"		, ownCustNm);
				tmpMap.put("sendCustCtn"	, ownCustCtn);
			} else {
				tmpMap.put("recvCustNm"		, ownCustNm);
				tmpMap.put("recvCustCtn"	, ownCustCtn);
				tmpMap.put("sendCustNm"		, custNm);
				tmpMap.put("sendCustCtn"	, custCtn);
			}
			
			ResultVo resultVo5 = this.regPhMmsSendHist(tmpMap);
			if( !"Y".equals(StringUtil.nvl(resultVo5.getSucYn(), "N")) ) {
				// TODO : 이력등록실패를 에러로 볼것인가?
				// MMS발송이력 등록에 실패하였습니다.
				log.error(methodName, "regPhMmsSendHist Fail!=" + messageManage.getMsgTxt("BZ_ERROR_170"));
				log.error(methodName, "ErrCode=" + StringUtil.nvl(resultVo5.getRstCd()));
				log.error(methodName, "ErrMsg="  + StringUtil.nvl(resultVo5.getRstMsg()));
			}

			
	        /*********************************************************************
	         * 성공
	         ********************************************************************/
			successYn = "Y";
			log.debug(methodName, "finish!");
			
		} catch (BizException be) {
			// write log
			log.error(methodName, "BizException code=" + StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg =" + StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));
			
			// 실패
			successYn = "N";
			eRstCd  = StringUtil.nvl(be.getErrCd());
			eRstMsg = StringUtil.nvl(be.getErrMsg());		
		} catch (Exception e) {
			// write log
			log.printStackTracePH(methodName, e);
        	log.error(methodName, e.getMessage());
        	log.error(methodName, "Exception stepCd=" + stepCd);
        	
        	// 실패
        	successYn = "N";
        	eRstCd  = messageManage.getMsgCd("SY_ERROR_900");
        	eRstMsg = messageManage.getMsgTxt("SY_ERROR_900");
        	
		} finally {
			// TODO
			// 실패시 유형에 따라 비지니스 정의가 필요 하다.
			// 1. 고객에게 쿠폰발송(MMS발송)은 가장 마지막 단계에서 진행 한다.
			// 2. 에러 발생시 finally 쪽에서 쿠폰생성까지 되었으며, 발행취소 전문을 호출해야 한다.
			// 3. MMS발송이력 등록하는 것은 별도의 transaction으로 처리?
			
			// 에러 발생시 쿠폰생성까지 되었으며, 발행취소 전문을 호출해야 한다.!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			//log.debug(methodName, "+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			//log.debug(methodName, "++++++++++++++++++++++++  확인필수  +++++++++++++++++++++++++++++++");
			//log.debug(methodName, "+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			
			
			if( "N".equals(successYn) && "Y".equals(cpnIsueYn) ) {
				ResultVo resultVofin = this.sendGiftiShowCouponCancelAPI(cpnMap2);
								
				if( !"Y".equals(StringUtil.nvl(resultVofin.getSucYn(), "N")) ) {
					// TODO - 중요에러다!!!!!!!!
					// 쿠폰발행취소에 실패하였습니다.(기프티쇼 쿠폰취소 실패!)
					log.error(methodName, "finally CouponCancelAPI Fail=" + messageManage.getMsgTxt("BZ_ERROR_171"));
		        	log.error(methodName, "finally CouponCancelAPI Fail Code=" + resultVofin.getRstCd());
		        	log.error(methodName, "finally CouponCancelAPI Fail Msg=" + resultVofin.getRstMsg());
		        	
				}				
			}
		}
		
		// return
		resultVo.setPhubCpnNo(phubCpnNo);
		resultVo.setSucYn(successYn);
		resultVo.setRstCd(eRstCd);
		resultVo.setRstMsg(eRstMsg);
		
		// 리턴
		log.debug(methodName, "resultVo="+ JsonUtil.toJson(resultVo));
		return resultVo;
	}
	
    /**
     * @description 패밀리포인트 쿠폰 결제화면 초기화(약관조회,본인인증진입점)
     * @param  Map HttpServletRequest,
     * @return List
     * @throws BizException 
     */	
	@SuppressWarnings({ "unused", "finally" })
	@Override
	public Map<String,Object> getPresentList(Map<String, Object> params, HttpServletRequest request) throws BizException {
		String methodName = "getPresentList";
		String stepCd     = "001";
		Map<String,Object> retmap  = new HashMap<String,Object>();
		Map<String,Object> clsmap  = new HashMap<String,Object>();
        String sLogCd    = Constant.SUCCESS_CODE;
        String sLogMsg   = Constant.SUCCESS_MSG;
        String sPhubTrNo = StringUtil.nvl(params.get("phub_tr_no"));
        
        SbankDealVO  voDeal = new SbankDealVO();
        
		retmap.put(Constant.RET_CODE , sLogCd);
		retmap.put(Constant.RET_MSG  , sLogMsg);
		
        try {
    		/*
    		 * 약관조회-본인인증 후 선물확인 화면으로 전환되므로 거래정보(SbankDealVO) 최소화
    		 * 모델뷰는 본인 쿠폰 발행 후 조회되는 데이타와 동일한 정보를 사용함.
    		 * */
        	
    		/************************************************************************ 
    		 * 1.SbankDealVO Setting
    		 * [비결제 조회건 표시, 개통자임 표시, 거래번호(fp_fmly_supot테이블 조회조건)] 
    		 ************************************************************************/
            voDeal = SessionUtils.getSbankDealVO(request);
            voDeal.setPayYn("N");
            voDeal.setOwnerYn("Y");
            voDeal.setpHubTrNo(sPhubTrNo);
            
            SessionUtils.setSbankDealVO(request, voDeal);        	
        	
    		/************************************************************************ 
    		 * 2.약관내용 셋팅
    		 ************************************************************************/            
        	clsmap.put("phub_tr_no", sPhubTrNo);
        	clsmap.put("pg_tr_no"  , "");
        	clsmap.put("service_id", Constant.SERVICE_ID_SVC_FP);
        	clsmap.put("owner_yn"  , voDeal.getOwnerYn());
        	
        	Map<String, Object> result = clipService.getClaUses(clsmap);
        	
			retmap.put("clsList"     , result.get("clsList"));
			retmap.put("cardInfoList", result.get("cardInfoList"));
			
			
        } catch (BizException be) {
			sLogCd  = be.getErrCd();
			sLogMsg = be.getErrMsg();
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			sLogCd  = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg = e.getMessage();
			
			throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));
			
		} finally {
			params.put("req_data", JsonUtil.MapToJson(params));
			params.put("res_data", "");			
            params.put("phub_tr_no", sPhubTrNo);
			params.put(Constant.RET_CODE, sLogCd);
			params.put(Constant.RET_MSG , sLogMsg);
			
			clipService.logRequestFromOut(params, request, methodName);	
		}
        
		retmap.put(Constant.RET_CODE , sLogCd);
		retmap.put(Constant.RET_MSG  , sLogMsg);
		
        return retmap;
	}
	
	
    /**
     * <pre>패밀리포인트 가족지원테이블 검색(PH거래번호별))</pre>
     * @see <pre>PH거래번호(PHUB_TR_NO)로 가족지원테이블(FP_FMLY_SUPOT)을 조회 한다. </pre>
     * @author lys
     * @since  2019.02.21
     * @param  Map 
     * @return List
     * @description
     */		
	@Override
	public List<Map<String, Object>> getFpFmlySupotListByPhubTrNo(Map<String, Object> params) {
		// SELECT FP_FMLY_SUPOT
		return dao.getFpFmlySupotListByPhubTrNo(params);
	}

	/* (non-Javadoc)
	 * @see com.olleh.pointHub.api.sdk.service.FamilyPointService#getReqStatCd(java.util.Map)
	 * 요청 상태 코드 조회
	 */
	@Override
	public String getReqStatCd(Map<String, Object> params) {
		return dao.getReqStatCd(params);
	}

	/* (non-Javadoc)
	 * @see com.olleh.pointHub.api.sdk.service.FamilyPointService#updateReqStatCd(java.util.Map)
	 * 요청 상태 코드 업데이트
	 */
	@Override
	public int updateReqStatCd(Map<String, Object> params) {
		return dao.updateReqStatCd(params);
	}
	
    /**
     * <pre>가족지원테이블 검색전 체크</pre>
     * @see <pre>가족지원테이블 검색전 권한 등을 체크한다.</pre>
     * @author sci
     * @since  2019.03.04
     * @param  Map (phub_tr_no)
     * @return Map
     * @description
     */		
    @Override
	public Map<String,Object> checkCountOwner(Map<String, Object> params) {
		String methodName = "getFamilyPresents";
		String stepCd = "001";
		
		Map<String,Object> retmap = new HashMap<String,Object>();
		Map<String,Object> map    = new HashMap<String,Object>();
		
		String phubTrNo	= StringUtil.nvl(params.get("phub_tr_no"));
		String pgTrNo	= StringUtil.nvl(params.get("pg_tr_no"));
		
		String sRetCode = Constant.SUCCESS_CODE;
		String sRetMsg  = Constant.SUCCESS_MSG;
		
		CertificationVO voCert = SessionUtils.getCertificationVO(SystemUtils.getCurrentRequest());
		
		// 파라미터 유효성 체크
		if(StringUtil.isNull(phubTrNo)){
			sRetCode = "IF_INFO_103";			
			retmap.put(Constant.RET_CODE, sRetCode);
			return retmap;
		}
		// 세션 체크
		if (Common.isNull(voCert)){
			sRetCode = "SY_INFO_901";			
			retmap.put(Constant.RET_CODE, sRetCode);
			return retmap;
		}
		
		/**********************************************************************
		 * 순차적으로 파라미터 입력 후 메시지 세분화(why should i do) 
		**********************************************************************/
		String lsCnt = "0";
		int    liAllcnt = 0;
		
		map.put("phub_tr_no", phubTrNo);
		lsCnt = dao.checkCountOwner(map);
		
		liAllcnt = Integer.valueOf(lsCnt); 
				
		if("0".equals(lsCnt)){
			// 거래번호 유효하지 않음
			sRetCode = "BZ_INFO_149";			
			retmap.put(Constant.RET_CODE, sRetCode);
			return retmap;
		}
		
		map.put("cust_ctn" , voCert.getPhoneNo());
		lsCnt = dao.checkCountOwner(map);
		if("0".equals(lsCnt)){
			// 개통자만 이용가능한 서비스 입니다.
			sRetCode = "BZ_ERROR_174";
			retmap.put(Constant.RET_CODE, sRetCode);
			return retmap;			
		}
		
		map.put("own_yn" , "N");
		lsCnt = dao.checkCountOwner(map);
		if("1".equals(lsCnt)){
			// 개통자만 이용가능한 서비스 입니다.
			sRetCode = "BZ_ERROR_174";
			retmap.put(Constant.RET_CODE, sRetCode);
			return retmap;
		}
		
		map.put("own_yn" , "Y");
		lsCnt = dao.checkCountOwner(map);
		
		if ("1".equals(lsCnt)) {
			if (1 == liAllcnt) {
				// 지원요청(조르기)할 가족이 없습니다.
				sRetCode = "BZ_ERROR_178";
				retmap.put(Constant.RET_CODE, sRetCode);
				return retmap;
			}
			
		}
		
		retmap.put(Constant.RET_CODE, sRetCode);
		retmap.put(Constant.RET_MSG , sRetMsg);
		
		return retmap;
	}	
	
    
    /**
     * <pre>PH_DEAL_REQ_INFO.ADD_PARAM Parser</pre>
     * @see <pre>ADD_PARAM의 json 데이터를 Parser</pre>
     * @author lys
     * @since  2019.02.21
     * @param  Map 
     * @return List
     * @description
     * 		1. 파라미터로 넘어온 key값에 해당하는 value값을 찾아서 리턴 한다.
     */	
	@Override
	public String getAddParamElmtVal(Map<String, Object> params) {
		// declare
		Map map = new HashMap();
		
		// set
		String addParam = StringUtil.nvl(params.get("add_param"));
		String key      = StringUtil.nvl(params.get("key"));
		String val      = "";
		
		if( !"".equals(addParam) ) {
			map = JsonUtil.JsonToMap(addParam);
			
			// key값이 null이 아닐때만.
			if( !"".equals(key) ) {
				val = StringUtil.nvl(map.get(key));
			}
		}
		
		return val;
	}

	@Override
	public String getCountFlmySupot(Map<String, Object> params) {
		Map<String,Object> map = new HashMap<String,Object>();
		map = dao.getCountFlmySupot(params);
		return  StringUtil.nvl(map.get("cnt"),"0");
	}
	
	@Override
	public String checkFmlySupotMustBeZero(Map<String, Object> params) {
		String sRet = "";
		sRet = dao.checkFmlySupotMustBeZero(params);
		return  sRet;
	}
	
	@Override
	public String checkOwnerMustBeOne(Map<String, Object> params) {
		String sRet = "";
		sRet = dao.checkOwnerMustBeOne(params);
		return  sRet;
	}
	
	
    /**
     * @description MMS메시지 조회(Parser)
     * @param  Map HttpServletRequest,
     * @return Map
     * @description
     * 		1. 메시지유형테이블에서 메시지ID에 해당하는 메시지정보를 조회 한다.
     *      2. 메시지전문 데이터의 변수값들을 파라미터값들로 변환 시킨다.
     *         (메시지전문 데이터를 등록할때 변수들은 정의된 변수만 사용이 가능한다.)  
     */	
	@Override
	public Map<String,Object> getMmsMsgInfo(Map<String, Object> params) {
		String methodName = "getMmsMsgInfo";
		String stepCd     = "001";
		
		// declare
		Map<String,Object> tmpMap  = new HashMap<String,Object>();
		Map<String,Object> mmsMap  = new HashMap<String,Object>();
		
        try {
        	// set param
        	String msgTitlKey 	 = "msg_titl";
        	String msgBodyKey 	 = "msg_body";
        	String phubTrNo      = StringUtil.nvl(params.get("phubTrNo"));
        	String ownerPhubTrNo = StringUtil.nvl(params.get("ownerPhubTrNo"));
        	String custNm     	 = StringUtil.nvl(params.get("custNm"));
        	String ownCustNm  	 = StringUtil.nvl(params.get("ownCustNm"));
        	String cpnAmt      	 = StringUtil.nvl(params.get("cpnAmt"));
        	
    		/************************************************************************ 
    		 * 1. 메시지유형 조회 
    		 *    PH_MMS_FORM_INFO
    		 ************************************************************************/
        	tmpMap.put("mmsMsgId", StringUtil.nvl(params.get("smsMsgId")));		// 메시지유형ID
        	mmsMap = dao.getMmsFormInfoByMsgId(tmpMap);
        	
        	
    		/************************************************************************ 
    		 * 2. 메시지전문 데이터 변수 Parse
    		 *    - ${phubTrNo}  : 포인트허브 거래번호
    		 *    - ${custNm}    : 고객명
    		 *    - ${ownCustNm} : 오너 고객명
    		 *    - ${cpnAmt}    : 쿠폰금액
    		 ************************************************************************/
        	if( !mmsMap.isEmpty() ) {
        		// 1. 메시지전문 제목 데이터 추출
        		String msgTitl = StringUtil.nvl(mmsMap.get(msgTitlKey));
        		
        		// 2. 메시지전문 내용 데이터 추출
        		String msgBody = StringUtil.nvl(mmsMap.get(msgBodyKey));
        		
        		// map처리 
        		Map<String, String> replaceMap = new HashMap<String, String>();
        		replaceMap.put(msgTitlKey, msgTitl);
        		replaceMap.put(msgBodyKey, msgBody);
        		
        		for( Map.Entry<String, String> replace : replaceMap.entrySet() ) {
        			String tmpStr = replace.getValue();
        			//log.debug(methodName, "bef "+replace.getKey()+"="+tmpStr);
        			
            		// 메시지 Parse
            		// 1. ${phubTrNo} : 포인트허브 거래번호
        			tmpStr = tmpStr.replaceAll("[$]\\{phubTrNo\\}",  phubTrNo);
            		
            		// 2. ${custNm} : 고객명
        			tmpStr = tmpStr.replaceAll("[$]\\{custNm\\}",    custNm);
            		
            		// 3. ${ownCustNm} : 오너 고객명
        			tmpStr = tmpStr.replaceAll("[$]\\{ownCustNm\\}", ownCustNm);
            		
            		// 4. ${cpnAmt} : 쿠폰금액
        			tmpStr = tmpStr.replaceAll("[$]\\{cpnAmt\\}",    Amount.num(cpnAmt));
            		
            		// 5. ${ownerPhubTrNo} : 포인트허브 거래번호
        			tmpStr = tmpStr.replaceAll("[$]\\{ownerPhubTrNo\\}", ownerPhubTrNo);
        			//log.debug(methodName, "aft"+replace.getKey()+"="+tmpStr);
        			
        			// 원 메시지정보map에 put
        			mmsMap.put(replace.getKey(), tmpStr);
        		}
        		
        		//log.debug(methodName, "aft msgBody="+msgBody);
        		// put
        		//mmsMap.put(msgBodyKey, msgBody);
        	}
        	
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		
        // return
        return mmsMap;
	}
	
	
	/**
     * @description MMS전송이력 등록(Insert or Update)
     * @param  Map,
     * @return ResultVo
     * @description
     * 		1. 메시지등록 중 에러가 발생해도 내부에서 처리 한다.
     *      2. input parameter
     *         rgstType : [옵션]I:insert, U:update (null이면 무조건 insert 시킨다.)
     *         ## insert시
     *         - mmsFormMap : [필수]MMS메시지양식(PH_MMS_FORM_INFO) 데이터를 Map형태로 담는다.
     *         - mmsFormMap.mms_msg_id : 메시지ID
     *         - mmsFormMap.msg_body   : 메시지전문(내용)
     *         - mmsFormMap.clbc_ctn   : SMS콜백 - 시스템파라미터
     *         
     *         - phubTrNo   : 포인트허브 거래번호
     *         - custCtn    : 수신자 전화번호
     *         - custNm     : 고객명
     *         - rstCd      : 전송결과코드(기프티쇼나, S-HUB 전문전송 결과를 전달 한다.) 
     *         - rstMsg     : 전송결과메시지(기프티쇼나, S-HUB 전문전송 결과를 전달 한다.)
     *         
     *      3. return
     *         resultVo.setSucYn("Y")       : [String]결과(Y:성공, N:실패)
     *         resultVo.setRstObj(mmsMsgNo) : [String]MMS전송이력ID
     *         resultVo.setRstCd			: [String]실패코드
     *         resultVo.setRstMsg           : [String]실패메시지
     */	
	@SuppressWarnings("unchecked")
	@Override
	public ResultVo regPhMmsSendHist(Map<String, Object> params) {
		String methodName = "insertPhMmsSendHist";
		String stepCd     = "001";
		
		// declare
		ResultVo resultVo  = new ResultVo();
		Map mmsSendHistMap = new HashMap();
        try {
        	// set param
        	resultVo.setSucYn("N");
        	String rgstType = StringUtil.nvl(params.get("rgstType"), "I");	// I:insert, U:update
        	String mmsMsgNo = StringUtil.nvl(params.get("mmsMsgNo"));		// MMS전송이력ID
        	// 공통파라미터
        	String rstCd    = StringUtil.nvl(params.get("rstCd"));
        	String rstMsg   = StringUtil.nvl(params.get("rstMsg"));
        	
        	
        	if( "U".equals(rgstType) ) {
    			// set param
        		mmsSendHistMap.put("mmsMsgNo", mmsMsgNo); // MMS전송이력ID
        		mmsSendHistMap.put("sendRsltCd", rstCd); // 전송 결과 코드
        		mmsSendHistMap.put("sendRsltMsg", rstMsg); // 전송 결과 메시지
        		// updae
    			dao.updatePhMmsSendHist(mmsSendHistMap);
        	} else {
        		//
            	Map<String, Object> mmsFormMap = (Map<String, Object>) params.get("mmsFormMap");	// MMS메시지양식(PH_MMS_FORM_INFO) 데이터
            	
            	// get param
            	String phubTrNo		= StringUtil.nvl(params.get("phubTrNo"));
            	String phubCpnNo	= StringUtil.nvl(params.get("phubCpnNo"));
            	String recvCustNm	= StringUtil.nvl(params.get("recvCustNm"));
            	String recvCustCtn	= StringUtil.nvl(params.get("recvCustCtn"));
            	String sendCustNm	= StringUtil.nvl(params.get("sendCustNm"));
            	String sendCustCtn	= StringUtil.nvl(params.get("sendCustCtn"));
            	
        		/************************************************************************ 
        		 * 1. 메시지전송이력 등록 
        		 *    PH_MMS_SEND_HIST
        		 ************************************************************************/
    			// 3.1. insert ph_mms_send_hist
    			mmsSendHistMap.put("mmsMsgId",		mmsFormMap.get("mms_msg_id"));	// 메시지ID
    			mmsSendHistMap.put("msgConts",		mmsFormMap.get("msg_body"));		// 메시지전문
    			mmsSendHistMap.put("phubTrNo",		phubTrNo);							// 포인트허브 거래번호
    			mmsSendHistMap.put("phubCpnNo",		phubCpnNo);							// 포인트허브 쿠폰번호
    			mmsSendHistMap.put("recvCustNm",	recvCustNm);							// 수신고객명
    			mmsSendHistMap.put("recvCustCtn",	recvCustCtn);							// 수신고객CTN(고객휴대번호(쿠폰을 수신할 고객의 휴대번호를 셋팅해야 한다))
    			mmsSendHistMap.put("sendCustNm",	sendCustNm);							// 발신고객명
    			mmsSendHistMap.put("sendCustCtn",	sendCustCtn);							// 발신고객CTN
    			mmsSendHistMap.put("rsvtSendYn", 	"N");								// 예약전송여부
    			mmsSendHistMap.put("smsCallBack",	mmsFormMap.get("clbc_ctn"));		// SMS콜백 - 시스템파라미터
    			mmsSendHistMap.put("sendRsltCd",	rstCd);							// 전송결과코드
    			mmsSendHistMap.put("sendRsltMsg",	rstMsg);							// 전송결과메시지			
    			//mmsSendHistMap.put("msgGenDt",    value);	// 메시지생성일시
    			//mmsSendHistMap.put("sendDt",      value);	// 전송일시			
    			//mmsSendHistMap.put("sendReqDt",   value);	// 전송요청일시			
    			
    			// insert
    			dao.insertPhMmsSendHist(mmsSendHistMap);
    			mmsMsgNo = StringUtil.nvl(mmsSendHistMap.get("mmsMsgNo"));        		
        	}
        	
			// success
			resultVo.setSucYn("Y");
			resultVo.setRstObj(mmsMsgNo);
			
		} catch (Exception e) {
			// log write
			log.printStackTracePH(methodName, e);
        	log.error(methodName, e.getMessage());
        	
        	// set Rst Info
			resultVo.setRstCd(messageManage.getMsgCd("SY_ERROR_900"));
			resultVo.setRstMsg(messageManage.getMsgTxt("SY_ERROR_900"));
		}
		
        // return
        return resultVo;
	}
	
	
	/**
     * @description 기프티쇼 쿠폰발행취소 전문 호출
     * @param  Map,
     * @return ResultVo
     * @description
     *  
     */	
	@Override
	public ResultVo sendGiftiShowCouponCancelAPI(Map<String, Object> params) {
		String methodName = "sendGiftiShowCouponCancelAPI";
		String stepCd     = "001";
		
		// declare
		ResultVo resultVo = new ResultVo();
		
        try {
    		/************************************************************************ 
    		 * 1. 기프티쇼 쿠폰발행취소 전문 호출 
    		 *    쿠폰 취소 (0452) POST
			 * 
			 * - tr_id : 거래아이디 ※둘중 하나는 필수
			 * - pin_no : 핀번호    ※둘중 하나는 필수    
    		 ************************************************************************/
			//call API
        	resultVo.setSucYn("N");
			resultVo = giftiShowRestHttpService.sendHttpPostGiftiShowCouponCancel(params);			
			
			if( !"Y".equals(StringUtil.nvl(resultVo.getSucYn(), "N")) ) {
				// response sample data
				/**
				{  "resCode": "0000",  "resMsg": "정상처리"  }
				*/	
				// TODO - 중요에러다!!!!!!!!
				// 쿠폰생성취소에 실패하였습니다.(기프티쇼 쿠폰취소 실패!)
				throw new BizException(stepCd, StringUtil.nvl(resultVo.getRstCd()), StringUtil.nvl(resultVo.getRstMsg()));
			}
        	
			// success
			resultVo.setSucYn("Y");
			
		} catch (BizException be) {
			log.error(methodName, "Bizzzzzzzzzzzzzzzzzzzzzzzzexception");
			// write log
			log.error(methodName, "BizException code=" + StringUtil.nvl(be.getErrCd()));
			log.error(methodName, "BizException msg =" + StringUtil.nvl(be.getErrMsg()));
			log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));
			
			// 실패
			resultVo.setRstCd(StringUtil.nvl(be.getErrCd()));
			resultVo.setRstMsg(StringUtil.nvl(be.getErrMsg()));
			
		} catch (Exception e) {
			log.error(methodName, "EEEEEEEEEEEEEEEEEEEEEEEEEEEExcption");
			// log write
			log.printStackTracePH(methodName, e);
        	log.error(methodName, e.getMessage());
        	
        	// set Rst Info
			resultVo.setRstCd(messageManage.getMsgCd("SY_ERROR_900"));
			resultVo.setRstMsg(messageManage.getMsgTxt("SY_ERROR_900"));
		}
		
        // return
        return resultVo;
	}
	
	
	
	/**
     * @description 패밀리포인트 최소 결제금액 가져오기
     * @param  Map,[1.포인트허브거래번호:phub_tr_no, 2.개통자(조르자여부[Y,N]) ]
     * @return int
     * @description 단순히 파라미터 값을 읽어오기에 한계가 발생하여 생성함
     *              1.개통자 1인 경우 최소거래금액 리턴
     *              2.2 이상일때 
     *                개통자인 경우
     *                피요청자인 경우
     */	
    @Override
	public int getFpMinPerDeal(Map<String, Object> params) {
    	String methodName = "getFpMinPerDeal";
    	int iRet = Integer.valueOf(StringUtil.nvl(sysPrmtManage.getSysPrmtVal("FP_MIN_PER_DEAL")));
    	
		Map<String,Object> retmap = dao.getCountFlmySupotByTrNo(params);
		log.debug(methodName, params.toString());
		String lsOwnerYn = StringUtil.nvl(params.get("owner_yn"),"N");
		
		int liCount = Integer.valueOf(StringUtil.nvl(retmap.get("cnt"),"0"));
				
		if (liCount > 1 && "Y".equals(lsOwnerYn) ){
			iRet = 0;
		}
		
		return iRet;
	}


	/**
     * @description 기프티쇼 상품조회
     * @param  Map,
     * @return Map
     * @description
     * 		1. 금액으로 기프티쇼 상품테이블에서 상품ID를 조회 한다.
     *      2. 조회파라미터는 금액이다.  
     */	
	@Override
	public Map<String,Object> getGiftiShowGoodsInfoByAmt(String cnsmPriceAmt) {
		String methodName = "getGiftiShowGoodsInfoByAmt";
		String stepCd     = "001";
		
        // return
        return dao.getGiftiShowGoodsInfoByAmt(cnsmPriceAmt);
	}

	/**
     * <pre>조르기 문자 발송 처리</pre>
     * @see <pre></pre>
     * @author hwlee
     * @since  2019.02.25
     * @param  Map 
     * @return Map
     * @description 
     * 1. 2회 초과하였는지 FP_FMLY_SUPOT 테이블 조회하여 체크
     * 2. FP_FMLY_SUPOT 테이블의 REQ_STAT_CD 상태 update
	 * 3. PH_MMS_SEND_HIST 테이블에 insert
	 * 4. S-HUB 연동하여 피요청자에게 MMS 발송 요청(실패 시 2~3 rollback 처리)
	 * 5. 결과 PH_MMS_SEND_HIST 테이블 update
     */
	@Override
	@Transactional(rollbackFor={Exception.class, BizException.class})
	public Map<String, Object> sendPesterMMSService(List<Map<String, Object>> list) throws Exception, BizException{
		String methodName = "sendPesterMMSService";
		String stepCd = "001";
		
		Map<String, Object> tmpMap = new HashMap<String,Object>();
		Map<String, Object> paramMap = new HashMap<String,Object>();
		Map<String, Object> resultMap = new HashMap<String,Object>();
		
		for(Map<String, Object> fmlyMap : list){
			String phubTrNo		= StringUtil.nvl(fmlyMap.get("phubTrNo"));
			String phubCpnNo	= StringUtil.nvl(fmlyMap.get("phubCpnNo"));
			String ktCustId		= StringUtil.nvl(fmlyMap.get("ktCustId"));
			String recvCustNm	= StringUtil.nvl(fmlyMap.get("recvCustNm"));
			String recvCustCtn	= StringUtil.nvl(fmlyMap.get("recvCustCtn"));
			String sendCustNm	= StringUtil.nvl(fmlyMap.get("sendCustNm"));
			String sendCustCtn	= StringUtil.nvl(fmlyMap.get("sendCustCtn"));
			
			try {
				stepCd = "002";
				paramMap.put("phubTrNo", phubTrNo);
				paramMap.put("custCtn", recvCustCtn);
				paramMap.put("ktCustId", ktCustId);
				
				String reqStatCd = this.getReqStatCd(paramMap);
				if(reqStatCd.equals("RE")){
					throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_172"));
				}
				
				stepCd = "003";
				//FP_FMLY_SUPOT 테이블의 REQ_STAT_CD 상태 update
				if(reqStatCd.equals("NON")){
					paramMap.put("reqStatCd", "REQ");
				}else if(reqStatCd.equals("REQ")){
					paramMap.put("reqStatCd", "RE");
				}
				
				int cnt = this.updateReqStatCd(paramMap);
				if(cnt == 0){
					// 쿠폰 요청 상태 업데이트 실패
					throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_173"));
				}
				
				stepCd = "004";
				tmpMap.put("phubTrNo"	, phubTrNo);
				tmpMap.put("custNm"		, recvCustNm);
				tmpMap.put("ownCustNm"	, sendCustNm);
				
				tmpMap.put("smsMsgId", sysPrmtManage.getSysPrmtVal(Constant.MMS_ID_FP_CPN_PSTR));
				Map<String, Object> mmsFormMap = this.getMmsMsgInfo(tmpMap);
				
				stepCd = "005";
				String msgTitl = StringUtil.nvl(mmsFormMap.get("msg_titl"));
				String msgBody = StringUtil.nvl(mmsFormMap.get("msg_body"));
				
				// PH_MMS_SEND_HIST 테이블에 insert
				tmpMap.clear();
				tmpMap.put("mmsFormMap"		, mmsFormMap);
				tmpMap.put("phubTrNo"		, phubTrNo);
				tmpMap.put("phubCpnNo"		, phubCpnNo);
				tmpMap.put("recvCustNm"		, recvCustNm);
				tmpMap.put("recvCustCtn"	, recvCustCtn);
				tmpMap.put("sendCustNm"		, sendCustNm);
				tmpMap.put("sendCustCtn"	, sendCustCtn);
				
				ResultVo resultVo = this.regPhMmsSendHist(tmpMap);
				
				if( !"Y".equals(StringUtil.nvl(resultVo.getSucYn(), "N")) ) {
					// MMS발송이력 등록에 실패하였습니다.(로그만 남김)
					String retCode = "BZ_ERROR_170";
					log.error(methodName, "ErrCode=" + messageManage.getMsgCd(retCode));
					log.error(methodName, "ErrMsg="  + messageManage.getMsgTxt(retCode));
				}
				
				stepCd = "006";
				// S-HUB 연동하여 피요청자에게 MMS 발송 요청
				Map<String, Object> shubParamMap = new HashMap<String, Object>();
				shubParamMap.put(SHUBHttpService.PARAM_TR_ID, resultVo.getRstObj());
				shubParamMap.put(SHUBHttpService.PARAM_SUBJECT, msgTitl);
				shubParamMap.put(SHUBHttpService.PARAM_ENC_CONTENT, msgBody);
				shubParamMap.put(SHUBHttpService.PARAM_RCV_CTN, recvCustCtn);
				shubParamMap.put("phub_tr_no", phubTrNo);
				
				ResultVo shubRstVo = shubService.sendHttpPostRequestOIF2102(shubParamMap);
				if( !"Y".equals(StringUtil.nvl(shubRstVo.getSucYn()))){
					//쿠폰 요청 MMS 발송을 실패하였습니다.
					throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_179"));
				}
				
				// 결과 PH_MMS_SEND_HIST 테이블 update
				//rstCd, rstMsg
				tmpMap.put("rgstType",     "U");
				tmpMap.put("mmsMsgNo", resultVo.getRstObj());
				tmpMap.put("rstCd", shubRstVo.getRstCd());
				tmpMap.put("rstMsg", shubRstVo.getRstMsg());
				resultVo = this.regPhMmsSendHist(tmpMap);
			}catch (BizException be){
				log.error(methodName, "BizException code="+StringUtil.nvl(be.getErrCd()));
				log.error(methodName, "BizException msg="+StringUtil.nvl(be.getErrMsg()));
				log.error(methodName, "BizException step="+StringUtil.nvl(be.getStepCd()));
				
				resultMap.put(Constant.RET_CODE, be.getErrCd());
				resultMap.put(Constant.RET_MSG , be.getErrMsg());
				
				throw new BizException(be.getStepCd(), be.getErrCd(), be.getErrMsg());
			}catch (Exception e) {
				log.printStackTracePH(methodName, e);
				log.error(methodName, "Exception stepCd="+StringUtil.nvl(stepCd));
				log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));
				
				resultMap.put(Constant.RET_CODE, Constant.FAIL_CODE);
				resultMap.put(Constant.RET_MSG , Constant.FAIL_MESSAGE);
				throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));
			}
		}
		return resultMap;
	}	

	/**
	 * <pre> 쿠폰리스트 조회 </pre>
	 * @author jungukjae
	 * @since  2019.04.23 
	 * @param Map<String,Object> [phub_tr_no:포인트허브 거래번호]
	 * @return map
	 */	
	 @Override
 	 public List<Map<String, Object>>  couponList(Map<String, Object> params) {		 
		 
		 return dao.couponList(params);
	 }
	
	
}



















