/**
 * Copyright (c) 2018 KT, Inc.
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.sdk.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.model.ResultVo;

/**
 * <pre>FamilyPoint 서비스를 위한 인터페이스 </pre>
 * @Class Name : SbankService
 * @author : cisohn
 * @since  2018.06.15
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자     수정내용
 * ----------  --------  -----------------
 * 2019.02.12   cisohn      최초생성
 * </pre>
 */

public interface FamilyPointService {
	
	/* 5G 단말쿠폰 결제창 초기화(=약관조회,본인인증진입경로) */
	public Map<String,Object> familyPointInit(Map<String, Object> params, HttpServletRequest request);
	
	/* 중복지원 방지 */
	public String getCountFlmySupot(Map<String, Object> params);
	
	/* 가족지원테이블 쿠폰사용 완료건 방지 */
	public String checkFmlySupotMustBeZero(Map<String, Object> params);	
	
	/* 패밀리포인트 개통자의 피요청자경로로 접근 막기 */
	public String checkOwnerMustBeOne(Map<String, Object> params);	
		
	/* 피요청자 지원현황 조회 방지 */
	public Map<String,Object> checkCountOwner(Map<String, Object> params);	
	
	/* 패밀리포인트 지원현황 검색 */
	public List<Map<String,Object>> getFamilyPresents(Map<String,Object> params);
	
	/* KT고객인증 및 패밀리 조회 */
	public String certifyKT(Map<String,Object> params);
	
	/* KT고객인증 및 패밀리 조회 */
	public Map<String,Object> getApiKtFamily(Map<String,Object> params);

	/* 개통자 약관조회 */
	public Map<String,Object> getPresentList(Map<String, Object> params, HttpServletRequest request) throws BizException;

	
	/* 쿠폰 발행 전에 사전 정보 조회 */
	public ResultVo getPreInfoB4CouponIssue(Map<String,Object> params);	
	
	/* 포인트결제 후 쿠폰발행 */
	public ResultVo issueCoupon(Map<String,Object> params);

	/* 패밀리포인트 가족지원테이블 검색(PH거래번호별) */
	public List<Map<String,Object>> getFpFmlySupotListByPhubTrNo(Map<String,Object> params);
	
	/* 요청 상태 코드 조회 */
	public String getReqStatCd(Map<String, Object> params);
	
	/* 요청 상태 코드 업데이트 */
	public int updateReqStatCd(Map<String, Object> params);
	
	/* PH_DEAL_REQ_INFO.ADD_PARAM Parser */
	public String getAddParamElmtVal(Map<String, Object> params);
	
	/* MMS메시지 조회(Parser) */
	public Map<String,Object> getMmsMsgInfo(Map<String,Object> params);
	
	/* MMS전송이력 등록(Insert or Update) */
	public ResultVo regPhMmsSendHist(Map<String,Object> params);	

	/* 기프티쇼 쿠폰발행취소 전문 호출 */
	public ResultVo sendGiftiShowCouponCancelAPI(Map<String,Object> params);
		
	/* 기프티쇼 상품조회(상품금액으로 상품ID를 조회) */
	public Map<String,Object> getGiftiShowGoodsInfoByAmt(String cnsmPriceAmt);
	
	/* 패밀리포인트 최소거래금액가져오기 */
	public int getFpMinPerDeal(Map<String, Object> params);
	
	/* 조르기 MMS 발송 */
	public Map<String, Object> sendPesterMMSService(List<Map<String, Object>> list) throws Exception, BizException;

	/* 쿠폰리스트 조회(params : phubTrNo포인트허브 거래번호) */
	public List<Map<String, Object>>  couponList(Map<String, Object> params);
}






