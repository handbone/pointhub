package com.olleh.pointHub.api.sdk.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.api.company.service.SHUBHttpService;
import com.olleh.pointHub.api.provider.dao.ClipDao;
import com.olleh.pointHub.api.provider.service.ClipHttpService;
import com.olleh.pointHub.api.provider.service.ClipService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.CertificationVO;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;

/**
 * <pre>Standard 서비스를 위한 인터페이스 구현체 </pre>
 * @Class Name : StdServiceImpl
 * @author : cisohn
 * @since  : 2019.03.06
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자     수정내용
 * ----------  --------  -----------------
 * 2019.03.06   cisohn      최초생성
 * </pre>
 */

@Service
public class StdServiceImpl implements StdService {
	private Logger log = new Logger(this.getClass());

	@Autowired
	MessageManage messageManage;
	@Autowired
	ClipDao dao;

	@Autowired
	ClipHttpService clipHttpService;

	@Autowired
	LogService logService;

	@Autowired
	ClipService clipService;
	
	@Autowired
	FamilyPointService fmlyService;

	@Autowired
	GiftiShowService gsService;
	
    /**
     * <pre>Clip회원체크 및 약관동의 등록처리</pre>
     * @see 
     * @author sci
     * @since  2019.03.06
     * @param  Map 
     * @return Map
     * @description
     */		
	/***********************************************************************
	 * 1.사용자 관련정보 등록 
	 * - ph_pnt_cust_info
	 * - ph_pnt_cust_ ctn 
	 * - ph_pnt_cmpn_user_id
	 * 
	 * 2.클립회원 가입여부(클립서버 연동 미가입시 가입처리) 
	 *   - 에러발생 콘트롤 나가기 가입 여부 method: rglrCheck
	 *   - 가입 method: regRglmmb 가입 후 가입정보 저장(ph_pnt_cust_info)
	 * 
	 * 3.약관동의 정보 저장 
	 * - ph_cls_ag_info 
	 * - ph_cls_ag_hist 
	 * - ph_info_prvd_item_hist
	 * 
	 ***********************************************************************/
	@Override
	public Map<String, Object> joinClipMember(Map<String, Object> params) {
		String methodName = "joinClipMember";
		String stepCd="001";
		log.debug(methodName, "Start!");
		//log.debug(methodName, "init's Param:: => \n" + params.toString());
		
		CertificationVO voCert = SessionUtils.getCertificationVO(SystemUtils.getCurrentRequest());
		SbankDealVO     voDeal = SessionUtils.getSbankDealVO(SystemUtils.getCurrentRequest());
		Map<String,Object> retMap   = new HashMap<String,Object>();
		Map<String,Object> paramMap = new HashMap<String,Object>();
		
		
		try {
			paramMap.put("cust_ci" 	   , voCert.getCustCI());
			paramMap.put("cust_ctn"	   , voCert.getPhoneNo());
			paramMap.put("biz_no"  	   , voDeal.getShopBizNo());
			paramMap.put("cust_nm" 	   , voCert.getCustName());
			paramMap.put("telecom_ind" , voCert.getPhoneCorp());
			 
			paramMap.put("cprt_cmpn_id", voDeal.getCprtCmpnId());

			/*
			 * No 4. 클립회원가입,여부 조회 Notes: 회원 가입 여부 조회 API: Notes: result values
			 * status 가 "NO"면 가입 method call
			 */
			Map<String, Object> chkMap = new HashMap<String, Object>();

			String ci    	 = voCert.getCustCI();
			String phone 	 = voCert.getPhoneNo();
			String serviceId = "pointhub";

			chkMap.put("ci"			, ci);
			chkMap.put("phone"		, phone);
			chkMap.put("serviceId"  , serviceId);
			chkMap.put("phub_tr_no" , voDeal.getpHubTrNo());
			chkMap.put("pg_tr_no"   , voDeal.getPgTrNo());
			
			stepCd="002";
			
			// 가입여부 조회
			Map<String, Object> resultMap = clipService.rglrCheck(chkMap);
			
			/*************************************************************************************
			 * 화면호출(KMC본인인증) 로직 변경에 따라 clsList정보를 SbankDealVO에서 꺼내 쓴다.[2019.01.07]
			 *************************************************************************************/
			@SuppressWarnings("unchecked")		
			List<Map<String, Object>> clsList = (List<Map<String, Object>>) voDeal.getClsList();
			List<Map<String, Object>> agreeList = new ArrayList<>();
			//log.debug(methodName, "voDeal==> \n"+ voDeal.toString());
			
			if (clsList.size() == 0) {
				log.error(methodName, "clsList is null");
				throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_137"));
			}
			
			for (Map<String, Object> clsMap : clsList) {
				Map<String, Object> tmpMap = new HashMap<String, Object>();
				tmpMap.put("agree_code", clsMap.get("clsId"));
				tmpMap.put("agree_yn"  , clsMap.get("checkYn"));
				agreeList.add(tmpMap);
			}
			
			// 미가입시
			if (resultMap.get("ret_code").equals("N")) {
				// 가입정보
				Map<String, Object> certimap = new HashMap<String, Object>();
				certimap.put("ci"	  , voCert.getCustCI()); // 암호화
				certimap.put("phone"  , voCert.getPhoneNo()); // 암호화
				certimap.put("name"   , voCert.getCustName()); // 암호화
				certimap.put("birth"  , voCert.getBirthDay()); // YYYYMMDD
				certimap.put("genders", voCert.getGender());
				certimap.put("nations", voCert.getNations());
				certimap.put("devtype", serviceId);
				certimap.put("osver"  , serviceId);
				certimap.put("agency" , voCert.getPhoneCorp()); // 통신사
				certimap.put("agree"  , agreeList); // 약관동의여부 (list)
				certimap.put("serviceId" , serviceId); // "pointHub"
				certimap.put("phub_tr_no", StringUtil.nvl(voDeal.getpHubTrNo()));
				certimap.put("pg_tr_no"  , StringUtil.nvl(params.get("pg_tr_no")));
				
				stepCd="003";
				
				// 정회원등록
				Map<String, Object> regRes = clipService.regRglmmb(certimap);
				paramMap.put("cust_id", regRes.get("cust_id"));
				
			}
			
			//클립 가입일자 파라미터 추가
			paramMap.put("clip_sbsc_dt", resultMap.get("regdt"));

			stepCd="004";
			
			// cust_ci로 유저 조회/등록 후 cust_id 조회
			retMap = clipService.manageCustInfo(paramMap);

			Map<String, Object> clsAgrInfoMap = new HashMap<String, Object>();

			for (Map<String, Object> clsMap : clsList) {
				clsAgrInfoMap.put("cust_id"	, retMap.get("custId"));
				clsAgrInfoMap.put("cls_id"	, clsMap.get("clsId"));
				clsAgrInfoMap.put("cls_ver" , clsMap.get("clsVer"));
				clsAgrInfoMap.put("prvdr_id", clsMap.get("prvdrId"));
				clsAgrInfoMap.put("agre_yn" , clsMap.get("checkYn"));
				clsAgrInfoMap.put("cls_titl", clsMap.get("clsTitl"));
				clsAgrInfoMap.put("user_id" , retMap.get("custId"));
				clsAgrInfoMap.put("phub_tr_no", params.get("phubTrNo"));
				// 약관동의 정보 저장
				clipService.setClsAgrInfo(clsAgrInfoMap);
			}
			
			// 조회된 cust_id Session에 저장
			voCert.setCustId((String) retMap.get("custId"));
			SessionUtils.setCertificationVO(SystemUtils.getCurrentRequest(), voCert);

		} catch (BizException be) {
			log.error(methodName, "[BizException]stepCd="+ be.getStepCd());
			log.error(methodName, "[BizException]errCd ="+ be.getErrCd());
			log.error(methodName, "[BizException]msg   ="+ be.getErrMsg());
			
			retMap.put(Constant.RET_CODE, be.getErrCd());
			retMap.put(Constant.RET_MSG , be.getErrMsg());
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			retMap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_ERROR_900"));
			retMap.put(Constant.RET_MSG , StringUtil.nvl(e.getMessage()));
		}		
		
		return retMap;
		
	}

    /**
     * <pre>KT고객인증 및 가족지원 기초정보 저장</pre>
     * @see 
     * @author sci
     * @since  2019.03.06
     * @param  Map 
     * @return Map
     * @description
     */		
	@SuppressWarnings("finally")
	/****************************************************************************** 
	 * 개통자 패밀리포인트 결제시(=조르기화면 조회아닐때) 
	 * 기프티쇼 IF통해 패밀리조회하여 fp_fmly_supot 에 저장하기
	 * 조건: 1.패밀리포인트서비스일것, 2.결제건일것, 3-1.개통자 본인일것
	 *                                           3-2.피요청자시 중복 쿠폰 등록 배제
	 ******************************************************************************/	
	@Override
	@Transactional(rollbackFor={Exception.class, BizException.class})
	public Map<String, Object> familyPoint(Map<String, Object> params) throws BizException {
		String methodName = "familyPoint";
		String stepCd="001";
		log.debug(methodName, "Start!");
		//log.debug(methodName, "init's Param:: => \n" + params.toString());
		
		CertificationVO voCert = SessionUtils.getCertificationVO(SystemUtils.getCurrentRequest());
		SbankDealVO     voDeal = SessionUtils.getSbankDealVO(SystemUtils.getCurrentRequest());
		Map<String,Object> retMap   = new HashMap<String,Object>();
		Map<String,Object> paramMap = new HashMap<String,Object>();

		String sRetCode = Constant.SUCCESS_CODE;
		String sRetMsg  = Constant.SUCCESS_MSG;
		
		try {
			if ("Y".equals(voDeal.getPayYn())) {
				Map<String,Object> reqmap = new HashMap<String,Object>();
			    
				// 개통자인 경우 fp_fmly_supot insert
				if("Y".equals(voDeal.getOwnerYn())) {
					reqmap.put(SHUBHttpService.PARAM_SVC_NO      , voCert.getPhoneNo());//cust_ctn
					reqmap.put(SHUBHttpService.PARAM_CUST_NM     , voCert.getCustName());//cust_name
					reqmap.put(SHUBHttpService.PARAM_CUST_IDFY_NO, voCert.getCtzNo7());//cust_ctz_no
					
					reqmap.put("phub_tr_no", voDeal.getpHubTrNo());
					
					// API호출하여 지원내역 등록하기
					Map<String,Object> resmap = fmlyService.getApiKtFamily(reqmap);
					
					log.debug(methodName, "getApiKtFamily return:: "+ resmap.toString());
					
					//TODO
					/* Constant.SUCCESS_CODE==> "0000" 으로 수정 
					 * err: 0001(KT고객아님), 0003(생년월일/고객명 불일치) 해당하는 메시지 adm_msg_info 에 등록관리
					 * */
					if(!Constant.SUCCESS_CODE.equals(resmap.get(Constant.RET_CODE))){			
						throw new BizException(stepCd, (String)resmap.get(Constant.RET_CODE), (String)resmap.get(Constant.RET_MSG));
					}
					
				} else {
					// 피요청자 중복(2개쿠폰이상으로 지원하기 예방)방지 & 요청 종료 여부
					reqmap.put("phubTrNo", voDeal.getOwnerTrNo());
					reqmap.put("custCtn" , voCert.getPhoneNo());
					String cnt = "0";
					
					// 피요청자 경로로 요청자 진입 막기
					cnt = fmlyService.checkOwnerMustBeOne(reqmap);
					
					if(!"1".equals(cnt)){
						throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_149"));
					}
					
				    cnt = "0";
					cnt = fmlyService.checkFmlySupotMustBeZero(reqmap);
					
					if(!"0".equals(cnt)){
						// 요청이 종료되어 스마트폰 할인권을 선물할 수 없습니다.
						throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_180"));
					} else {
					
						cnt = fmlyService.getCountFlmySupot(reqmap);
					
						// 중복지원 체크(이미 거래를 진행하셨습니다.(노출))
						if(!"0".equals(cnt)){
							throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_161"));
						} else {// 피요청자 지원상태 업데이트
	
							Map<String,Object> fmap = new HashMap<String,Object>();
							
							// 본인인증 완료 PR120
							fmap.put("prgr_stat", Constant.CPN_REQ_PRGRS_PR120);
							fmap.put("custCtn"  , voCert.getPhoneNo());
							fmap.put("phubTrNo" , voDeal.getOwnerTrNo());
							
							int liFamYn =  gsService.updateFmlySupot(fmap);
							
							if (liFamYn == 0){								
								throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_177"));
							}
						}
					}
				}
				
			} else {
				/****************************************************************************** 
				 * hwlee
				 * 개통자 패밀리포인트 조회시(=조르기화면 조회) 
				 * 조건: 1.결제건아닐것, 2.패밀리포인트서비스일것, 3.개통자 무관
				 ******************************************************************************/
				retMap.put("serviceId", voDeal.getServiceId());
				retMap.put("payYn", voDeal.getPayYn());
				retMap.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
				retMap.put(Constant.RET_MSG , Constant.SUCCESS_MSG);
			}
		
		} catch (BizException be) {
			// write log
			sRetCode  = StringUtil.nvl(be.getErrCd());
			sRetMsg   = StringUtil.nvl(be.getErrMsg());
			log.error(methodName, "BizException code=" + sRetCode);
			log.error(methodName, "BizException msg =" + sRetMsg);
			log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			retMap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_ERROR_900"));
			retMap.put(Constant.RET_MSG , StringUtil.nvl(e.getMessage()));
			throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));
		}
		
		retMap.put(Constant.RET_CODE, sRetCode);
		retMap.put(Constant.RET_MSG , sRetMsg );
		
		log.debug(methodName, "retMap==> "+ retMap.toString());
		return retMap;
	}
	
	
}
