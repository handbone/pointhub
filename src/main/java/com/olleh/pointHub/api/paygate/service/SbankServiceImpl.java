/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.paygate.service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pointHub.api.comn.crypto.SbankCrypto;
import com.olleh.pointHub.api.comn.model.ClipPointIfVo;
import com.olleh.pointHub.api.comn.service.ComnService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.api.company.dao.GiftiShowDao;
import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.api.paygate.dao.SbankDao;
import com.olleh.pointHub.api.provider.service.ClipPointService;
import com.olleh.pointHub.api.sdk.service.FamilyPointService;
import com.olleh.pointHub.api.sdk.service.StdService;
import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.PgCmpnInfoManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.exception.IfException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.CryptoUtil;
import com.olleh.pointHub.common.utils.DateUtils;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;

/**
 * @Class Name : SbankServiceImpl
 * @author : cisohn
 * @since 2018.07.31
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자     수정내용
 * ----------  --------  -----------------
 * 2018.07.31   cisohn      최초생성
 * 2019.01.07   cisohn      checkDupReqByPgDealNo 삭제 
 * </pre>
 */

@Service
public class SbankServiceImpl implements SbankService  {

	private Logger log = new Logger(this.getClass());

	@Autowired
	PgCmpnInfoManage pgManage;
	
	@Autowired
	private SbankHttpService httpservice;
	
	@Autowired
	private ComnService comnservice;
	
	@Autowired
	LogService logService;
	
	@Autowired
	private ClipPointService clipservice;
	
	@Resource(name="sbankDao")
	private SbankDao dao;

	@Autowired
	GiftiShowDao giftiShowDao;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	SysPrmtManage   sysPrmtManage;

	@Autowired
	SbankCrypto	sbankCrypto;
	
	@Autowired
	FamilyPointService fpService;
	
	@Autowired
	AccsCtrnManage  accsManage;
	
	@Autowired
	GiftiShowService gsService;
	
	@Autowired
	StdService stdService;
	
	@Autowired
	SbankService sbankService;
	
	/**
	 * 수/발신처(SEND_RECV_PLC) 연동시스템 코드
	 * PH	포인트허브
	 * CP	클립포인트
	 */
    private String SEND_PLC = Constant.PLC_SB;
    private String RECV_PLC = Constant.PLC_PH;
    
    
    
	/**
     * @author cisohn
     * @since  2018.11.09
     * @param  Map (ret_code = msgId )
     * @return void
     * @description PG사의 인증처리
     *              접속IP, Authorization key, PG사ID, 거래구분
     */	
	@Override
	@Transactional
	public Map<String, Object> checkAuthOfPg(Map<String, Object> params) {
		String methodName = "checkAuthOfPg";
		
		Map<String,Object> retmap  = new HashMap<String,Object>();
		Map<String,Object> reqmap  = new HashMap<String,Object>();
		Map<String,Object> dealparam = new HashMap<String,Object>();
		Map<String,Object> dealret   = new HashMap<String,Object>();
		HttpServletRequest request   = SystemUtils.getCurrentRequest();
		String stepCd = "001";
		String sPgId  = "";
		String sAuth  = StringUtil.nvl(request.getHeader("Authorization"));
		String sIp    = SystemUtils.getIpAddress(request);
		String sTrDiv = "";
		String sVoAuth= "";
		String sRetCode =  Constant.SUCCESS_CODE;
		String sRetMsg  =  Constant.SUCCESS_MSG;
		String sServiceId = Constant.SERVICE_ID_SVC_BASE; 
				
		// StringUtil.nvl(params.get("service_id"),"SVC_BASE");
		
		/* 오류발생시 PG사에 리턴, 로깅할 자료 복사  */
		retmap.put("pg_tr_no"  , StringUtil.nvl(params.get("pg_tr_no")));
		retmap.put("phub_tr_no", StringUtil.nvl(params.get("phub_tr_no")));
		retmap.put("pay_amt"   , StringUtil.nvl(params.get("points"),"0"));
		retmap.put("points"    , StringUtil.nvl(params.get("points")));
		retmap.put("tr_dtm"    , "");
		retmap.put("return_url", StringUtil.nvl(params.get("return_url"),""));
		retmap.put("ret_code"  , sRetCode);
		retmap.put("ret_msg"   , sRetMsg );
		
		try {
		
			sServiceId = this.getServiceId(params);
			
	    	if("".equals(sServiceId)){
	    		sRetCode = "IF_INFO_104";
	    	}
			
			sPgId = this.getPgId(params);
			
			// pg사 id 없음
	    	if("".equals(sPgId)){
	    		sRetCode = "IF_INFO_104";
	    	}
			
	    	stepCd  = "002";
	        // ip check(접근권한)
	        if (!accsManage.isAccsIp(sPgId, sIp)) {
	    		sRetCode = "IF_INFO_148";
	        }
	
	        stepCd  = "003";
	    	// authorization check
	    	sVoAuth = StringUtil.nvl(pgManage.getPgAthnKey(sPgId));
	    	if (!sVoAuth.equals("")&& !sAuth.equals("")) {
	    		if( !sAuth.equals(sVoAuth) ) {
		    		sRetCode = "IF_INFO_107";
	    		}
	    	}
	    	
	    	stepCd = "004";
    		dealparam.put("phub_tr_no"  , StringUtil.nvl(params.get("phub_tr_no")  ,"-"));
    		dealparam.put("pg_tr_no"    , StringUtil.nvl(params.get("pg_tr_no")    ,"-"));
    		dealparam.put("ori_pg_tr_no", StringUtil.nvl(params.get("ori_pg_tr_no"),"-"));
    		dealparam.put("points"      , StringUtil.nvl(params.get("points")      ,"-"));
    		dealparam.put("tr_div"      , StringUtil.nvl(params.get("tr_div")      ,"-"));
    		dealparam.put("stDealDt"    , DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[0]);
    		dealparam.put("endDealDt"   , DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[1]);
    		
    		log.debug(methodName, "pg/pay Init:: \n" + dealparam.toString());	    	
	    	
    		sTrDiv = StringUtil.nvl(params.get("tr_div"));
    		
    		// invalid parameter (패밀리포인트는 결제"PA" 존재)
    		if(Constant.SERVICE_ID_SVC_FP.equals(sServiceId)) {
    			if(!"PA".equals(sTrDiv)) {
    				sRetCode = "IF_INFO_103";
    			}
    		}
    		
    		/* 결제,취소,망취소(거래번호&금액체크 안함) */
    		if(sTrDiv.equals("PA")||sTrDiv.equals("CA")) {	        	
    			dealret = checkPgPayDealInfo(dealparam);
    			
    			if (Common.isNull(dealret)) {
    				sRetCode = "IF_INFO_103";
    			} else {
    				if (StringUtil.nvl(dealret.get("pass_yn")).equals("F")){
    					sRetCode = "IF_INFO_103";        			
    				}
    			}
    		} else {
    			// PA,CA,MA를 제외한 건은 모두 예외처리 대상임
    			if (!"MA".equals(sTrDiv)) {
    				sRetCode = "IF_INFO_103";
    			}
    		}

    		stepCd = "005";
    		reqmap = dao.selectResultDataToPg(params);
    		retmap.put("cust_ctn", reqmap.get("cust_ctn"));
    		
	    	
		} catch (Exception e){
    		sRetCode = "SY_ERROR_900";
    		sRetMsg  = StringUtil.nvl(e.getMessage());
		}

		retmap.put("service_id", sServiceId);
		retmap.put(Constant.RET_CODE, sRetCode);
		retmap.put(Constant.RET_MSG , sRetMsg);
		
		return retmap;
	}
    
    /**
     * @description 포인트허브 거래번호를 이용하여 중복된 거래신청 등록하지 않도록.
     * @param  Map
     * @return List
     */	
	@Override	
	public Map<String,Object> checkDupReqByPhubTrNo(Map<String, Object> params) throws BizException, Exception{
		String methodName = "checkDupReqByPhubTrNo";
		String stepCd     = "001";
		Map<String,Object> retmap = new HashMap<String,Object>();
		try {
			stepCd = "002";
			retmap = dao.checkDupReqByPhubTrNo(params);
			
			retmap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_INFO_00"));
			retmap.put(Constant.RET_MSG , messageManage.getMsgTxt("SY_INFO_00"));
			
			stepCd = "003";
			if( !StringUtil.nvl(retmap.get("cnt")).equals("0")){
				retmap.put(Constant.RET_CODE, "FAIL");
				retmap.put(Constant.RET_MSG , "FAIL");
			}

		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));
		}
		return retmap;
	}	
	
    /**
    * @description 거래내역 정보를 가져와 PG사에서 온 해쉬값과 동등한지 비교함. 
    * @param  Map
    * @return Map
    */
	public Map<String, Object> checkHash(Map<String, Object> params) {
		String methodName = "checkHash";
		Map<String, Object> retmap = new HashMap<String,Object>();
		Map<String, Object> map   = new HashMap<String,Object>();
		String sCheckHash =  StringUtil.nvl(params.get("check_hash"));
		String sTrData = "";
		String encHash = "";
		
		if(StringUtil.isNull(sCheckHash)) {
			retmap.put(Constant.RET_CODE, "FAIL");
			return retmap;			
		}
		
		try {
			
			map = dao.selectDataToHashValue(params);
			
			if (Common.isNull(map)){
				retmap.put(Constant.RET_CODE, "FAIL");
				return retmap;
			}
			
			sTrData = (String)map.get("hashstr");
			encHash = CryptoUtil.sha256(sTrData);
			//log.debug(methodName, "Hash From PG      => "+ sCheckHash );
			//log.debug(methodName, "Hash Enc By Phub  => "+ encHash );
			
			if(encHash.equals(sCheckHash)){
				retmap.put(Constant.RET_CODE, "00");
			}else{
				retmap.put(Constant.RET_CODE, "FAIL");
			}
			
		} catch (NoSuchAlgorithmException e) {
			retmap.put(Constant.RET_CODE, "FAIL");
			log.error(methodName, "NoSuchAlgorithmException = " + StringUtil.nvl(e.getMessage()));
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			retmap.put(Constant.RET_CODE, "FAIL");
		}
		
		return retmap;
	}

	
	/**
     * @description phub거래번호를 이용하여 거래정보 일치하는지 체크
     * @param  Map
     * @return List
     */	
	@Override	
	public Map<String,Object> checkPgPayDealInfo(Map<String, Object> params) throws BizException, Exception{
		Map<String,Object> retmap = new HashMap<String,Object>();
		retmap = dao.checkPgPayDealInfo(params);
		return retmap;
	}
	
	
    /**
     * @description 포인트 전환율 검색
     * @param  Map
     * @return List
     */		
    @Override
	public List<Map<String, Object>> getPrvdrExchRate(List<Map<String,Object>> params) {
		return dao.getPrvdrExchRate(params);
	}

    
	/**
	 * @description - 거래정보를 이용하여 세틀뱅크 id 가져오기
	 * @param Map
	 * @return List
	 */
	@Override
	public String getPgId(Map<String, Object> params) throws  Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String sPgId = "";
		map = dao.getPgIdByDealInfo(params);
		
		if (Common.isNull(map)){
		   sPgId = "";	
		} else {
			sPgId = StringUtil.nvl(map.get("pg_cmpn_id"));			
		}		
		return sPgId;
	}	
	
	
	/**
	 * @description - 거래신청정보에서 서비스 아이디 가져오기
	 * @param Map
	 * @return List
	 */
	@Override
	public String getServiceId(Map<String, Object> params) throws  Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String sServiceId = "";
		map = dao.getServiceId(params);
		
		if (Common.isNull(map)){
			sServiceId = "";	
		} else {
			sServiceId = StringUtil.nvl(map.get("service_id"));			
		}		
		return sServiceId;
	}		
	
    /**
     * @description 결제요청 정보를 저장함.
     * @param  Map
     * @return Map
     */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Map<String,Object> saveDealReq(Map<String, Object> params)  throws Exception{
		String methodName = "saveDealReq";
		Map<String,Object> retmap = new HashMap<String,Object>();
		Map<String,Object> map    = new HashMap<String,Object>();
		int iRes=0;
		String stepCd = "001";
		
		/* 패밀리포인트 or 일반거래 구분(Constant에서 정의) */
		String sServiceId = "";
		String sTrDiv     = (String)params.get("tr_div");
		
		SbankDealVO dealVo = SessionUtils.getSbankDealVO(SystemUtils.getCurrentRequest());
		
		List<Map<String,Object>> list = (List<Map<String,Object>>) params.get("list");
		
		try {
			/*****************************************************************************************************
			 * PG사로부터 요청사항 전송시 요청정보 insert가 이루어지므로 기존로직은 UPDATE로 정정(only Master)
			 * ph_deal_req_dtl은 포인트 조회 후 결정되므로 이 때 insert
			 * ph_deal_req_info의 deal_stat가 0100,0200,0300 인 상태인 건은 상세정보가 없는 상태로 저장됨(이슈여부 체크)
			 * ***************************************************************************************************/
			//log.debug(methodName, "'init params::==> \n"+ params.toString());
			
			iRes = dao.updateDealReqInfoPre(params);

			stepCd = "002";
			
			String pHubTrNo = (String)params.get("phub_tr_no");		
			
			/* 상세정보 등록시 마스터에서 생성된 Key 반복복사 */
	        for(Map<String, Object> mp : list){
	        	mp.put("phub_tr_no", pHubTrNo);
	        }
	        
	        stepCd = "003";
	        
	        /*************************************************************************** 
	         * 결제요청 상세 등록 
	         ***************************************************************************/
	        // 총포인트 결제금액
	        int liTtlPntAmt = Integer.valueOf(StringUtil.nvl(params.get("ttl_pnt_amt"),"0"));
			sServiceId = dealVo.getServiceId();
			sTrDiv = dealVo.getTrDiv();
			String lsOwnYn = dealVo.getOwnerYn();			
			
			if( sServiceId.equals(Constant.SERVICE_ID_SVC_FP) 
					&& sTrDiv.equals("PA") 
					&& "Y".equals(lsOwnYn)
					&& liTtlPntAmt == 0) {
				log.debug(methodName, stepCd + "0원 결제시 신청내역 저장 안함");
			}else{
				stepCd = "004";
				dao.insertDealReqDtl(params);

				/* 결제요청 상세 sum데이타 마스타에 수정 */
				params.put("stDealDt" , DateUtils.getBetweenDates(pHubTrNo)[0]);
				params.put("endDealDt", DateUtils.getBetweenDates(pHubTrNo)[1]);
				if(iRes > 0) 	iRes = dao.updateDealReqTtlAmt(params);
				
			}
			
			if(iRes>0){
				retmap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_INFO_00"));
				retmap.put(Constant.RET_MSG , messageManage.getMsgTxt("SY_INFO_00"));
			} else {
				retmap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_ERROR_800"));
				retmap.put(Constant.RET_MSG , messageManage.getMsgTxt("SY_ERROR_800"));
			}
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);		
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));
		}
		return retmap;
	}
	
    /**
     * @description 비동기로 요청된 정보를 저장 후 PG사에서 인증용 저장작업 할 수 있도록 전송(포인트SUM) 
     * @param  Map
     * @return List
     * @throws IfException 
     */
	@SuppressWarnings("unused")
	public Map<String,Object> sendToPgPay(Map<String,Object> params) throws BizException, Exception {
		String methodName = "sendToPgPay";
		String stepCd = "001";
		Map<String,Object> retmap  = new HashMap<String,Object>();
		Map<String,Object> pgmap   = new HashMap<String,Object>();
		Map<String,Object> sendmap = new HashMap<String,Object>();
		Map<String,Object> etcMap  = new HashMap<String,Object>();
		
		/**************************************************************  
		 * 1.연동규약서에 정의된 데이타 수집하기<token, auth_limit_dtm 포함>
		 **************************************************************/
		stepCd = "002";
		sendmap = dao.selectReqDataToPg(params);
		
		sendmap.remove("cust_ctn");
		
		etcMap.put("authorization", sendmap.get("authorization"));
		sendmap.remove("authorization");

		if(!Common.isNull(sendmap)){
			sendmap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_INFO_00"));
			sendmap.put(Constant.RET_MSG , messageManage.getMsgTxt("SY_INFO_00"));
		}else{				
			sendmap.put(Constant.RET_CODE, messageManage.getMsgCd("IF_INFO_104"));
			sendmap.put(Constant.RET_MSG , messageManage.getMsgTxt("IF_INFO_104"));
		}
		
		//log.debug(methodName , "From Params Sdk ==> \n "+ params.toString() );
		log.debug(methodName , "From Sdk Params length ==> "+ params.toString().length() );

		stepCd = "003";
		
		/**************************************************************  
		 * 2.거래요청 테이블에 저장할 데이타 셋팅
		 *  - 거래 제한 시간
		 *  - 암호화 토큰
		 **************************************************************/			
		params.put("auth_limit_dtm", sendmap.get("auth_limit_dtm"));

		stepCd = "031";
		
		/* adm_api_if_log insert column set */
		etcMap.put("phub_tr_no", StringUtil.nvl(params.get("phub_tr_no")));
		etcMap.put("pgDealNo"  , params.get("pg_deal_no"));
		
		/**************************************************************  
		 * 3.세틀뱅크로 신청정보 전송(DB Insert in SettleBank)
		 **************************************************************/			
		ResultVo result = httpservice.sendHttpPostSB((String)params.get("return_url_1"), sendmap, etcMap);
		
		/**************************************************************  
		 * 4.세틀뱅크간 처리내역 저장
		 **************************************************************/
		stepCd = "032";
		pgmap = (Map<String, Object>)result.getRstBody();

		/***********************************************************************
		 * 5.PG인증용 저장 후 리턴받은 정보 검증
		 ***********************************************************************/
		stepCd = "033";
		if(Common.isNull(pgmap)){
			log.error(methodName, "["+stepCd+"]"+ "ReturnURL#1 Error");
			throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_112"));
		} else {
			/* PG사에서 성공 또는 오류처리 되었을때 */
			// 성공시
			stepCd = "034";
			
			if ( pgmap.get(Constant.RET_CODE).equals("0061")) {
				retmap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_INFO_00"));
				retmap.put(Constant.RET_MSG , messageManage.getMsgTxt("SY_INFO_00"));
				
				stepCd = "035";
				
				String sPgTrNo   = StringUtil.nvl(pgmap.get("pg_tr_no"),"-");
				String sPhubTrNo = StringUtil.nvl(pgmap.get("phub_tr_no"),"-");
									
				if (!StringUtil.nvl(params.get("phub_tr_no")).equals(sPhubTrNo) ||
					!StringUtil.nvl(params.get("pg_deal_no")).equals(sPgTrNo)	){
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_103"));
				}
				
			} else {// 오류시
				stepCd = "036";
				log.error(methodName, "["+stepCd+"]"+ "ReturnURL#1 Error");
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_112"));
			}
		}
	
		/**************************************************************  
		 * 6.server to server 통신정보 화면단에 전달
		 **************************************************************/		
		retmap.put("sendmap", sendmap);

		return retmap;
		
	}
	

    /**
    * @description PG사로부터의 포인트결제 요청 처리
    *              결제: 1.check_hash 검증 -> 2.token 검증(시간내처리) 프로세스가 선행
    *              server TO server 통신으로 uri는 "/pg/pay" 임
    * @param  Map
    * @return Map
    */
	@Override
	@Transactional(rollbackFor = {Exception.class, BizException.class})
	public Map<String,Object> requestPayFromPg(Map<String, Object> param) throws BizException, Exception{
		String methodName = "requestPayFromPg";
		String stepCd = "000";
		Map<String,Object> params    = new HashMap<String,Object>(param);
		HttpServletRequest req       = SystemUtils.getCurrentRequest();
		Map<String,Object> retmap    = new HashMap<String,Object>();
		Map<String,Object> hashmap   = new HashMap<String,Object>();
		Map<String,Object> tokenmap  = new HashMap<String,Object>();
		Map<String,Object> statparam = new HashMap<String,Object>();
		Map<String,Object> timemap   = new HashMap<String,Object>();
		Map<String,Object> dealdd    = new HashMap<String,Object>();
		Map<String, Object> tmpMap   = new HashMap<String,Object>();
		ResultVo resultVo    		 = new ResultVo();
		ResultVo rollbackVo  		 = new ResultVo();
		int iDeal = 0;
		int iDtl  = 0;
		String sDealDate = "";
		String sDealTime = "";
		String sDealDt = "";
		String sReturnUrl = "";
		
		List<ClipPointIfVo> cliplist =  new ArrayList<ClipPointIfVo>();
		
		String isPointPayed = "N";
		String isCouponIsed = "N";
		String sCustCtn     = "";
		String sDealStat    = Constant.DEAL_STAT_2000;
		
		/* 패밀리포인트 or 일반거래 구분(Constant에서 정의) */
		String sServiceId = "";
		String sTrDiv     = (String)params.get("tr_div");
		String sPhubCpnNo = "";
		int liTtlPntAmt = 0;
		
        String sLogCd  = Constant.SUCCESS_CODE;
        String sLogMsg = Constant.SUCCESS_MSG;
		
		retmap.put(Constant.RET_CODE , sLogCd);
		retmap.put(Constant.RET_MSG  , sLogMsg);		
		retmap.put("return_url" , "");		
		
		
		// 검색 성능향상용 거래일자 범위 셋팅
		params.put("stDealDt" , DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[0]);
		params.put("endDealDt", DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[1]);
		
		dealdd.put("stDealDt" , DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[0]);
		dealdd.put("endDealDt", DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[1]);
		
		/*****************************************************************
		 * 0.신청정보 상태 Setting In Case Of Success
		 *****************************************************************/
		stepCd = "001";
		statparam.put("phub_tr_no", params.get("phub_tr_no"));
		statparam.put("user_id"   , params.get("cust_id"));
		statparam.put("deal_stat" , Constant.DEAL_STAT_2000);
		statparam.put("err_cd"    , messageManage.getMsgCd("SY_INFO_00"));
		statparam.put("err_msg"   , messageManage.getMsgTxt("SY_INFO_00"));
		
		try {
		
			
			/*****************************************************************  
			 * 1.파라미터(연동규약서) - 중간 오류 발생 대비하여 초기에 셋팅
				1.pg_tr_no   PG거래번호.
				2.phub_tr_no 포인트허브 거래번호.
				3.pay_amt    결제상품금액.
				4.points     결제/취소 포인트금액.
				5.ret_code   리턴코드.
				6.ret_msg    리턴메시지.
				7.tr_dtm     거래일시.
			 *****************************************************************/
			
			retmap = dao.selectResultDataToPg(params);
			
			sCustCtn = StringUtil.nvl(retmap.get("cust_ctn"));
			retmap.put("cust_ctn", sCustCtn);
			
			sServiceId = StringUtil.nvl(retmap.get("service_id"));
			
			log.debug(methodName, "selectResultDataToPg ret::=> \n" + retmap.toString());
	
			stepCd = "002";
			/**************************************************************  
			 * 2.해시 체크
			 **************************************************************/
			hashmap  = checkHash(params);
			if(hashmap != null){
				if(!hashmap.get(Constant.RET_CODE).equals("00")){
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_109"));
				}
			}
			
			stepCd = "003";
			
			/**************************************************************  
			 * 3.토큰 체크
			 **************************************************************/
			tokenmap  = dao.selectReqToken(params);
			String dbToken = StringUtil.nvl(tokenmap.get("token"));
			
			if (!dbToken.equals("-")){
				if( !StringUtil.nvl(params.get("token")).equals(dbToken)){
					 throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_108"));
				}
			}
			
			stepCd = "004";
			/*****************************************************************  
			 * 4.파라미터 시간내 거래요청하는지 체크(adm_sysprmt_info: 900 secs)
			 *  (복호화 토큰예: "PH18090500000590|1536129266"
			 ****************************************************************/
			String sDecToken = sbankCrypto.decAES(dbToken);
			
			/* 암복호화 모듈 오류처리 */
			if (sDecToken.equals("")){
				throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_153"));
			}
			
			String sTimeLimit= sDecToken.substring(sDecToken.indexOf("|") + 1);
			params.put("token_dts", sTimeLimit);
			//log.debug(methodName, "sTimeLimit==> "+ sTimeLimit);
			
			timemap = dao.checkTransactionTime(params);
			
			if(timemap != null){
				if(StringUtil.nvl(timemap.get("check_interval")).equals("F")){
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_110"));
				}
			}
			
			stepCd = "005";
			/*****************************************************************
			 * 5.클립포인트 연동처리(포인트 가감처리)
			 *****************************************************************/
			log.debug(methodName, stepCd + "==> \n"+ params.toString() );
	
			/* fetch the point information */
			cliplist = dao.selectDataToClip(params);
					
			log.debug(methodName, "stepCd =>"+ stepCd);
			log.debug(methodName, "pntList=>"+ cliplist.toString());
			
			/*****************************************************************
			 * 클립포인트 차감 요청
			 * - 예외: 패밀리포인트 개통자 0 원 결제는 클립포인트 타지 않는다.
			 *        단, 거래원장은 등록(거래상세는 저장하지 않음)
			 *****************************************************************/		
			Map<String,Object> tMap = new HashMap<String,Object>();
			tMap.put("pgDealNo"  , params.get("pg_tr_no"));
			tMap.put("service_id", sServiceId);
			
			// 총포인트 결제금액
			liTtlPntAmt = Integer.valueOf(StringUtil.nvl(retmap.get("points"),"0"));
			
			if( sServiceId.equals(Constant.SERVICE_ID_SVC_FP)  && sTrDiv.equals("PA")  && liTtlPntAmt == 0) {
				
				/* 패밀리 포인트 개통자의 0 원 결제는 클립포인트와의 연계처리 없음 */
				// sReturnUrl ="https://tb.pointhub.clipwallet.co.kr/phub/fp/familyList.do";
				sReturnUrl = sysPrmtManage.getSysPrmtVal("PH_DOMAIN") + "/phub/fp/familyList.do";
				log.debug(methodName, "(패밀리포인트) 개통자의 결제금액 0원 허용"); 
				
				iDeal  = dao.insertDealInfo(params);
				
			} else {
				
				/* ----------------------------------------------------------------------------
				 * ClipPoint API Call "minuspoint"
				------------------------------------------------------------------------------*/
				resultVo = clipservice.minuspoint(cliplist, tMap);
				
				/*---------------------------------------------------------------------------
				 * resultVo.getSucYn() N: 부분실패(1건이라도 차감성공건 존재)
				 *                   , F: 전체실패(차감성공건 0건)
				 *                   , Y: 전체성공
				 *-------------------------------------------------------------------------*/
				String resultVoSucYn = resultVo.getSucYn();
				String rollbackVoSucYn = "";
				
				log.debug(methodName, "clipservice.minuspoint's Result:: "+ resultVoSucYn);
				
				// 포인트결제 여부 업데이트
				isPointPayed = resultVoSucYn;
				//log.debug(methodName, "stepCd:"+ stepCd +" 클립포인트처리결과 => \n"+ JsonUtil.toJson(resultVo));
				stepCd = "006";
				/*****************************************************************
				 * 6.정확한 거래일자 표기
				 *****************************************************************/
				
				sDealDate = StringUtil.nvl(resultVo.getRstDate());
				sDealTime = StringUtil.nvl(resultVo.getRstTime());
				sDealDt   = sDealDate + sDealTime;
				
				retmap.put("tr_dtm", sDealDt);
				
				stepCd = "007";
				/************************************************************************  
				 *  7.성공/실패 정보를 ph_deal_req_dtl 테이블에 적용한다.
				 *   - 성공건에 대한 취소처리: INSERT
				 *   - 실패건에 대한 에러코드 저장: update "sucs_yn", "err_cd", "err_msg"
				 ************************************************************************/
				
				@SuppressWarnings("unchecked")
				// List<ClipPointIfVo> retList  = (List<ClipPointIfVo>)resultVo.getRstBody();
				List<ClipPointIfVo> retList = new ArrayList<ClipPointIfVo>((List<ClipPointIfVo>)resultVo.getRstBody());
				for(ClipPointIfVo fVo : retList) {
					ClipPointIfVo vo = new ClipPointIfVo();
					vo.setCopy(fVo);
					vo.setStDealDt(DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[0]);
					vo.setEndDealDt(DateUtils.getBetweenDates((String)params.get("phub_tr_no"))[1]);
					vo.setDealDt(sDealDt);
					
					dao.updatePayPntResult(vo);
				}
				
				
				stepCd = "008";
				/**************************************************************************  
				 *  8.포인트결제 실패시 처리
				 **************************************************************************/					
				if (resultVoSucYn.equals("N")) {
					List<ClipPointIfVo> rollbackList = new ArrayList<ClipPointIfVo>();
					List<ClipPointIfVo> errorList    = new ArrayList<ClipPointIfVo>();
					//List<ClipPointIfVo> pointList    = (List<ClipPointIfVo>)resultVo.getRstBody();
					List<ClipPointIfVo> pointList    = new ArrayList<ClipPointIfVo>((List<ClipPointIfVo>)resultVo.getRstBody());
					
					/*-----------------------------------------------------------------------------------------------
					 * 카드사 오류발생시 ClipPointService에서 ResultVo's rstCd 에 포인트처 오류를 내려줌.(ex:"9998")
					 * 위 4자리 코드와 매핑된 포인트허브 메시지 ID를 찾아 ph_deal_req_info(요청마스터)의 err_code, err_msg
					 * 에 반영한다.
					------------------------------------------------------------------------------------------------*/
					String sClipMsgCd  = resultVo.getRstCd();
					String sClipMsgTxt = resultVo.getRstMsg();
					String sPhubMsgId = "";
					
					String sPntRootId = comnservice.getPntTrNoRoot(params);
					
					for(ClipPointIfVo fVo : pointList) {
						ClipPointIfVo vo = new ClipPointIfVo();
						vo.setCopy(fVo);
						
						if( "Y".equals(vo.getSucYn())) {
							// 포인트거래번호 접두사 찾기
							String sPntPrfx = comnservice.getPntTrNoPrfx(vo.getPntCd());
							String sPntTrNo = sPntPrfx + sPntRootId;
							
							vo.setOriPntTrNo(vo.getPntTrNo());
							vo.setPntTrNo(sPntTrNo);
							
							rollbackList.add(vo);
							
						} else {
							
							sPhubMsgId = comnservice.convClipPointErrCd("", sClipMsgCd);
							
							errorList.add(vo);				
							
							if( !"".equals(sPhubMsgId) ) {
								sClipMsgCd  = messageManage.getMsgCd(sPhubMsgId);
								sClipMsgTxt = messageManage.getMsgTxt(sPhubMsgId);
								
								// 실패한 카드사정보 추가 - 2019.06.12.lys
								if( !"".equals(StringUtil.nvl(vo.getPntCd())) ) {
									sClipMsgTxt = sClipMsgTxt + "("+vo.getPntCd()+")";
								}
							}
						}
					}
					
					stepCd = "081";
					log.debug(methodName, "stepCd["+ stepCd +"] rollbackList => \n "+ rollbackList.toString());
					log.debug(methodName, "stepCd["+ stepCd +"] errorList    => \n "+ errorList.toString());
					
					if(rollbackList.size() > 0) {
					
						/* 성공건에 한하여 차감신청 취소요청(포인트+) */
						rollbackVo      = clipservice.RollbackMinusPoint(rollbackList, tMap);
						rollbackVoSucYn = rollbackVo.getSucYn();
						
						// List<ClipPointIfVo> rbResultList = (List<ClipPointIfVo>)rollbackVo.getRstBody();
						List<ClipPointIfVo> rbResultList = new ArrayList<ClipPointIfVo>((List<ClipPointIfVo>)rollbackVo.getRstBody());
						log.debug(methodName, "stepCd["+ stepCd +"] rbResultList    => \n "+ rbResultList.toString());
						
						for(ClipPointIfVo fVo : rbResultList) {
							ClipPointIfVo vo = new ClipPointIfVo();
							vo.setCopy(fVo);
							vo.setStDealDt(DateUtils.getBetweenDates(vo.getPhubTrNo())[0]);
							vo.setEndDealDt(DateUtils.getBetweenDates(vo.getPhubTrNo())[1]);
							dao.insertPointRollback(vo);
						}
						
						stepCd = "082";
						
						if(rollbackVoSucYn.equals("F")||rollbackVoSucYn.equals("N")) {
							log.error(methodName, "stepCd["+ stepCd +"] Rollback Fail! (rollbackVoSucYn="+rollbackVoSucYn+")");
							// 취소실패시 Fatal Level Error처리
							throw new BizException(stepCd, messageManage.getMsgVOY("BZ_FATAL_160"), retmap);
						} else {
							isPointPayed = "F";
						}
					}
					
					log.debug(methodName, "+++++++++++ 카드결제 실패 BizException++++++++++++");
					
					// 클립포인트 메시지코드를 포인트허브 메시지 아이디로 전환하여 처리함.
					throw new BizException(stepCd, sClipMsgCd, sClipMsgTxt, retmap);
					
				} else if (resultVoSucYn.equals("F")){// not exist
					stepCd = "083";
					throw new BizException(stepCd, messageManage.getMsgVOY("BZ_FATAL_160"), retmap);
				}
				
				stepCd = "009";
				/*****************************************************************  
				 * 09.거래원장&상세 저장
				 *****************************************************************/
				iDeal = dao.insertDealInfo(params);
				iDtl  = dao.insertDealDtl(params);
					
				stepCd = "010";
				
		        /********************************************************** 
		         * 10. 쿠폰발행 진행조건(SbankDealVo기준)
		         * 10-1. 서비스아이디 serviceId = "SVC_FP"
		         * 10-2. PA인 경우만 쿠폰발행을 한다. (패밀리포인트의 경우 CA, MA는 없다.)
		         * 10-3. 0원 결제는 쿠폰발행은 하지 않는다
		         **********************************************************/
			    //log.debug(methodName, "resultVoSucYn=" + resultVoSucYn);
			    //log.debug(methodName, "sTrDiv=" + sTrDiv);
			    //log.debug(methodName, "service_id=" + StringUtil.nvl(retmap.get("service_id")));
			    //log.debug(methodName, "pay_amt=" + Integer.valueOf(StringUtil.nvl(retmap.get("pay_amt"), "0")));
			    sServiceId = StringUtil.nvl((retmap.get("service_id")));
			    
		        if ("Y".equals(resultVoSucYn)){
		        	
			        if( sServiceId.equals(Constant.SERVICE_ID_SVC_FP) && sTrDiv.equals("PA") ) {
			        	
			        	Map<String,Object> sMap   = new HashMap<String,Object>(retmap);
			        	Map<String,Object> cpnMap = new HashMap<String,Object>(retmap);
			        	cpnMap.put("cliplist" , cliplist);
			        	cpnMap.put("serviceId", sServiceId);
			        	
			        	Map<String,Object> issmap = new HashMap<String,Object>();
			        	
			        	issmap = sbankService.iSsueCoupon(cpnMap);
			        	
			        	sPhubCpnNo = StringUtil.nvl(issmap.get("phub_cpn_no"));
			        	
			        	sReturnUrl = StringUtil.nvl(issmap.get("return_url"));
			        	retmap.put("return_url" , sReturnUrl);
			        	retmap.put("phub_cpn_no", sPhubCpnNo);
			        }
		        }
		        
		        // 롤백을 위한 변수
		        isCouponIsed = "Y";
			}//~ SVC_FP, 0원 결제 이외 Case처리 & SVC_BASE
			
			if( sServiceId.equals(Constant.SERVICE_ID_SVC_FP) && sTrDiv.equals("PA") ) {
				retmap.put("return_url", sReturnUrl);
			}
			
		} catch (BizException be) {
			sLogCd    = StringUtil.nvl(be.getErrCd());
			sLogMsg   = StringUtil.nvl(be.getErrMsg());
			sDealStat = Constant.DEAL_STAT_3000;
			
			log.error(methodName, "BizException code=" + sLogCd);
			log.error(methodName, "BizException msg="  + sLogMsg);
			log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.debug(methodName, "☆☆☆☆☆☆ Ori Excpt = " + sServiceId+ " / "+ isPointPayed);
			sLogCd = messageManage.getMsgCd("SY_ERROR_900");
			sLogMsg= messageManage.getMsgTxt("SY_ERROR_900");
			sDealStat = Constant.DEAL_STAT_3000;
			
			throw new BizException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));
			
		} finally {
			
			retmap.put(Constant.RET_CODE, sLogCd);
			retmap.put(Constant.RET_MSG , sLogMsg);
			
			if(!"F".equals(isPointPayed) && !Constant.SUCCESS_CODE.equals(sLogCd)) {
				Map<String,Object> rbParam = new HashMap<String,Object>(params);
				
				//rbParam.put("cliplist", cliplist);
				rbParam.put("cliplist", resultVo.getRstBody());
				rbParam.put("serviceId",  sServiceId);
				
				if(!"F".equals(isPointPayed)){
					/* 포인트결제 롤백 */
					sbankService.rollbackDealInfo(rbParam);
					sbankService.rollbackPay(rbParam);
				}
			}

		
			/* Logging */
			params.remove("stDealDt");
			params.remove("endDealDt");
			params.remove("token_dts");

			log.debug(methodName, "rPFP return ==> \n" + retmap.toString());

		}
		
		
		return retmap;
		
	}
	

	@Override
	public void rollbackDealInfo(Map<String, Object> params) {
		int cnt = dao.deletePhDealInfo(params);
		
		log.debug("rollbackDealInfo", "$$$$$$ rollbackDealInfo $$$$==> "+ cnt);
		if (cnt > 0) {
			log.debug("rollbackDealInfo", "$$$$$$ before deletePhDealDtl $$$$");
			dao.deletePhDealDtl(params);
		}
		
	}

	/**
     * <pre>결제후속처리(오류발생시에만 처리)</pre>
     * @see <pre></pre>
     * @author cisohn
     * @since  2019.03.15
     * @param  Map (String phub_tr_no)
     * @return void
	 * @throws BizException 
     * @description 
     */
	@SuppressWarnings("unchecked")	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void updateFamilySupotSbank(Map<String,Object> params) throws BizException , Exception {
		String methodName = "updateFamilySupotSbank";
		String stepCd     = "001";
		String sPrgrCd    = Constant.CPN_REQ_PRGRS_PR200;
		String sTtlPayAmt = StringUtil.nvl(params.get("ttlPayAmt"),"0");
		
		Map<String,Object> fmap = new HashMap<String,Object>(params);
		
		if(!StringUtil.nvl(params.get("code")).equals(Constant.SUCCESS_CODE)){
			sPrgrCd = Constant.CPN_REQ_PRGRS_PR900;
			fmap.put("prgr_stat", sPrgrCd);
			gsService.updateFmlySupotSbank(fmap);
		} else {
			fmap.put("prgr_stat", sPrgrCd);
			gsService.updateFmlySupotSbank(fmap);
		}
		

	}
	

	/**
     * <pre>포인트 결제 취소(내부 예외처리용)</pre>
     * @see <pre></pre>
     * @author cisohn
     * @since  2019.03.11
     * @param  Map (String phub_tr_no)
     * @return int
     * @description : Transactional(propagation=Propagation.REQUIRES_NEW) 처리시
     *                취소처리할 원장정보가 보이지 않는다.(새로운 트랜잭션이므로) 
     */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void rollbackPay(Map<String, Object> params)  throws BizException, Exception {
		String methodName = "rollbackPay";
		String stepCd     = "001";
		Map<String,Object> retmap = new HashMap<String,Object>();
		Map<String,Object> tMap   = new HashMap<String,Object>();
		
		List<ClipPointIfVo> rollbackList = new ArrayList<ClipPointIfVo>();
		//List<ClipPointIfVo> cliplist =  (List<ClipPointIfVo>) params.get("cliplist");
		List<ClipPointIfVo> cliplist = new ArrayList<ClipPointIfVo>((List) params.get("cliplist"));

		String sServiceId = StringUtil.nvl(params.get("serviceId"));
		
		if(cliplist.size() > 0) {
			//Root 포인트거래번호채번
			String sPntRootId = comnservice.getPntTrNoRoot(params);
			for(ClipPointIfVo fVo: cliplist){
				ClipPointIfVo vo = new ClipPointIfVo();
				vo.setCopy(fVo);
				String sPntPrfx = comnservice.getPntTrNoPrfx(vo.getPntCd());
				String sPntTrNo = sPntPrfx + sPntRootId;
				
				vo.setOriPntTrNo(vo.getPntTrNo());
				vo.setPntTrNo(sPntTrNo);
				
				rollbackList.add(vo);
			}
			
			ResultVo rollbackVo = new ResultVo();
			tMap.put("pgDealNo" , params.get("pg_tr_no"));
			tMap.put("service_id", sServiceId);
			
			rollbackVo = clipservice.RollbackMinusPoint(rollbackList, tMap);
			String rollbackVoSucYn = rollbackVo.getSucYn();
			
			stepCd = "094";
			//List<ClipPointIfVo> rbResultList = (List<ClipPointIfVo>)rollbackVo.getRstBody();
			List<ClipPointIfVo> rbResultList = new ArrayList<ClipPointIfVo>((List<ClipPointIfVo>)rollbackVo.getRstBody());
			
			log.debug(methodName, "stepCd["+ stepCd +"] rbResultList    => \n "+ rbResultList.toString());
			
			for(ClipPointIfVo fVo : rbResultList) {
				ClipPointIfVo vo = new ClipPointIfVo();
				vo.setCopy(fVo);
				vo.setStDealDt(DateUtils.getBetweenDates(vo.getPhubTrNo())[0]);
				vo.setEndDealDt(DateUtils.getBetweenDates(vo.getPhubTrNo())[1]);
				dao.insertPointRollback(vo);
			}
			
			stepCd = "095";
			if(rollbackVoSucYn.equals("F")||rollbackVoSucYn.equals("N")) {
				log.error(methodName, "stepCd["+ stepCd +"] Rollback Fail! (rollbackVoSucYn="+rollbackVoSucYn+")");
				// 취소실패시 Fatal Level Error처리
				throw new BizException(stepCd, messageManage.getMsgVOY("BZ_FATAL_160"), retmap);
			}
			
			stepCd = "096";
			
			//sbankService.rollbackDealInfo(params);
			
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(rollbackFor={BizException.class, Exception.class})
	public Map<String,Object> iSsueCoupon(Map<String,Object> params) throws BizException, Exception {
		//public Map<String,Object> iSsueCoupon(Map<String,Object> params) throws BizException, Exception {
		Map<String,Object> retmap = new HashMap<String,Object>();
		Map<String,Object> tmpMap = new HashMap<String,Object>();
		Map<String,Object> fmap   = new HashMap<String,Object>();
		Map<String,Object> tMap   = new HashMap<String,Object>();
		String methodName = "iSsueCoupon";
		String sPhubCpnNo = "";
		String stepCd     = "001";
		String sServiceId = StringUtil.nvl(params.get("serviceId"),"SVC_BASE");
		
		//log.debug(methodName, "iSsueCoupon==> \n" + params.toString());
		
		// 쿠폰발행을 위한 기초정보 조회
		ResultVo cpnIsuInitDatVo = fpService.getPreInfoB4CouponIssue(params);
		log.debug(methodName, "cpnIsuInitDatVo==> \n" + cpnIsuInitDatVo.toString());
		
		stepCd = "091";
		if( ("N".equals(StringUtil.nvl(cpnIsuInitDatVo.getSucYn(), "N"))) || (cpnIsuInitDatVo.getMap().isEmpty()) ) {
			log.error(methodName, "getPreInfoB4CouponIssue Fail!=" + messageManage.getMsgTxt("BZ_ERROR_166"));
			throw new BizException(stepCd, StringUtil.nvl(cpnIsuInitDatVo.getRstCd()), StringUtil.nvl(cpnIsuInitDatVo.getRstMsg()));		        		
		} else {
			stepCd = "092";
			tmpMap.clear();
			tmpMap = cpnIsuInitDatVo.getMap();
			
			log.debug(methodName, "tmpMap.toString()tmpMap.toString()tmpMap.toString()tmpMap.toString()=> \n"+ tmpMap.toString());
			
			retmap.put("return_url", StringUtil.nvl(tmpMap.get("return_url")));
			
			// 쿠폰발행 (0원 결제는 쿠폰발행은 하지 않는다)
			if( Integer.valueOf(StringUtil.nvl(params.get("pay_amt"), "0")) > 0 ) {
				// 쿠폰발행
				//log.debug(methodName, "bef issueCoupon.implmap=" + tmpMap.toString());
				ResultVo fpResultVo = fpService.issueCoupon(tmpMap);
				
				log.debug(methodName, "§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§=> \n" + fpResultVo.toString());
				
				// 쿠폰생성 실패하면 포인트환불처리 해야 한다!!!!!!!!!!!!!!!!!!
				stepCd = "093";
				if( "Y".equals(StringUtil.nvl(fpResultVo.getSucYn(), "N")) ) {
					sPhubCpnNo = fpResultVo.getPhubCpnNo();
					
					retmap.put("phub_cpn_no", sPhubCpnNo);
					
					/* fp_fmly_supot */
					fmap.put("custCtn"  , StringUtil.nvl(tmpMap.get("cust_ctn")));
					fmap.put("prgr_stat", Constant.CPN_REQ_PRGRS_PR200);
					fmap.put("phub_cpn_no", StringUtil.nvl(tmpMap.get("phub_cpn_no")));
					
					if("Y".equals(StringUtil.nvl(tmpMap.get("ownYn")))){
						fmap.put("phubTrNo", StringUtil.nvl(tmpMap.get("phub_tr_no")));
					} else {
						fmap.put("phubTrNo", StringUtil.nvl(tmpMap.get("ownerPhubTrNo")));
					}
					
				} else {
					// 쿠폰생성 실패
					stepCd = "096";
					log.error(methodName, "issueCoupon Fail!=" + messageManage.getMsgTxt("BZ_ERROR_175"));
					throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_175"), retmap);
				}
			}
		}
		
		return retmap;
		
	}
	
	/**
    * @description PG사로부터의 포인트 결제취소 요청 처리
    *              1.선행 결제번호의 유무확인
    *              2.check_hash 검증 후 진행
    *                server TO server 통신으로 uri는 "/pg/pay" 임
    *              3.유입되는 결제구분은 "CA" & "MA"(망취소)
    *                (망취소는 해쉬체크 없음)
    * @param  Map
    * @return Map
    */
	@Override
	//@Transactional(propagation=Propagation.REQUIRES_NEW)
	public Map<String,Object> requestCancelFromPg(Map<String, Object> param)  throws BizException, Exception{
		String methodName = "requestCancelFromPg";
		String stepCd = "001";
		
		Map<String,Object> params    = new HashMap<String,Object>(param);
		HttpServletRequest request   = SystemUtils.getCurrentRequest();
		Map<String,Object> retmap    = new HashMap<String,Object>();
		Map<String,Object> checkhash = new HashMap<String,Object>();
		Map<String,Object> orimap    = new HashMap<String,Object>();
		Map<String,Object> statparam = new HashMap<String,Object>();
		Map<String,Object> dealdd    = new HashMap<String,Object>();
		
		String sDealInd     = StringUtil.nvl(params.get("tr_div"));
		String sOriPhubTrNo = StringUtil.nvl(params.get("phub_tr_no"));
		String sPhubTrNo = "";
		String sDealDate = "";
		String sDealTime = "";
		int iDeal  = 0;
		int iDealD = 0;
		String sServiceId = "";
		
		// 취소건으로서 거래요청건이 생성됬는지 여부(오류메시지 저장여부 판단용)
		String isKeyChanged = "N";
		
		retmap.put(Constant.RET_CODE , messageManage.getMsgCd("SY_INFO_00"));
		retmap.put(Constant.RET_MSG  , messageManage.getMsgTxt("SY_INFO_00"));
		
		// 파티션테이블 조회성능향상용
		params.put("stDealDt" , DateUtils.getBetweenDates(sOriPhubTrNo)[0]);
		params.put("endDealDt", DateUtils.getBetweenDates(sOriPhubTrNo)[1]);		
		
		try {
		
			/***********************************************************  
			 * 1.Return값 셋팅
			 ***********************************************************/		
			retmap = dao.selectResultDataToPg(params);
			
			/***********************************************************  
			 * 2.Check용 해시 Check(망취소는 Skip)
			 ***********************************************************/
			if (sDealInd.equals("CA")) {
				checkhash = checkHash(params);
				if(checkhash != null){
					if(!checkhash.get(Constant.RET_CODE).equals("00")){
						// 요청사항 등록전이므로 기존번호건 수정 방지
						retmap.put("phub_tr_no", "*");
						throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_109"), retmap);
					}
				}
			}
			
			stepCd = "003";
			
			/***********************************************************
			 * 3.취소대상 건 존재유무 체크("CA","MA" in common)
			 *   - 패밀리 포인트 쿠폰과 연계된 건은 취소대상 아님.
			 ***********************************************************/
			orimap = dao.checkDealByPgTrNo(params);
			
			Map<String,Object> exmap = new HashMap<String,Object>();
			
			if(Common.isNull(orimap)) {
				exmap.put("tr_dtm" , "");
				exmap.put("pay_amt", 0);
				
				// 요청사항 등록전이므로 기존번호건 수정 방지
				exmap.put("phub_tr_no", "*");
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			} else {
				/* 
				 * 취소플래그 cncl_yn 은 "Y","A","N" 각각 일반취소,어드민취소,취소안됨의 의미임
				 * "N" 이 아닌 이미 취소된 건을 취소하려고 하는 의도이므로 예외처리함.
				 * */
				if (!"N".equals(StringUtil.nvl(orimap.get("cncl_yn")))){
					exmap.put("tr_dtm" , orimap.get("cancel_dt"));
					exmap.put("pay_amt", orimap.get("pay_amt"));
					
					// 요청사항 등록전이므로 기존번호건 수정 방지
					exmap.put("phub_tr_no", "*");
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_156"), exmap);
				}
				
				sOriPhubTrNo = (String)orimap.get("ori_phub_tr_no");
			}
			
			// 서비스 아이디 가져오기
			sServiceId   = this.getServiceId(params);
			
			// 원거래번호 셋팅
			params.put("ori_phub_tr_no", (String)orimap.get("ori_phub_tr_no"));
	
			log.debug(methodName, "service 파람: \n"+ params.toString());
			
			/***********************************************************
			 * 4.거래요청 정보 등록(ph_deal_req_info, ph_deal_req_dtl)
			 *   포인트거래번호 생성로직 변경(19.01.28)
			 ***********************************************************/
			stepCd = "003";
			iDeal  = dao.insertDealCancelReqInfo(params);
			
			// 변경됨(19.01.28)
			String sPntRootId = comnservice.getPntTrNoRoot(params);
			params.put("pnt_root_id", sPntRootId);
			
			iDealD = dao.insertDealCancelReqDtl(params);
			
			// 취소요청 등록 후 params 의 phub_tr_no 가 새롭게 바뀐다.
			sPhubTrNo = (String) params.get("phub_tr_no");
			
			// 새로 생성된 Key를 리턴하도록 한다.
			retmap.put("phub_tr_no", sPhubTrNo);
			
			// 테스트아님
			isKeyChanged = "Y";
			
			
			/*********************************************************** 
			 * 5.클립포인트 연동처리(포인트 가감처리)
			 ***********************************************************/
			ResultVo resultVo    = new ResultVo();
			ResultVo rollbackVo  = new ResultVo();
			
			params.put("ori_phub_tr_no", sOriPhubTrNo);
			
			// 파티션테이블 조회성능향상용(새로운 거래번호로 날짜구간을 갱신해야 함)
			params.put("stDealDt", DateUtils.getBetweenDates(sPhubTrNo)[0]);
			params.put("endDealDt", DateUtils.getBetweenDates(sPhubTrNo)[1]);			
			
			List<ClipPointIfVo> cliplist = dao.selectCancelDataToClip(params);
	
			log.debug(methodName, "before cancelpoint ==>\n"+ params.toString() );
					
			/* 포인트사용 취소요청 */
			stepCd = "004";
			Map<String,Object> tMap = new HashMap<String,Object>();
			tMap.put("pgDealNo"  , params.get("pg_tr_no"));	
			tMap.put("service_id", sServiceId);	
			
			/*+++++++++++++++++++++++++++++++++++++++++++++++++++++*/
			/*+++++++++++++++ ClipPoint Api Call ++++++++++++++++++*/
			/*+++++++++++++++++++++++++++++++++++++++++++++++++++++*/
			resultVo = clipservice.cancelpoint(cliplist, tMap );
			
			log.debug(methodName, "cancelpoint's Result => \n"+ JsonUtil.toJson(resultVo));
	
			/*----------------------------------------------------------------------  
			   성공/실패 정보를 ph_deal_req_dtl 테이블에 적용한다.
			   성공건에 대한 취소처리: INSERT
			   실패건에 대한 에러코드 저장: update "sucs_yn", "err_cd", "err_msg"
			 ----------------------------------------------------------------------*/
			//List<ClipPointIfVo> retList = (List<ClipPointIfVo>)resultVo.getRstBody();
			List<ClipPointIfVo> retList = new ArrayList<ClipPointIfVo>((List<ClipPointIfVo>)resultVo.getRstBody());
			
			for(ClipPointIfVo fVo : retList) {
				ClipPointIfVo vo = new ClipPointIfVo();
				vo.setCopy(fVo);
				vo.setStDealDt(DateUtils.getBetweenDates(sPhubTrNo)[0]);
				vo.setEndDealDt(DateUtils.getBetweenDates(sPhubTrNo)[1]);			
				dao.updateCancelPntResult(vo);
			}
			
			stepCd = "006";
			
			/*----------------------------------------------------------------------
			  resultVo.getSucYn() N: 부분실패(1건이라도 차감성공건 존재)
			                    , F: 전체실패(차감성공건 0건)
			                    , Y: 전체성공
			 ----------------------------------------------------------------------*/
			String resultVoSucYn = resultVo.getSucYn();
			
			if(!resultVoSucYn.equals("Y")) {
				// RollbackMinusPoint, cancelpoint : 실패시 BZ_FATAL_160 으로 고정 셋팅
				log.error(methodName, "Rollback Fail in Cancel Process");
				throw new BizException(stepCd, messageManage.getMsgVOY("BZ_FATAL_160"), retmap);
			}
	
			/*----------------------------------------------------------------------
			  정확한 거래일자 표기 
			 ---------------------------------------------------------------------*/
			sDealDate = StringUtil.nvl(resultVo.getRstDate());
			sDealTime = StringUtil.nvl(resultVo.getRstTime());
			
			dealdd.put("phub_tr_no", sPhubTrNo);
			dealdd.put("deal_dt"   , sDealDate + sDealTime);
	
			dealdd.put("stDealDt" , DateUtils.getBetweenDates(sPhubTrNo)[0]);
			dealdd.put("endDealDt", DateUtils.getBetweenDates(sPhubTrNo)[1]);
			
			dao.updateDealReqInfoDealDt(dealdd);
			dao.updateDealReqDtlDealDt(dealdd);
			
			/*********************************************************** 
			 * 7.거래원장&상세 저장(ph_deal_info, ph_deal_dtl)
			 ***********************************************************/
			stepCd = "007";
			
			iDeal  = dao.insertDealInfo(params);
			iDealD = dao.insertDealDtl(params);
			
			/***********************************************************  
			 * 8.이전 거래정보 수정(ph_deal_info.cncl_yn='Y') [취소여부Y]
			 ***********************************************************/
			stepCd = "008";
			params.put("cncl_yn" , "Y");
			params.put("cncl_ind", "Y");
			
			dao.updateOriDealInfo(params);
			
			retmap.put("tr_dtm", sDealDate+sDealTime);
	
			/***********************************************************  
			 * 9.거래일자 저장하기(pg_deal_req_info & pg_deal_info)
			 ***********************************************************/
			if(retmap.get(Constant.RET_CODE).equals("00")){
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("phub_tr_no", params.get("phub_tr_no"));						
				map.put("user_id"   , params.get("cust_id"));
				map.put("deal_dt"   , sDealDate + sDealTime);
				map.put("err_cd"    , retmap.get(Constant.RET_CODE));
				map.put("err_msg"   , retmap.get(Constant.RET_MSG));
				map.put("deal_stat" , Constant.DEAL_STAT_2000);
				
				dao.updateDealReqInfoStat(map);
			}
	
			/*TODO FOR TEST
			if(sysPrmtManage.getSysPrmtVal("TEST_CARD_NM").equals("888")){
				if(cliplist.size() == 1){
					if(cliplist.get(0).getPntAmt().equals("888")) {
						retmap.put("points" , "1888");
						retmap.put("pay_amt", "1888");
					}
				}
			}		
			*/
		} catch (BizException be) {
			retmap.put(Constant.RET_CODE , be.getErrCd());
			retmap.put(Constant.RET_MSG  , be.getErrMsg());
		} catch (Exception e){
			retmap.put(Constant.RET_CODE , messageManage.getMsgCd("SY_ERROR_900"));
			retmap.put(Constant.RET_MSG  , StringUtil.nvl(e.getMessage()));			
		} finally {
			// 오류발생시 & 취소요청건이 생성된 경우에 한하여 업데이트
			if (!retmap.get(Constant.RET_CODE).equals(Constant.SUCCESS_CODE) && "Y".equals(isKeyChanged)) {
				statparam.put("phub_tr_no"  , StringUtil.nvl(params.get("phub_tr_no")  ,"-"));
				statparam.put("err_cd"      , StringUtil.nvl(retmap.get(Constant.RET_CODE)));
				statparam.put("err_msg"     , StringUtil.nvl(retmap.get(Constant.RET_MSG)));
				statparam.put("deal_stat"   , Constant.DEAL_STAT_3000);
		    	
		    	updateDealReqInfoStat(statparam);
			}
			
			logRequestFromPg(params, retmap, request , methodName);
			
		}
		
		log.debug(methodName, "Service Last::==> \n" + retmap.toString());
		
		return retmap;
		
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int updateDealReqInfoStat(Map<String, Object> params){
		return dao.updateDealReqInfoStat(params);
	}
	
	/**
     * @author cisohn
     * @since  2018.11.09
     * @param  Map 
     * @return void
     * @description 외부로부터의 요청정보 로깅
     */	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void logRequestFromPg(Map<String,Object> params, Map<String,Object> resmap,HttpServletRequest req , String pMethod ) {
		Map<String, Object> logMap  = new HashMap<String,Object>();
		Map<String, Object> sendmap = new HashMap<String,Object>(resmap);
		String methodName = "logRequestFromPg";
		
		log.debug(methodName, "params => \n"+ params.toString());
		
		sendmap.remove("add_param");
		sendmap.remove("service_id");
		sendmap.remove("cust_id");
		sendmap.remove("cust_ctn");
		sendmap.remove("deal_stat");
		sendmap.remove("phub_cpn_no");
		
		try {
			
			logMap.put("actn_uri"      , req.getRequestURI());
			logMap.put("send_plc"	   , SEND_PLC);
			logMap.put("rcv_plc"	   , RECV_PLC);
			logMap.put("phub_tr_no"	   , StringUtil.nvl(params.get("phub_tr_no")));
			logMap.put("pg_deal_no"	   , StringUtil.nvl(params.get("pg_tr_no")));
			logMap.put("req_data"	   , JsonUtil.MapToJson(params));
			logMap.put("res_data"	   , JsonUtil.MapToJson(sendmap));
			logMap.put("rply_cd"	   , StringUtil.nvl(sendmap.get(Constant.RET_CODE),"00"));
			logMap.put("rply_msg"	   , StringUtil.nvl(sendmap.get(Constant.RET_MSG), "200"));
			logMap.put("rgst_user_id"  , pMethod);
			
			logService.insertAdmApiIfLog(logMap);
		
		} catch (Exception e) {
			log.error(methodName, JsonUtil.MapToJson(logMap));
			log.error(methodName, StringUtil.nvl(e.getMessage()));
		}
	}	
	
	@Override
	public int insertDealReqInfoPre(Map<String, Object> params) throws Exception {
		return dao.insertDealReqInfoPre(params);
	}

	@Override
	public int updateDealReqInfoStatPre(Map<String, Object> params) {
		return dao.updateDealReqInfoStatPre(params);
	}

	/**
	    * @description 패밀리포인트로부터의 포인트 결제취소 요청 처리
	    * @param  Map (phub_tr_no)
	    * @return Map
	    */
		@SuppressWarnings("unchecked")
		@Override
		@Transactional(rollbackFor={BizException.class, Exception.class})
		public Map<String,Object> requestCancelFromFp(Map<String, Object> param, HttpServletRequest request) throws Exception,BizException {
			String methodName = "requestCancelFromFp";
			String stepCd = "001";
			Map<String,Object> retmap    = new HashMap<String,Object>();
			
			/* local param */
			Map<String,Object> params    = new HashMap<String,Object>(param);
			Map<String,Object> lparam    = new HashMap<String,Object>();
			Map<String,Object> orimap    = new HashMap<String,Object>();
			Map<String,Object> svc       = new HashMap<String,Object>();
			Map<String,Object> statparam = new HashMap<String,Object>();
			Map<String,Object> dealdd    = new HashMap<String,Object>();
			
			String sPhubTrNo = "", sPgTrNo = "", sOriPhubTrNo = "";
			String sDealDate = "";
			String sDealTime = "";
			int iDeal  = 0;
			int iDealD = 0;
	        String sLogCd  = Constant.SUCCESS_CODE;
	        String sLogMsg = Constant.SUCCESS_MSG;
	        String sServiceId = Constant.SERVICE_ID_SVC_BASE;
			
			// 취소건으로서 거래요청건이 생성됬는지 여부(오류메시지 저장여부 판단용)
			String isKeyChanged = "N";
	        
			log.debug(methodName, "requestCancelFromFp init:: \n"+ params.toString());
				
			retmap.put(Constant.RET_CODE , sLogCd);
			retmap.put(Constant.RET_MSG  , sLogMsg);
			retmap.put("ori_phub_tr_no"  , params.get("phub_tr_no"));
			retmap.put("pg_tr_no"        , params.get("pg_tr_no"));
			
			try {
			
				/* 포인트허브 거래번호, PG거래번호 셋팅 */
				sOriPhubTrNo = StringUtil.nvl(params.get("phub_tr_no"));
				sPgTrNo      = StringUtil.nvl(params.get("pg_tr_no"));
				sPhubTrNo    = StringUtil.nvl(params.get("phub_tr_no"));
				
				svc.put("phub_tr_no", sPhubTrNo);
				sServiceId   = this.getServiceId(svc);
				
				/* 검색성능 강화를 위한 조건부[pg거래번호는원거래건과 취소건이 동일하다.] */
				lparam.put("ori_phub_tr_no", sOriPhubTrNo);
				lparam.put("ori_pg_tr_no"  , sPgTrNo);
				lparam.put("pg_tr_no"      , sPgTrNo);
				lparam.put("stDealDt"      , DateUtils.getBetweenDates(sPhubTrNo)[0]);
				lparam.put("endDealDt"	   , DateUtils.getBetweenDates(sPhubTrNo)[1]);
				// TODO
				// 거래구분 null등록으로 인한 소스 추가 - 2019.03.21. lys
				lparam.put("tr_div"        , StringUtil.nvl(params.get("tr_div"),"CA"));
				
				/***********************************************************
				 * 1.취소대상 건 존재유무 체크
				 ***********************************************************/
				orimap = dao.checkBeforeCancel(lparam);
				if ("0".equals(StringUtil.nvl(orimap.get("cnt")))){
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
				}
				
				/***********************************************************
				 * 1-1.취소된 건인지 체크
				 ***********************************************************/				
				lparam.put("cncl_yn", "N");
				orimap = dao.checkBeforeCancel(lparam);
				if (!"0".equals(StringUtil.nvl(orimap.get("cnt")))){
					//이미 취소된 건이다
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_156"));
				}				

				log.debug(methodName, "service 파람 1: \n"+ lparam.toString());
				
				/***********************************************************
				 * 2.거래요청 정보 등록(ph_deal_req_info, ph_deal_req_dtl)
				 *   포인트거래번호 생성로직 변경(19.01.28)
				 ***********************************************************/		
				stepCd = "002";
				String sPntRootId = comnservice.getPntTrNoRoot(lparam);
				lparam.put("pnt_root_id", sPntRootId);
				
				iDeal  = dao.insertDealCancelReqInfo(lparam);	
				iDealD = dao.insertDealCancelReqDtl(lparam);
				
				log.debug(methodName, "service 파람 2: \n"+ lparam.toString());		
				
				sPhubTrNo = (String) lparam.get("phub_tr_no");
				
				/* 새로운 거래번호를 리턴값에 셋팅한다.(for update) */
				params.put("phub_tr_no", sPhubTrNo);
				
				params.put("ori_phub_tr_no", sOriPhubTrNo);
				
				statparam.put("phub_tr_no", sPhubTrNo);
				
				isKeyChanged = "Y";
				
				/*********************************************************** 
				 * 3.클립포인트 연동처리(포인트 가감처리)
				 ***********************************************************/
				stepCd = "003";
				
				List<ClipPointIfVo> cliplist = dao.selectCancelDataToClip(params);
				List<ClipPointIfVo> retList  = new ArrayList<ClipPointIfVo>();
				ResultVo resultVo = new ResultVo();
				
			  	String inDate   = new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
			  	String inTime   = new java.text.SimpleDateFormat("HHmmss").format(new java.util.Date());
				
				Map<String,Object> tMap = new HashMap<String,Object>();
				tMap.put("pgDealNo", params.get("pg_tr_no"));	
				tMap.put("service_id", sServiceId);
				
				// ClipPoint Interface Call
				resultVo = clipservice.cancelpoint(cliplist, tMap );
				
				log.debug(methodName, "패밀리포인트 취소결과 => \n"+ JsonUtil.toJson(resultVo));
		
				retList = (List<ClipPointIfVo>)resultVo.getRstBody();
			
				for(ClipPointIfVo vo : retList) {
					dao.updateCancelPntResult(vo);
				}
				
				stepCd = "004";		
				/*----------------------------------------------------------------------
				  resultVo.getSucYn() N: 부분실패(1건이라도 차감성공건 존재)
				                    , F: 전체실패(차감성공건 0건)
				                    , Y: 전체성공
				 ----------------------------------------------------------------------*/
				String resultVoSucYn = resultVo.getSucYn();
				
				/*----------------------------------------------------------------------
				  4.정확한 거래일자 표기 
				 ---------------------------------------------------------------------*/
				sDealDate = StringUtil.nvl(resultVo.getRstDate(), inDate);
				sDealTime = StringUtil.nvl(resultVo.getRstTime(), inTime);
				
				dealdd.put("phub_tr_no", sPhubTrNo);
				dealdd.put("deal_dt"   , sDealDate + sDealTime);
				retmap.put("deal_dt"   , sDealDate + sDealTime);
				
				if(!resultVoSucYn.equals("Y")) {
					throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_111"), retmap);
				}
				
				dao.updateDealReqInfoDealDt(dealdd);
				dao.updateDealReqDtlDealDt(dealdd);
				
				/*********************************************************** 
				 * 5.거래원장&상세 저장(ph_deal_info, ph_deal_dtl)
				 ***********************************************************/
				stepCd = "005";
				iDeal  = dao.insertDealInfo(params);
				iDealD = dao.insertDealDtl(params);
				
				/***********************************************************  
				 * 6.이전 거래정보 수정(ph_deal_info.cncl_yn='Y') [취소여부Y]
				 ***********************************************************/
				stepCd = "006";
				params.put("cncl_yn", "Y");
				params.put("cncl_ind", "Y");
				dao.updateOriDealInfo(params);
				
				retmap.put("tr_dtm", sDealDate+sDealTime);
		
				/***********************************************************  
				 * 7.거래일자 저장하기(pg_deal_req_info & pg_deal_info)
				 ***********************************************************/
				if(retmap.get(Constant.RET_CODE).equals("00")){
					Map<String,Object> map = new HashMap<String,Object>();
					map.put("phub_tr_no", params.get("phub_tr_no"));						
					map.put("user_id"   , params.get("cust_id"));
					map.put("deal_dt"   , sDealDate + sDealTime);
					map.put("err_cd"    , retmap.get(Constant.RET_CODE));
					map.put("err_msg"   , retmap.get(Constant.RET_MSG));
					map.put("deal_stat" , Constant.DEAL_STAT_2000);
					
					dao.updateDealReqInfoStat(map);
				}
			} catch (BizException be) {
				log.error(methodName, "BizException code=" + StringUtil.nvl(be.getErrCd()));
				log.error(methodName, "BizException msg =" + StringUtil.nvl(be.getErrMsg()));
				log.error(methodName, "BizException step=" + StringUtil.nvl(be.getStepCd()));
				sLogCd  = be.getErrCd();
				sLogMsg = be.getErrMsg();
				
				retmap.put("ret_code", be.getErrCd());
				retmap.put("ret_msg" , be.getErrMsg());
				
				statparam.put("err_cd"   , sLogCd);
				statparam.put("err_msg"  , sLogMsg);
				statparam.put("deal_stat", Constant.DEAL_STAT_3000);
				
			} catch (Exception e) {
				sLogCd  = messageManage.getMsgCd("SY_ERROR_900");
				sLogMsg = messageManage.getMsgTxt("SY_ERROR_900");
				retmap.put("ret_code", sLogCd);
				retmap.put("ret_msg" , sLogMsg);				
			} finally {
								
				// 오류발생시 & 취소요청건이 생성된 경우에 한하여 업데이트
				if (!retmap.get(Constant.RET_CODE).equals(Constant.SUCCESS_CODE) && "Y".equals(isKeyChanged)) {
					statparam.put(Constant.RET_CODE, sLogCd);
					statparam.put(Constant.RET_MSG , sLogMsg);
					this.updateDealReqInfoStat(statparam);
				}
			}
			
			return retmap;
		}
		
		
}
