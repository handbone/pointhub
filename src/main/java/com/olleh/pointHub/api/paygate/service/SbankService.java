/**
 * Copyright (c) 2018 KT, Inc.
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.paygate.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.exception.IfException;


/**
 * <pre> Sbank서비스를 위한 인터페이스 </pre>
 * @Class Name : SbankService
 * @author : cisohn
 * @since 2018.10.15
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일       수 정 자     수정내용
 * ----------  --------  -----------------
 * 2018.10.15   cisohn      최초생성
 * 2019.01.07   cisohn      checkDupReqByPgDealNo 삭제
 * </pre>
 */

public interface SbankService {
	/* 쿠폰발행 */
	public Map<String,Object> iSsueCoupon(Map<String,Object> params) throws BizException, Exception;
	
	/* 패밀리 포인트결제시 내부오류에 인한 거래정보 삭제 */
	public void rollbackDealInfo(Map<String, Object> params);
	
	/* 포인트결제시 내부오류에 인한 취소처리 */
	public void rollbackPay(Map<String, Object> params) throws BizException, Exception;
	
	/* 포인트규칙 검증 및 저장 */
	public Map<String,Object> checkAuthOfPg(Map<String, Object> params);
	
	/* 중복등록 방지 */
	public Map<String,Object> checkDupReqByPhubTrNo(Map<String, Object> params) throws BizException, Exception;
	
	/* 거래정보 체크(결제시) */
	public Map<String,Object> checkPgPayDealInfo(Map<String, Object> params) throws Exception;
	
	/* PG거래정보로부터 세틀뱅크 id 가져오기 */
	public String getPgId(Map<String, Object> params) throws Exception;

	/* 신청정보에서 서비스 아이디 가져오기 */
	public String getServiceId(Map<String, Object> params) throws Exception;

	
	/* 포인트 전환율 검색 */
	public List<Map<String,Object>> getPrvdrExchRate(List<Map<String,Object>> params); 

	/* 거래요청정보 등록 */
	public int insertDealReqInfoPre(Map<String, Object> params) throws Exception;	

	/* PG사로부터의 포인트결제요청 처리 */
	public Map<String,Object> requestCancelFromPg(Map<String, Object> params) throws BizException, Exception;
	
	/* PG사로부터의 포인트결제요청 처리 */
	public Map<String,Object> requestPayFromPg(Map<String, Object> params) throws BizException, Exception;
	
	/* 패밀리포인트로부터 포인트결제취소 요청 처리 */
	public Map<String,Object> requestCancelFromFp(Map<String, Object> params,HttpServletRequest request) throws Exception,BizException;
	
	/* 포인트규칙 검증 및 저장 */
	public Map<String,Object> saveDealReq(Map<String, Object> params) throws Exception;

	/* 포인트 결제정보 PG사로 전송하기 */
	public Map<String,Object> sendToPgPay(Map<String,Object> params) throws BizException, Exception;
	
	/* 신청정보 업데이트 */
	public int updateDealReqInfoStat(Map<String, Object> params);

	/* 신청정보 업데이트PRE */
	public int updateDealReqInfoStatPre(Map<String, Object> params);	
	
	/* 가족지원 상태 업데이트 */
	public void updateFamilySupotSbank(Map<String,Object> params) throws BizException , Exception;
	
	/* PG사로부터의  로그처리 */
	public void logRequestFromPg(Map<String,Object> params,Map<String,Object> sendmap, HttpServletRequest req , String pMethod );
	
}
