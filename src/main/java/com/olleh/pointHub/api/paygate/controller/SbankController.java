/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.paygate.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.olleh.pointHub.api.comn.service.ComnService;
import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.api.paygate.service.SbankService;
import com.olleh.pointHub.api.sdk.service.FamilyPointService;
import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.PgCmpnInfoManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>포인트 결제용 메인 콘트롤러</pre>
 * @Class Name : SbankController
 * @author : sci
 * @since :  2018.07.30
 * @version : 1.0
 * @see
 * <pre>
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    ---------------------------
 *  2018. 07. 30.     cisohn        최초 생성
 *  2018. 11. 02.     cisohn        2phase
 *
 * </pre>
 */

@RestController
public class SbankController {

	private Logger log = new Logger(this.getClass());
    
	@Autowired
	SbankService sbankService;
	
	@Autowired
	GiftiShowService gsService;
	
	@Autowired
	FamilyPointService fpService;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	PgCmpnInfoManage pgManage;
	
	@Autowired
	AccsCtrnManage  accsManage;
	
	@Autowired
	SysPrmtManage   sysPrmtManage;
	
	@Autowired
	ComnService comnService;

    /**
     * <pre>PG사로부터의 결제요청 처리(requestPayFromPg)</pre>
     * @see <pre>파라미터 상세정의는 외부연동규약서 참조</pre>
     * @author cisohn
     * @since  2019.03.07
     * @param  Map 
     * @return Map
     * @description Sㅔ틀뱅크로부터의 결제 요청 처리
     *  : 거래구분 "tr_div": "PA"[결제], "CA"[취소일반], "MA"[망취소]
     */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/pg/pay", method = RequestMethod.POST ,produces = "application/json; charset=utf-8")
	public Map<String,Object> initPayFromPg( @RequestBody Map<String,Object> params, HttpServletRequest request) throws  Exception {
		String methodName = "requestPayFromPg";
		String stepCd  = "001";
        String sLogCd  = Constant.SUCCESS_CODE;
        String sLogMsg = Constant.SUCCESS_MSG;
        String sDealStat = Constant.DEAL_STAT_2000;
        
        Map<String, Object> oriparams = new HashMap<String,Object>(params);
        Map<String, Object> resultmap = new HashMap<String, Object>();
        Map<String, Object> implmap   = new HashMap<String, Object>();
        Map<String, Object> tmpMap    = new HashMap<String, Object>();
        
        String sTrDiv = StringUtil.nvl(params.get("tr_div"));
        String sErr = "";
        String sServiceId = Constant.SERVICE_ID_SVC_BASE;
        String sPhubCpnNo = "";
        
        String sFpSupotPrgr = Constant.CPN_REQ_PRGRS_PR200;
        String custCtn = "";
        
        resultmap.put(Constant.RET_CODE, sLogCd);
        resultmap.put(Constant.RET_MSG , sLogMsg);
        
        /* 뒤로가기 반복에 의한 꼬임 방지 */
		boolean blMustUpdate = true;
		Map<String, Object> checkmap = new HashMap<String, Object>();
		checkmap.put("phub_tr_no", (String)params.get("phub_tr_no"));
		checkmap.put("uri"       , request.getRequestURI());

		
        try {
        	
        	log.debug(methodName, "pg/pay Init:: \n" + params.toString());
        	
            /****************************************************************************
             * 1.아이피 및 Header's key "Authorization" 검증[신청정보에 오류 메시지 저장]
             ****************************************************************************/
        	resultmap  = sbankService.checkAuthOfPg(oriparams);
        	sErr       = StringUtil.nvl(resultmap.get(Constant.RET_CODE));
        	
        	custCtn    = StringUtil.nvl(resultmap.get("cust_ctn"));
        	        	
        	sServiceId = StringUtil.nvl(resultmap.get("service_id"));
        	
        	if (!Constant.SUCCESS_CODE.equals(sErr)){
        		resultmap.put("tr_dtm", "");
        		throw new BizException(stepCd, messageManage.getMsgVOY(sErr));
        	}
        	
	        if(sTrDiv.equals("PA")){
	        	
				/***************************************************************
				 * 뒤로가기 반복에 의한 꼬임 방지
				 ***************************************************************/
	        	stepCd  = "002";
				if (!comnService.preventRetro(checkmap)){
					blMustUpdate = false;
					throw new BizException(stepCd , messageManage.getMsgVOY("IF_INFO_116"));
				}
	        	
	        	implmap = sbankService.requestPayFromPg(oriparams);
	        	
	        } else if(sTrDiv.equals("CA")){
	        	stepCd  = "003";
	        	implmap = sbankService.requestCancelFromPg(oriparams);
	        	
	        } else if(sTrDiv.equals("MA")){
	        	stepCd  = "004";
	        	implmap = sbankService.requestCancelFromPg(oriparams);	        	
	        }
	        
	        log.debug(methodName, "From Service:: => "+ implmap.toString());
	        
	        if(!StringUtil.nvl(implmap.get(Constant.RET_CODE)).equals(Constant.SUCCESS_CODE)){
	        	stepCd  = "005";
	        	throw new BizException(stepCd, StringUtil.nvl(implmap.get(Constant.RET_CODE)), StringUtil.nvl(implmap.get(Constant.RET_MSG)) );
	        }
	        
	        // PG전달용 데이타 셋팅  
	        resultmap.put("phub_tr_no", StringUtil.nvl(implmap.get("phub_tr_no")));
	        resultmap.put("tr_dtm"    , StringUtil.nvl(implmap.get("tr_dtm")));
	        resultmap.put("return_url", StringUtil.nvl(implmap.get("return_url")));
        	resultmap.put(Constant.RET_CODE, implmap.get(Constant.RET_CODE));
        	resultmap.put(Constant.RET_MSG , implmap.get(Constant.RET_MSG));
        	    
        } catch (BizException be) {
        	sLogCd  = be.getErrCd();
        	sLogMsg = be.getErrMsg();
        	sDealStat = Constant.DEAL_STAT_3000;
        	log.debug(methodName, "[BizException]"+ sLogMsg + "("+be.getStepCd()+")");
        	
        } catch (Exception e) {
        	sLogCd  = messageManage.getMsgCd("SY_ERROR_900");
        	sLogMsg = messageManage.getMsgTxt("SY_ERROR_900");
        	sDealStat = Constant.DEAL_STAT_3000;
        	log.printStackTracePH(methodName, e);
        	
        } finally {
        	
        	// 취소CA,MA는 서비스에서 처리
        	/* blMustUpdate 뒤로가기 반복 후 재조회 오류 회피 조건 추가 */
        	if(sTrDiv.equals("PA") && blMustUpdate){
				Map<String,Object> reqmap = new HashMap<String,Object>();
				reqmap.put("phub_tr_no"  ,  StringUtil.nvl(params.get("phub_tr_no")  ,"-"));
				reqmap.put("err_cd"      ,  sLogCd );
				reqmap.put("err_msg"     ,  sLogMsg);
				reqmap.put("deal_stat"   ,  sDealStat);		
				reqmap.put("deal_dt"     ,  StringUtil.nvl(implmap.get("tr_dtm")));
				
				sbankService.updateDealReqInfoStat(reqmap);
        	}
        	
        	log.debug(methodName, "Coooooooontrolllller "+ implmap.toString() );
        	
        	// temperally insert
        	/* blMustUpdate 뒤로가기 반복 후 재조회 오류 회피 조건 추가 */
			if( sServiceId.equals(Constant.SERVICE_ID_SVC_FP) && sTrDiv.equals("PA") && blMustUpdate) {
				String ownerPhubTrNo="";
				sPhubCpnNo = StringUtil.nvl(implmap.get("phub_cpn_no"));
				
				Map<String,Object> fmap = new HashMap<String,Object>();
				
				// 피요청자 거래번호를 add_param에서 개통자는 본 거래번호를 이용한다.
				if( !"".equals(StringUtil.nvl(implmap.get("add_param"))) ) {
					tmpMap.clear();
					tmpMap.put("add_param", StringUtil.nvl(implmap.get("add_param")));
					tmpMap.put("key",       "phubTrno");
					ownerPhubTrNo = StringUtil.nvl(fpService.getAddParamElmtVal(tmpMap), ownerPhubTrNo);
				} else {
					ownerPhubTrNo = (String) params.get("phub_tr_no");
				}
				
				fmap.put("ttlPayAmt", StringUtil.nvl(implmap.get("points"),"0"));
				fmap.put("custCtn"  , custCtn);
				fmap.put("phubTrNo" , ownerPhubTrNo);
				fmap.put("code"     , sLogCd);
				fmap.put("phub_cpn_no", sPhubCpnNo);
				
				sbankService.updateFamilySupotSbank(fmap);
				
			}
			
			resultmap.put(Constant.RET_CODE, sLogCd);
	        resultmap.put(Constant.RET_MSG , sLogMsg);
	        
	    	if( !(Constant.SUCCESS_CODE).equals(sLogCd) ) {
	    		if( !sLogCd.startsWith("I") ) {
	    			resultmap.put(Constant.RET_MSG , "결제에 실패하였습니다.");
	    		}
	    	}
	        log.debug(methodName, "\n resultmap=\n"+ JsonUtil.toJson(resultmap));
	        
        	params.remove(Constant.RET_CODE);
        	params.remove(Constant.RET_MSG);
        	
			sbankService.logRequestFromPg(oriparams, resultmap, request, methodName);
        	
        }
        log.debug(methodName, "finaly!");
        return resultmap;
        
	}
	
}
