/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.paygate.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pointHub.api.comn.model.ClipPointIfVo;
import com.olleh.pointHub.common.dao.AbstractDAO;
import com.olleh.pointHub.common.log.Logger;

/**
 * <pre>포인트결제를 위한 Dao</pre>
 * @Class Name : SbankDao
 * @author : cisohn
 * @since :  2018.07.30
 * @version : 1.0
 * @see
 * 
 * <pre>
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 7. 30.     cisohn        최초 생성
 *
 * </pre>
 * 
 */
@SuppressWarnings("unchecked")
@Repository
public class SbankDao extends AbstractDAO {
	private Logger log = new Logger(this.getClass());

    /**
     * <pre> 포인트결제 취소를 위한 이전거래 삭제</pre> 
     * @param  Map
     * @return Map
     */	
	public int deletePhDealInfo(Map<String,Object> params){
		return delete("mybatis.paygate.deletePhDealInfo",params);
	}
	
    /**
     * <pre> 포인트결제 취소를 위한 이전거래(상세) 삭제</pre> 
     * @param  Map
     * @return Map
     */	
	public int deletePhDealDtl(Map<String,Object> params){
		return delete("mybatis.paygate.deletePhDealDtl",params);
	}
	
    /**
     * <pre> 포인트결제 취소를 위한 이전자료 유무체크</pre> 
     * @param  Map
     * @return Map
     */	
	public Map<String,Object> checkDupReqByPhubTrNo(Map<String,Object> params){
		return selectOne("mybatis.paygate.checkDupReqByPhubTrNo",params);
	}	
	
    /**
     * <pre> 거래정보 체크 (결제)</pre>
     * @param  Map
     * @return Map
     */
	public Map<String,Object> checkPgPayDealInfo(Map<String,Object> params){
		return selectOne("mybatis.paygate.checkPgPayDealInfo", params);
	}	
	
    /**
     * <pre> 포인트결제 취소를 위한 이전자료 유무체크 </pre>
     * @param  Map
     * @return Map
     */	
	public Map<String,Object> checkDealByPgTrNo(Map<String,Object> params){
		return selectOne("mybatis.paygate.checkDealByPgTrNo",params);
	}
	
	
    /**
     * <pre>  결제요청 시각으로부터 PG사의 요청시각까지의 30분 제한시간 체크</pre>
     * @param  Map
     * @return Map
     */	
	public Map<String,Object> checkTransactionTime(Map<String,Object> params){
		return selectOne("mybatis.paygate.checkTransactionTime",params);
	}	
	
    /**
     * <pre> 포인트거래 번호 채번</pre> 
     * @param  Map
     * @return Map
     */
	public Map<String,Object> getPointTrNo(Map<String,Object> params){
		return selectOne("mybatis.paygate.getPointTrNo", params);
	}
	
	
	/**
     * <pre> 포인트전환율 검색</pre> 
     * @param  Map
     * @return Map
     */
	public List<Map<String,Object>> getPrvdrExchRate(List<Map<String,Object>> params){
		return selectList("mybatis.paygate.getPrvdrExchRate", params);
	}
	
	
    /**
     * <pre> 포인트 거래요청정보 마스터 등록</pre>
     * @param map
     * @return int
     */
	public int insertDealReqInfo(Map<String, Object> params) {
		return insert("mybatis.paygate.insertDealReqInfo", params);
	}

    /**
     * <pre> 포인트 거래요청정보 마스터 등록(PG사 요청시 즉시 등록)</pre>
     * @param map
     * @return int
     */
	public int insertDealReqInfoPre(Map<String, Object> params) {
		return insert("mybatis.paygate.insertDealReqInfoPre", params);
	}	
	
    /**
     * <pre> 포인트 거래요청정보 상세 등록 </pre>
     * @param map
     * @return int
     */
	public int insertDealReqDtl(Map<String, Object> params) {
		return insert("mybatis.paygate.insertDealReqDtl", params);
	}
	
    /**
     * <pre> 포인트 거래취소요청정보 등록 </pre>
     * @param map
     * @return int
     */
	//@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int insertDealCancelReqInfo(Map<String, Object> params) {
		return insert("mybatis.paygate.insertDealCancelReqInfo", params);
	}	
	
	
    /**
     * <pre> 포인트 거래취소요청정보 상세 등록 </pre>
     * @param map
     * @return int
     */
	//@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int insertDealCancelReqDtl(Map<String, Object> params) {
		return insert("mybatis.paygate.insertDealCancelReqDtl", params);
	}	
	
	
    /**
     * <pre> 포인트 거래취소요청정보 상세 등록 </pre>
     * @param map
     * @return int
     */
	//@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int insertPointRollback(ClipPointIfVo vo) {
		return insert("mybatis.paygate.insertPointRollback", vo);
	}	
	

	/**
     * <pre> 포인트거래 완료 후 처리(거래마스타 저장)</pre>
     * @param map
     * @return int
     */
	public int insertDealInfo(Map<String, Object> params) {
		return insert("mybatis.paygate.insertDealInfo", params);
	}

    /**
     * <pre> 포인트거래 완료 후 처리(거래상세 저장) </pre>
     * @param  map
     * @return int
     */
	public int insertDealDtl(Map<String, Object> params) {
		return insert("mybatis.paygate.insertDealDtl", params);
	}	
	
	
    /**
     * <pre> pg사 전송용 데이타 말기 </pre>
     * @param  map
     * @return List
     */	
	public Map<String,Object> selectReqDataToPg(Map<String,Object> params){
		return selectOne("mybatis.paygate.selectReqDataToPg",params);
	}

    /**
     * <pre> token check</pre>
     * @param  map
     * @return List
     */	
	public Map<String,Object> selectReqToken(Map<String,Object> params){
		return selectOne("mybatis.paygate.selectReqToken",params);
	}
	

    /**
     * <pre> PG사 아이디 검색메소드</pre>
     * @param  map
     * @return List
     */	
	public Map<String,Object> getPgIdByDealInfo(Map<String,Object> params){
		return selectOne("mybatis.paygate.getPgIdByDealInfo",params);
	}	
	
    /**
     * <pre> 포인트결제 결과 조회 </pre>
     * @param  Map
     * @return List
     */	
	public List<Map<String,Object>> selectPntPayResult(Map<String,Object> params){
		return selectList("mybatis.paygate.selectPntPayResult",params);
	}	

    /**
     * <pre> 포인트결제를 위한 클립연동 데이타 말기 </pre>
     * @param  Map
     * @return List
     */	
	public List<ClipPointIfVo> selectDataToClip(Map<String,Object> params){
		return selectList("mybatis.paygate.selectDataToClip",params);
	}
	
    /**
     * <pre> 포인트결제 취소를 위한 클립연동 데이타 말기 </pre>
     * @param  Map
     * @return List
     */	
	public List<ClipPointIfVo> selectCancelDataToClip(Map<String,Object> params){
		return selectList("mybatis.paygate.selectCancelDataToClip",params);
	}	
	
    /**
     * <pre> 포인트 가감처리 후 처리결과 PG사에 보낼 데이타 말기 </pre>
     * @param  Map
     * @return Map
     */
	public Map<String,Object> selectResultDataToPg(Map<String,Object> params){
		return selectOne("mybatis.paygate.selectResultDataToPg", params);
	}

	/**
     * <pre> 거래 유효성 체크를 위한(with PG) 데이타 해쉬용 데이타 조회 </pre>
     * @param  Map
     * @return Map
     */
	public Map<String,Object> selectDataToHashValue(Map<String,Object> params){
		return selectOne("mybatis.paygate.selectDataToHashValue", params);
	}	
	
	
    /**
     * <pre> 포인트 가감 신청(TO CLIP POINT) 성공 후 </pre>
     * @param map
     * @return int
     */
	public int updateDealReqInfoStat(Map<String, Object> params) {
		return update("mybatis.paygate.updateDealReqInfoStat", params);
	}
	
	
    /**
     * <pre>요청성공전 상태저장</pre>
     * @param map
     * @return int
     */
	public int updateDealReqInfoStatPre(Map<String, Object> params) {
		return update("mybatis.paygate.updateDealReqInfoStatPre", params);
	}	
	
	
    /**
     * <pre> 결제취소시 일대사(pg)기준일을 위해 클립포인트 거래일시로 맞춘다.</pre>
     * @param map
     * @return int
     */
	public int updateDealReqInfoDealDt(Map<String, Object> params) {
		return update("mybatis.paygate.updateDealReqInfoDealDt", params);
	}	
	
	public int updateDealReqDtlDealDt(Map<String, Object> params) {
		return update("mybatis.paygate.updateDealReqDtlDealDt", params);
	}	
	
    /**
     * <pre> 거래원장 거래일자 셋팅하기</pre>
     * @param map
     * @return int
    
	public int updateDealInfoDealDd(Map<String, Object> params) {
		return update("mybatis.paygate.updateDealInfoDealDd", params);
	}	
     */
	
    /**
     * <pre> SDK로 부터 결제요청시 신청정보 업데이트(결제요청ONLY)</pre>
     * @param  map
     * @return int
     */	
	public int updateDealReqInfoPre(Map<String,Object> params) {
		return update("mybatis.paygate.updateDealReqInfoPre", params);
	}
	
    /**
     * <pre> 취소처리 후 원장정보 수정</pre>
     * @param map
     * @return int
     */	
	public int updateOriDealInfo(Map<String,Object> params) {
		return update("mybatis.paygate.updateOriDealInfo", params);
	}
	
    /**
     * <pre> 거래내역 종합하여 마스터 수정 </pre>
     * @param map
     * @return int
     */
	public int updateDealReqTtlAmt(Map<String, Object> params) {
		return update("mybatis.paygate.updateDealReqTtlAmt", params);
	}
	
    /**
     * <pre> 클립포인트 결제요청 결과 업데이트 </pre>
     * @param map
     * @return int
     */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int updatePayPntResult(ClipPointIfVo vo) {
		return update("mybatis.paygate.updatePayPntResult", vo);
	}
	
    /**
     * <pre> 클립포인트 결제요청 결과 업데이트 </pre>
     * @param map
     * @return int
     */
	public int updateCancelPntResult(ClipPointIfVo vo) {
		return update("mybatis.paygate.updateCancelPntResult", vo);
	}

    /**
     * <pre> 포인트결제 취소를 위한 이전자료 유무체크(패밀리포인트용)</pre> 
     * @param  Map
     * @return Map
     */	
	public Map<String,Object> checkBeforeCancel(Map<String,Object> params){
		return selectOne("mybatis.paygate.checkBeforeCancel",params);
	}		
	
    /**
     * <pre> PG/PAY시 서비스아이디 가져오기</pre> 
     * @param  Map
     * @return Map
     */	
	public Map<String,Object> getServiceId(Map<String,Object> params){
		return selectOne("mybatis.paygate.selectServiceId",params);
	}	
	
}
