/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.paygate.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.api.comn.service.HttpService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.IfException;
import com.olleh.pointHub.common.model.MsgInfoVO;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre> 세틍뱅크(PG) http통신 서비스 </pre>
 * @Class Name : SbankHttpService
 * @author lys
 * @since 2018.07.25
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.25   lys        최초생성
 * </pre>
 */
@Service("SbankHttpService")
public class SbankHttpService extends HttpService {
	
	/**
	 * 수/발신처(SEND_RECV_PLC) 연동시스템 코드
	 * PH	포인트허브
	 * SB	세틀뱅크(PG)
	 */
    private String SEND_PLC = Constant.PLC_PH;
    private String RECV_PLC = Constant.PLC_SB;	
	
    @Autowired
    SysPrmtManage sysPrmtManage;
    
    @Autowired
    LogService logService;

    @Autowired
    MessageManage messageManage;
    
	/**
	 * <pre> 세틍뱅크(PG) http post방식 통신 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */
	public ResultVo sendHttpPostSB(String uri, Map<String, Object> params, Map<String, Object> etcMap) throws Exception {
		String methodName = "sendHttpPost";
		log.debug(methodName, "Start!");
		
		// set url
		String sendUrl = getDomain(uri);
		
		ResultVo resultVo = new ResultVo();
		
		// set header
	    Map<String, Object> hmap = new HashMap<String, Object>();
	    hmap.put("authorization", getAuthorization(etcMap));
	    hmap.put("appType", "json");
	    
	    try {
	    
	    // send
		//ResponseEntity<MultiValueMap> responseEntity = httpUtil.sendHttpPost("http://localhost/api/test", map, null);
	    log.debug(methodName, "sendUrl="+sendUrl);
	    resultVo = sendHttpPost(sendUrl, hmap, params);
	    resultVo.setCaller("SbankHttpService.sendHttpPostSB");
	    
	    // 수신 데이터 유효성 체크 (HttpService 에서 헤더체크 로직 호출하도록 해야 한다? override?)
	    // 1. authorization 체크
	    // 2. check_hash 체크 : PG거래번호 + 결제금액 + 가맹점코드(상덤ID) + 포인트허브거래번호 sha256암호화
	    // 3. token 체크 : 포인트허브 발급 인증키 + "|" + 현재시간(Unix시간, sec) AES-256으로 암호화 값  
			   
		} catch (IfException e) {
			MsgInfoVO msgInfoVO = messageManage.getMsgVOY("SY_ERROR_153");
			resultVo.setSucYn("N");
			resultVo.setRstCd(msgInfoVO.getMsgCd());
			resultVo.setRstMsg(msgInfoVO.getMsgNm());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.printStackTracePH(methodName, e);
			MsgInfoVO msgInfoVO = messageManage.getMsgVOY("SY_ERROR_900");
			resultVo.setSucYn("N");
			resultVo.setRstCd(msgInfoVO.getMsgCd());
			resultVo.setRstMsg(msgInfoVO.getMsgNm());
		}
	    
	    // log write
	    insertAdmApiIfLog(sendUrl, StringUtil.nvl(etcMap.get("phub_tr_no")), StringUtil.nvl(etcMap.get("pgDealNo")), resultVo);	
	    
		return resultVo;
	}
	
	/**
	 * <pre> 세틀뱅크(PG) http post방식 통신 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */	
	private String getDomain(String uri){
		// 도메인값 시스템파라미터에서 가져와야 한다.
		//String domain = "http://localhost" + uri;
		//String domain = sysPrmtManage.getSysPrmtVal("SB_CONN_IP") + uri;
		String domain = uri;
		
		return StringUtil.nvl(domain);
	}
	
	/**
	 * <pre> 세틍뱅크(PG) http post방식 통신 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */	
	private String getAuthorization(Map<String, Object> params){
		// 인증키값 암호화 해서 넘겨야 한다. (사이트별 인증키는 사이트정보 테이블에서 인증키를 가져와야 한다. 인증키가 암호화되서 저장되어 있으면 암호화 할 필요 없다.)
		String authorization = StringUtil.nvl(params.get("authorization"));
		
		return StringUtil.nvl(authorization);
	}
	
	/**
	 * <pre>전문 송/수신 Log기록</pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see
	 * 
	 */		
	private void insertAdmApiIfLog(String sendUrl, String phub_tr_no, String pgDealNo, ResultVo resultVo) {
		String methodName = "insertAdmApiIfLog";
		
		// declare
	    Map<String, Object> logMap = new HashMap<String, Object>();
	    
	    try {
		    logMap.put("actn_uri",     sendUrl);
		    logMap.put("send_plc",     SEND_PLC);
		    logMap.put("rcv_plc",      RECV_PLC);
		    logMap.put("phub_tr_no",   phub_tr_no);
		    logMap.put("pg_deal_no",   pgDealNo);
		    if (resultVo.getRstBody() != null){
			    logMap.put("req_data",     JsonUtil.MapToJson((Map)resultVo.getReqBody()));
			    logMap.put("res_data",     JsonUtil.MapToJson((Map)resultVo.getRstBody()));
			    logMap.put("rply_cd",      getRplyCd((Map)resultVo.getRstBody()));
			    logMap.put("rply_msg",     StringUtil.nvl(resultVo.getRstMsg()));
		    } else {
		    	logMap.put("req_data", "resultVo.getReqBody() is null");
		    	logMap.put("res_data", "resultVo.getReqBody() is null");
		    	logMap.put("rply_cd" ,  messageManage.getMsgCd("IF_ERROR_908"));
		    	logMap.put("rply_msg",  messageManage.getMsgTxt("IF_ERROR_908"));
		    }
		    
		    
		    logMap.put("rgst_user_id", "sendHttpPostSB");
		    
		    // insert log
		    logService.insertAdmApiIfLog(logMap);
		    
	    } catch(Exception e) {
	    	// 에러 로그 기록
	    	log.error(methodName, StringUtil.nvl(e.getMessage()));
	    }
	}
	
	/**
	 * <pre>전문 송/수신 결과코드 추출</pre>
	 * 
	 * @param Map<String, Object> rstBody
	 * @return String
	 * @see
	 * 
	 */		
	private String getRplyCd(Map<String, Object> rstBody) {
		String methodName = "getRplyCd";
		
		// declare
		String rplyCd = "";	
	    
	    try {
			rplyCd = StringUtil.nvl(rstBody.get(Constant.RET_CODE));
			
			// 세틀뱅크 결과코드가 "0061"인 경우 성공코드로 치환한다.
			if( "0061".equals(rplyCd) ) rplyCd = Constant.SUCCESS_CODE;
		    
	    } catch(Exception e) {
	    	// 에러 로그 기록
	    	log.error(methodName, StringUtil.nvl(e.getMessage()));
	    }
	    
	    return rplyCd;
	}	
}
