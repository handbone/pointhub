/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.provider.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.api.comn.model.ClipPointIfVo;
import com.olleh.pointHub.api.comn.service.ComnService;
import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.api.provider.dao.ClipDao;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.CertificationVO;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.DateUtils;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;


/**
 * 클립포인트서버 연동 service
 * @Class Name : ClipPointService
 * @author lys
 * @since 2018.08.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.05    lys        최초생성
 * </pre>
 */
@Service
public class ClipPointServiceImpl implements ClipPointService {
	private Logger log = new Logger(this.getClass());

	@Autowired
	ClipDao clipDao;

	@Autowired
	ClipPointHttpService clipPointHttpService;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	ClipService clipService;
	
	@Autowired
	ClipPointService clipPointService;

	@Autowired
	ComnService comnService;
	
	@Autowired
	GiftiShowService gsService;
	
	/**
	 * <pre> 카드사포인트 조회 </pre>
	 * 
	 * @param CPReqVo cPReqVo
	 * @return Map<String, Object>
	 * @see <pre>
	 *      여러 카드사 포인트 조회 
	 *      </pre>
	 */
	@Override
	public ResultVo getpoints(List<ClipPointIfVo> params, Map<String, Object> etcParam) throws Exception {
		//메소드명, 로그내용
		String methodName = "getpoints";
		log.debug(methodName, "Start!");
		//log.debug(methodName, "params=" + JsonUtil.toJson(params));
		
		// declare
		String stepCd                = "001";									// step 코드 (익셉션 발생시 사용)
		Map<String, Object> etcData  = new HashMap<String, Object>();
		Map<String, Object> sendData = new HashMap<String, Object>();
		List<ClipPointIfVo> rtnList  = new ArrayList<ClipPointIfVo>();
		ResultVo resultVo            = new ResultVo();		

		
		// 카드사별 포인트 조회
		String rstSucYn  = "N";
		String successYn = "N";
		
		// 04.09
		int totCnt = params.size();
		int f153900 = 0;
		// ~ end of 04.09
		
		for(ClipPointIfVo card : params) {
			try {
				// 송신데이터 셋팅
				sendData.clear();
				sendData.put("user_ci",   StringUtil.nvl(card.getUserCi()));	// 유저ci
				sendData.put("hub_tr_id", StringUtil.nvl(card.getPntTrNo()));	// 거래고유번호 (카드사별)
				sendData.put("ctn",       StringUtil.nvl(card.getCtn()));		// 전화번호(암호화 대상)
				sendData.put("user_id",   StringUtil.nvl(card.getUserId()));	// 거래요청자				
				sendData.put("partner",   StringUtil.nvl(card.getPntCd()));		// 카드사
				sendData.put("date",      DateUtils.defaultDate());				// 요청일자
				sendData.put("time",      DateUtils.defaultTime());				// 요청시간	
				
				// 기타추가 데이터 셋팅
				etcData.clear();
				etcData.put("phubTrNo",  StringUtil.nvl(card.getPhubTrNo()));		// 포인트허브거래번호				
				etcData.put("pgDealNo",  StringUtil.nvl(etcParam.get("pgDealNo")));	// PG거래번호
				
				// 카드사별 리턴 데이터 셋팅 - 송신데이터
				card.setReqBody(new HashMap<String, Object>(sendData));
				
				// 유효성 체크
				stepCd = "002";
				if( validateSendData(sendData) ) {
					// 포인트 조회 통신
					stepCd = "003";
					ResultVo result = clipPointHttpService.sendHttpPostCP("/hub/getpoint.do", sendData, etcData);
					
					// 결과값 체크하여 성공유무 플래그 변경
					stepCd = "004";
					if( "Y".equals(result.getSucYn()) ) {
						// 전문 결과값 체크
						//cPResVo = (CPResVo)result.getRstBody();
						stepCd = "005";
						Map rstBody = (Map)result.getRstBody();
						
						stepCd = "006";
						if( "0000".equals(rstBody.get("retcode")) ) {
							// 정상 결과값 셋팅
							rstSucYn  = "Y";
							successYn = "Y";
							stepCd = "007";
							card.setCurrentPoint((String)rstBody.get("current_point"));
														
						} else {
							// 실패
							log.debug(methodName, "retcode not 0000=" + rstBody.get("retcode"));
							successYn = "N";
						}
						
						// set retCode, retMsg, optcode
						stepCd = "008";
						card.setRetCode((String)rstBody.get("retcode"));
						card.setRetMsg((String)rstBody.get("retmsg"));
						card.setOptcode((String)rstBody.get("optcode"));
					} else {
						// 실패
						stepCd = "009";
						log.debug(methodName, "No result.getSucYn=" + result.getSucYn());
						successYn = "N";
						card.setRetCode((String)result.getRstCd());
						card.setRetMsg((String)result.getRstMsg());						
					}
				} else {
					// 필수항목 체크 실패
					stepCd = "010";
					successYn = "N";
					card.setRetCode(messageManage.getMsgCd("BZ_ERROR_152"));
					card.setRetMsg(messageManage.getMsgTxt("BZ_ERROR_152"));
				}
				
			} catch (Exception e) {
				// 테스트 하면서 익셉션 처리 보강 필요 - 2018/08/05. lys
				log.printStackTracePH(methodName, e);
				log.error(methodName, "Exception msg="+StringUtil.nvl(e.getMessage()));
				successYn = "N";
				card.setRetCode(messageManage.getMsgCd("SY_ERROR_900"));
				card.setRetMsg(StringUtil.nvl(e.getMessage()));
				
			} finally {
				// 성공이든 실패이든 전체를 다 담는다.
				card.setSucYn(successYn);
				rtnList.add(card);
				//log.debug(methodName, "finally card=" + JsonUtil.toJson(card));
			}
		}		
		
		// 04.09
		int jj=0;
		for(ClipPointIfVo card : rtnList) {
			if (StringUtil.nvl(card.getRetCode()).equals("F153") 
					|| StringUtil.nvl(card.getRetCode()).equals("F900")
					|| StringUtil.nvl(card.getRetCode()).equals("F907")
					|| StringUtil.nvl(card.getRetCode()).equals("F908")){				
				f153900 ++;
			}
		}
		//log.debug(methodName, "All F153900=> "+ totCnt + " / " + f153900);
		
		if(f153900 == totCnt){
			rstSucYn = "N";
		} else {
			rstSucYn = "Y";
		}
		// ~ end of 04.09 */
		
		
		// 리턴 오브젝트 셋팅
		// TODO
		// 한건이라도 조회 성공시 전체를 성공으로 본다. - 이건 확인해봐야 한다. 2018/08/05. lys
		resultVo.setSucYn(rstSucYn);
		resultVo.setRstBody(rtnList);
		
		// 리턴
		return resultVo;
	}

	/**
	 * <pre> 카드사포인트 차감 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. 카드사 포인트 차감
	 *      2. 다건 차감일 경우 1건이라도 실패시, 호출하는 쪽에서 차감성공건인것들 다시 차감취소처리 요청해야 한다. (성공유무 플레그값으로 판단 한다.)
	 *      3. 한건이라도 차감실패시 차감중단하고 전체를 실패로 본다? - 2018/08/05. lys
	 *      </pre>
	 */		
	@Override
	public ResultVo minuspoint(List<ClipPointIfVo> params, Map<String, Object> etcParam) throws Exception {
		//메소드명, 로그내용
		String methodName = "minuspoint";
		
		String reqDate = DateUtils.defaultDate();
		String reqTime = DateUtils.defaultTime();

		// declare
		String stepCd                 = "001";	// step 코드 (익셉션 발생시 사용)
		Map<String, Object> etcData   = new HashMap<String, Object>();
		Map<String, Object> sendData  = new HashMap<String, Object>();
		List<ClipPointIfVo> rtnList   = new ArrayList<ClipPointIfVo>();
		List<ClipPointIfVo> paramList = new ArrayList<ClipPointIfVo>(params);
		ResultVo resultVo             = new ResultVo();
		String sServiceId = StringUtil.nvl(etcParam.get("service_id"));
		
		resultVo.setRstDate(reqDate);
		resultVo.setRstTime(reqTime);
		
		log.debug(methodName, "Start!");
		log.debug(methodName, "params=" + JsonUtil.toJson(params));	
		
		// 카드사별 포인트 조회
		String successYn  = "Y";
		
		forCard : for(ClipPointIfVo fCard : paramList) {
			ClipPointIfVo card = new ClipPointIfVo();
			card.setCopy(fCard);
			try {
				// 카드사별 송신데이터 셋팅
				stepCd = "002";
				sendData.clear();
				sendData.put("user_ci"   , StringUtil.nvl(card.getUserCi()));	// 유저ci
				sendData.put("hub_tr_id" , StringUtil.nvl(card.getPntTrNo()));	// 거래고유번호 (카드사별)
				sendData.put("ctn"       , StringUtil.nvl(card.getCtn()));		// 전화번호(암호화 대상)
				sendData.put("user_id"   , StringUtil.nvl(card.getUserId()));	// 거래요청자				
				sendData.put("partner"   , StringUtil.nvl(card.getPntCd()));	// 카드사
				sendData.put("req_point" , StringUtil.nvl(card.getPntAmt()));	// 요청포인트
				sendData.put("date"      , reqDate);		// 요청일자
				sendData.put("time"      , reqTime);		// 요청시간	
				sendData.put("service_id", sServiceId);		// 서비스아이디	
				
				
				// 기타추가 데이터 셋팅
				etcData.clear();
				etcData.put("phubTrNo",  StringUtil.nvl(card.getPhubTrNo()));		// 포인트허브거래번호
				etcData.put("pgDealNo",  StringUtil.nvl(etcParam.get("pgDealNo")));	// PG거래번호
				
				// 카드사별 리턴 데이터 셋팅 - 송신데이터
				card.setReqBody(new HashMap<String, Object>(sendData));
				
				// 유효성 체크
				stepCd = "003";
				if( validateSendData(sendData) ) {
					// 포인트 차감 통신
					stepCd = "004";
					ResultVo result = clipPointHttpService.sendHttpPostCP("/hub/minuspoint.do", sendData, etcData);
					
					// 공통 결과값 체크하여 성공일때만 결과리스트에 담는다.
					stepCd = "005";
					if( "Y".equals(result.getSucYn()) ) {
						// 전문 결과값 체크
						stepCd = "006";
						Map rstBody = (Map)result.getRstBody();
						//if( "0000".equals(cPResVo.getRetcode()) ) {
						if( "0000".equals(rstBody.get("retcode")) ) {
							// 정상 결과값 셋팅 : 성공건만 리스트에 등록 한다.	
							stepCd = "007";
							//card.setCurrentPoint((String)rstBody.get("current_point"));
							card.setCurrentPoint(StringUtil.nvl(rstBody.get("current_point")));
						} else {
							// 실패
							log.debug(methodName, "retcode not 0000=" + rstBody.get("retcode"));
							successYn = "N";							
						}
						
						// set retCode, retMsg, optcode
						//card.setRetCode((String)rstBody.get("retcode"));
						//card.setRetMsg((String)rstBody.get("retmsg"));
						//card.setOptcode((String)rstBody.get("optcode"));
						card.setRetCode(StringUtil.nvl(rstBody.get("retcode")));
						card.setRetMsg(StringUtil.nvl(rstBody.get("retmsg")));
						card.setOptcode(StringUtil.nvl(rstBody.get("optcode")));						
						
		    			// 부분실패 테스트를 위한 bccard 오류 유도(성공한 다른 건 원복 테스트)
						/*
		    			String testYn =  sysPrmtManage.getSysPrmtVal("TEST_CARD_NM");
		    			if( !"N".equals(testYn)) {
		        			if (StringUtil.nvl(card.getPntCd()).equals(testYn)){
		        				// TODO throw 시켜야함
		        			}
		    			}
						*/
					} else {
						// 실패
						stepCd = "008";
						log.error(methodName, "No result.getSucYn=" + result.getSucYn());
						successYn = "N";
						card.setRetCode((String)result.getRstCd());
						card.setRetMsg((String)result.getRstMsg());						
					}
					
				} else {
					// 필수항목 체크 실패
					stepCd = "009";
					successYn = "N";					
					card.setRetCode(messageManage.getMsgCd("BZ_ERROR_152"));
					card.setRetMsg(messageManage.getMsgTxt("BZ_ERROR_152"));					
				}
				
			} catch (Exception e) {
				log.printStackTracePH(methodName, e);
				log.error(methodName, "Exception msg="+StringUtil.nvl(e.getMessage()));
				successYn = "N";
				card.setRetCode(messageManage.getMsgCd("SY_ERROR_900"));
				card.setRetMsg(StringUtil.nvl(e.getMessage()));				
				
			} finally {
				card.setSucYn(successYn);
				rtnList.add(card);
				log.debug(methodName, "finally card=" + JsonUtil.toJson(card));
				if( "N".equals(successYn) ) {
					resultVo.setRstCd(card.getRetCode());
					resultVo.setRstMsg(card.getRetMsg());
					break forCard;
				}
			}
		}
		
		// 리턴 오브젝트 셋팅
		// TODO
		// 한건이라도 차감실패시 전체를 실패로 본다.
		resultVo.setSucYn(successYn);
		resultVo.setRstBody(rtnList);
		
		// 리턴
		return resultVo;
	}
	
	
	/**
	 * <pre> 카드사포인트 차감 실패시 성공건에 대한 롤백처리 </pre>
	 * 
	 * @param CPReqVo cPReqVo
	 * @return ResultVo
	 * @see <pre>
	 *      1. 카드사포인트 차감 처리 중 실패할 시 성공건에 대해서는 클립포인트에 취소전문을 날린다.
	 *      2. 롤백처리 중 에러발생시 중단하지 않고 나머지건 계속 처리 한다.
	 *      3. resultVo.getSucYn() N: 부분실패(1건이라도 차감성공건 존재)
	 *                           , F: 전체실패(차감성공건 0건)
	 *                           , Y: 전체성공
	 *      </pre> 
	 */		
	@Override
	public ResultVo RollbackMinusPoint(List<ClipPointIfVo> params, Map<String, Object> etcParam) throws Exception {
		//메소드명, 로그내용
		String methodName = "RollbackMinusPoint";
		log.debug(methodName, "Start!");
		log.debug(methodName, "params=" + JsonUtil.toJson(params));
		
		// declare
		String stepCd                 = "001"; // step 코드 (익셉션 발생시 사용)
		List<ClipPointIfVo> paramList = new ArrayList<ClipPointIfVo>(params);
		Map<String, Object> etcData   = new HashMap<String, Object>();
		Map<String, Object> sendData  = new HashMap<String, Object>();
		List<ClipPointIfVo> rtnList   = new ArrayList<ClipPointIfVo>();
		ResultVo resultVo             = new ResultVo();
		String sServiceId = StringUtil.nvl(etcParam.get("service_id"));
		
		// 카드사별 포인트 차감 롤백 처리
		//String rstSucYn  = "F";
		String successYn = "N";
		int    forCnt    = 0;	// for문 횟수
		int    sucCnt    = 0;	// 성공건수
		int    failCnt   = 0;	// 실패건수
		for(ClipPointIfVo fCard : paramList) {	
			ClipPointIfVo card = new ClipPointIfVo();
			card.setCopy(fCard);
			
			try {
				// 성공인건만 취소전문을 날린다.
				stepCd = "002";
				successYn = "Y";
				if( "Y".equals(card.getSucYn()) ) {
					// 카드사별 송신데이터 셋팅
					stepCd = "003";
					sendData.clear();
					sendData.put("user_ci"     , StringUtil.nvl(card.getUserCi()));		// 유저ci
					sendData.put("hub_tr_id"   , StringUtil.nvl(card.getPntTrNo()));	// 거래고유번호 (카드사별)
					sendData.put("ctn"         , StringUtil.nvl(card.getCtn()));		// 전화번호(암호화 대상)
					sendData.put("user_id"     , StringUtil.nvl(card.getUserId()));		// 거래요청자				
					sendData.put("partner"     , StringUtil.nvl(card.getPntCd()));		// 카드사
					sendData.put("cancel_tr_id", StringUtil.nvl(card.getOriPntTrNo()));	// 취소거래고유번호 (취소하고자하는 원거래고유번호)
					sendData.put("cancel_point", StringUtil.nvl(card.getPntAmt()));		// 취소포인트
					sendData.put("date"        , DateUtils.defaultDate());				// 요청일자
					sendData.put("time"        , DateUtils.defaultTime());				// 요청시
					sendData.put("service_id"  , sServiceId);    // 서비스아이디
					
					// 기타추가 데이터 셋팅
					etcData.clear();
					etcData.put("phubTrNo",  StringUtil.nvl(card.getPhubTrNo()));	    // 포인트허브거래번호
					etcData.put("pgDealNo",  StringUtil.nvl(etcParam.get("pgDealNo")));	// PG거래번호
					
					// 카드사별 리턴 데이터 셋팅 - 송신데이터
					card.setReqBody(new HashMap<String, Object>(sendData));
					
					// TODO
					// 다건 취소는 실패해도 나머지건은 실패 처리로 한다? 확인필요! - 2018/08/05. lys
					// 유효성 체크
					stepCd = "004";
					if( validateSendData(sendData) ) {
						// 포인트 차감 통신
						stepCd = "005";
						ResultVo result = clipPointHttpService.sendHttpPostCP("/hub/cancelpoint.do", sendData, etcData);
						
						// 공통 결과값 체크하여 성공일때만 결과리스트에 담는다.
						stepCd = "006";
						if( "Y".equals(result.getSucYn()) ) {
							// 전문 결과값 체크
							//cPResVo = (CPResVo)result.getRstBody();
							stepCd = "007";
							Map rstBody = (Map)result.getRstBody();
							if( "0000".equals(rstBody.get("retcode")) ) {
								// 정상 결과값 셋팅
								card.setCurrentPoint((String)rstBody.get("current_point"));
								card.setOptcode((String)rstBody.get("optcode"));
								
							} else {
								log.debug(methodName, "retcode not 0000=" + rstBody.get("retcode"));
								successYn = "N";							
							}
							
							stepCd = "008";
							card.setRetCode((String)rstBody.get("retcode"));
							card.setRetMsg((String)rstBody.get("retmsg"));						
						} else {
							// 실패
							stepCd = "009";
							log.debug(methodName, "No result.getSucYn=" + result.getSucYn());
							successYn = "N";
							card.setRetCode((String)result.getRstCd());
							card.setRetMsg((String)result.getRstMsg());
						}
						
					} else {
						// 필수항목 체크 실패
						stepCd = "010";
						successYn = "N";
						card.setRetCode(messageManage.getMsgCd("BZ_ERROR_152"));
						card.setRetMsg(messageManage.getMsgTxt("BZ_ERROR_152"));						
					}
				} else {
					// 취소전문 안날린 거는 리턴목록(rtnList)에 담지 않는다. - 2019-03-28. lys
					stepCd = "011";
					successYn = "";					
				}
				
			} catch (Exception e) {
				log.printStackTracePH(methodName, e);
				log.error(methodName, "Exception msg="+StringUtil.nvl(e.getMessage()));
				successYn = "N";
				card.setRetCode(messageManage.getMsgCd("SY_ERROR_900"));
				card.setRetMsg(StringUtil.nvl(e.getMessage()));					
				
			} finally {
				// 성공이든 실패이든 list에 담고 마지막건까지 전부 처리 한다.
				stepCd = "011";
				if( !"".equals(successYn) ) {
					card.setSucYn(successYn);
					rtnList.add(card);
					
					// for문/성공/실패건수 증감처리
					forCnt++;
					if( "Y".equals(successYn) ) {
						sucCnt++;
					} else {
						failCnt++;
					}					
				}
				log.debug(methodName, "finally card=" + JsonUtil.toJson(card));				
			}
		}
		
		// 리턴 오브젝트 셋팅
		// TODO
		// 다건 취소는 실패해도 나머지건은 실패 처리로 한다? 확인필요! - 2018/08/05. lys 
		log.debug(methodName, "End sucCnt=" + sucCnt + ", failCnt=" + failCnt);
		
		// 결과값 셋팅
		resultVo.setRstBody(rtnList);
		if( forCnt == 0 ) {
			resultVo.setSucYn("Y");
		} else if( (sucCnt + failCnt) < 1 ) {
			resultVo.setSucYn("F");
		} else if( failCnt > 0 ) {
			resultVo.setSucYn("N");
		} else {
			resultVo.setSucYn("Y");
		}
		
		// 리턴
		return resultVo;
	}
	

	/**
	 * <pre> 카드사포인트 차감 취소 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. 카드사 포인트 차감 취소
	 *      2. 다건 취소는 실패해도 나머지건은 실패 처리로 한다? 확인필요! - 2018/08/05. lys
	 *      3. resultVo.getSucYn() N: 부분실패(1건이라도 차감성공건 존재)
	 *                           , F: 전체실패(차감성공건 0건)
	 *                           , Y: 전체성공
	 *      </pre>
	 */	
	@Override
	public ResultVo cancelpoint(List<ClipPointIfVo> params, Map<String, Object> etcParam) throws Exception {
		//메소드명, 로그내용
		String methodName = "cancelpoint";

		String reqDate = DateUtils.defaultDate();
		String reqTime = DateUtils.defaultTime();		
		
		// declare
		String stepCd                = "001";	// step 코드 (익셉션 발생시 사용)
		String sServiceId = StringUtil.nvl(etcParam.get("service_id"));
		Map<String, Object> etcData   = new HashMap<String, Object>();
		Map<String, Object> sendData  = new HashMap<String, Object>();
		List<ClipPointIfVo> rtnList   = new ArrayList<ClipPointIfVo>();
		List<ClipPointIfVo> paramList = new ArrayList<ClipPointIfVo>(params);
		ResultVo resultVo             = new ResultVo();
		
		resultVo.setRstDate(reqDate);
		resultVo.setRstTime(reqTime);		
		
		log.debug(methodName, "Start!");
		log.debug(methodName, "params=" + JsonUtil.toJson(params));
		
		
		String successYn = "N";
		int    sucCnt    = 0;	// 성공건수
		int    failCnt   = 0;	// 실패건수		
				
		// 카드사별 포인트 조회
		//List<Map<String, String>> pntList = (List)cPReqVo.getPntList();	// 카드사 리스트(거래고유번호 (카드사별), 카드사, 취소거래고유번호, 취소포인트)
		for(ClipPointIfVo fCard : paramList) {
			ClipPointIfVo card = new ClipPointIfVo();
			card.setCopy(fCard);
			try {
				// 카드사별 송신데이터 셋팅
				stepCd = "002";
				sendData.clear();
				sendData.put("user_ci"      , StringUtil.nvl(card.getUserCi()));		// 유저ci
				sendData.put("hub_tr_id"    , StringUtil.nvl(card.getPntTrNo()));	// 거래고유번호 (카드사별)
				sendData.put("ctn"          , StringUtil.nvl(card.getCtn()));		// 전화번호(암호화 대상)
				sendData.put("user_id"      , StringUtil.nvl(card.getUserId()));		// 거래요청자
				sendData.put("partner"      , StringUtil.nvl(card.getPntCd()));	// 카드사
				sendData.put("cancel_tr_id" , StringUtil.nvl(card.getOriPntTrNo()));	// 취소거래고유번호 (취소하고자하는 원거래고유번호)
				sendData.put("cancel_point" , StringUtil.nvl(card.getPntAmt()));		// 취소포인트
				sendData.put("date"         , reqDate);		// 요청일자
				sendData.put("time"         , reqTime);		// 요청시
				sendData.put("service_id"   , sServiceId);	// 서비스아이디
				
				// 기타추가 데이터 셋팅
				etcData.clear();
				etcData.put("phubTrNo",  StringUtil.nvl(card.getPhubTrNo()));		// 포인트허브거래번호
				etcData.put("pgDealNo",  StringUtil.nvl(etcParam.get("pgDealNo")));	// PG거래번호
				
				// 카드사별 리턴 데이터 셋팅 - 송신데이터
				card.setReqBody(new HashMap<String, Object>(sendData));
				
				// TODO
				// 다건 취소는 실패해도 나머지건은 실패 처리로 한다? 확인필요! - 2018/08/05. lys
				// 유효성 체크
				stepCd = "003";
				if( validateSendData(sendData) ) {
					// 포인트 차감 통신
					stepCd = "004";
					ResultVo result = clipPointHttpService.sendHttpPostCP("/hub/cancelpoint.do", sendData, etcData);
					
					// 공통 결과값 체크하여 성공일때만 결과리스트에 담는다.
					stepCd = "005";
					if( "Y".equals(result.getSucYn()) ) {
						// 전문 결과값 체크
						stepCd = "006";
						Map rstBody = (Map)result.getRstBody();
						if( "0000".equals(rstBody.get("retcode")) ) {
							// 정상 결과값 셋팅
							successYn = "Y";
							card.setCurrentPoint((String)rstBody.get("current_point"));
							card.setOptcode((String)rstBody.get("optcode"));
						} else {
							log.debug(methodName, "retcode not 0000=" + rstBody.get("retcode"));
							successYn = "N";
						}
						
						// set retCode, retMsg
						card.setRetCode((String)rstBody.get("retcode"));
						card.setRetMsg((String)rstBody.get("retmsg"));
					} else {
						// 실패
						stepCd = "007";
						log.debug(methodName, "No result.getSucYn=" + result.getSucYn());
						successYn = "N";
						card.setRetCode((String)result.getRstCd());
						card.setRetMsg((String)result.getRstMsg());
					}
				} else {
					// 필수항목 체크 실패
					stepCd = "008";
					successYn = "N";
					card.setRetCode(messageManage.getMsgCd("BZ_ERROR_152"));
					card.setRetMsg(messageManage.getMsgTxt("BZ_ERROR_152"));					
				}
			} catch (Exception e) {
				log.printStackTracePH(methodName, e);
				log.error(methodName, "Exception msg="+StringUtil.nvl(e.getMessage()));
				successYn = "N";
				card.setRetCode(messageManage.getMsgCd("SY_ERROR_900"));
				card.setRetMsg(StringUtil.nvl(e.getMessage()));				
				
			} finally {
				// 성공이든 실패이든 list에 담고 마지막건까지 전부 처리 한다.
				card.setSucYn(successYn);
				rtnList.add(card);
				
				// 성공/실패건수 증감처리
				if( "Y".equals(successYn) ) {
					sucCnt++;
				} else {
					failCnt++;
				}				
				log.debug(methodName, "finally card=" + JsonUtil.toJson(card));
			}
		}
		
		// 리턴 오브젝트 셋팅
		// TODO
		// 다건 취소는 실패해도 나머지건은 실패 처리로 한다? 확인필요! - 2018/08/05. lys 
		resultVo.setRstBody(rtnList);
		
		// 결과값 셋팅
		if( (sucCnt + failCnt) < 1 ) {
			resultVo.setSucYn("F");
		} else if( failCnt > 0 ) {
			resultVo.setSucYn("N");
		} else {
			resultVo.setSucYn("Y");
		}		
		
		// 리턴
		return resultVo;
	}

	
	/**
	 * <pre> 클립포인트 연동 송신데이터 유효성 체크 </pre>
	 * 
	 * @param Map<String, String> params
	 * @return boolean
	 * @see <pre>
	 *      1. 포인트 허브는 송신데이터 항목 전부가 필수 값이다. 
	 *      </pre>
	 */		
	@Override
	public boolean validateSendData(Map<String, Object> params) {
		
        // 클립포인트 송신데이터는 모두 필수 값이다.
		// 파라미터값중 1개라도 null이면 에러를 리턴한다.
        for( String key : params.keySet() ) {
            if( "".equals(StringUtil.nvl(params.get(key))) ) return false;
        }
		
        // 유효성 체크 결과 리턴
		return true;
	}
	
	
	
	/**
	 * <pre> 클립포인트 조회 </pre>
	 * 
	 * @param  Map<String, Object>
	 * @return Map<String, Object>
	 * @see <pre>
	 *      여러 카드사 포인트 조회 
	 *      </pre>
	 */
	@Override
	public Map<String, Object> getPoint(Map<String, Object> params) throws BizException, Exception {
		String methodName = "getPoint";
		log.debug(methodName, "Start!");
		log.debug(methodName, "params=" + JsonUtil.toJson(params));	

		/***************************************************************
		 * 0. declare
		 ***************************************************************/
		String stepCd                     = "001";									// step 코드 (익셉션 발생시 사용)
		List<ClipPointIfVo> sendList      = new ArrayList<ClipPointIfVo>();			// 클립포인트 연동 송신데이터 리스트
		Map<String, Object> cardMap       = new HashMap<String, Object>();			// 카드사정보 맵
		List<Map<String, Object>> pntList = new ArrayList<Map<String, Object>>();	// 포인트조회결과 셋팅  List 
		Map<String, Object> returnMap     = new HashMap<String, Object>();			// 리턴맵

		/***************************************************************
		 * 1. 세션에서 CertificationVO 객체 취득
		 ***************************************************************/
		CertificationVO certVO = SessionUtils.getCertificationVO(SystemUtils.getCurrentRequest());
		SbankDealVO sbankDealVO = SessionUtils.getSbankDealVO(SystemUtils.getCurrentRequest());
		
		try {
			stepCd = "002";
			//log.debug(methodName, "certVO="+JsonUtil.toJson(certVO));
			
			/***************************************************************
			 * 2. 카드조회 파라미터 셋팅 (클립포인트에 송신할 데이터 셋팅)
			 *  - db에서 지원 카드사목록 조회
			 *  - 카드목록 조회시 os, 단말 구분 및 대리점별로 지원하는 카드사를 조회해야 한다.
			 ***************************************************************/
			stepCd = "003";
			List<Map<String, Object>> cardlist = clipService.getBasicPointInfo(params);
			
			String sPntRootId = comnService.getPntTrNoRoot(params);
			
			// 클립포인트 조회 파라미터 셋팅
			stepCd = "031";
			for (Map<String, Object> map : cardlist) {
				ClipPointIfVo clipPointIfVo = new ClipPointIfVo();
				String sPrfx = StringUtil.nvl(map.get("pntPrfx"));
				String sPntTrNo = sPrfx + sPntRootId;
				clipPointIfVo.setPhubTrNo(StringUtil.nvl(params.get("phubTrNo")));	// 포인트허브거래번호
				clipPointIfVo.setUserCi(certVO.getCustCI());						// 유저ci
				//clipPointIfVo.setPntTrNo(StringUtil.nvl(map.get("pntTrNo")));		// 거래고유번호 (카드사별)
				clipPointIfVo.setPntTrNo(sPntTrNo);		                            // 거래고유번호 (카드사별)
				clipPointIfVo.setCtn(certVO.getPhoneNo());							// 전화번호(암호화 대상)
				clipPointIfVo.setUserId(certVO.getCustId());						// 거래요청자
				clipPointIfVo.setPntCd(StringUtil.nvl(map.get("pntCd")));			// 카드사 코드
				clipPointIfVo.setDate(DateUtils.defaultDate());						// 요청일자
				clipPointIfVo.setTime(DateUtils.defaultTime());						// 요청시간
				
				sendList.add(clipPointIfVo);
				
				// 리턴값 선셋팅 : 카드사정보 목록을 맵으로 변환
				stepCd = "032";
				cardMap.put(StringUtil.nvl(map.get("pntCd")), map);
			}
			
			// 클립포인트 조회 기타정보 파라미터 셋팅
			Map<String, Object> etcMap = new HashMap<String, Object>();
			etcMap.put("pgDealNo", StringUtil.nvl(sbankDealVO.getPgTrNo()));		// PG거래번호
			
			/***************************************************************
			 * 3. 클립포인트 조회
			 ***************************************************************/
			stepCd = "003";	// ResultVo is Null
			ResultVo resultVo = clipPointService.getpoints(sendList, etcMap);
			if( Common.isNull(resultVo) ) {
				returnMap.put(Constant.SUCCESS_YN,  Constant.FAIL);
				returnMap.put(Constant.RTN_CD,  messageManage.getMsgCd("IF_INFO_111"));
				returnMap.put(Constant.RTN_MSG, messageManage.getMsgTxt("IF_INFO_111"));
				return returnMap;
			}
			
			// 4-1 : 조회결과 체크
			stepCd = "041";
			if( "N".equals(resultVo.getSucYn()) ) {
				// 포인트조회 전체가 실패일 경우
				returnMap.put(Constant.SUCCESS_YN,  Constant.FAIL);
				returnMap.put(Constant.RTN_CD,  messageManage.getMsgCd("BZ_ERROR_139"));
				returnMap.put(Constant.RTN_MSG, messageManage.getMsgTxt("BZ_ERROR_139"));
				return returnMap;
			} 
			
			
			/***************************************************************
			 * 5. 리턴값 셋팅 : 클립포인트 조회결과 (1건이라도 조회성공시) 
			 ***************************************************************/
			// 5-1 : 카드사별 리턴값 셋팅
			stepCd = "005";
			for( ClipPointIfVo card : (List<ClipPointIfVo>)resultVo.getRstBody() ) {
				// 포인트조회결과가 성공인건만 담는다.
				//if( "Y".equals(card.getSucYn()) && (Integer.valueOf(card.getCurrentPoint()) > 0) ) {
				stepCd = "051";
				//if( "Y".equals(card.getSucYn()) ) {
					// 카드조회 파라미터 셋팅할때 map에 담았던 카드사정보를 꺼낸다.
					Map<String, Object>  cardInfoMap = (Map<String, Object>) cardMap.get(card.getPntCd());
					
					// db에서 조회한 카드사정보 map에 추가정보 셋팅 한다.
					cardInfoMap.put("pntCd", card.getPntCd());			//	포인트ID	
					cardInfoMap.put("pntTrNo", card.getPntTrNo());		//	포인트별 거래번호	
					cardInfoMap.put("avlPnt",  StringUtil.nvl(card.getCurrentPoint(),"0"));	//	소유한 포인트			
					
					// 결과 리스트객체에 추가
					pntList.add(cardInfoMap);				
				//}
			}			
		} catch (Exception e) {
			// 포인트조회 에러 발생
			log.printStackTracePH(methodName, e);
			throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_139"));
		}
		
		/***************************************************************
		 * 6. 리턴 
		 ***************************************************************/		
		returnMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
		returnMap.put("data", pntList);
		return returnMap;		
	}

	/**
	 * <pre> 고객별 포인트 결제 내역 조회 </pre>
	 * 
	 * @param  Map
	 * @return Map
	 * @throws Exception 
	 * @see <pre>
	 *        클립포인트로부터 요청을 받아 데이타를 서비스해준다.
	 *      </pre>
	 */
	@Override
	public Map<String, Object> getPointUseList(Map<String, Object> params) throws Exception {
		// TODO Auto-generated method stub
		return clipDao.getPointUseList(params);
	}	
	
	
}
