/**
 * 
 */
package com.olleh.pointHub.api.provider.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.dao.AbstractDAO;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>클립서버 연동을 지원하는 서비스</pre>
 * @Class Name : ClipDao
 * @author : InseokSeo
 * @since : 2018.07.30
 * @version : 1.0
 * @see
 * 
 * 		<pre>
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 7. 30.     InseokSeo        최초 생성
 *
 *      </pre>
 * 
 */

@Repository
public class ClipDao extends AbstractDAO {
	private Logger log = new Logger(this.getClass());

	public List<Map<String, Object>> getBasicPointInfo(Map<String, Object> params) {
		return selectList("mybatis.clip.getBasicPointInfo", params);
	}
	
	/**
	 * <pre> 정보제공사항 정보</pre>
	 * @description 정보제공사항 정보
	 * @param Map
	 * @return List
	 */
	public List<Map<String, Object>> getPhubInfoPrvdItem(Map<String, Object> params) {
		return selectList("mybatis.clip.getPhubInfoPrvdItem", params);
	}

	/**
	 * <pre> 가맹점 체크(비가맹점 접속차단용)</pre>
	 * @description 가맹점 체크(비가맹점 접속차단용)
	 * @param map
	 * @return map
	 */
	public Map<String, Object> countPgCprt(Map<String, Object> params) {
		return selectOne("mybatis.clip.countPgCprt", params);
	}	
	
	/**
	 * <pre> 가맹점 정보 가져오기(결제/충전구분)</pre>
	 * @description 가맹점 정보 가져오기(결제/충전구분)
	 * @param map
	 * @return map
	 */
	public Map<String, Object> getPgCprtInfo(Map<String, Object> params) {
		return selectOne("mybatis.clip.getPgCprtInfo", params);
	}
	
	/**
	 * <pre> 거래번호 채번</pre>
	 * @description 거래번호 채번
	 * @param map
	 * @return map
	 */	
	public Map<String, Object> getPhubTrNo() {
		Map<String, Object> map = new HashMap<String, Object>();
		map = selectOne("mybatis.clip.getPhubTrNo");
		return map;
	}

	/**
	 * <pre> 포인트 사용자 고객 조회</pre>
	 * @description 포인트 사용자 고객 조회
	 * @param map
	 * @return int
	 */
	public Map<String,Object> countPntCustInfo(Map<String, Object> params) {
		return selectOne("mybatis.clip.countPntCustInfo", params);
	}

	/**
	 * <pre> 사용자 cust id 가져오기</pre>
	 * @description 사용자 cust id 가져오기
	 * @param map
	 * @return map
	 */
	public Map<String,Object> selectCustIdByCi(Map<String, Object> params) {
		return selectOne("mybatis.clip.selectCustIdByCi", params);
	}
	
	
	/**
	 * <pre> 포인트 사용자 고객 등록</pre>
	 * @description  포인트 사용자 고객 등록
	 * @param map
	 * @return int
	 */
	public int insertPntCustInfo(Map<String, Object> params) {
		return insert("mybatis.clip.insertPntCustInfo", params);
	}

	/**
	 * <pre> 포인트 사용자 고객 조회</pre>
	 * @description 포인트 사용자 고객 조회
	 * @param map
	 * @return int
	 */
	public Map<String,Object> countPntCustCtn(Map<String, Object> params) {
		return selectOne("mybatis.clip.countPntCustCtn", params);
	}

	/**
	 * <pre> 사용자별 CTN 등록</pre>
	 * @description 사용자별 CTN 등록
	 * @param map
	 * @return int
	 */
	public int insertPntCustCtn(Map<String, Object> params) {
		return insert("mybatis.clip.insertPntCustCtn", params);
	}

	/**
	 * <pre> 사용자별 CTN 조회</pre>
	 * @description 사용자별 CTN 조회
	 * @param map
	 * @return int
	 */
	public Map<String,Object> countPntCmpnUser(Map<String, Object> params) {
		return selectOne("mybatis.clip.countPntCmpnUser", params);
	}
	
	/**
	 * <pre> 약관동의 이력 여부 체크</pre>
	 * @description  약관동의 이력 여부 체크
	 * @param map
	 * @return int
	 */
	public Map<String,Object> countClsAgInfo(Map<String, Object> params) {
		return selectOne("mybatis.clip.countClsAgInfo", params);
	}
	
	/**
	 * <pre> 약관동의 이력 저장</pre>
     * @description  
     * @param map
     * @return int
     */
	public int updateClsAgInfo(Map<String, Object> params) {
		return update("mybatis.clip.updateClsAgInfo", params);
	}
	
	/**
	 * <pre> 약관정보 조회</pre>
     * @description 약관정보 조회  
     * @param  map
     * @return List
     */
	public List<Map<String, Object>> getPhubClsInfo(Map<String,Object> params){
		String sServiceId = StringUtil.nvl(params.get("service_id"),"SVC_BASE");
		String sSqlId     =  "mybatis.clip.getPhubClsInfo";
		
		if(Constant.SERVICE_ID_SVC_FP.equals(sServiceId)) {
			sSqlId     =  "mybatis.clip.getPhubClsInfoFp";
		}
		
		return selectList(sSqlId, params);
	}

	/**
	 * <pre>  사용자별 CTN 등록</pre>
	 * @description  사용자별 CTN 등록
	 * @param map
	 * @return int
	 */
	public int insertPntCmpnUser(Map<String, Object> params) {
		return insert("mybatis.clip.insertPntCmpnUser", params);
	}

	/**
	 * <pre> SDK(화면단)에서 포인트 사용자 지정 후 요청받을 시 아래 작업 후 PG사에 통보하기</pre>
	 * @param map
	 * @param
	 * @return string
	 */
	public int insertDealReqInfo(Map<String, Object> params) {
		return insert("mybatis.clip.insertDealReqInfo", params);
	}
	
	/**
	 * <pre>약관동의 정보 저장</pre> 
	 * @param  map
	 * @return int
	 */
	public int insertClsAgInfo(Map<String, Object> params) {
		return insert("mybatis.clip.insertClsAgInfo", params);
	}
	
	/**
	 * <pre>약관동의 이력 저장</pre>
	 * @param  Map
	 * @return int
	 */
	public int insertClsAgHist(Map<String, Object> params) {
		return insert("mybatis.clip.insertClsAgHist", params);
	}
	
	/**
	 * <pre>정보제공 약관동의 이력 저장</pre>
	 * @param  Map
	 * @return int
	 */
	public int insertInfoPrvdItemHist(Map<String, Object> params) {
		return insert("mybatis.clip.insertInfoPrvdItemHist", params);
	}
	
	/**
     * <pre>클립카드로부터 회원탈퇴 요청 처리</pre>
     * @description  클립카드로부터 회원탈퇴 요청 처리
     * @param map
     * @return int
     */
	public int updateCustClipDisjoin(Map<String, Object> params) {
		return update("mybatis.clip.updateCustClipDisjoin", params);
	}

	/**
     * <pre>고객별 포인트 결제 내역 조회</pre>
     * @description  고객별 포인트 결제 내역 조회
     * @param  map
     * @return map
     */
	public Map<String, Object> getPointUseList(Map<String, Object> params) {
		return selectOne("mybatis.clip.getPointUseList", params);
	}
	
}
