package com.olleh.pointHub.api.provider.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.api.provider.dao.ClipDao;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.DateUtils;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * @Class Name : ClipServiceImpl
 * @author : InseokSeo
 * @since 2018.07.31
 * @version 1.0
 * @see
 * 
 * 		<pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자         	  수정내용
 * ----------  			---------  		-----------------
 * 2018.08.02   		InseokSeo      		최초생성
 * 2018.08.21   		InseokSeo      		예외처리 추가
 *      </pre>
 */

@Service
public class ClipServiceImpl implements ClipService {
	private Logger log = new Logger(this.getClass());

	@Autowired
	MessageManage messageManage;
	@Autowired
	ClipDao dao;

	@Autowired
	ClipHttpService clipHttpService;

	@Autowired
	LogService logService;
	
	
	/**
	 * @description - P/H 거래 번호 생성
	 * @param Map
	 * @return Map
	 */
	@Override
	public Map<String, Object> getPhubTrNo() {
		return dao.getPhubTrNo();
	}
	
	/**
	 * <pre> 클립포인트 연동 송신데이터 유효성 체크 </pre>
	 * 
	 * @param Map<String, String> params
	 * @return boolean
	 * @see <pre>
	 *      1. 포인트 허브는 송신데이터 항목 전부가 필수 값이다. 
	 *      </pre>
	 */		
	@Override
	public boolean validateSendData(Map<String, Object> params) {
		
        // 클립포인트 송신데이터는 모두 필수 값이다.
		// 파라미터값중 1개라도 null이면 에러를 리턴한다.
        for( String key : params.keySet() ) {
            if( "".equals(StringUtil.nvl(params.get(key))) ) return false;
        }
        // 유효성 체크 결과 리턴
		return true;
	}
	

	/**
	 * 약관 조회
	 * 
	 * @param
	 * @return Map<String, Object>
	 * @see 1. getClaUses()
	 */

	@Override
	public Map<String, Object> getClaUses(Map<String, Object> params) throws BizException, Exception {
		String methodName = "getClaUses";		
		log.debug(methodName, "Start!");
		
		String stepCd = "001";
		ResultVo result = null;
		Map<String, Object> jsonObject = new HashMap<String, Object>();
		Map<String, Object> etcMap     = new HashMap<String, Object>();
		try {
			
			etcMap.put("phub_tr_no", StringUtil.nvl(params.get("phub_tr_no")));
			etcMap.put("pgDealNo"  , StringUtil.nvl(params.get("pg_tr_no")));
			params.remove("phub_tr_no");
			params.remove("pg_tr_no");

			// 유효성 체크
			stepCd = "002";
			result = clipHttpService.sendHttpPostCLIP("/clipext/v2/user/get_agree.json", params, etcMap);

			@SuppressWarnings("unchecked")
			Map<String, Object> mapBody = (Map<String, Object>) result.getRstBody();
			 
			//클립 연동 약관리스트
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> list = (List<Map<String, Object>>) mapBody.get("agree");

			
			String resultCd = (String) mapBody.get("result");
			String msg = (String) mapBody.get("msg");

			// 시스템 파라미터에서 사용될 약관 정보 체크
			SysPrmtManage sysPrmtManage = new SysPrmtManage();
			stepCd = "003";
			String[] csAgreeList = sysPrmtManage.getSysPrmtAry("CS_AGREE_LIST");

			// 클립 약관 조회 결과 내부연동 리스트로 파싱
			List<Map<String, Object>> clsList = new ArrayList<Map<String, Object>>();
			stepCd = "004";
			for (Map<String, Object> map2 : list) {
				Map<String, Object> clsMap = new HashMap<String, Object>();
				String clsId = (String) map2.get("agree_code");
				String mandYn = (String) map2.get("agree_compulsory_yn");
				String clsTitl = (String) map2.get("agree_title") ;
				clsMap.put("clsId", clsId);
				clsMap.put("clsTitl", clsTitl);
				clsMap.put("clsUrl", map2.get("agree_content"));
				clsMap.put("mandYn", mandYn);
				clsMap.put("clsVer", map2.get("agree_ver"));

				// 사용될 약관 정보만 리스트에 추가
				for (int i = 0; i < csAgreeList.length; i++) {
					if (csAgreeList[i].equals(clsId)) {
						clsList.add(clsMap);
					}
				}
			}
			// 마스터 약관리스트
			stepCd = "005";
			List<Map<String, Object>> clsInfoList = getPhubClsInfo(params);				
			// 약관 리스트에 마스터 약관 리스트 추가
			for (Map<String, Object> map2 : clsInfoList) {
				clsList.add(map2);
			}

			for (Map<String, Object> map2 : clsList) {
				for (Map.Entry<String, Object> entry : map2.entrySet()) {
					String key = entry.getKey();
					Object value = entry.getValue();
				}
			}
			// 카드사별 약관 리스트
			stepCd = "006";
			List<Map<String, Object>> cardInfoList = getPhubInfoPrvdItem(params);				
			for (Map<String, Object> map2 : cardInfoList) {
				for (Map.Entry<String, Object> entry : map2.entrySet()) {
					String key = entry.getKey();
					Object value = entry.getValue();
				}
			}

			jsonObject.put("clsList", clsList);
			jsonObject.put("cardInfoList", cardInfoList);
			jsonObject.put("result", resultCd);
			jsonObject.put("msg", msg);

		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Search Clauses Fail");
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_134"));
		}
			
		//log.debug(methodName, "getClaUses result=" + JsonUtil.MapToJson(jsonObject));
		return jsonObject;
	}

	
	
	@Override
	public Map<String, Object> getPgCprtInfo(Map<String, Object> params) {
		String methodName = "countPgCprt";
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		returnMap = dao.getPgCprtInfo(params);
		
		if (Common.isNull(returnMap)) {
			returnMap = new HashMap<String, Object>();
			returnMap.put("rchr_pay_ind" , "");
			returnMap.put("cprt_cmpn_id" , "");
			returnMap.put("ret_code" , messageManage.getMsgCd("IF_INFO_113"));
			returnMap.put("ret_msg"  , messageManage.getMsgTxt("IF_INFO_113"));
		} else {
			returnMap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_INFO_00"));
			returnMap.put(Constant.RET_MSG, messageManage.getMsgTxt("SY_INFO_00"));
		} 
		
		return returnMap;
	}

	public Map<String, Object> countPgCprt(Map<String, Object> params) {
		String methodName = "countPgCprt";
		log.debug(methodName, "Start!");
		
		Map<String, Object> returnMap = new HashMap<String, Object>();
		Map<String, Object> pgMap = new HashMap<String, Object>();
		
		returnMap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_INFO_00"));
		returnMap.put(Constant.RET_MSG, messageManage.getMsgTxt("SY_INFO_00"));		
		
		pgMap = dao.countPgCprt(params);

		if (StringUtil.nvl(pgMap.get("cnt")).equals("0")) {
			returnMap.put("ret_code", messageManage.getMsgCd("IF_INFO_113"));
			returnMap.put("ret_msg" , messageManage.getMsgTxt("IF_INFO_113"));
		}
		
		return returnMap;
	}
	

	/**
	 * @description - 회원가입 여부조회 - 이용자 고유번호 , 휴대폰 번호 암호화.
	 * @param Map
	 * @return List
	 */
	@Override
	public Map<String, Object> rglrCheck(Map<String, Object> params) throws BizException, Exception {
		String methodName = "rglrCheck";
		log.debug(methodName, "Start!");
		String stepCd = "001";

		Map<String, Object> retmap = new HashMap<String, Object>();
		Map<String, Object> etcMap = new HashMap<String, Object>();

		ResultVo result = null;

		try {
			etcMap.put("phub_tr_no", StringUtil.nvl(params.get("phub_tr_no")));
			etcMap.put("pgDealNo"  , StringUtil.nvl(params.get("pg_tr_no")));
			params.remove("phub_tr_no");
			params.remove("pg_tr_no");
			
			stepCd = "002";
			result = clipHttpService.sendHttpPostCLIP("/clipext/v2/user/check.json", params, etcMap);

			stepCd = "003";
			Map<String, Object> chkmap = (Map<String, Object>) result.getRstBody();

			// 가입 여부
			String status = (String) chkmap.get("status");
			stepCd = "004";
			String regdt = "";
			if (status.equals("N")) {				
				retmap.put("ret_code", "N");
				retmap.put("ret_msg", messageManage.getMsgTxt("BZ_INFO_138"));

			} else {				
				retmap.put("ret_code", "Y");
				/* 2018.12.04
				regdt = (String)chkmap.get("regdt");
				regdt = regdt.substring(0, 8);
				*/
				// 가입일 (연동규격 참조: varchar(14)임.)
				regdt = StringUtil.nvl(chkmap.get("regdt"));
				retmap.put("ret_msg", messageManage.getMsgTxt("BZ_INFO_135"));
			}
			
			retmap.put("regdt", StringUtil.nvl(regdt, DateUtils.defaultDate()+DateUtils.defaultTime()));

		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_137"));
		}
		return retmap;
	}

	/**
	 * @description - 포인트 사용자 고객 관리
	 * @param map
	 * @return int
	 */

	@Override
	@Transactional(rollbackFor={Exception.class, BizException.class})
	public Map<String, Object> manageCustInfo(Map<String, Object> params) throws BizException, Exception{
		String methodName = "manageCustInfo";
		log.debug(methodName, "Start!");
		
		String stepCd = "001";
		
		Map<String, Object> retmap = new HashMap<String, Object>();
		Map<String, Object> cntmap = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		retmap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_INFO_00"));
		retmap.put(Constant.RET_MSG, messageManage.getMsgTxt("SY_INFO_00"));

		try {
		cntmap = dao.countPntCustInfo(params);
		
			if (StringUtil.nvl(cntmap.get("cnt")).equals("0")) {
	
				dao.insertPntCustInfo(params);
				dao.insertPntCustCtn(params);
				dao.insertPntCmpnUser(params);
	
				// 인서트후 맵에 신규아이디 담긴것 가져오기
				retmap.put("custId", params.get("cust_id"));
	
			} else {
				// 있으므로 고객아이디 추출
				map = dao.selectCustIdByCi(params);
				
				if (map != null) {
					retmap.put("custId", map.get("cust_id"));
					
					// CTN등록여부 체크 후 등록여부 결정
					cntmap = dao.countPntCustCtn(params);
					
					params.put("cust_id", map.get("cust_id"));
					if (StringUtil.nvl(cntmap.get("cnt")).equals("0")) {
						dao.insertPntCustCtn(params);
					}				
				} 
			}
		
		
		 } catch (Exception e) {
			 log.printStackTracePH(methodName, e);
			 log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			throw new BizException(stepCd, messageManage.getMsgVOY("BZ_INFO_141"));
		}
		return retmap;
	}

	/**
	 * @description 카드사별 포인트정보
	 * @param Map
	 * @return List
	 */
	@Override
	public List<Map<String, Object>> getBasicPointInfo(Map<String, Object> params) {
		String methodName = "getBasicPointInfo";
		log.debug(methodName, "Start!");
		return dao.getBasicPointInfo(params);
	}

	/**
	 * @description 정보제공사항 정보
	 * @param Map
	 * @return List
	 */
	@Override
	public List<Map<String, Object>> getPhubInfoPrvdItem(Map<String, Object> params) throws BizException, Exception {
		String methodName = "getPhubInfoPrvdItem";
		log.debug(methodName, "Start!");
		String stepCd = "001";

		List<Map<String,Object>> phInfoList = dao.getPhubInfoPrvdItem(params);
		if(Common.isNull(phInfoList)) {
			throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_140"));
		}

		//return dao.getPhubInfoPrvdItem(params);
		return phInfoList;
	}

	/**
	 * @description - 정회원(인증회원)가입
	 * @param Map
	 * @return List
	 * @see  clipext/v2/auth/{serviceId}/signup.json 
	 */
	@Override
	public Map<String, Object> regRglmmb(Map<String, Object> params) throws BizException, Exception {
		String methodName = "regRglmmb";
		log.debug(methodName, "Start!");
		String stepCd = "001";
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> retmap = new HashMap<String, Object>();
		Map<String, Object> etcMap = new HashMap<String, Object>();

		// 암호화 , cryptoSendData 메서드 이용
		ResultVo resultVo = null;
		String resultCd = "";
		String msg = "";
		String cust_id = "";

		etcMap.put("phub_tr_no", StringUtil.nvl(params.get("phub_tr_no")));
		etcMap.put("pgDealNo"  , StringUtil.nvl(params.get("pg_tr_no")));
		params.remove("phub_tr_no");
		params.remove("pg_tr_no");
		
		stepCd = "002";
		resultVo = clipHttpService.sendHttpPostCLIP("/clipext/v2/auth/pointhub/signup.json", params, etcMap);
		
		Map<String, Object> clmap = (Map<String, Object>) resultVo.getRstBody();

		// 결과 코드
		resultCd = String.valueOf(clmap.get("result"));
		// 결과 메시지
		msg = (String) clmap.get("msg");
		cust_id = (String) clmap.get("cust_id");
		
		if (resultCd.equals("0")) {
			retmap.put("result", "200");
			retmap.put("msg", "Successed");
			retmap.put("cust_id", cust_id);
		} else if (resultCd.equals("-104")) {
			retmap.put("result", "-104");
			retmap.put("msg", "already joined user");
			retmap.put("cust_id", cust_id);
		} else {
			throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_146"));
		}
			
		return retmap;
	}

	/**
	 * @description - 약관동의 정보 저장
	 * @param Map
	 * @return List
	 */
	@Override
	@Transactional(rollbackFor={Exception.class, BizException.class})
	public Map<String, Object> setClsAgrInfo(Map<String, Object> params)  throws BizException, Exception {
		String methodName = "setClsAgrInfo";
		String stepCd = "001";
		Map<String, Object> returnMap = new HashMap<String, Object>();
		Map<String, Object> clsChkMap = dao.countClsAgInfo(params);
		String prvdrId = (String) params.get("prvdr_id");
		
		returnMap.put(Constant.RET_CODE, messageManage.getMsgCd("SY_INFO_00"));
		returnMap.put(Constant.RET_MSG , messageManage.getMsgTxt("SY_INFO_00"));		

		stepCd = "002";
		try {
			if (StringUtil.isNull(prvdrId)) {
				// insertClsAgInfo PH_CLS_INFO 테이블 조회 후 없으면 insert 있으면 update
				if (clsChkMap.get("cnt").toString().equals("0")) {
					dao.insertClsAgInfo(params);
				} else {
					dao.updateClsAgInfo(params);
				}
				dao.insertClsAgHist(params);
			} else {
				dao.insertInfoPrvdItemHist(params);
			}
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception = " + StringUtil.nvl(e.getMessage()));
			throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_143"));
		}
		return returnMap;
	}

	@Override
	public List<Map<String, Object>> getPhubClsInfo(Map<String, Object> params) {
		String methodName = "getPhubClsInfo";
		log.debug(methodName, "Start!");
		return dao.getPhubClsInfo(params);
	}
	
	public int disjoinClipMember(Map<String, Object> params) throws BizException, Exception {
		String methodName = "disjoinClipMember";
		log.debug(methodName, "Start!");		
		return dao.updateCustClipDisjoin(params);
	}

	@Override
	public void logRequestFromOut(Map<String, Object> params, HttpServletRequest req, String pMethod) {
		Map<String, Object> logMap = new HashMap<String,Object>();
		String sSender = req.getRequestURI().contains("kmcisRes.do") ? Constant.PLC_KMC:Constant.PLC_SB ;
		String methodName = "logRequestFromOut";
		
		String sUri = StringUtil.nvl(params.get("from_uri"),"-");
		
		log.debug(methodName, "logRequestFromOut==> \n"+ params.toString());
		
		try {
			
			if("-".equals(sUri)){
				logMap.put("actn_uri"      , req.getRequestURI());
			} else {
				logMap.put("actn_uri"      , sUri);
			}
			
			logMap.put("send_plc"	   , sSender);
			logMap.put("rcv_plc"	   , Constant.PLC_PH);
			logMap.put("phub_tr_no"	   , params.get("phub_tr_no"));
			logMap.put("pg_deal_no"	   , StringUtil.nvl(params.get("pg_tr_no")  ,"-"));
			logMap.put("req_data"	   , params.get("req_data"));
			logMap.put("res_data"	   , params.get("res_data"));
			logMap.put("rply_cd"	   , StringUtil.nvl(params.get(Constant.RET_CODE),"00"));
			logMap.put("rply_msg"	   , StringUtil.nvl(params.get(Constant.RET_MSG), "200"));
			logMap.put("rgst_user_id"  , pMethod);
			
			logService.insertAdmApiIfLog(logMap);
		
		} catch(Exception e){
			log.error(methodName, JsonUtil.MapToJson(logMap));
			log.error(methodName, StringUtil.nvl(e.getMessage()));
		}
		
	}
	
	
	
}