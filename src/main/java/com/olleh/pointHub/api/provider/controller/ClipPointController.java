package com.olleh.pointHub.api.provider.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.olleh.pointHub.api.paygate.service.SbankService;
import com.olleh.pointHub.api.provider.service.ClipPointService;
import com.olleh.pointHub.api.provider.service.ClipServiceImpl;
import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.components.UtilsComponent;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.Common;
import com.olleh.pointHub.common.utils.DateUtils;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>클립포인트 처리 콘트롤러</pre>
 * 
 * @Class Name : ClipPointController
 * @author : lys
 * @since : 2018.08.09
 * @version : 1.0
 * @see
 * 
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.09   lys        최초생성
 *      </pre>
 */

@RestController
public class ClipPointController {
	private Logger log = new Logger(this.getClass());

	@Autowired
	MessageManage messageManage;

	@Autowired
	ClipServiceImpl clipService;

	@Autowired
	UtilsComponent utilsComponent;

	@Autowired
	ClipPointService clipPointService;

	@Autowired
	SysPrmtManage sysManage;
	
	// accsManage
	@Autowired
	AccsCtrnManage accsManage;

	@Autowired
	SbankService  sbankService;
	
    
	/**
	 * <pre> 클립포인트 환경에서 고객별 포인트 사용내역 조회를 돕는 메소드</pre>
	 * @author cisohn
	 * @since  2018.11.15
	 * @param  Map (1.사용자CI 2.조회기간(7,30,90) 3.현재페이지 4.페이지당 로우수)
	 * @return Map
	 */
    @RequestMapping(value="/cp/use", method=RequestMethod.POST, produces="application/json; charset=utf-8")
    public Map<String, Object> getPointUseList(@RequestParam Map<String,Object> params, HttpServletRequest request) throws Exception {
    	/* 소스 로깅 */
    	String methodName = "getPointUseList";
    	String stepCd     = "";
    	
    	/* 리턴객체 */
    	Map<String, Object> resultmap = new HashMap<String, Object>();
    	resultmap.put("ret_code", messageManage.getMsgCd("SY_INFO_00"));
    	resultmap.put("ret_msg" , messageManage.getMsgTxt("SY_INFO_00"));	
    	
    	Map<String,Object> daoreturn = new HashMap<String,Object>();

    	/* 조회구간 */
    	String sFromDt = "", sToDt = "";
    	
		try {
			
			log.debug(stepCd, "getPointUseList's 파라미터 => \n"+ JsonUtil.MapToJson(params));
			
			stepCd = "01";
			/***************************************************************
			 * 2.조회구간 가져오기
			 ***************************************************************/
            sFromDt = DateUtils.getBetweenDatesByNum(Integer.parseInt((String)params.get("deal_term")))[0];
            sToDt = DateUtils.getBetweenDatesByNum(Integer.parseInt((String)params.get("deal_term")))[1];
            
            params.put("st_dt", sFromDt);
            params.put("end_dt", sToDt);
            
            log.debug(methodName, "/cp/use=>\n"+ params.toString());
            
            daoreturn = clipPointService.getPointUseList(params);
            
            if(!Common.isNull(daoreturn)) {
	            resultmap.put("total_rows", daoreturn.get("total_rows")); 
	            resultmap.put("use_data", daoreturn.get("use_data"));
	            
	            log.debug(stepCd, "daoreturn ==> \n"+ JsonUtil.MapToJson(daoreturn));
            } else {
            	resultmap.put("ret_code", messageManage.getMsgCd("IF_INFO_104"));
            	resultmap.put("ret_msg" , messageManage.getMsgTxt("IF_INFO_104"));	
            }
            
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			log.error(methodName, "Exception e="+StringUtil.nvl(e.getMessage()));			
			resultmap.put("ret_code", messageManage.getMsgCd("SY_ERROR_900"));
			resultmap.put("ret_msg" , StringUtil.nvl(e.getMessage()));	
		}
		
    	return resultmap;
    
    }
}
