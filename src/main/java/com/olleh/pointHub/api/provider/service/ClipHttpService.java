/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.provider.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.api.comn.crypto.ClipCrypto;
import com.olleh.pointHub.api.comn.service.HttpService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.IfException;
import com.olleh.pointHub.common.model.MsgInfoVO;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>http통신 서비스</pre>
 * 
 * @Class Name : ClipHttpService
 * @author
 * @since 2018.07.25
 * @version 1.0
 * @see
 * 
 * 		<pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.25   lys        최초생성
 * 2018.08.01   sis	     SbankHttpService 복사
 *      </pre>
 */
@Service("ClipHttpService")
public class ClipHttpService extends HttpService {
	
	/**
	 * 수/발신처(SEND_RECV_PLC) 연동시스템 코드
	 * PH	포인트허브
	 * CS	클립서버
	 */
    private String SEND_PLC = Constant.PLC_PH;
    private String RECV_PLC = Constant.PLC_CS;	

	@Autowired
	SysPrmtManage sysPrmtManage;
	
    @Autowired
    MessageManage messageManage;
    
    @Autowired
    LogService logService;
    
    @Autowired
    ClipCrypto clipCrypto;
    
    

	/**
	 * <pre>
	 *  Clip server  http post방식 통신
	 * </pre>
	 * 
	 * @param Map<String,Object>
	 *            params
	 * @param HttpServletRequest
	 *            request
	 * @return ModelAndView
	 * @see
	 * 
	 * 		<pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */
	public ResultVo sendHttpPostCLIP(String uri, Map<String, Object> params, Map<String, Object> etcMap) throws Exception {

		String methodName = "sendHttpPostCLIP";
		log.debug(methodName, "Start!");
		
		// set url
		String sendUrl = getDomain(uri);
		ResultVo resultVo = new ResultVo();
		
		// set header
		Map<String, Object> hmap = new HashMap<String, Object>();
		Map<String, Object> postparams = new HashMap<String, Object>();
		
		hmap.put("authorization", getAuthorization());
		hmap.put("appType", "json");

		// 송신 데이터 암호화 항목 적용
		try {
			postparams = cryptoSendData(params);

			// send
			// ResponseEntity<MultiValueMap> responseEntity =
			// httpUtil.sendHttpPost("http://localhost/api/test", map, null);
			resultVo = sendHttpPost(sendUrl, hmap, postparams);
			resultVo.setCaller("ClipHttpService.sendHttpPostCLIP");
			
		} catch (IfException e) {
			MsgInfoVO msgInfoVO = messageManage.getMsgVOY("SY_ERROR_153");
			resultVo.setSucYn("N");
			resultVo.setRstCd(msgInfoVO.getMsgCd());
			resultVo.setRstMsg(msgInfoVO.getMsgNm());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.printStackTracePH(methodName, e);
			MsgInfoVO msgInfoVO = messageManage.getMsgVOY("SY_ERROR_900");
			resultVo.setSucYn("N");
			resultVo.setRstCd(msgInfoVO.getMsgCd());
			resultVo.setRstMsg(msgInfoVO.getMsgNm());
		}
		
	    // log write
//	    insertAdmApiIfLog(sendUrl, (String)params.get("hub_tr_id"), resultVo);		
	    insertAdmApiIfLog(sendUrl, StringUtil.nvl(etcMap.get("phub_tr_no")) , StringUtil.nvl(etcMap.get("pgDealNo")), resultVo);		
		
		// result
		return resultVo;
	}

	/**
	 * <pre>
	 *  Clip server http post방식 통신
	 * </pre>
	 * 
	 * @param Map<String,Object>
	 *            params
	 * @param HttpServletRequest
	 *            request
	 * @return ModelAndView
	 * @see
	 * 
	 * 		<pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */
	private String getDomain(String uri) {
		// 도메인값 시스템파라미터에서 가져와야 한다.
		// String domain = "http://localhost" + uri;
		String domain = sysPrmtManage.getSysPrmtVal("CS_CONN_IP") + uri;

		return StringUtil.nvl(domain);
	}

	/**
	 * <pre>
	 *  Clip server  http post방식 통신
	 * </pre>
	 * 
	 * @param Map<String,Object>
	 *            params
	 * @param HttpServletRequest
	 *            request
	 * @return ModelAndView
	 * @see
	 * 
	 * 		<pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */
	private String getAuthorization() {
		// 클립연동에는 인증키값 없음.
		String authorization = "";

		return StringUtil.nvl(authorization);
	}

	/**
	 * <pre>
	 *  ClipPoint 송신데이터 암호화 처리
	 * </pre>
	 * 
	 * @param Map<String,
	 *            Object> params
	 * @return Map<String, Object>
	 * @see
	 * 
	 * 		<pre>
	 *      1. Clip 연동시 전달할 데이터를 암호화 한다.
	 *      2. 암호화 항목은 ci, phone, cust_id, name
	 *      </pre>
	 */
	public Map<String, Object> cryptoSendData(Map<String, Object> params) throws NoSuchAlgorithmException,
			InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException,
			UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
		// declare
		Map<String, Object> rtnMap = params;
		
//		for (String mapkey : rtnMap.keySet()){
//	        log.debug("cryptoSendData", "[cryptoSendData] param Data(s):  key:"+mapkey+", value:"+rtnMap.get(mapkey));
//	    }
		
		if(rtnMap.containsKey("ci")) {
			String ci = StringUtil.nvl((String)params.get("ci"));
			if(!"".equals(ci))
				rtnMap.put("ci", clipCrypto.encAES(ci));
		}

		if(rtnMap.containsKey("cust_Id")) {
			String cust_Id = StringUtil.nvl(params.get("cust_Id"));
			if(!"".equals(cust_Id))
				rtnMap.put("ccust_IdustId", clipCrypto.encAES(cust_Id));
		}
		
		if(rtnMap.containsKey("phone"))	{
			String phone = StringUtil.nvl((String)params.get("phone"));
			if(!"".equals(phone))
				rtnMap.put("phone", clipCrypto.encAES(phone));
		}
		
		if(rtnMap.containsKey("name")) {
			String name = StringUtil.nvl((String)params.get("name"));
			if(!"".equals(name))
				rtnMap.put("name", clipCrypto.encAES(name));
		}

		return rtnMap;
	}
	
	/**
	 * <pre>전문 송/수신 Log기록</pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      </pre>
	 */		
	private void insertAdmApiIfLog(String sendUrl, String phub_tr_no, String pgDealNo, ResultVo resultVo) {
		String methodName = "insertAdmApiIfLog";
		
		// declare
	    Map<String, Object> logMap = new HashMap<String, Object>();
	    
	    try {
	    	Map tmpReq = (Map)resultVo.getReqBody();
	    	Map tmpRst = (Map)resultVo.getRstBody();
	    	if( null == tmpReq) tmpReq = new HashMap();
	    	if( null == tmpRst) tmpRst = new HashMap();
	    	
		    logMap.put("actn_uri",     sendUrl);
		    logMap.put("send_plc",     SEND_PLC);
		    logMap.put("rcv_plc",      RECV_PLC);
		    logMap.put("phub_tr_no",   phub_tr_no);
		    logMap.put("pg_deal_no",   pgDealNo);
		    logMap.put("req_data",     JsonUtil.MapToJson(tmpReq));
		    logMap.put("res_data",     JsonUtil.MapToJson(tmpRst));
		    logMap.put("rply_cd",      getRplyCd(tmpRst));
		    logMap.put("rply_msg",     StringUtil.nvl(resultVo.getRstMsg()));
		    logMap.put("rgst_user_id", "sendHttpPostCLIP");
		    
		    logService.insertAdmApiIfLog(logMap);
		    
	    } catch(Exception e) {
	    	log.error(methodName, JsonUtil.MapToJson(logMap));
	    	log.error(methodName, StringUtil.nvl(e.getMessage()));
	    }
	}
	
	/**
	 * <pre>전문 송/수신 결과코드 추출</pre>
	 * 
	 * @param Map<String, Object> rstBody
	 * @return String
	 * @see
	 * 
	 */		
	private String getRplyCd(Map<String, Object> rstBody) {
		String methodName = "getRplyCd";
		
		// declare
		String rplyCd = "";	
	    
	    try {
			rplyCd = StringUtil.nvl(rstBody.get("result"), messageManage.getMsgCd("IF_ERROR_908"));
			
			// 클립은 결과코드가 "0" 인경우 성공이다.
			if( "0".equals(rplyCd) ) rplyCd = Constant.SUCCESS_CODE;
		    
	    } catch(Exception e) {
	    	// 에러 로그 기록
	    	log.error(methodName, StringUtil.nvl(e.getMessage()));
	    }
	    
	    return rplyCd;
	}
}
