/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.provider.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.api.comn.crypto.ClipPointCrypto;
import com.olleh.pointHub.api.comn.service.HttpService;
import com.olleh.pointHub.api.comn.service.LogService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.IfException;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>클립포인트 http통신 서비스</pre>
 * @Class Name : ClipPointHttpService
 * @author 
 * @since 2018.08.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.05   lys        최초생성        
 * </pre>
 */
@Service("ClipPointHttpService")
public class ClipPointHttpService extends HttpService {
	
    @Autowired
    SysPrmtManage sysPrmtManage;
    
    @Autowired
    MessageManage messageManage;
    
    @Autowired
    LogService logService;
    
    @Autowired
    ClipPointCrypto clipPointCrypto;
    
	/**
	 * 수/발신처(SEND_RECV_PLC) 연동시스템 코드
	 * PH	포인트허브
	 * CP	클립포인트
	 */
    private String SEND_PLC = Constant.PLC_PH;
    private String RECV_PLC = Constant.PLC_CP;

	/**
	 * <pre> ClipPoint server  http post방식 통신 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ResultVo
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */
	public ResultVo sendHttpPostCP(String uri, Map<String, Object> pBodyParams, Map<String, Object> etcParams) throws Exception {
	//public ResultVo sendHttpPost(String uri, MultiValueMap<String, Object> params) {
		String methodName = "sendHttpPostCP";
		log.debug(methodName, "Start!");
		//log.debug(methodName, "etcParams=" + JsonUtil.toJson(etcParams));
		
		// declare
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller("SbankHttpService.sendHttpPostSB");
		
		// set url
		String sendUrl = getDomain(uri);
		
		// set header
	    Map<String, Object> hmap = new HashMap<String, Object>();
	    hmap.put("authorization", getAuthorization());
	    hmap.put("appType", "json");
	    

	    try {
		    // 송신 데이터 암호화 항목 적용	
		    // ctn (전화번호)	 
	    	Map<String, Object> tmpBodyParams = cryptoSendData(pBodyParams);
			
		    // send
			//ResponseEntity<MultiValueMap> responseEntity = httpUtil.sendHttpPost("http://localhost/api/test", map, null);
		    resultVo = sendHttpPost(sendUrl, hmap, tmpBodyParams);
		    
		    // 클립포인트 결과메시지 재정의(카드사코드로 재정의) - 2019.06.14. lys
		    if( "200".equals(StringUtil.nvl(resultVo.getRstMsg())) ) {
		    	resultVo.setRstMsg(StringUtil.nvl(pBodyParams.get("partner")));
		    }
		    
		} catch (IfException e) {
			log.error("cryptoSendData", "["+e.getErrCd()+"]"+e.getErrMsg());
			resultVo.setSucYn("N");
			resultVo.setRstCd(messageManage.getMsgCd("SY_ERROR_153"));
			resultVo.setRstMsg(messageManage.getMsgTxt("SY_ERROR_153"));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.printStackTracePH(methodName, e);
			resultVo.setSucYn("N");
			resultVo.setRstCd(messageManage.getMsgCd("SY_ERROR_900"));
			resultVo.setRstMsg(messageManage.getMsgTxt("SY_ERROR_900"));
		}
	    
	    // log write
	    insertAdmApiIfLog(sendUrl, (String)etcParams.get("phubTrNo"), (String)etcParams.get("pgDealNo"), resultVo);
 	    
			
		// result
		return resultVo;
	}
	
	/**
	 * <pre> 시스템파라미터 중 Domain 읽기</pre>
	 * 
	 * @param  String
	 * @return String
	 */	
	private String getDomain(String uri){
		// 도메인값 시스템파라미터에서 가져와야 한다.
		//String domain = "http://localhost" + uri;
		String domain = sysPrmtManage.getSysPrmtVal("CP_CONN_IP") + uri;
		
		return StringUtil.nvl(domain);
	}
	
	/**
	 * <pre>연동시 Authorization키 가져오기 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */	
	private String getAuthorization(){
		// 클립포인트 연동 인증키 없음.
		String authorization = "";
		
		return StringUtil.nvl(authorization);
	}
	
	
	/**
	 * <pre> ClipPoint 송신데이터 암호화 처리 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. ClipPoint 연동시 전달할 데이터를 암호화 한다.
	 *      2. 암호화 항목은 전화번호(ctn) 항목밖에 없다.
	 *      </pre>
	 */		
	private Map<String, Object> cryptoSendData(Map<String, Object> params) throws IfException {
		// declare
		String stepCd = "001";
		Map<String, Object> rtnMap = params;
		
		
		// 전화번호 암호화
		stepCd = "002";
		String ctn = clipPointCrypto.encAES((String)params.get("ctn"));
		if( "".equals(ctn) ) {
			log.error("cryptoSendData", "crypto Fail!");
			throw new IfException(stepCd, messageManage.getMsgVOY("SY_ERROR_153"));
		}
		
		rtnMap.put("ctn", ctn);
		
		// 불필요 항목 제거
		rtnMap.remove("phubTrNo");	// 포인트허브 거래 번호
		
		return rtnMap;
	}
	
	
	/**
	 * <pre>전문 송/수신 Log기록</pre>
	 * 
	 * @param  string & ResultVo
	 * @return void
	 */		
	private void insertAdmApiIfLog(String sendUrl, String phubTrNo, String pgDealNo, ResultVo resultVo) {
		String methodName = "insertAdmApiIfLog";
		
		// declare
	    Map<String, Object> logMap = new HashMap<String, Object>();
	    
	    try {
	    	Map tmpReq = (Map)resultVo.getReqBody();
	    	Map tmpRst = (Map)resultVo.getRstBody();
	    	if( null == tmpReq) tmpReq = new HashMap();
	    	if( null == tmpRst) tmpRst = new HashMap();
	    	
		    logMap.put("actn_uri",     sendUrl);
		    logMap.put("send_plc",     SEND_PLC);
		    logMap.put("rcv_plc",      RECV_PLC);
		    logMap.put("phub_tr_no",   phubTrNo);
		    logMap.put("pg_deal_no",   pgDealNo);
		    logMap.put("req_data",     JsonUtil.MapToJson(tmpReq));
		    logMap.put("res_data",     JsonUtil.MapToJson(tmpRst));
		    logMap.put("rply_cd",      getRplyCd(tmpRst));
		    logMap.put("rply_msg",     StringUtil.nvl(resultVo.getRstMsg()));
		    logMap.put("rgst_user_id", "sendHttpPostCP");
		    
		    // insert log
		    logService.insertAdmApiIfLog(logMap);
	    } catch(Exception e) {
	    	log.error(methodName, JsonUtil.MapToJson(logMap));
	    	log.error(methodName, StringUtil.nvl(e.getMessage()));
	    }
	}
	
	/**
	 * <pre>전문 송/수신 결과코드 추출</pre>
	 * 
	 * @param  Map<String, Object>
	 * @return String
	 * @see
	 * 
	 */		
	private String getRplyCd(Map<String, Object> rstBody) {
		String methodName = "getRplyCd";
		
		// declare
		String rplyCd = "";	
	    
	    try {
			rplyCd = StringUtil.nvl(rstBody.get("retcode"), messageManage.getMsgCd("IF_ERROR_908"));
			
			// 클립은 결과코드가 "0" 인경우 성공이다.
			if( "0000".equals(rplyCd) ) rplyCd = Constant.SUCCESS_CODE;
		    
	    } catch(Exception e) {
	    	// 에러 로그 기록
	    	log.error(methodName, StringUtil.nvl(e.getMessage()));
	    }
	    
	    return rplyCd;
	}
}
