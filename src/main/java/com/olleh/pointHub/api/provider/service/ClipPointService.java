/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.api.provider.service;

import java.util.List;
import java.util.Map;

import com.olleh.pointHub.api.comn.model.ClipPointIfVo;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.model.ResultVo;

/**
 * <pre>클립포인트서버 연동 service</pre>
 * @Class Name : ClipPointService
 * @author lys
 * @since 2018.08.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.05    lys        최초생성
 * </pre>
 */
public interface ClipPointService {	
	
	
	/**
	 * <pre> 카드사포인트 조회 </pre>
	 * 
	 * @param CPReqVo cPReqVo
	 * @return Map<String, Object>
	 * @see <pre>
	 *      여러 카드사 포인트 조회 
	 *      </pre>
	 */
	public ResultVo getpoints(List<ClipPointIfVo> params, Map<String, Object> etcParam) throws Exception;
	
	
	/**
	 * <pre> 카드사포인트 차감 </pre>
	 * 
	 * @param CPReqVo cPReqVo
	 * @return ResultVo
	 * @see <pre>
	 *      카드사 포인트 차감 
	 *      </pre>
	 */
	public ResultVo minuspoint(List<ClipPointIfVo> params, Map<String, Object> etcParam) throws Exception;
	
	
	/**
	 * <pre> 카드사포인트 차감 실패시 성공건에 대한 롤백처리 </pre>
	 * 
	 * @param CPReqVo cPReqVo
	 * @return ResultVo
	 * @see <pre>
	 *      1. 카드사포인트 차감 처리 중 실패할 시 성공건에 대해서는 클립포인트에 취소전문을 날린다.
	 *      </pre>
	 */	
	public ResultVo RollbackMinusPoint(List<ClipPointIfVo> params, Map<String, Object> etcParam) throws Exception;	
	
	
	/**
	 * <pre> 카드사포인트 차감 취소 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @return ResultVo
	 * @see <pre>
	 *      카드사 포인트 차감 취소 
	 *      </pre>
	 */
	public ResultVo cancelpoint(List<ClipPointIfVo> params, Map<String, Object> etcParam) throws Exception;
	
	
	/**
	 * <pre> 클립포인트 연동 송신데이터 유효성 체크 </pre>
	 * 
	 * @param Map<String, String> params
	 * @return boolean
	 * @see <pre>
	 *      1. 포인트 허브는 송신데이터 항목 전부가 필수 값이다. 
	 *      </pre>
	 */	
	public boolean validateSendData(Map<String, Object> params) throws Exception;
	
	
	
	/**
	 * <pre> 포인트조회 </pre>
	 * 
	 * @param Map<String, String> params
	 * @return boolean
	 * @see <pre>
	 *      1. 추후  phubService.java로 이동시켜야 한다.
	 *      </pre>
	 */
	public Map<String, Object> getPoint(Map<String, Object> params) throws BizException, Exception;
	
	public Map<String, Object> getPointUseList(Map<String, Object> params) throws Exception;
}
