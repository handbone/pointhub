package com.olleh.pointHub.api.provider.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.olleh.pointHub.common.exception.BizException;

public interface ClipService {	
	
	// PH 거래번호 생성
	public Map<String, Object> getPhubTrNo();

	// 약관(CLAUSES)정보
	public Map<String, Object> getClaUses(Map<String, Object> params) throws BizException, Exception;

	// 포인트사용처(가맹점) 체크
	public Map<String, Object> countPgCprt(Map<String, Object> params);
	
	// 포인트사용처(가맹점) 정보 가져오기
	public Map<String, Object> getPgCprtInfo(Map<String, Object> params);											
	
	// 클립 회원가입 여부 조회
	public Map<String, Object> rglrCheck(Map<String, Object> params) throws BizException, Exception;		
	
	// 카드사별 포인트정보
	public List <Map<String, Object>> getBasicPointInfo(Map<String,Object> params);								
	
	// 정보제공사항 정보
	public List <Map<String, Object>> getPhubInfoPrvdItem(Map<String,Object> params)  throws BizException, Exception ;
	
	// 포인트 사용자 고객 조회
	public Map<String, Object> manageCustInfo(Map<String, Object> params)  throws BizException, Exception;
	
	// public 클랩회원등록();, RGLMMB 																	
	public Map<String, Object> regRglmmb(Map<String,Object> params)  throws BizException, Exception;
	
	// 약관동의 정보 저장
	public Map<String, Object> setClsAgrInfo(Map<String,Object> params) throws BizException, Exception;	
	
	// 마스터 약관 정보 조회
	public List<Map<String, Object>> getPhubClsInfo(Map<String,Object> params);

	// Clip회원탈퇴처리
	public int disjoinClipMember(Map<String, Object> params) throws BizException, Exception;
	
	//클립포인트 연동 송신데이터 유효성 체크
	public boolean validateSendData(Map<String, Object> params) throws Exception;
	
	// 외부로부터의  로그처리
	public void logRequestFromOut(Map<String,Object> params, HttpServletRequest req , String pMethod );
}
