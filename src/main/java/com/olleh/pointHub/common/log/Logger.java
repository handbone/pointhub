/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.log;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;

import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.exception.UserException;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 로그출력 레퍼 클래스 (추천시스템의 로그를 일괄 관리하기 위한 Class, Log4j 이용)
 * @Class Name : Logger
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class Logger {

    /** 호출한 클래스의 이름 */
    private String CLASS_NAME = "Logger";
    /** Apache Log4j Logger */
//    private static org.apache.log4j.Logger logger = null;
    private org.apache.log4j.Logger logger = null;
    /** Log Level - Debug */
    private static int DEBUG = 1;
    /** Log Level - Info */
    private static int INFO = 2;
    /** Log Level - Warn */
    private static int WARN = 3;
    /** Log Level - Error */
    private static int ERROR = 4;
    /** Log Level - Fatal */
    private static int FATAL = 5;

    /** Exception Log - Level is Temp */
    private static int EX_TEMP = 1;
    /** Exception Log - Level is Error */
    private static int EX_ERROR = 2;
    /** Exception Log - Level is Fatal */
    private static int EX_FATAL = 3;
    
    private static int LOG_LEVEL = 1;
    
    @Autowired
    MessageManage messageManage;

    /**
     * 생성자
     */
    public Logger(Class clazz) {
    	CLASS_NAME = clazz.getName();
        logger = org.apache.log4j.Logger.getLogger(clazz);
        Level level = null;
        int logLevel = LOG_LEVEL;
        if (FATAL == logLevel) {
        	level = Level.FATAL;
        } else if (ERROR == logLevel) {
        	level = Level.ERROR;
        } else if (WARN == logLevel) {
        	level = Level.WARN;
        } else if (INFO == logLevel) {
        	level = Level.INFO;
        } else if (DEBUG == logLevel) {
        	level = Level.DEBUG;
        }
        if (level != null) {
            logger.setLevel(level);
        }
    }

    /**
     * Debug Message Logging.
     * @param method 메소드이름
     * @param message 로그출력 메세지
     */
    public void debug(String method, String message){
        logging( DEBUG, method, message);
    }
    
    /**
     * Debug Message Logging.
     * @param Exception e
     */
    public void debug(Exception e){
        logging( DEBUG, e.getStackTrace()[0].getMethodName(), e.getMessage());
    }    

    /**
     * Infomation Message Logging.
     * @param method 메소드이름
     * @param message 로그출력 메세지
     */
    public void info(String method, String message){
        logging( INFO, method, message);
    }

    /**
     * Warning Message Logging.
     * @param method 메소드이름
     * @param message 로그출력 메세지
     */
    public void warn(String method, String message){
        logging( WARN, method, message);
    }

    /**
     * Error Message Logging.
     * @param method 메소드이름
     * @param message 로그출력 메세지
     */
    public void error(String method, String message){
        logging( ERROR, method, message);
    }
    
    /**
     * printStackTrace Logging.
     * @param method 메소드이름
     * @param message 로그출력 메세지
     */
    public void printStackTracePH(String method, Throwable e){
    	exStackTrace(method, e);       
    }    

    /**
     * Fatal Message Logging.
     * @param method 메소드이름
     * @param message 로그출력 메세지
     */
    public void fatal(String method, String message){
        logging( FATAL, method, message);
    }

    /**
     * Exception Message Logging - Level is Temp.
     * Error Code 로 메세지 처리.
     * @param method 발생 개소 (메소드명)
     * @param errorCode 로그출력 에러코드
     */
    public void exTemp(String method, String errorCode){
        exLogging( EX_TEMP, method, errorCode);
    }

    /**
     * Exception Message Logging - Level is Error.
     * Error Code 로 메세지 처리.
     * @param method 발생 개소 (메소드명)
     * @param errorCode 로그출력 에러코드
     */
    public void exError(String method, String errorCode){
        exLogging( EX_ERROR, method, errorCode);
    }

    /**
     * Exception Message Logging - Level is Fatal.
     * Error Code 로 메세지 처리.
     * @param method 발생 개소 (메소드명)
     * @param errorCode 로그출력 에러코드
     */
    public void exFatal(String method, String errorCode){
        exLogging( EX_FATAL, method, errorCode);
    }

    /**
     * Exception Message Logging - Level is Error.
     * CommonErrorException 으로 메세지 처리.
     * @param method 발생 개소 (메소드명)
     * @param e 사용자정의 예외
     */
    public void exError(String method, UserException e){
        //logging( ERROR, e.getMessage());
    	exLogging(ERROR, method, e);
    }

    /**
     * Exception Message Logging - Level is Fatal.
     * CommonFatalException 으로 메세지 처리.
     * @param method 발생 개소 (메소드명)
     * @param e 사용자정의 예외
     */
    public void exFatal(String method, UserException e){
        //logging( FATAL, e.getMessage());
    	exLogging(FATAL, method, e);
    }

    /**
     * 런타임으로 발생한 예외의 경우 처리
     * Throwable 으로 메세지 처리.
     * @param method 발생 개소 (메소드명)
     * @param e RunTime 예외의 트레이스 출력
     */
    public void exStackTrace(String method, Throwable e) {
        StringBuffer strLog = new StringBuffer();
        int idx             = 0;

        if (e != null) {
            // StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        	StackTraceElement[] ste = e.getStackTrace();
        	idx = ste.length - 1;
        	
            //0
        	strLog.append("Exception=");
            strLog.append(e.getMessage());
            strLog.append("\r\n");
            strLog.append("(");
            strLog.append(ste[0].getFileName());
            strLog.append(":");
            strLog.append(ste[0].getLineNumber());
            strLog.append(")");
            
            // max
            strLog.append(", (");
            strLog.append(ste[idx].getFileName());
            strLog.append(":");
            strLog.append(ste[idx].getLineNumber());
            strLog.append(")");                        
        }

        logging( ERROR, method, strLog.toString());
    }

    /**
     * 실제 로그 처리 메소드, 일반로그 처리
     * Log4j Logger 를 호출한다.
     * @param logLevel 로그추력레벨
     * @param method 발생 개소 (메소드명)
     * @param message 로그메세지
     */
    private void logging(int logLevel, String method, String message){
        // 오류메세지를 가공한다.
    	StringBuffer sbMessage = new StringBuffer();
    	sbMessage.append("[");
//    	sbMessage.append(CLASS_NAME);
//    	sbMessage.append(".");
    	sbMessage.append(method);
    	sbMessage.append("]");
    	sbMessage.append(message);
        String msg = sbMessage.toString();

        switch( logLevel ){
            case 1:
                logger.debug(msg);
                break;
            case 2:
                logger.info(msg);
                break;
            case 3:
                logger.warn(msg);
                break;
            case 4:
                logger.error(msg);
                break;
            case 5:
                logger.fatal(msg);
                break;
            default :
            	logger.debug(msg);
        }
    }

    /**
     * 실제 로그 처리 메소드, Exception 로그 처리.
     * MessageDataMng 를 이용한다.
     * @param logLevel 로그추력레벨
     * @param method 발생 개소 (메소드명)
     * @param errorCode 에러코드
     */
    private void exLogging(int logLevel, String method, String errorCode){
        String errorMessage = messageManage.getMsgTxt(errorCode);

        StringBuffer sbMessage = new StringBuffer();
        sbMessage.append("[");
        sbMessage.append(errorCode);
        sbMessage.append("] ");
        sbMessage.append(errorMessage);
        String message = sbMessage.toString();

        switch( logLevel ){
            case 1:
                logging(WARN, method, message);
                break;
            case 2:
                logging(ERROR, method, message);
                break;
            case 3:
                logging(FATAL, method, message);
                break;
            default :
                logging(WARN, method, message);            	
        }
    }

    /**
     * 상위클래스에서 사용자 정의 예외를 받은 경우 찍어주는처리.(최상위로직에서 한번수행)
     * @param logLevel 예외출력레벨
     * @param method 발생 개소 (메소드명)
     * @param ex 사용자정의 예외
     */
    private void exLogging(int logLevel, String method, UserException ex) {
    	String message = StringUtil.printUserSysMessage(ex);

    	//로그출력호출
    	logging( logLevel, method, message);
    	//전체 예외 스택트레이스 출력
    	exStackTrace (method, ex.getErrorInfo().getOriginException());
    }
}
