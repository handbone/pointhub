package com.olleh.pointHub.common.scheduler.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.scheduler.dao.PHubDAO;
import com.olleh.pointHub.common.scheduler.task.NoUseCpnExtDtTask;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre> 패밀리포인트 Job Service 및 Task 실행 </pre>
 */
@Service
public class GiftiShowJobService {
	
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	NoUseCpnExtDtTask noUseCpnExtDtTask;
	
	/**
	 * <pre> 일대사번호 가져오기 </pre>
	 * 
	 * @return
	 */
	public String getCmprDd() {
		Map<String, Object> result = pHubDAO.selectDdCmprNo();
		if (result != null) {
			return StringUtil.nvl(result.get("dd_cmpr_no"));
		} else {
			return "";
		}
	}
	
	/**
	 * <pre> 로그번호 가져오기 </pre>
	 * 
	 * @return
	 */
	public String getLogNo() {
		Map<String, Object> result = pHubDAO.selectLogNo();
		if (result != null) {
			return StringUtil.nvl(result.get("LOG_NO"));
		} else {
			return "";
		}
	}
	
	/**
	 * <pre> 로그 마스터 생성 (INSERT INTO ADM_BATCH_LOG_M) </pre>
	 * 
	 * @param ddCmprNo
	 * @param params
	 * @return
	 */
	public int insertAdmBatchLogM(Map<String, String> params) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dd_cmpr_no"	, params.get("ddCmprNo"));
		map.put("cmpr_dd"		, params.get("cmprDd"));
		map.put("user_id"		, params.get("userId"));
		map.put("wrkn_nm"		, params.get("wrknNm"));
		int ret = pHubDAO.insertAdmBatchLogM(map);
		log.debug("insertAdmBatchLogM", "배치작업로그 마스터 생성 결과: "+ ret);
		return ret;
	}
	
	/**
	 * <pre> 로그 마스터 수정 </pre>
	 * 
	 * @return
	 * @throws Exception
	 */
	public int updateAdmBatchLogM(Map<String, String> params, int total, int mtch, int noAcr) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dd_cmpr_no"	, params.get("ddCmprNo"));
		map.put("cmpr_dd"		, params.get("cmprDd"));
		map.put("wrkn_nm"		, params.get("wrknNm"));
		map.put("user_id"		, params.get("userId"));
		map.put("ttl_deal_cnt"	, total);
		map.put("mtch_cnt"		, mtch);
		map.put("no_acr_cnt"	, noAcr);
		map.put("doc_file_nm"	, "");
		map.put("rslt_code"		, StringUtil.nvl(params.get("retcode"), messageManage.getMsgCd("SY_INFO_00")));
		map.put("rslt_msg"		, StringUtil.nvl(params.get("retmsg"), messageManage.getMsgCd("SY_INFO_00")));
		int ret = pHubDAO.updateAdmBatchLogM(map);
		log.debug("updateAdmBatchLogM", "배치작업로그M 수정 결과 : " + ret);
		return ret;
	}
	
	/**
	 * <pre> 배치실행로그 저장 </pre>
	 * 
	 * @param params
	 * @return
	 */
	public int insertAdmBatchLogNo(Map<String, String> params, String taskId, String strDt, int tgtCnt, int cmprCnt, String rsltCode, String rsltMsg) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("log_no"		, params.get("logNo"));
		map.put("dd_cmpr_no"	, params.get("ddCmprNo"));
		map.put("cmpr_dd"		, params.get("cmprDd"));
		map.put("task_id"		, taskId);
		map.put("strt_dt"		, strDt);
		map.put("tgt_cnt"		, tgtCnt);
		map.put("cmpr_cnt"		, cmprCnt);
		map.put("rslt_code"		, StringUtil.nvl(rsltCode, messageManage.getMsgCd("SY_INFO_00")));
		map.put("rslt_msg"		, StringUtil.nvl(rsltMsg,  messageManage.getMsgTxt("SY_INFO_00")));
		map.put("user_id"		, params.get("userId"));
		int ret = pHubDAO.insertAdmBatchLogNo(map);
		log.debug("insertAdmBatchLogNo", "배치실행로그 생성 결과: "+ ret);
		return ret;
	}
	
	/**
	 * <pre> 미사용 쿠폰 기한 만료 처리 Task 실행 </pre>
	 * 
	 * @param params
	 * @return
	 */
	public Map<String, Object> noUseCpnExtDtResult(Map<String, String> params) {
		//현재 실행중인 함수
		String methodName ="noUseCpnExtDtResult";
		log.debug(methodName, "미사용 쿠폰 기한 만료 처리:"+ params.toString());
		return noUseCpnExtDtTask.noUseCpnExtDtResult(params);
	}
}
