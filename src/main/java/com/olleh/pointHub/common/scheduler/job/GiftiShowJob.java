package com.olleh.pointHub.common.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.dao.CommonDAO;
import com.olleh.pointHub.common.exception.UserException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.SysPrmtInfoVO;
import com.olleh.pointHub.common.scheduler.service.GiftiShowJobService;
import com.olleh.pointHub.common.scheduler.task.NoUseCpnExtDtTask;
import com.olleh.pointHub.common.utils.DateUtils;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;

/**
 * 기프티쇼 미사용 쿠폰 기한 만료 처리 수행 job
 * @Class Name : SystemJob
 * @author mgBang
 * @since 2019.04.09
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.04.09   mgBang     최초생성
 * </pre>
 */
@Service("giftiShowJob")
public class GiftiShowJob {
	
	private Logger log = new Logger(this.getClass());
	private String stepCd = "0";	// 수행 순번
	private int cmprProcStep = 0;	// 대사 단계별 순번
	
	private int ttlDealCnt = 0;		// 총 대사건수
	private int mtchCnt = 0;		// 총 일치건수
	private int noAcrCnt = 0;		// 총 불일치건수
	
	private String ddCmprNo;		// 일대사 번호
	
	@Resource
	CommonDAO commonDAO;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	GiftiShowJobService giftiShowJobService;
	
	/**
	 * <pre> job 실행 함수 </pre>
	 *
	 * @throws JobExecutionException
	 */
	public void execute() throws JobExecutionException {
		Map<String, String> params = new HashMap<String, String>();
		log.debug("execute", this.getCurrentDate());
		
		// get was1 addr : 
		Map<String, Object> sendParam = new HashMap<String, Object>();
		sendParam.put("prmtCd", "PH_CONN_IP2");
		SysPrmtInfoVO sysPrmtInfoVO = commonDAO.getSysPrmtY(sendParam);
		
		String wasAddr = StringUtil.nvl(sysPrmtInfoVO.getPrmtVal());
		//log.debug(methodName, "hostAddr="+hostAddr+", was1Addr="+was1Addr);
		
		// was check : was2호기에서만 수행되도록 한다.
		if(SystemUtils.isHostServerIP(wasAddr)) {
			params.put("userId", "btch");
			params.put("cmprDd", this.getCurrentDate());
			//params.put("cmprDd", "20190530");
			
			this.stepCd = "0";
			this.cmprProcStep = 0;
			this.ttlDealCnt = 0;
			this.mtchCnt = 0;
			this.noAcrCnt = 0;
			this.ddCmprNo = "";
			
			process(params);
		} else {
			log.debug("execute", "this Server is WAS1. (wasAddr="+wasAddr+")");
		}
	}
	
	/**
	 * <pre> 배치 프로세스 </pre>
	 * 	
	 * @param params
	 */
	public void process(Map<String, String> params) {
		String method = "process";
		log.debug(method, "Start Process! (params=" + JsonUtil.toJson(params) + ")");

		/* -----Set Parameter ---------------------------------------------------------------------------- */
		int idx = 0;
		Map<Integer, String> batchProcMap = new HashMap<Integer, String>();
		batchProcMap.put(idx++, "1."+ NoUseCpnExtDtTask.TASK_ID);
		
		try {
			/* ----------------------------------------------------------------------------------------------- */
			// 1. 일대사번호 추출
			this.stepCd = "1";
			this.ddCmprNo = giftiShowJobService.getCmprDd();
			// 에러 처리
			if (StringUtils.isEmpty(ddCmprNo)) {
				log.error(method,"일대사번호 추출 오류");
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}
			
			/* ----------------------------------------------------------------------------------------------- */
			// 2. 로그 마스터 생성
			this.stepCd = "2";
			params.put("ddCmprNo", this.ddCmprNo);
			params.put("wrknNm"	 , getClass().getSimpleName());
			params.put("userId"	 , StringUtil.nvl(params.get("userId"), "btch"));							// 작업자
			int ret = giftiShowJobService.insertAdmBatchLogM(params);
			if (ret < 1) {
				log.error(method, "클립포인트일대사결과M 오류");
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_ERROR_126"));
			}
			
			/* ----------------------------------------------------------------------------------------------- */
			// 3. 단계별 대사 처리
			this.stepCd = "3";
			for (int num=0; num<idx; num++) {
				String strDt = DateUtils.defaultDate() + DateUtils.defaultTime();
				cmprProcStep++;
				
				// log_no 얻기
				params.put("logNo", giftiShowJobService.getLogNo());
				
				// 대사 수행 전 초기화
				String taskId   = StringUtil.nvl(batchProcMap.get(num));
				String rsltCode = messageManage.getMsgCd("SY_INFO_00");
				String rsltMsg  = messageManage.getMsgTxt("SY_INFO_00");
				int tgtCnt = 0;
				int cmprCnt = 0;
				
				switch(num) {
					case 0:	// 미사용 쿠폰 기한 만료 처리 수행
						Map<String, Object> result0 = giftiShowJobService.noUseCpnExtDtResult(params);
						if (result0 == null) {
							throw new UserException(getClass().getName(), method, stepCd, "F999", "["+taskId+"] NoUseCpnExtDtTask 수행 오류");
						}
						tgtCnt   = Integer.parseInt(StringUtil.nvl(result0.get("tgt_cnt"),"0"));
						cmprCnt  = Integer.parseInt(StringUtil.nvl(result0.get("cmpr_cnt"),"0"));
						rsltCode = StringUtil.nvl(result0.get("rslt_code"));
						rsltMsg  = StringUtil.nvl(result0.get("rslt_msg")); 
						break;
					default:
				}
				
				// 단계별 대사 결과 저장
				giftiShowJobService.insertAdmBatchLogNo(params, taskId, strDt, tgtCnt, cmprCnt, rsltCode, rsltMsg);
				
				// 단계 별 마스터 데이터 세팅
				ttlDealCnt += tgtCnt;
				mtchCnt += cmprCnt;
				params.put("retcode", rsltCode);
				params.put("retmsg" , rsltMsg);
			}
			
			this.cmprProcStep = 0;
			this.noAcrCnt = ttlDealCnt - mtchCnt;
			log.debug(method, "총 대사 건수["+ ttlDealCnt +"], 일치건수["+ mtchCnt +"], 불일치건수["+ noAcrCnt +"]");
			
		} catch (UserException e) {
			log.error(method, "Exception : "+ e.getErrorInfo().getErrMsg());
			params.put("retcode", e.getErrorInfo().getErrCd());
			params.put("retmsg" , e.getErrorInfo().getErrMsg());
		} catch (Exception e) {
			log.printStackTracePH(method, e);
			params.put("retcode", messageManage.getMsgCd("SY_ERROR_900"));
			params.put("retmsg" , messageManage.getMsgTxt("SY_ERROR_900"));
		} finally {
			// 배치 로그마스터 업데이트
			giftiShowJobService.updateAdmBatchLogM(params, ttlDealCnt, mtchCnt, noAcrCnt);
			log.debug(method, "End Process! (rsltCode=" + params.get("retcode") + ")");
		}
	}
	
	/**
	 * <pre> 시스템 현재일자 가져오기 </pre>
	 * 
	 * @return
	 */
	public String getCurrentDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1); //현재일자에서 D-1
		return new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
	}
}
