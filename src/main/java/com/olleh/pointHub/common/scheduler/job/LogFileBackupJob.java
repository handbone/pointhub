package com.olleh.pointHub.common.scheduler.job;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.exception.UserException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.scheduler.service.CommJobService;
import com.olleh.pointHub.common.scheduler.task.WasLogFileBackupTask;
import com.olleh.pointHub.common.utils.DateUtils;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre>
 *  로그 압축 Job Class
 * </pre>
 * 
 * @author harry
 */
@Service("logFileBackupJob")
public class LogFileBackupJob {

	private Logger log = new Logger(this.getClass());
	private String stepCd = "0"; 						// 수행 순번
	private int cmprProcStep = 0; 						// 대사 단계별 순번

	private int ttlDealCnt = 0; 						// 총 대사건수
	private int mtchCnt = 0; 							// 총 일치건수
	private int noAcrCnt = 0; 							// 총 불일치건수

	private String ddCmprNo; 							// 일대사 번호

	@Autowired
	MessageManage messageManage;

	@Autowired
	WasLogFileBackupTask wasLogFileBackupTask;

	@Autowired
	CommJobService commJobService;

	/**
	 * <pre>
	 *  job 실행 함수
	 * </pre>
	 * 
	 * @throws JobExecutionException
	 */
	public void execute() throws JobExecutionException {
		
		Map<String, String> params = new HashMap<String, String>();
		log.debug("execute", commJobService.getCurrentDate());
		params.put("cmprDd", commJobService.getCurrentDate());
		params.put("userId", "btch");
		params.put("trgetDe", commJobService.getCurrentDate("yyyy-MM-dd"));

		this.stepCd = "0";
		this.cmprProcStep = 0;
		this.ttlDealCnt = 0;
		this.mtchCnt = 0;
		this.noAcrCnt = 0;
		this.ddCmprNo = "";

		process(params);
	}

	/**
	 * <pre>
	 *  배치 프로세스
	 * </pre>
	 * 
	 * @param params
	 */
	public void process(Map<String, String> params) {

		String method = "process";

		log.debug(method, "Start Process! (params=" + JsonUtil.toJson(params) + ")");

		int idx = 0;
		Map<Integer, String> batchProcMap = new HashMap<Integer, String>();
		batchProcMap.put(idx++, "1." + wasLogFileBackupTask.TASK_ID);

		try {
			
			// 1. 일대사번호 추출
			this.stepCd = "1";
			this.ddCmprNo = commJobService.getCmprDd(); // 공통 일대사번호

			// 에러 처리
			if (StringUtils.isEmpty(ddCmprNo)) {
				
				log.error(method, "일대사번호 추출 오류");
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}

			
			// 2. 로그 마스터 생성
			this.stepCd = "2";
			
			params.put("ddCmprNo", this.ddCmprNo);
			params.put("wrknNm", getClass().getSimpleName());
			
			int ret = commJobService.insertAdmBatchLogM(params);
			
			if (ret < 1) {
				
				log.error(method, "클립포인트일대사결과M 오류");
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_ERROR_126"));
			}


			// 3. 단계별 대사 처리
			this.stepCd = "3";
			
			for (int num = 0; num < idx; num++) {
				
				String strDt = DateUtils.defaultDate() + DateUtils.defaultTime();
				cmprProcStep++;

				// 대사 수행 전 초기화
				String taskId = StringUtil.nvl(batchProcMap.get(num));
				String rsltCode = messageManage.getMsgCd("SY_INFO_00");
				String rsltMsg = messageManage.getMsgTxt("SY_INFO_00");
				int tgtCnt = 0;
				int cmprCnt = 0;

				switch (num) {
				
				case 0: // 로그파일 압축

					Map<String, Object> result0 = wasLogFileBackupTask.logFileBackup(params);

					if (result0 == null) {
						throw new UserException(getClass().getName(), method, stepCd, "F999",
								"[" + taskId + "] WasLogFileBackupTask 수행 오류");
					}

					tgtCnt = Integer.parseInt(StringUtil.nvl(result0.get("tgt_cnt"), "0"));
					cmprCnt = Integer.parseInt(StringUtil.nvl(result0.get("cmpr_cnt"), "0"));
					rsltCode = StringUtil.nvl(result0.get("rslt_code"));
					rsltMsg = StringUtil.nvl(result0.get("rslt_msg"));
					params.put("tgt_file_path", (String)result0.get("tgt_file_path"));
					params.put("cmpr_file_path", (String)result0.get("cmpr_file_path"));
					break;

				default:
				}

				// 단계별 대사 결과 저장
				commJobService.insertAdmBatchLog(params, taskId, strDt, tgtCnt, cmprCnt, rsltCode, rsltMsg);

				// 단계 별 마스터 데이터 세팅
				ttlDealCnt += tgtCnt;
				mtchCnt += cmprCnt;
				params.put("retcode", rsltCode);
				params.put("retmsg", rsltMsg);
			}

			this.cmprProcStep = 0;
			this.noAcrCnt = ttlDealCnt - mtchCnt;
			log.debug(method, "총 대사 건수[" + ttlDealCnt + "], 일치건수[" + mtchCnt + "], 불일치건수[" + noAcrCnt + "]");

		} catch (UserException e) {
			log.error(method, "Exception : " + e.getErrorInfo().getErrMsg());
			params.put("retcode", e.getErrorInfo().getErrCd());
			params.put("retmsg", e.getErrorInfo().getErrMsg());
		} catch (Exception e) {
			log.printStackTracePH(method, e);
			params.put("retcode", messageManage.getMsgCd("SY_ERROR_900"));
			params.put("retmsg", messageManage.getMsgTxt("SY_ERROR_900"));
		} finally {
			// 배치 로그마스터 업데이트
			commJobService.updateAdmBatchLogM(params, ttlDealCnt, mtchCnt, noAcrCnt);
			log.debug(method, "End Process! (rsltCode=" + params.get("retcode") + ")");
		}
	}

}
