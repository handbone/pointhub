package com.olleh.pointHub.common.scheduler.job;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.components.CodeManage;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.PgCmpnInfoManage;
import com.olleh.pointHub.common.components.PntPrvdrMsgManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.components.UrlManage;
import com.olleh.pointHub.common.dao.CommonDAO;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.SysPrmtInfoVO;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;
/**
 * 시스템 job
 * @Class Name : SystemJob
 * @author mason
 * @since 2018.10.16
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.16   lys      최초생성
 * </pre>
 */
@Service("systemJob")
public class SystemJob {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	CommonDAO commonDAO;
	
	@Autowired
	AccsCtrnManage accsCtrnManage;	
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	PgCmpnInfoManage pgCmpnInfoManage;	
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	UrlManage urlManage;
	
	@Autowired
	PntPrvdrMsgManage pntPrvdrMsgManage;
	

	/**
	 * <pre> 시스템데이터 was메모리 리로드 처리 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */	
	public void sysDataReload() throws JobExecutionException {
		// 메소드명, 로그내용
		String methodName = "sysDataReload";
		//log.debug(methodName, "Start sysDataReload!");
		
		// declare
		String phMemReloadYnKey = "";
		//boolean isPhConnIp1 = false;
		
		// ip체크
		// get host addr
		//String hostAddr = SystemUtils.getHostServerIP();
		
		// get was1 addr : 
		Map<String, Object> sendParam = new HashMap<String, Object>();
		sendParam.put("prmtCd", "PH_CONN_IP1");
		SysPrmtInfoVO sysPrmtInfoVO = commonDAO.getSysPrmtY(sendParam);
		
		String was1Addr = StringUtil.nvl(sysPrmtInfoVO.getPrmtVal());
		//log.debug(methodName, "hostAddr="+hostAddr+", was1Addr="+was1Addr);
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////
		// was check
		if(SystemUtils.isHostServerIP(was1Addr)) {
			phMemReloadYnKey = "PH_MEM_RELOAD_YN1";		// was1 호기
		} else {
			phMemReloadYnKey = "PH_MEM_RELOAD_YN2";		// was2 호기
		}
		
		// was 메모리 리로드 여부
		// PH_MEM_RELOAD_YN
		// ONM_MEM_RELOAD_YN
		sendParam.put("prmtCd", phMemReloadYnKey);
		sysPrmtInfoVO = commonDAO.getSysPrmtY(sendParam);
		
		String memReloadYn = StringUtil.nvl(sysPrmtInfoVO.getPrmtVal(), "N");
		log.debug(methodName, "phMemReloadYnKey="+phMemReloadYnKey+", was1Addr="+was1Addr+", memReloadYn="+memReloadYn);
		
		if( "Y".equals(memReloadYn) ) {
			// 접근제어 정보 조회
			accsCtrnManage.reset();
			
			// 공통코드
			codeManage.reset();
			
			// 메시지정보
			messageManage.reset();
			
			// PG사정보  관리
			pgCmpnInfoManage.reset();			
			
			// 시스템파라미터
			sysPrmtManage.reset();
			
			// url 정보
			urlManage.reset();
			
			// 클립포인트 결과코드 정보
			pntPrvdrMsgManage.reset();			
			
			// 시스템 파라미터 값 N으로 갱신
			sendParam.put("prmtVal", "N");
			sendParam.put("userId", "btch");
			int uptCnt = commonDAO.updateSysprmtVal(sendParam);
			log.debug(methodName, "uptCnt="+uptCnt);
			
			// 배치로그 마스터 기록?
			
			// 배치로그 상세 기록?
			
			log.info(methodName, "End sysDataReload!");			
		}
	}
}
