package com.olleh.pointHub.common.scheduler.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pointHub.common.dao.AbstractDAO;

/**
 * <pre> Scheduler DAO Class </pre>
 */
@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class PHubDAO extends AbstractDAO {
	
	public Map<String, Object> selectDdCmprNo() {
		return selectOne("mybatis.scheduler.selectDdCmprNo");
	}
	
	/**
	 * <pre> 공통 일대사번호 가져오기 </pre>
	 * 
	 * @return
	 */
	public Map<String, Object> selectCommDdCmprNo() {
		return selectOne("mybatis.scheduler.selectSeqCmId");
	}

	public Map<String, Object> selectLogNo() {
		return selectOne("mybatis.scheduler.selectAdmBatchLog");
	}

	public Map<String, Object> selectNoUseCpnExpDtTotal(Map<String, Object> params) {
		return selectOne("mybatis.scheduler.selectNoUseCpnExpDtTotal", params);
	}
	
	public List<Map> selectNoUseCpnExpDt(Map<String, Object> params) {
		return selectList("mybatis.scheduler.selectNoUseCpnExpDt", params);
	}
	
	public int insertAdmBatchLogM(Map<String, Object> params) {
		return insert("mybatis.scheduler.insertAdmBatchLogM", params);
	}
	
	public int insertAdmBatchLog(Map<String, Object> params) {
		return insert("mybatis.scheduler.insertAdmBatchLog", params);
	}
	
	public int insertAdmBatchLogNo(Map<String, Object> params) {
		return insert("mybatis.scheduler.insertAdmBatchLogNo", params);
	}
	
	public int updateAdmBatchLogM(Map<String, Object> params) {
		return update("mybatis.scheduler.updateAdmBatchLogM", params);
	}
	
	public int insertAdmBatchDtl(Map<String, Object> params) {
		return insert("mybatis.scheduler.insertAdmBatchDtl", params);
	}
}
