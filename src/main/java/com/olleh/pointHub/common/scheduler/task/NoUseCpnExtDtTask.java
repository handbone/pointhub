package com.olleh.pointHub.common.scheduler.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.api.company.service.GiftiShowRestHttpService;
import com.olleh.pointHub.api.company.service.GiftiShowService;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.exception.BizException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.ResultVo;
import com.olleh.pointHub.common.scheduler.dao.PHubDAO;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * <pre> 미사용 쿠폰 기한 만료 처리 Task </pre>
 */
@Component
public class NoUseCpnExtDtTask {
	public static final String TASK_ID = "nUseCpnTask";
	
	private Logger log = new Logger(this.getClass());
	private String stepCd = "00";
	
	private String failCd = "";
	private String failMsg = "";
	
	private int tgtCnt = 0;
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	GiftiShowService giftiShowService;
	
	@Autowired
	GiftiShowRestHttpService giftiShowRestHttpService;
	
	/**
	 * <pre> 미사용 쿠폰 기한 만료 처리 </pre>
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map<String,Object> noUseCpnExtDtResult(Map<String, String> params) {
		String methodName = "noUseCpnExtDtResult";
		log.debug(methodName, params.toString());
		
		Map<String,Object> ret = new HashMap<String,Object>();
		try {
			/* ----------------------------------------------------------------------------------------------- */
			// 0. 조회 파라미터 세팅
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("cmpr_dd", params.get("cmprDd"));
			
			/* ----------------------------------------------------------------------------------------------- */
			// 1. 총 대상 건수 얻기
			this.stepCd = "01";
			Map<String, Object> totInfo = pHubDAO.selectNoUseCpnExpDtTotal(map);
			int total = totInfo != null ? Integer.parseInt(totInfo.get("cnt").toString()) : 0;
			this.tgtCnt = total;
			log.debug(methodName, "총 대상 건수: "+ total);
			
			/* ----------------------------------------------------------------------------------------------- */
			// 2. 쿠폰 대상 목록 조회
			this.stepCd = "02";
			List<Map> cpnList = pHubDAO.selectNoUseCpnExpDt(map);
			
			/* ----------------------------------------------------------------------------------------------- */
			// 3. 대상 목록 기한 만료 처리
			String userId = params.get("userId");
			String logNo = params.get("logNo");
			this.stepCd = "03";
			int listIndex = 0;
			int cmprCnt = 0;
			for (Map cpn : cpnList) {
				try {
					log.debug(methodName, listIndex + ". cpn=" + JsonUtil.toJson(cpn));
					if (cpnResult(userId, logNo, listIndex, cpn)) {
						cmprCnt++;
					}
					log.debug(methodName, listIndex + ". cmprCnt=" + cmprCnt);
				} catch (Exception e) {
					log.error(methodName, "Exception : ["+ stepCd +"] " +e.getMessage());
				} finally {
					listIndex++;
				}
			}
			
			String rsltCode = messageManage.getMsgCd("SY_INFO_00");
			String rsltMsg  = messageManage.getMsgTxt("SY_INFO_00");
			
			// 대상 건수와 처리건수가 맞지 않을 경우
			if (tgtCnt != cmprCnt) {
				rsltCode = failCd;
				rsltMsg = failMsg;
				log.error(methodName, "Exception : code="+ failCd + ", msg="+ failMsg);
			}
			
			// 결과 세팅
			ret.put("tgt_cnt"	, tgtCnt);
			ret.put("cmpr_cnt"	, cmprCnt);
			ret.put("rslt_code" , rsltCode);
			ret.put("rslt_msg" 	, rsltMsg);
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		
		return ret;
	}
	
	/**
	 * <pre> 미사용 쿠폰 1건 환불 처리 </pre>
	 * 
	 * @param cpn
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	private boolean cpnResult(String userId, String logNo, int index, Map cpn) throws Exception {
		String method = "cpnResult";
		
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller("NoUseCpnExtDtTask");
		
		boolean ret = false;
		String rsltCode  = "";
		String rsltMsg   = "";
		String pinNo     = StringUtil.nvl(cpn.get("pin_no"));
		String phubTrNo  = StringUtil.nvl(cpn.get("phub_tr_no"));
		String pgDealNo  = StringUtil.nvl(cpn.get("pg_deal_no"));
		String custCtn   = StringUtil.nvl(cpn.get("cust_ctn"));		// 고객전화번호
		String phubCpnNo = StringUtil.nvl(cpn.get("phub_cpn_no"));	// 포인트허브 쿠폰번호
		try {
			/* ----------------------------------------------------------------------------------------------- */
			// 3-1. 단일 쿠폰 정보 얻기 (기프티쇼 연동)
			this.stepCd = "03-1";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(GiftiShowRestHttpService.PARAM_PIN_NO, pinNo);
			map.put(GiftiShowRestHttpService.PARAM_TR_ID,  phubTrNo);
			resultVo = giftiShowRestHttpService.sendHttpPostGiftiShowCoupon(map);
			if (!StringUtils.equals(resultVo.getSucYn(), "Y") || resultVo.getMap() ==  null) {
				// 통신 에러
				setFail(resultVo.getRstCd(), "[/coupon]"+resultVo.getRstMsg());
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_ERROR_908"));
			}
			Map cpnInfo = resultVo.getMap();
			String pinStatusCd = StringUtil.nvl(cpnInfo.get("pinStatusCd"));
			log.debug(method, index +". pinNo: "+ pinNo +", pinStatusCd: "+ pinStatusCd + ", pinStatusNm: "+ StringUtil.nvl(cpn.get("pinStatusNm")));
			
			boolean isApply = false;
			switch (pinStatusCd) {
				case "01":	// 발행
				case "03":	// 반품
				case "06":	// 재발행
				case "08":	// 기간만료
					isApply = true;
					break;
				default:
					isApply = false;
			}
			if (!isApply) {
				// 유효하지 않는 건입니다.
				setFail("F104", "[pinStatusCd:"+ pinStatusCd +"]유효하지 않는 건입니다");
				throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_155"));
			}
			
			/* ----------------------------------------------------------------------------------------------- */
			// 3-2. 기프티쇼 구매취소 서비스 호출
			this.stepCd = "03-2";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("cpnCnclInd", "B");
			params.put("cnclInd", "B");
			params.put("phubTrNo", phubTrNo);
			params.put("pinNo", pinNo);
			params.put("custCtn", custCtn);
			params.put("phubCpnNo", phubCpnNo);
			
			Map<String, Object> cmsParams = new HashMap<String, Object>();
			cmsParams.put("phub_tr_no", phubTrNo);
			cmsParams.put("pg_tr_no", pgDealNo);
			cmsParams.put("if_yn", "Y");
			cmsParams.put("tr_div", "CA");
			
			Map<String, Object> resultMap = giftiShowService.cancelCms(params, cmsParams, null);
			if (resultMap == null) {
				// 통신 에러
				setFail("F999", "[cancelCms]response is null]");
				throw new BizException(stepCd, messageManage.getMsgVOY("IF_ERROR_908"));
			}
			String retCode = StringUtil.nvl(resultMap.get(Constant.RET_CODE));
			String retMsg = StringUtil.nvl(resultMap.get(Constant.RET_MSG));
			if (!StringUtils.equals(retCode, Constant.SUCCESS_CODE)) {
				// 쿠폰발행 취소에 실패하였습니다.(기프티쇼 쿠폰취소 실패)
				log.error(method, " -> ret_code: "+ retCode +", ret_msg: "+ retMsg);
				setFail(retCode, "[cancelCms]"+ retMsg);
				throw new BizException(stepCd, messageManage.getMsgVOY("BZ_ERROR_171"));
			}
			log.debug(method, "Success[cancelCms]ret_code:"+StringUtil.nvl(resultMap.get(Constant.RET_CODE)));
			ret = true;
			rsltCode = messageManage.getMsgCd("SY_INFO_00");
			rsltMsg  = messageManage.getMsgTxt("SY_INFO_00");
			
		} catch (BizException e) {
			setFail(e.getErrCd(), e.getErrMsg());
			rsltCode = e.getErrCd();
			rsltMsg  = e.getErrMsg();
			throw e;
		} catch (Exception e) {
			setFail("F999", e.getMessage());
			rsltCode = "F999";
			rsltMsg  = e.getMessage();
			throw e;
		} finally {
			// 배치작업상세내역 저장
			insertAdmBatchDtl(userId, logNo, index, cpn, rsltCode, rsltMsg);
		}
		return ret;
	}
	
	/**
	 * <pre> 실패 코드, 메시지 세팅 </pre>
	 * 
	 * @param code
	 * @param msg
	 */
	private void setFail(String code, String msg) {
		this.failCd = code;
		this.failMsg = msg;
	}
	
	/**
	 * <pre> 배치작업상세내역 저장 </pre>
	 * 
	 * @param params
	 * @return
	 */
	public int insertAdmBatchDtl(String userId, String logNo, int index, Map<String, String> cpn, String rsltCode, String rsltMsg) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("log_no"		, logNo);
		map.put("seq"			, index);
		map.put("phub_tr_no"	, cpn.get("phub_tr_no"));
		map.put("phub_cpn_no"	, cpn.get("phub_cpn_no"));
		map.put("pin_no"		, cpn.get("pin_no"));
		map.put("cust_id"		, cpn.get("cust_id"));
		map.put("tr_amt"		, cpn.get("tr_amt"));
		map.put("rslt_code"		, rsltCode);
		map.put("rslt_msg"		, rsltMsg);
		map.put("user_id"		, userId);
		int ret = pHubDAO.insertAdmBatchDtl(map);
		log.debug("insertAdmBatchDtl", "배치작업상세내역 생성 결과: "+ ret);
		return ret;
	}
}
