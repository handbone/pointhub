package com.olleh.pointHub.common.scheduler.task;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.scheduler.dao.PHubDAO;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

/**
 * <pre>
 *  was 로그 압축 처리 Task
 * </pre>
 */
@Service
public class WasLogFileBackupTask {

	public final String TASK_ID = "wasLogFileBackup";

	private Logger log = new Logger(this.getClass());
	private String stepCd = "00";

	private String failCd = "";
	private String failMsg = "";
	
	private final int tgtCnt = 1;	// 한개의 파일에대한 처리이므로 총 처리건수는 1로 고정

	@Resource
	PHubDAO pHubDAO;

	@Autowired
	SysPrmtManage sysPrmtManage;

	@Autowired
	MessageManage messageManage;

	/**
	 * <pre>
	 * Constructor
	 * </pre>
	 */
	public WasLogFileBackupTask() {

		this.stepCd = "00";
		this.failCd = "";
		this.failMsg = "";

		log.debug("WasLogFileBackupTask()", "Constructor");
	}

	/**
	 * <pre>
	 * 로그파일 압축
	 * </pre>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> logFileBackup(Map<String, String> params) throws Exception {

		String methodName = "logFileBackup";
		log.debug(methodName, params.toString());
		
		Map<String, Object> ret = new HashMap<String, Object>();
		
		String logFolderPath = sysPrmtManage.getSysPrmtVal("ONM_WLOG_TGT_DIR");		    // C:/PHDev/jb_log/pointhubtb11
		String backupFolderPath = sysPrmtManage.getSysPrmtVal("ONM_WLOG_BAK_DIR");		// C:/PHDev/jb_log/pointhubtb11/backup
		String filePrefix = "server.log.";
		String fileSuffix = ".log";													// 다른 시스템 로그를 위해 시스템 파라메터로 넣을지 확인...

		String zipFileName = backupFolderPath + File.separatorChar + filePrefix + (String)params.get("trgetDe") + ".zip";	// 압축파일

		// findFiles는 경로까지 포함한 파일들의 list 이다.
		ArrayList<String> findFiles = new ArrayList<String>();
		String targetFilePath = logFolderPath + File.separatorChar + filePrefix + (String)params.get("trgetDe") + fileSuffix; 
		findFiles.add(targetFilePath);
		
		try {
			
			// 1. 압축
			this.stepCd = "01";
				
			ZipFile zipfile = new ZipFile(zipFileName);
			
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			parameters.setEncryptFiles(true);
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
			parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
			parameters.setPassword(sysPrmtManage.getSysPrmtVal("COM_WLOG_ZIP_PW"));
						
			for (int i = 0; i < findFiles.size(); i++) {
				
				zipfile.addFile(new File(findFiles.get(i)), parameters);
			}
			
			
			// 2. 정리 및 결과 셋팅
			this.stepCd = "02";
			
			// 결과 셋팅
			ret.put("tgt_cnt", tgtCnt);
			ret.put("cmpr_cnt", findFiles.size());							// 실제 처리 건수는 파일 목록의 size OR 실패시 0 
			ret.put("rslt_code", messageManage.getMsgCd("SY_INFO_00"));
			ret.put("rslt_msg", messageManage.getMsgTxt("SY_INFO_00"));
			ret.put("tgt_file_path", targetFilePath);
			ret.put("cmpr_file_path", zipFileName);
						
			// 대상 로그파일 삭제
			File targetFile = new File(targetFilePath);
			targetFile.delete();

		} catch (Exception e) {

			log.printStackTracePH(methodName, e);
		}

		return ret;
	}
}
