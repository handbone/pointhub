/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.components;

/**
 * 상수관리 콤포넌트
 * @Class Name : Constant
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * 2018.11.02   sci        CALC_STR_MODE 추가
 * 2019.01.07   sci        SUCCESS_MSG = SUCCESS로 수정
 * 2019.02.14   lys        SERVICE_ID 추가(패밀리포인트)
 * </pre>
 */
public class Constant {
	
	/**
	 * 결과 (성공/실패)
	 */	
	public final static String SUCCESS      = "SUCCESS";
	public final static String FAIL         = "FAIL";
	public final static String SUCCESS_CODE = "00";
	public final static String SUCCESS_MSG  = "SUCCESS";
	public final static String FAIL_CODE    = "-1";
	public final static String FAIL_MESSAGE = "처리에 실패했습니다.";
	public final static String JSON_OBJECT  = "JSON_OBJECT";	
	
	/**
	 * [서버내]연동시 공통성공여부, 결과코드, 결과메시지
	 */
	public final static String SUCCESS_YN = "successYn";
	public final static String RTN_CD     = "rtnCd";
	public final static String RTN_MSG    = "rtnMsg";
	
	/**
	 * [서버 - 프론트]내부 연동시 공통성공여부 키값, 결과코드, 결과메시지
	 */
	public final static String RET_CODE = "ret_code";
	public final static String RET_MSG  = "ret_msg";
	
	/**
	 * 수/발신처(SEND_RECV_PLC) 연동시스템 코드
	 * PH	포인트허브
	 * CS	클립서버
	 * CP	클립포인트
	 * SB	세틀뱅크(PG)
	 * KMC	KMC(본인인증)
	 * GS	기프티쇼
	 * KS	KOS
	 */	
	public final static String PLC_PH  = "PH";
	public final static String PLC_CS  = "CS";
	public final static String PLC_CP  = "CP";
	public final static String PLC_SB  = "SB";
	public final static String PLC_KMC = "KMC";
	public final static String PLC_GS  = "GS";
	public final static String PLC_KOS = "KS";
	
	/**
	 * 거래상태 PH_DEAL_REQ_INFO.DEAL_STAT
	 * 0100 :PG사로부터 거래요청('identification.do')  
	 * 0200 :본인인증 결과 수신시('/kmc/kmcisRes.do')  
	 * 0300 :포인트조회 완료시('/phub/std/point.do') 
	 * 1000 :포인트 선택 후 중간단계
	 * 2000 :결제완료
	 * 3000 :결제요청 후 처리과정에서 오류 발생
	 */	
	public final static String DEAL_STAT_0100  = "0100";
	public final static String DEAL_STAT_0200  = "0200";
	public final static String DEAL_STAT_0300  = "0300";
	public final static String DEAL_STAT_1000  = "1000";
	public final static String DEAL_STAT_2000  = "2000";
	public final static String DEAL_STAT_3000  = "3000";
	
	
	
	/**
	 * ajax request header 값
	 * 해당값이 있을 경우 ajax 통신으로 판단하고, 관련 로직을 처리한다.
	 */	
	public final static String AJAX_HEADER = "PH_AJAX";	
	
	/**
	 * 사용로그 기록
	 */	
	public final static String LOG_WRITE_YN = "LOG_WRITE_YN";
	
	/**
	 * API로그기록 여부(response값 로그기록여부만 제어 한다.)
	 */	
	public final static String API_LOG_WRITE_YN = "logWriteYN";		
	
	/**
	 * Common.calcMath("FLOOR", double, -1)
	 * 포인트 전환율, 가맹점 수수료율 계산 방식을 정의하기 위한 변수
	 * ROUND:반올림, CEIL:올림, FLOOR:버림
	 */		
	public final static String CALC_STR_MODE = "CEIL";
	
	/**
	 * 포인트허브 서비스코드 정의
	 */		
	public final static String SERVICE_ID_SVC_BASE  = "SVC_BASE";
	public final static String SERVICE_ID_SVC_FP    = "SVC_FP";
	
	
	// cpnTypId
	/**
	 * 쿠폰종류 - 추후에 쿠폰종류 테이블 생성되면, 상수해제 고려 필요 - 2019-02-21. lys
	 * FP001 : 패밀리포인트쿠폰1  
	 */
	public final static String CPN_TYP_ID_FP001 = "FP001";
	
	/**
	 * 쿠폰상태 (공통코드: CPN_STAT_IND )
	 * 0000 :쿠폰생성  
	 * 1000 :쿠폰배포  
	 * 2000 :사용가능(사용가능:쿠폰판매완료) 
	 * 2100 :사용취소(사용가능:사용취소)
	 * 4000 :사용완료
	 * 9000 :발행취소(환불)
	 */	
	public final static String CPN_STAT_0000 = "0000";
	public final static String CPN_STAT_1000 = "1000";
	public final static String CPN_STAT_2000 = "2000";
	public final static String CPN_STAT_2100 = "2100";
	public final static String CPN_STAT_4000 = "4000";
	public final static String CPN_STAT_9000 = "9000";
	
	/**
	 * 쿠폰정보변경 유형 (공통코드: CHNG_TYP_CD )
	 * ISSUE  : 발행	
	 * USE    : 쿠폰사용	
	 * CANCEL : 쿠폰사용취소	
	 * REFUND : 쿠폰발행취소(환불)	
	 * ETC    : 기타	
	 */	
	public final static String CHNG_TYP_CD_ISSUE  = "ISSUE";
	public final static String CHNG_TYP_CD_USE    = "USE";
	public final static String CHNG_TYP_CD_CANCEL = "CANCEL";
	public final static String CHNG_TYP_CD_REFUND = "REFUND";
	public final static String CHNG_TYP_CD_ETC    = "ETC";
	
	/**
	 * MMS메시지 유형별 메시지ID의 시스템파라미터ID
	 * MMS_ID_FP_ISU_ALON : 쿠폰발송 MMS (비패밀리고객(KT단독고객))
	 * MMS_ID_FP_ISU_FMLY : 쿠폰발송 MMS (패밀리용)
	 * MMS_ID_FP_ISU_GIFT : 쿠폰 선물 MMS
	 * MMS_ID_FP_CPN_PSTR : 쿠폰 조르기 MMS
	 */	
	public final static String MMS_ID_FP_ISU_ALON  = "MMS_ID_FP_ISU_ALON";
	public final static String MMS_ID_FP_ISU_FMLY  = "MMS_ID_FP_ISU_FMLY";
	public final static String MMS_ID_FP_ISU_GIFT  = "MMS_ID_FP_ISU_GIFT";
	public final static String MMS_ID_FP_CPN_PSTR  = "MMS_ID_FP_CPN_PSTR";
	
	
	/**
	 * 포인트허브 디폴트 등록/갱신자
	 * db 테이블 RGST_USER_ID, MDFY_USER_ID 컬럼의 default 값 
	 */	
	public final static String MDFY_USER_ID_ADMIN = "admin";
	
	/**
	 * 쿠폰 취소 경로 구분
	 * CPN_CNCL_IND_N  : 미취소
	 * CPN_CNCL_IND_Y  : 자동취소
	 * CPN_CNCL_IND_A  : ONM 취소
	 * CPN_CNCL_IND_M  : 망취소
	 * CPN_CNCL_IND_B  : 배치취소
	 * CPN_CNCL_IND_CB : 망취소
	 */	
	public final static String CPN_CNCL_IND_N   = "N";
	public final static String CPN_CNCL_IND_Y   = "Y";
	public final static String CPN_CNCL_IND_A   = "A";
	public final static String CPN_CNCL_IND_M   = "M";
	public final static String CPN_CNCL_IND_B   = "B";
	public final static String CPN_CNCL_IND_CB  = "CB";
	
	/**
	 * 조르기진행상태 (공통코드: CPN_REQ_PRGRS)
	 * PR000	미진행	
	 * PR110	진행중1(본인인증 페이지)	
	 * PR120	진행중2(본인인증 완료)	
	 * PR130	진행중3(포인트조회 페이지)	
	 * PR140	진행중4(결제요청)	
	 * PR200	완료	
	 * PR900	실패(결제실패)	
	 */	
	public final static String CPN_REQ_PRGRS_PR000 = "PR000";
	public final static String CPN_REQ_PRGRS_PR110 = "PR110";
	public final static String CPN_REQ_PRGRS_PR120 = "PR120";
	public final static String CPN_REQ_PRGRS_PR130 = "PR130";
	public final static String CPN_REQ_PRGRS_PR140 = "PR140";
	public final static String CPN_REQ_PRGRS_PR200 = "PR200";
	public final static String CPN_REQ_PRGRS_PR900 = "PR900";
}
