/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.common.dao.CommonDAO;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.SysPrmtInfoVO;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 시스템파라미터 관리 콤포넌트
 * @Class Name : SysPrmtManage
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Component("sysPrmtManage")
public class SysPrmtManage {
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 시스템파라미터맵
	 */	
	private static Map<String, SysPrmtInfoVO> admSysPrmtMap;
	//private static Map<String, Map<String, Object>> admSysPrmtMap;
	
	private List<Map<String, Object>> list;
	private Map<String, Object> map;
	private String delimiter = ";";
	
	@Autowired
	private CommonDAO commonDao;

	
	/**
	 * <pre>요청한 파라미터코드의 prmt_val값을 리턴 한다. (USE_YN=Y 인 건만)</pre>
     *
     * @param  String indCd
     * @return String
     * @see
	 */
	public String getSysPrmtVal(String indCd) {
		String rtn = "";
		SysPrmtInfoVO tmpMap = get(indCd);
		
		if( "Y".equals(StringUtil.nvl(tmpMap.getUseYn(), "N")) ) rtn = tmpMap.getPrmtVal();
		
		return rtn;
	}
	
	/**
	 * <pre>요청한 파라미터코드를 ";" 구분자로 분리하여 String[]을 리턴 한다. (USE_YN=Y 인 건만)</pre>
     *
     * @param  String indCd
     * @return String[]
     * @see
	 */
	public String[] getSysPrmtAry(String indCd) {
		String tmpVal = "";
		String[] rtn = {};
		SysPrmtInfoVO tmpMap = get(indCd);
		
		if( "Y".equals(StringUtil.nvl(tmpMap.getUseYn(), "N")) ) {
			tmpVal = tmpMap.getPrmtVal();
			
			if( tmpVal.indexOf(delimiter) > 0 ) {
				rtn = tmpVal.split(delimiter);
			} else {
				rtn = new String[]{tmpVal};
			}
		}
		
		return rtn;
	}	
	
	/**
	 * <pre>요청한 파라미터코드의 정보를 리턴 한다. (전체)</pre>
     *
     * @param  String indCd
     * @return SysPrmtInfoVO
     * @see
	 */
	public SysPrmtInfoVO getSysPrmtVO(String indCd) {
		return get(indCd);
	}
	
	/**
	 * <pre>요청한 파라미터코드의 정보를 리턴 한다. (USE_YN=Y 인 건만)</pre>
     *
     * @param  String indCd
     * @return SysPrmtInfoVO
     * @see
	 */
	public SysPrmtInfoVO getSysPrmtVOY(String indCd) {
		// declare
		SysPrmtInfoVO map    = new SysPrmtInfoVO();
		SysPrmtInfoVO tmpMap = get(indCd);
		
		// use_yn(사용여부)가 Y인 데이터만 리턴 한다.
		//if( "Y".equals(StringUtil.nvl(tmpMap.get("use_yn"), "N")) ) map = tmpMap;
		if( "Y".equals(StringUtil.nvl(tmpMap.getUseYn(), "N")) ) map = tmpMap;
		
		return map;
	}	
	
//	public List<CmnCdVO> getCodeList(String indCd) {
//		return admCmnCdMap.get(indCd);
//	}
	

	/**
	 * <pre>시스템파라미터 조회</pre>
     *
     * @param  
     * @return Map<String, SysPrmtInfoVO>
     * @see
	 */
	private Map<String, SysPrmtInfoVO> getAllList() {
		String methodName = "getAllList";
		log.debug(methodName, "getAllList() Start!");
		// declare
		Map<String, SysPrmtInfoVO> map = new HashMap<String, SysPrmtInfoVO>();		
		List<SysPrmtInfoVO> list       = new ArrayList<SysPrmtInfoVO>();
		String cd      = "";	// 코드
		
		try {
			// 전체 시스템파라미터 조회
			list = commonDao.getAllSysPrmtList();
			
			// 파라미터코드별로 Map에 해당정보 등록
			for(SysPrmtInfoVO vo : list) {
				//cd = StringUtil.nvl(vo.get("prmt_cd"));
				cd = StringUtil.nvl(vo.getPrmtCd());
				
				map.put(cd, vo);
			}			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		return map;
	}
	
	/**
	 * <pre>admSysPrmtMap 설정</pre>
     *
     * @param  
     * @return 
     * @see
     *  1. 전체 시스템파라미터(ADM_SYSPRMT_INFO)를 db에서 읽어서 admSysPrmtMap에 저장 한다.
	 */	
	@PostConstruct
	public void start() {
		// 시스템파라미터 정보 메모리 로드
		admSysPrmtMap = getAllList();
	}
	
	/**
	 * <pre>admSysPrmtMap 재설정</pre>
     *
     * @param  
     * @return 
     * @see
	 */
	public void reset() {
		admSysPrmtMap = getAllList();
	}
	
	/**
	 * <pre>요청한 파라미터코드의 SysPrmtInfoVO를 리턴 한다.</pre>
     *
     * @param  String indCd
     * @return SysPrmtInfoVO
     * @see
	 */	
	private SysPrmtInfoVO get(String indCd) {
		SysPrmtInfoVO tmpMap = admSysPrmtMap.get(indCd);
		
		if( tmpMap == null ) tmpMap = new SysPrmtInfoVO();
		
		return tmpMap;
	}
}