/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.components;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.NameMatchMethodPointcut;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.utils.JsonUtil;

/**
 * 비즈니스 로직과는 별개의 유틸리티성 기능을 정의한다.
 * @Class Name : UtilsComponent
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Component
public class UtilsComponent  {
	private Logger log = new Logger(this.getClass());
	
	
	/**
	 * <pre>ajax 통신시 메소드에서 jsonObject 리턴할때의 공통적인 후처리를 기술 한다.</pre>
     *
	 * @param request		HttpServletRequest
	 * @param jsonObject	리턴 jsonObject
	 * @param isFlag        request set 여부 (true일 경우에만 request에 set 한다.)
     * @return Map<String, List<String>>
     * @see
     *  1. 공통 키등을 추가처리 할 수 있다.
	 */
	public String afterResMap(HttpServletRequest request, Map<String, Object> jsonObject, boolean isFlag) throws Exception {
		
		if( request == null ) {
			log.debug("afterResponseBody", "request is Null.");
		}
		
		if( jsonObject == null ) {
			log.debug("afterResponseBody", "jsonObject is Null.");
		}		
		
		// 사용로그 처리를 위하여 request에 jsonObject를 set 한다.
		if( isFlag ) {
			request.setAttribute(Constant.JSON_OBJECT, jsonObject.get(Constant.RET_CODE));
		}
		//return jsonObject;
		return JsonUtil.MapToJson(jsonObject);
	}
	public String afterResMap(HttpServletRequest request, Map<String, Object> jsonObject) throws Exception {
		
		//return jsonObject;
		return afterResMap(request, jsonObject, false);
	}
	
	
	/**
	 * <pre>ProxyFactoryBean 을 사용하여 target object 에 부가 기능을 추가 처리 할때 사용한다.</pre>
	 * @param target 대상 오브젝트
	 * @param advice 부가 기능을 구현한 오브젝트
	 * @param mappedPattern 부가 기능이 적용될 메소드 이름 지정( "*" 사용가능 ) 
	 * @return 부가 기능이 적용된 대상 오브젝트
	 * @see
	 */
	public Object getPFBobjec(Object target, MethodInterceptor advice, String[] mappedPattern) {
		ProxyFactoryBean proxyFactoryBean = new ProxyFactoryBean();
		proxyFactoryBean.setProxyTargetClass(true);	//CGLIB 사용해서 인터페이스 없는 오브젝트도 위임 처리 할수 있도록 설정
													//CGLIB 사용시 target object 는 public 생성자를 가져야 한다.
		NameMatchMethodPointcut pointcut = new NameMatchMethodPointcut();

		proxyFactoryBean.setTarget(target);
		pointcut.setMappedNames(mappedPattern);
		proxyFactoryBean.addAdvisor(new DefaultPointcutAdvisor(pointcut, advice));

		return proxyFactoryBean.getObject();
	}
}
