/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.common.dao.CommonDAO;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.ActionInfoVO;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * url관리 콤포넌트
 * @Class Name : UrlManage
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Component
public class UrlManage {

	//urlMap.put("/admin/svc/managerGroup/list.hnc", setAuthMap("managerGroup", SRCH, usrLogY));
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 시스템Uri맵
	 */
	private static Map<String, ActionInfoVO> admUriMap;
	//private static Map<String, Map<String, Object>> admUriMap;
	
	@Autowired
	private CommonDAO commonDao;
	
	

	/**
	 * <pre>요청한 uri 객체를 리턴 한다. (전체)</pre>
     *
     * @param  String uri
     * @return ActionInfoVO
     * @see
	 */	
	public ActionInfoVO getSysMsg(String uri) {
		return get(uri);
	}	
//	public List<CmnCdVO> getCodeList(String indCd) {
//		return admCmnCdMap.get(indCd);
//	}
	
	
	/**
	 * <pre>uri 정보 조회</pre>
     *
     * @param  
     * @return Map<String, ActionInfoVO>
     * @see
	 */
	private Map<String, ActionInfoVO> getAllList() {
		String methodName = "getAllList";
		log.debug(methodName, "getAllList() Start!");
		// declare
		Map<String, ActionInfoVO> map = new HashMap<String, ActionInfoVO>();		
		List<ActionInfoVO> list       = new ArrayList<ActionInfoVO>();
		String cd      = "";	// 코드
		
		try {
			// 전체 uri 조회
			list = commonDao.getAllUriList();
			
			// uri별로 Map에 해당정보 등록
			for(ActionInfoVO vo : list) {
				//cd = StringUtil.nvl(vo.get("uri"));
				cd = StringUtil.nvl(vo.getUri());
				
				map.put(cd, vo);
			}			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		return map;
	}
	
	/**
	 * <pre>admUriMap 설정</pre>
	 * 
     * @param  
     * @return 
     * @see
     *  1. 전체 시스템 Uri정보(ADM_ACTION_INFO)를 db에서 읽어서 admUriMap에 저장 한다.
	 */	
	@PostConstruct
	public void start() {
		// 시스템Uri정보 메모리 로드
		admUriMap = getAllList();
	}
	
	/**
	 * <pre>시스템Uri정보 메모리 리로드</pre>
     *
     * @param  
     * @return 
     * @see
	 */
	public void reset() {
		admUriMap = getAllList();
	}
	
	/**
	 * <pre>요청한 uri의 ActionInfoVO를 리턴 한다.</pre>
     *
     * @param  String uri
     * @return ActionInfoVO
     * @see
	 */	
	private ActionInfoVO get(String uri) {
		ActionInfoVO tmpMap = admUriMap.get(uri);
		
		if( tmpMap == null ) tmpMap = new ActionInfoVO();
		
		return tmpMap;
	}
}