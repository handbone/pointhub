/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.common.dao.CommonDAO;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.CmnCdVO;
import com.olleh.pointHub.common.model.PntPrvdrMsgVO;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 클립포인트 결과코드 조회 컴포넌트
 * @Class Name : PntPrvdrMsgManage
 * @author lys
 * @since 2018.11.08
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.08   lys        최초생성
 * </pre>
 */
@Component
public class PntPrvdrMsgManage {
	//private org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 공통코드맵
	 */	
	//private static Map<String, List<PntPrvdrMsgVO>> admPntPrvdrMsgMap;
	private static Map<String, Map<String, List<PntPrvdrMsgVO>>> admPntPrvdrMsgMap;
	
	private List<Map<String, Object>> list;
	private Map<String, Object> map;
	
	@Autowired
	private CommonDAO commonDao;
	
	/**
	 * <pre>요청한 prvdrId, PntCd 해당하는 list를 리턴 한다.</pre>
     *
     * @param  String prvdrId
     * @param  String PntCd
     * @return List<PntPrvdrMsgVO>
     * @see
	 */
	public List<PntPrvdrMsgVO> getPntMsgList(String prvdrId, String PntCd) {
		Map<String, List<PntPrvdrMsgVO>> map = get(prvdrId);
		List<PntPrvdrMsgVO> list = map.get(PntCd);
		
		if( list == null ) list = new ArrayList<PntPrvdrMsgVO>();
		
		return list;
	}
	
	/**
	 * <pre>요청한 prvdrId, PntCd, msgId에 해당하는 vo를 리턴 한다.</pre>
     *
     * @param  String prvdrId
     * @param  String PntCd
     * @param  String msgId
     * @return PntPrvdrMsgVO
     * @see
	 */
	public PntPrvdrMsgVO getPntMsgIdVO(String prvdrId, String PntCd, String msgId) {
		// declare
		PntPrvdrMsgVO rtnVo = new PntPrvdrMsgVO();
		
		// 정보 조회
		List<PntPrvdrMsgVO> list = getPntMsgList(prvdrId, PntCd);
		
		// msgId에 해당하는 vo검색
		for(PntPrvdrMsgVO vo : list) {
			if( msgId.equals(vo.getMsgId()) ) {
				rtnVo = vo;
			} 
		}
		
		// test log
		/**
		log.debug("getPntMsgIdVO", "admPntPrvdrMsgMap.size="+admPntPrvdrMsgMap.size());
		
		for(String key : admPntPrvdrMsgMap.keySet()) {
			//
			Map<String, List<PntPrvdrMsgVO>> tmpMap = admPntPrvdrMsgMap.get(key);
			log.debug("getPntMsgIdVO", key+"="+tmpMap.size());
			
			for(String key2 : tmpMap.keySet()) {
				//
				List<PntPrvdrMsgVO> list2 = tmpMap.get(key2);
				log.debug("getPntMsgIdVO", key2+"="+list2.size());
			}
		}
		*/
		
		return rtnVo;
	}

	
	/**
	 * <pre>코드값 조회</pre>
     *
     * @param  
     * @return 
     * @see
	 */
	private Map<String, Map<String, List<PntPrvdrMsgVO>>> getAllList() {
		String methodName = "getAllList";
		log.debug(methodName, "getAllList() Start!");
		// declare
		Map<String, Map<String, List<PntPrvdrMsgVO>>> prvdrMap = new HashMap<String, Map<String, List<PntPrvdrMsgVO>>>();
		//Map<String, Map> pntCdMap = new HashMap<String, Map>();
		
		Map<String, List<PntPrvdrMsgVO>> map = new HashMap<String, List<PntPrvdrMsgVO>>();
		List<PntPrvdrMsgVO> tmpList          = new ArrayList<PntPrvdrMsgVO>();		
		List<PntPrvdrMsgVO> list             = new ArrayList<PntPrvdrMsgVO>();
		
		String prvdrId    = "";	// 제공처ID
		String tmpPrvdrId = "";	// tmp제공처ID
		
		String pntCd    = "";	// 제공처연동코드
		String tmpPntCd = "";	// tmp제공처연동코드
		
		
		try {
			// 전체 정보 조회
			list = commonDao.getAllPntPrvdrMsgInfoList();
			
			// Map에 정보 등록
			for(PntPrvdrMsgVO vo : list) {
				// set
				tmpPrvdrId = StringUtil.nvl(vo.getPrvdrId());
				tmpPntCd   = StringUtil.nvl(vo.getPntCd());
				
				// 제공처ID별로 셋팅
				if( prvdrId.equals(tmpPrvdrId) ) {
					// 제공처연동 코드별로 셋팅
					if( !pntCd.equals(tmpPntCd) ) {
						if( tmpList.size() > 0 ) map.put(pntCd, new ArrayList<PntPrvdrMsgVO>(tmpList));
						pntCd = tmpPntCd;
						tmpList.clear();
					}
					
				} else {
					// 제공처연동 코드별로 셋팅
					if( tmpList.size() > 0 ) map.put(pntCd, new ArrayList<PntPrvdrMsgVO>(tmpList));
					pntCd = tmpPntCd;
					tmpList.clear();
					
					// 제공처ID별로 셋팅
					if( map.size() > 0 ) prvdrMap.put(prvdrId, new HashMap<String, List<PntPrvdrMsgVO>>(map));
					prvdrId = tmpPrvdrId;
					map.clear();					
				}
				
				// vo 기록
				tmpList.add(vo);
			}
			
			// 마지막건 처리
			if( tmpList.size() > 0 ) map.put(pntCd, new ArrayList<PntPrvdrMsgVO>(tmpList));
			pntCd = tmpPntCd;
			tmpList.clear();
			
			if( map.size() > 0 ) prvdrMap.put(prvdrId, new HashMap<String, List<PntPrvdrMsgVO>>(map));
			prvdrId = tmpPrvdrId;
			map.clear();			
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}		

		return prvdrMap;
	}

	/**
	 * <pre>admPntPrvdrMsgMap 설정</pre>
     *
     * @param  
     * @return 
     * @see <pre>
     *  1. ADM_PNT_PRVDR_MSG를 db에서 읽어서 admPntPrvdrMsgMap에 저장 한다.
     *  </pre>
	 */	
	@PostConstruct
	public void start() {
		// 메모리 로드
		admPntPrvdrMsgMap = getAllList();
	}
	
	/**
	 * <pre>Map<String, Object> 설정</pre>
     *
     * @param
     * @return 
     * @see
	 */
	public void reset() {
		admPntPrvdrMsgMap = getAllList();
	}
	
	/**
	 * <pre>요청한 제공처별의 객체를 리턴 한다.</pre>
     *
     * @param  String prvdrId
     * @return Map<String, List<PntPrvdrMsgVO>>
     * @see
	 */	
	private Map<String, List<PntPrvdrMsgVO>> get(String prvdrId) {
		Map<String, List<PntPrvdrMsgVO>> map = admPntPrvdrMsgMap.get(prvdrId);
		
		if( map == null ) map = new HashMap<String, List<PntPrvdrMsgVO>>();
		
		return map;
	}
}