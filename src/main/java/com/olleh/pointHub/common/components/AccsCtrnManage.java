/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.common.dao.CommonDAO;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.AccsCtrnInfoVO;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 접근제어  관리 콤포넌트
 * @Class Name : AccsCtrnManage
 * @author lys
 * @since 2018.08.31
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.31   lys        최초생성
 * </pre>
 */
@Component
public class AccsCtrnManage {
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 접급제어맵
	 */	
	private static Map<String, List<AccsCtrnInfoVO>> accsCtrnMap;
	
	@Autowired
	private CommonDAO commonDao;

	
	/**
	 * <pre>요청한 관계사ID에 해당하는 접근제어 정보를 리턴 한다.</pre>
     *
     * @param  String rltnCorpId
     * @return AccsCtrnInfoVO
     * @see
	 */
	public List<AccsCtrnInfoVO> getAccsCtrnInfoVO(String rltnCorpId) {
		return get(rltnCorpId);
	}
	
	/**
	 * <pre>요청한 관계사ID, IP가 등록된 정보인지 체크해서 결과를 boolean 형으로 리턴 한다.</pre>
     *
     * @param  String pgCmpnId
     * @return String
     * @see
     *     1. 등록됐으면 true
     *     2. 미등록건이면 false
	 */
	public boolean isAccsIp(String pgCmpnId, String ipAddr) {
		int isCnt = 0;
		
		// 관계사IP정보 조회
		List<AccsCtrnInfoVO> list = get(pgCmpnId);
		
		// 등록된 IP인지 체크
		for(AccsCtrnInfoVO vo : list) {
			if( ipAddr.equals(vo.getIpAddr()) ) {
				isCnt++;
			}
		}
		
		// isCnt가 0이상이면 true
		return (isCnt > 0)?true:false;
	}

	
	/**
	 * <pre>
	 * 요청한 관계사ID의 인증키와 파라메터 kosAuth와 비교
	 * </pre>
	 * @param cprtCmpnId
	 * @param kosAuth
	 * @return
	 */
	public boolean isAccsAuth(String cprtCmpnId, String kosAuth) {
		
		int isCnt = 0;
		
		// 관계사 정보 조회
		List<AccsCtrnInfoVO> list = get(cprtCmpnId);
		
		// 등록된 인증키인지 체크
		for(AccsCtrnInfoVO vo : list){
			
			if(StringUtils.equals(kosAuth, vo.getAthnKey())){
				
				isCnt = isCnt + 1;
			}
		}
		
		// isCnt가 0이상이면 true
		return (isCnt > 0) ? true : false;
	}
	
	
	/**
	 * <pre>접근제어 정보 조회</pre>
     *
     * @param  
     * @return Map<String, SysPrmtInfoVO>
     * @see
	 */
	private Map<String, List<AccsCtrnInfoVO>> getAllList() {
		String methodName = "getAllList";
		log.debug(methodName, "getAllList() Start!");
		
		// declare
		Map<String, List<AccsCtrnInfoVO>> map = new HashMap<String, List<AccsCtrnInfoVO>>();
		List<AccsCtrnInfoVO> tmpList          = new ArrayList<AccsCtrnInfoVO>();
		List<AccsCtrnInfoVO> list             = new ArrayList<AccsCtrnInfoVO>();
		
		String cd = "";		// 코드
		String tmpCd = "";	// tmp코드
		
		try {
			// 전체 접근제어정보 조회
			list = commonDao.getAllAccsCtrnInfoList();
			
			// 마스터코드별로 Map에 해당디테일코드List를 등록
			for(AccsCtrnInfoVO vo : list) {
				tmpCd = StringUtil.nvl(vo.getRltnCorpId());
				
				if( !cd.equals(tmpCd) ) {
					if( tmpList.size() > 0 ) map.put(cd, new ArrayList<AccsCtrnInfoVO>(tmpList));
					cd = tmpCd;
					tmpList.clear();
				}
				
				tmpList.add(vo);
			}
			
			// 마지막건 처리
			if( tmpList.size() > 0 ) {
				map.put(cd, new ArrayList<AccsCtrnInfoVO>(tmpList));
			}			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		return map;
	}
	
	/**
	 * <pre>accsCtrnMap 설정</pre>
     *
     * @param  
     * @return 
     * @see
     *  1. 전체 접근제어정보(ADM_ACCS_CTRL_D, ADM_ACCS_CTRL_M)를 db에서 읽어서 accsCtrnMap에 저장 한다.
	 */	
	@PostConstruct
	public void start() {
		// 시스템파라미터 정보 메모리 로드
		accsCtrnMap = getAllList();
	}
	
	/**
	 * <pre>accsCtrnMap 재설정</pre>
     *
     * @param  
     * @return 
     * @see
	 */
	public void reset() {
		accsCtrnMap = getAllList();
	}
	
	/**
	 * <pre>요청한 관계사ID의 AccsCtrnInfoVO를 리턴 한다.</pre>
     *
     * @param  String rltnCorpId
     * @return AccsCtrnInfoVO
     * @see
	 */	
	private List<AccsCtrnInfoVO> get(String rltnCorpId) {
		List<AccsCtrnInfoVO> tmpMap = accsCtrnMap.get(rltnCorpId);
		
		if( tmpMap == null ) tmpMap = new ArrayList<AccsCtrnInfoVO>();
		
		return tmpMap;
	}

}