/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pointHub.common.dao.CommonDAO;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.PgCmpnInfoVO;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * PG사정보  관리 콤포넌트
 * @Class Name : PgCmpnInfoManage
 * @author lys
 * @since 2018.08.31
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.31   lys        최초생성
 * </pre>
 */
@Component
public class PgCmpnInfoManage {
	private Logger log = new Logger(this.getClass());
	
	/**
	 * PG사정보맵
	 */	
	private static Map<String, PgCmpnInfoVO> pgCmpnInfoMap;
	
	@Autowired
	private CommonDAO commonDao;

	
	/**
	 * <pre>요청한 PG사ID에 해당하는 PG사 정보를 리턴 한다.</pre>
     *
     * @param  String pgCmpnId
     * @return PgCmpnInfoVO
     * @see
	 */
	public PgCmpnInfoVO getPgCmpnInfoVO(String pgCmpnId) {
		return get(pgCmpnId);
	}
	
	/**
	 * <pre>요청한 PG사ID에 해당하는 PG사 인증키정보를 리턴 한다.</pre>
     *
     * @param  String pgCmpnId
     * @return String
     * @see
	 */
	public String getPgAthnKey(String pgCmpnId) {
		return get(pgCmpnId).getAthnKey();
	}

	/**
	 * <pre>PG사 정보 조회</pre>
     *
     * @param  
     * @return Map<String, PgCmpnInfoVO>
     * @see
	 */
	private Map<String, PgCmpnInfoVO> getAllList() {
		String methodName = "getAllList";
		log.debug(methodName, "getAllList() Start!");
		// declare
		Map<String, PgCmpnInfoVO> map = new HashMap<String, PgCmpnInfoVO>();		
		List<PgCmpnInfoVO> list       = new ArrayList<PgCmpnInfoVO>();
		String cd = "";	// 코드
		
		try {
			// 전체 PG사정보 조회
			list = commonDao.getAllPgCmpnInfoList();
			
			// PG사별로 Map에 해당정보 등록
			for(PgCmpnInfoVO vo : list) {
				//cd = StringUtil.nvl(vo.get("prmt_cd"));
				cd = StringUtil.nvl(vo.getPgCmpnId());
				
				map.put(cd, vo);
			}			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		return map;
	}
	
	/**
	 * <pre>pgCmpnInfoMap 설정</pre>
     *
     * @param  
     * @return 
     * @see
     *  1. 전체 PG사 정보(PH_PG_CMPN_INFO)를 db에서 읽어서 pgCmpnInfoMap에 저장 한다.
	 */	
	@PostConstruct
	public void start() {
		// PG사정보 정보 메모리 로드
		pgCmpnInfoMap = getAllList();
	}
	
	/**
	 * <pre>pgCmpnInfoMap 재설정</pre>
     *
     * @param  
     * @return 
     * @see
	 */
	public void reset() {
		pgCmpnInfoMap = getAllList();
	}
	
	/**
	 * <pre>요청한 PG사ID의 PgCmpnInfoVO를 리턴 한다.</pre>
     *
     * @param  String indCd
     * @return SysPrmtInfoVO
     * @see
	 */	
	private PgCmpnInfoVO get(String pgCmpnId) {
		PgCmpnInfoVO tmpMap = pgCmpnInfoMap.get(pgCmpnId);
		
		if( tmpMap == null ) tmpMap = new PgCmpnInfoVO();
		
		return tmpMap;
	}
}