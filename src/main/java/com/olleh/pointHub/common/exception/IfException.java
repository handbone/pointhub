/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        처리가능한 예외 정의 클래스 
 */
package com.olleh.pointHub.common.exception;

import com.olleh.pointHub.common.model.MsgInfoVO;
import com.olleh.pointHub.common.utils.DateUtils;

/**
 * I/F 영역 Exception
 * @Class Name : IfException
 * @author : lys
 * @since : 2018.08.15
 * @version : 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.15   lys        최초생성
 * </pre>
 */
public class IfException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3000058036652370974L;

	private String datePattern = "yyyy-MM-dd HH:mm:ss";
	
	private String time = "";
	private String className = "";
	private String methodName = "";
	private String stepCd = "";
	private String errCd = "";
	private String errMsg = "";
	private String pageURL = "";
	private Object paramObj = "";
	private java.lang.Exception ex = null;
    
	/**
     * IterfaceException
     */
    public IfException() {
        super();
        this.time = DateUtils.getDateTimeByPattern(datePattern);   
    }    
    
    /**
     * IterfaceException
     * @param e     예외
     */
    public IfException(java.lang.Exception e) {
    	super(e);
    	this.time = DateUtils.getDateTimeByPattern(datePattern);
        this.ex = e ;
    }
    
    /**
     * IterfaceException
     * @param stepCd 스텝코드  
     * @param e     예외
     */
    public IfException(String stepCd, java.lang.Exception e) {
    	super(e);
    	this.time = DateUtils.getDateTimeByPattern(datePattern);
        this.stepCd = stepCd;
        this.ex = e ;
    }    
    
    /**
     * IterfaceException 
     * @param stepCd 스텝코드
     */
    public IfException(String stepCd) {
    	super();
    	this.time = DateUtils.getDateTimeByPattern(datePattern);    	
        this.stepCd = stepCd;
    }
    
    /**
     * BusinessException 
     * @param stepCd 스텝코드
     */
    public IfException(String stepCd, MsgInfoVO msgInfoVO) {
    	super();
    	this.time   = DateUtils.getDateTimeByPattern(datePattern);    	
        this.stepCd = stepCd;
    	this.errCd  = msgInfoVO.getMsgCd();
    	this.errMsg = msgInfoVO.getMsgNm();        
    }
    
    /**
     * IterfaceException 
     * @param stepCd 스텝코드
     * @param errCd 에러코드
     */
    public IfException(String stepCd, String errCd) {
    	super();
    	this.time = DateUtils.getDateTimeByPattern(datePattern);
    	this.stepCd = stepCd;
        this.errCd = errCd;
    }
    
    public IfException(String stepCd, String errCd, String errMsg) {
    	super();
    	this.time = DateUtils.getDateTimeByPattern(datePattern);
    	this.stepCd = stepCd;
    	this.errCd = errCd;
    	this.errMsg = errMsg;
    }
    
    public IfException(String calssName, String methodName, String stepCd, String errCd, String errMsg) {
    	super();
    	this.time = DateUtils.getDateTimeByPattern(datePattern);    	
    	this.className = calssName;
    	this.methodName = methodName;
    	this.stepCd = stepCd;
    	this.errCd = errCd;
    	this.errMsg = errMsg;
    }
    
    /**
     * IterfaceException
     * @param stepCd 스텝코드
     * @param errCd 에러코드
     * @param e     예외
     */
    public IfException(String stepCd, String errCd, java.lang.Exception e) {
    	super(e);
        this.time = DateUtils.getDateTimeByPattern(datePattern);
        this.stepCd = stepCd;
        this.errCd = errCd;
        this.ex = e ;
    }
    
    /**
     * IterfaceException 
     * @param className 클래스명
     * @param methodName 메소드명
     * @param stepCd 스텝코드
     * @param errCd 에러코드   
     * @param pageURL 페이지 URL
     * @param paramObj 파라미터OBJ   
     */
    public IfException(String className, String methodName, 
    		String stepCd, String errCd, String pageURL, Object paramObj) {
    	super();
        this.time = DateUtils.getDateTimeByPattern(datePattern);
        this.className = className;
        this.methodName = methodName;
        this.stepCd = stepCd;
        this.errCd = errCd;
        this.pageURL = pageURL;
        this.paramObj = paramObj;
    }
    
    /**
     * IterfaceException 
     * @param className 클래스명
     * @param methodName 메소드명
     * @param stepCd 스텝코드
     * @param errId 에러아이디   
     * @param pageURL 페이지 URL
     * @param paramObj 파라미터OBJ   
     * @param e     예외
     */
    public IfException(String className, String methodName, 
    		String stepCd, String errCd, String pageURL, Object paramObj, java.lang.Exception e) {
    	super(e);
    	this.time = DateUtils.getDateTimeByPattern(datePattern);   
        this.className = className;
        this.methodName = methodName;
        this.stepCd = stepCd;
        this.errCd = errCd;
        this.pageURL = pageURL;
        this.paramObj = paramObj;
        this.ex = e ;
    }    
    
	/**
	 * 예외 취득
	 * 
	 * @return java.lang.Exception 예외
	 */
	public java.lang.Exception getException() {
		return ex;
	}
	/**
	 * 스텝코드 취득
	 * 
	 * @return String 스텝코드
	 */
	public String getStepCd() {
		return stepCd;
	}	
	/**
	 * 에러코드 취득
	 * 
	 * @return String 에러코드
	 */
	public String getErrCd() {
		return errCd;
	}
	/**
	 * 에러메세지 취득
	 * 
	 * @return java.lang.String 에러메세지
	 */
	public java.lang.String getErrMsg() {
		return errMsg;
	}
	/**
	 * 에러 발생 시간 취득
	 * 
	 * @return java.lang.String 에러 발생 시간
	 */
	public java.lang.String getTime() {
		return time;
	}
	/**
	 * 클래스명 취득
	 * 
	 * @return java.lang.String 클래스명
	 */
	public java.lang.String getClassName() {
		return className;
	}
	/**
	 * 메소드명 취득
	 * 
	 * @return java.lang.String 메소드명
	 */
	public java.lang.String getMethodName() {
		return methodName;
	}
	
	/**
	 * 페이지 URL 취득
	 * 
	 * @return java.lang.String 페이지 URL
	 */
	public java.lang.String getPageURL() {
		return pageURL;
	}
	/**
	 * 파라미터OBJ  취득
	 * 
	 * @return Object 파라미터OBJ
	 */
	public Object getParmObj() {
		return paramObj;
	}
}