/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.AccsCtrnInfoVO;
import com.olleh.pointHub.common.model.ActionInfoVO;
import com.olleh.pointHub.common.model.CmnCdVO;
import com.olleh.pointHub.common.model.MsgInfoVO;
import com.olleh.pointHub.common.model.PgCmpnInfoVO;
import com.olleh.pointHub.common.model.PntPrvdrMsgVO;
import com.olleh.pointHub.common.model.SysPrmtInfoVO;
import com.olleh.pointHub.common.utils.JsonUtil;

/**
 * 공통적인 DB 처리 로직을 정의한다.
 * @Class Name : CommonDAO
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Repository("commonDAO")
public class CommonDAO extends AbstractDAO {
	private Logger log = new Logger(this.getClass());
	
		
	/**
	 * <pre>전체 공통코드 정보를 리턴 한다. (was 재기동시나, was 메모리 리로드시에 사용)</pre>
	 * 
	 * @param 
	 * @return 공통코드
	 * @see
	 */
	public List<CmnCdVO> getAllCodeList() {
		log.debug("getAllCodeList", "CommonDAO.getAllCodeList() Start.");
		//List<Map> list = (List<Map>) selectList("mybatis.common.getAllCodeList", hm);
		List<CmnCdVO> list = (List<CmnCdVO>) selectList("mybatis.common.getAllCodeList");
		log.debug("getAllCodeList", "CommonDAO.getAllCodeList()=" + JsonUtil.toJson(list));

		return list;
	}
	
	/**
	 * <pre>전체 시스템파라미터 정보를 리턴 한다. (was 재기동시나, was 메모리 리로드시에 사용)</pre>
	 * 
	 * @param 
	 * @return 시스템파라미터
	 * @see
	 */
	public List<SysPrmtInfoVO> getAllSysPrmtList() {
		log.debug("getAllSysPrmtList", "CommonDAO.getAllSysPrmtList() Start.");
		List<SysPrmtInfoVO> list = (List<SysPrmtInfoVO>) selectList("mybatis.common.getAllSysPrmtList");
		log.debug("getAllSysPrmtList", "CommonDAO.getAllSysPrmtList()=" + JsonUtil.toJson(list));

		return list;
	}
	
	/**
	 * <pre>전체 메시지 정보를 리턴 한다. (was 재기동시나, was 메모리 리로드시에 사용)</pre>
	 * 
	 * @param 
	 * @return 메시지
	 * @see
	 */
	public List<MsgInfoVO> getAllMsgList() {
		log.debug("getAllMsgList", "CommonDAO.getAllMsgList() Start.");
		List<MsgInfoVO> list = (List<MsgInfoVO>) selectList("mybatis.common.getAllMsgList");
		log.debug("getAllMsgList", "CommonDAO.getAllMsgList()=" + JsonUtil.toJson(list));

		return list;
	}
	
	/**
	 * <pre>전체 uri 정보를 리턴 한다. (was 재기동시나, was 메모리 리로드시에 사용)</pre>
	 * 
	 * @param 
	 * @return uri
	 * @see
	 */
	public List<ActionInfoVO> getAllUriList() {
		log.debug("getAllUriList", "CommonDAO.getAllUriList() Start.");
		List<ActionInfoVO> list = (List<ActionInfoVO>) selectList("mybatis.common.getAllUriList");
		log.debug("getAllUriList", "CommonDAO.getAllUriList()=" + JsonUtil.toJson(list));

		return list;
	}
	
	/**
	 * <pre>전체 PG사 정보를 리턴 한다. (was 재기동시나, was 메모리 리로드시에 사용)</pre>
	 * 
	 * @param 
	 * @return uri
	 * @see
	 */
	public List<PgCmpnInfoVO> getAllPgCmpnInfoList() {
		log.debug("getAllPgCmpnInfoList", "CommonDAO.getAllPgCmpnInfoList() Start.");
		List<PgCmpnInfoVO> list = (List<PgCmpnInfoVO>) selectList("mybatis.common.getAllPgCmpnInfoList");
		log.debug("getAllPgCmpnInfoList", "CommonDAO.getAllPgCmpnInfoList()=" + JsonUtil.toJson(list));

		return list;
	}
	
	/**
	 * <pre>전체 접근제어 정보를 리턴 한다. (was 재기동시나, was 메모리 리로드시에 사용)</pre>
	 * 
	 * @param 
	 * @return uri
	 * @see
	 */
	public List<AccsCtrnInfoVO> getAllAccsCtrnInfoList() {
		log.debug("getAllAccsCtrnInfoList", "CommonDAO.getAllAccsCtrnInfoList() Start.");
		List<AccsCtrnInfoVO> list = (List<AccsCtrnInfoVO>) selectList("mybatis.common.getAllAccsCtrnInfoList");
		log.debug("getAllAccsCtrnInfoList", "CommonDAO.getAllAccsCtrnInfoList()=" + JsonUtil.toJson(list));

		return list;
	}
	
	/**
	 * <pre>클립포인트 결과코드 정보 조회 (was 재기동시나, was 메모리 리로드시에 사용)</pre>
	 * 
	 * @param 
	 * @return uri
	 * @see
	 */
	public List<PntPrvdrMsgVO> getAllPntPrvdrMsgInfoList() {
		log.debug("getAllPntPrvdrMsgInfoList", "CommonDAO.getAllPntPrvdrMsgInfoList() Start.");
		List<PntPrvdrMsgVO> list = (List<PntPrvdrMsgVO>) selectList("mybatis.common.getAllPntPrvdrMsgInfoList");
		log.debug("getAllPntPrvdrMsgInfoList", "CommonDAO.getAllPntPrvdrMsgInfoList()=" + JsonUtil.toJson(list));

		return list;
	}	
	
	/**
	 * <pre>시스템파라미터 사용여부가 Y인 정보만 리턴 한다. (파라미터코드 값 필수)</pre>
	 * 
	 * @param  Map params
	 * @return 시스템파라미터
	 * @see
	 */
	public SysPrmtInfoVO getSysPrmtY(Map params) {
		//return (SysPrmtInfoVO)selectOne("mybatis.common.getSysPrmtY", params);
		return sqlSession.selectOne("mybatis.common.getSysPrmtY", params);
	}
	
	/**
	 * <pre>시스템파라미터 value값을 update 한다. (파라미터코드, value, userId 값 필수)</pre>
	 * 
	 * @param  Map params
	 * @return 시스템파라미터
	 * @see
	 */
	public int updateSysprmtVal(Map params) {
		return update("mybatis.common.updateSysprmtVal", params);
	}
}