/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.olleh.pointHub.common.utils.JsonUtil;

/**
 * 공통적인 DB 처리 기본로직을 정의
 * @Class Name : AbstractDAO
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class AbstractDAO {
	// private org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
    
	@Autowired
	public SqlSessionTemplate sqlSession;
	
	public String KEY_SEPARATOR = "_";
	public String VAL_SEPARATOR = "_";
	
	
	/**
	 * <pre>DB insert</pre>
     *
     * @param  String queryId
     * @param  Object params
     * @return int
     * @see
	 */	
    public int insert(String queryId, Object params){
        return sqlSession.insert(queryId, params);
    }

	/**
	 * <pre>DB update</pre>
     *
     * @param  String queryId
     * @param  Object params
     * @return int
     * @see
	 */	    
    public int update(String queryId, Object params){
        return sqlSession.update(queryId, params);
    }
    
	/**
	 * <pre>DB delete</pre>
     *
     * @param  String queryId
     * @param  Object params
     * @return int
     * @see
	 */	    
    public int delete(String queryId, Object params){
        return sqlSession.delete(queryId, params);
    }
    
	/**
	 * <pre>DB select one</pre>
     *
     * @param  String queryId
     * @return Map
     * @see
	 */	    
    public Map selectOne(String queryId){
        return sqlSession.selectOne(queryId);
    }
    
	/**
	 * <pre>DB select one</pre>
     *
     * @param  String queryId
     * @return Map
     * @see
	 */	    
    public Map selectOne(String queryId, Object params){
        return sqlSession.selectOne(queryId, params);
    }
    
	/**
	 * <pre>DB select list</pre>
     *
     * @param  String queryId
     * @return List
     * @see
	 */	    
    public List selectList(String queryId){
        return sqlSession.selectList(queryId);
    }
    
	/**
	 * <pre>DB select list</pre>
     *
     * @param  String queryId
     * @param  Object params
     * @return List
     * @see
	 */	    
    public List selectList(String queryId, Object params){
        return sqlSession.selectList(queryId,params);
    }
}