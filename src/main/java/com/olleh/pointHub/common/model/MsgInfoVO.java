/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.model;

/**
 * 메시지정보 vo
 * @Class Name : MsgInfoVO
 * @author lys
 * @since 2018.07.16
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class MsgInfoVO {

	private String msgId;		// 메시지ID
	private String msgCd;		// 메시지코드
	private String msgGrp;		// 메시지그룹
	private String msgNm;		// 메시지명
	private String useYn;		// 사용여부
	private String msgInd;		// 메시지구분
	private String remark;		// 비고
	
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getMsgCd() {
		return msgCd;
	}
	public void setMsgCd(String msgCd) {
		this.msgCd = msgCd;
	}
	public String getMsgGrp() {
		return msgGrp;
	}
	public void setMsgGrp(String msgGrp) {
		this.msgGrp = msgGrp;
	}
	public String getMsgNm() {
		return msgNm;
	}
	public void setMsgNm(String msgNm) {
		this.msgNm = msgNm;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getMsgInd() {
		return msgInd;
	}
	public void setMsgInd(String msgInd) {
		this.msgInd = msgInd;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
