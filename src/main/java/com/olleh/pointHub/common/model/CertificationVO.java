/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.model;

import java.io.Serializable;

import com.olleh.pointHub.common.utils.Common;

/**
 * 인증관련 VO
 * 
 * @Class Name : CertificationVO
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see
 * 
 * 		<pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 *      </pre>
 */
public class CertificationVO implements Serializable {

	/**
	 * 세션 관리 대상으로 Serializable 처리
	 */
	private static final long serialVersionUID = 4085903738181079818L;

	/**
	 * vo 항목
	 */

	private String phoneCorp;	// 통신사구분(SKT, KT, LGT, 별정통신등)
	private String validResult;	// 인증결과
	private String CustDI;		// DI값
	private String SuccYn;		// 본인인증 성공 여부(Y:성공,N:실패)	
	private String custCI;   	// CI값
	private String phoneNo;  	// 핸드폰번호
	private String custName; 	// 고객명
	private String birthDay; 	// 생년월일
	private String gender;   	// 성별
	private String nations;  	// 내/외국인
	private String custId;   	// ph_pnt_cust_info.cust_id
	
	private String ctzNo7;      // 주민번호 앞 7자리(예: 990101 + "남녀구분(1~4))
	
	public String getCtzNo7() {
		return ctzNo7;
	}
	
	public void setCtzNo7(String ctzNo7) {
		this.ctzNo7 = ctzNo7;
	}
	
	public String getPhoneCorp() {
		return phoneCorp;
	}
	public void setPhoneCorp(String phoneCorp) {
		this.phoneCorp = phoneCorp;
	}
	public String getValidResult() {
		return validResult;
	}
	public void setValidResult(String validResult) {
		this.validResult = validResult;
	}
	public String getCustDI() {
		return CustDI;
	}
	public void setCustDI(String custDI) {
		CustDI = custDI;
	}
	public String getSuccYn() {
		return SuccYn;
	}
	public void setSuccYn(String succYn) {
		SuccYn = succYn;
	}
	public String getCustCI() {
		return custCI;
	}
	public void setCustCI(String custCI) {
		this.custCI = custCI;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNations() {
		return nations;
	}
	public void setNations(String nations) {
		this.nations = nations;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
	public String getLogData() {
		return "{phoneCorp=" + phoneCorp + ", validResult=" + validResult + ", CustDI=" + CustDI
				+ ", SuccYn=" + SuccYn + ", custCI=" + custCI + ", phoneNo=" + Common.getMaskHpNo(phoneNo) + ", custName=" + Common.getMaskCustNm(custName)
				+ ", birthDay=" + birthDay + ", gender=" + gender + ", nations=" + nations + ", custId=" + custId + "}";
	}	
	
	@Override
	public String toString() {
		return "CertificationVO [phoneCorp=" + phoneCorp + ", validResult=" + validResult + ", CustDI=" + CustDI
				+ ", SuccYn=" + SuccYn + ", custCI=" + custCI + ", phoneNo=" + phoneNo + ", custName=" + custName
				+ ", birthDay=" + birthDay + ", gender=" + gender + ", nations=" + nations + ", custId=" + custId
				+ ", ctzNo7=" + ctzNo7 + "]";
	}
	
	
}
