/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.model;

import java.io.Serializable;
import java.util.Map;

/**
 * <pre>세틀뱅크(PG) 거래  VO</pre>
 * @Class Name : SbankDealVO
 * @author lys
 * @since 2018.07.31
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.31   lys        최초생성
 * 2018.11.12   sci        가맹점 정보 추가(shopPntRate,shopPntName,rchrPayInd)
 * 2018.11.28   sci        가맹점아이디 추가(포인트사용처 ph_pnt_cmpn_info.cprt_cmpn_id)
 * 2019.01.04   sci        본인인증시(getKmcSendParam.do) 세션에 동의여부(clsList)담기
 * </pre>
 */
public class SbankDealVO implements Serializable {
	/**
	 * 세션 관리 대상으로 Serializable 처리
	 */
	private static final long serialVersionUID = 6618513939262704400L;
	
	/**
	 * vo 항목
	 */	
	private String pgTrNo;		// PG거래번호
	private String pgCd;		// PG코드
	private String userId;		// 사용자아이디
	private String trDiv;	    // 거래구분("PA":결제,"CA":취소)
	private String shopCd;		// 가맹점코드(상점ID)
	private String shopName;	// 가맹점명
	private String shopBizNo;	// 가맹점사업자번호
	private String goodsName;	// 상품명
	private String payAmt;		// 상품금액
	private String payMethod;	// 복합(카드,휴대폰)/단순결제 구분
	private String returnUrl1;	// return URL1
	private String returnUrl2;	// return URL2
	
	private String token;       // 거래토큰(포인트허브거래번호 + "|"(구분자) + 현재시간(UNIX시간, sec) AES-256으로 암호화한 값)
	private String pHubTrNo;    // 포인트허브 거래번호
	
	private String shopPntRate; // 가맹점 포인트 수수료율(%)
	private String shopPntName; // 가맹점 포인트명
	private String rchrPayInd;  // 가맹점 충전/결제 구분 (rchr_pay_ind)
	
	private String cprtCmpnId;  // 가맹점 아이디(포인트허브에서 관리)
	
	private String serviceId;   // 서비스 아이디("SVC_FP":패밀리포인트, "SVC_BASE":기존일반결제건)
	private String addParam;    // 추가파라미터(패밀리포인트 전용,SVC_CD="SVC_BASE"엔 ""값으로 전달됨from PG)
	private String payYn;       // 패밀리포인트 전용(개통자,피요청자 이외 개통자mms통한 조르기화면분기용)
	
	private String ownerYn;     // 패밀리포인트 결제시 5G개통자,피요청자 구분("Y":개통자,"N":피요청자) 
	private String ownerName;   // 패밀리포인트 전용(개통자명:ownerYn="N"인경우 有) 
	private String ownerCtn;    // 패밀리포인트 전용(개통자전화번호:ownerYn="N"인경우 有) 
	private String ownerTrNo;   // 패밀리포인트 전용(개통자의 거래번호:ownerYn="N"인경우 有) 
	
	private String certPath;
	
	private String platform;    // 패밀리포인트 전용(호출 시스템 플랫폼)
	private String rtUrl;		// 패밀리포인트 전용(호출 시스템의 return url);
	
	/**
	 * @return the rtUrl
	 */
	public String getRtUrl() {
		return rtUrl;
	}

	/**
	 * @param rtUrl the rtUrl to set
	 */
	public void setRtUrl(String rtUrl) {
		this.rtUrl = rtUrl;
	}

	/**
	 * @return the certPath
	 */
	public String getCertPath() {
		return certPath;
	}

	/**
	 * @param certPath the certPath to set
	 */
	public void setCertPath(String certPath) {
		this.certPath = certPath;
	}

	/* clsList
	 * clsId  :약관아이디
	 * checkYn:동의여부
	 * prvdrId:카드사ID
     * clsVer :약관버전
	 * clsTitl:약관명
    */
	private Object clsList;     // 약관동의 리스트
	
	
	public String getOwnerTrNo() {
		return ownerTrNo;
	}

	public void setOwnerTrNo(String ownerTrNo) {
		this.ownerTrNo = ownerTrNo;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerCtn() {
		return ownerCtn;
	}

	public void setOwnerCtn(String ownerCtn) {
		this.ownerCtn = ownerCtn;
	}
	
	public SbankDealVO() {
		super();
		this.ownerYn = "Y";
		this.payYn   = "Y";
	}
	
	public String getPayYn() {
		return payYn;
	}

	public void setPayYn(String payYn) {
		this.payYn = payYn;
	}
	
	public String getOwnerYn() {
		return ownerYn;
	}
	public void setOwnerYn(String ownerYn) {
		this.ownerYn = ownerYn;
	}

	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getAddParam() {
		return addParam;
	}
	public void setAddParam(String addParam) {
		this.addParam = addParam;
	}
	
	/**
	 * Getter, Setter
	 */
	public String getPgTrNo() {
		return pgTrNo;
	}
	public String getShopPntRate() {
		return shopPntRate;
	}
	public void setShopPntRate(String shopPntRate) {
		this.shopPntRate = shopPntRate;
	}
	public String getShopPntName() {
		return shopPntName;
	}
	public void setShopPntName(String shopPntName) {
		this.shopPntName = shopPntName;
	}
	public String getRchrPayInd() {
		return rchrPayInd;
	}
	public void setRchrPayInd(String rchrPayInd) {
		this.rchrPayInd = rchrPayInd;
	}
	public void setPgTrNo(String pgTrNo) {
		this.pgTrNo = pgTrNo;
	}
	public String getPgCd() {
		return pgCd;
	}
	public void setPgCd(String pgCd) {
		this.pgCd = pgCd;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTrDiv() {
		return trDiv;
	}
	public void setTrDiv(String trDiv) {
		this.trDiv = trDiv;
	}	
	public String getShopCd() {
		return shopCd;
	}
	public void setShopCd(String shopCd) {
		this.shopCd = shopCd;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopBizNo() {
		return shopBizNo;
	}
	public void setShopBizNo(String shopBizNo) {
		this.shopBizNo = shopBizNo;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getPayAmt() {
		return payAmt;
	}
	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}
	public String getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	public String getReturnUrl1() {
		return returnUrl1;
	}
	public void setReturnUrl1(String returnUrl1) {
		this.returnUrl1 = returnUrl1;
	}
	public String getReturnUrl2() {
		return returnUrl2;
	}
	public void setReturnUrl2(String returnUrl2) {
		this.returnUrl2 = returnUrl2;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getpHubTrNo() {
		return pHubTrNo;
	}
	public void setpHubTrNo(String pHubTrNo) {
		this.pHubTrNo = pHubTrNo;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getCprtCmpnId() {
		return cprtCmpnId;
	}
	public void setCprtCmpnId(String cprtCmpnId) {
		this.cprtCmpnId = cprtCmpnId;
	}

	public Object getClsList() {
		return clsList;
	}
	public void setClsList(Object clsList) {
		this.clsList = clsList;
	}

	/**
	 * @return the platForm
	 */
	public String getPlatform() {
		return platform;
	}

	/**
	 * @param platForm the platForm to set
	 */
	public void setPlatform(String platForm) {
		this.platform = platForm;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SbankDealVO [pgTrNo=" + pgTrNo + ", pgCd=" + pgCd + ", userId=" + userId + ", trDiv=" + trDiv
				+ ", shopCd=" + shopCd + ", shopName=" + shopName + ", shopBizNo=" + shopBizNo + ", goodsName="
				+ goodsName + ", payAmt=" + payAmt + ", payMethod=" + payMethod + ", returnUrl1=" + returnUrl1
				+ ", returnUrl2=" + returnUrl2 + ", token=" + token + ", pHubTrNo=" + pHubTrNo + ", shopPntRate="
				+ shopPntRate + ", shopPntName=" + shopPntName + ", rchrPayInd=" + rchrPayInd + ", cprtCmpnId="
				+ cprtCmpnId + ", serviceId=" + serviceId + ", addParam=" + addParam + ", payYn=" + payYn + ", ownerYn="
				+ ownerYn + ", ownerName=" + ownerName + ", ownerCtn=" + ownerCtn + ", ownerTrNo=" + ownerTrNo
				+ ", certPath=" + certPath + ", clsList=" + clsList + "]";
	}

	
}
