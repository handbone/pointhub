/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 공용VO (정의된 항목에 대해서만 공용으로 관리 한다.)
 * @Class Name : ShareVO
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class ShareVO implements Serializable {

	/**
	 * 세션 관리 대상으로 Serializable 처리
	 */
	private static final long serialVersionUID = 4651648210333963678L;

	/**
	 * 랜딩 유입시 전달되는 파라메터 전용 Map 
	 */
	private Map<String, Object> landingParams;
	
	/**
	 * 공통적으로 사용하는 Map
	 */
	private Map<String, Object> shareMap;
	
	/**
	 * 공통적으로 사용하는 List 
	 */
	private List<Map<String, Object>> shareList;
	
	/**
	 * <pre>shareMap 객체를 리턴한다.</pre>
     *
     * @param  
     * @return Map<String, Object>
     * @see
	 */
	public Map<String, Object> getShareMap() {
		return shareMap;
	}
	
	/**
	 * <pre>shareMap 객체를 셋팅 한다.</pre>
     *
     * @param  Map<String, Object> shareMap
     * @return 
     * @see
	 */	
	public void setShareMap(Map<String, Object> shareMap) {
		this.shareMap = shareMap;
	}
	
	
	/**
	 * <pre>shareList 객체를 반환 한다.</pre>
     *
     * @param  
     * @return List<Map<String, Object>>
     * @see
	 */	
	public List<Map<String, Object>> getShareList() {
		return shareList;
	}
	
	/**
	 * <pre>shareList 객체를 셋팅 한다.</pre>
     *
     * @param  List<Map<String, Object>> shareList
     * @return 
     * @see
	 */	
	public void setShareList(List<Map<String, Object>> shareList) {
		this.shareList = shareList;
	}
}
