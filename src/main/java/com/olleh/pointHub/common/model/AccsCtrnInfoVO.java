/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.model;

/**
 * 포인트허브 접근제어 정보 vo
 * 
 * @Class Name : AccsCtrnInfoVO
 * @author lys
 * @since 2018.08.31
 * @version 1.0
 * @see
 * 
 * 		<pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.31   lys        최초생성
 * 2019.06.05   harry      athnKey 필드 추가
 *      </pre>
 */
public class AccsCtrnInfoVO {

	private String ctrlId; // 제어ID
	private String rltnCorpInd; // 관계사구분 (PG PG사, PV 포인트제공처, CL 포인트사용처, PN	개인사용자)
	private String rltnCorpId; // 관계사아이디
	private String ctrlStat; // 제어상태(Y 허용, N 중지, D 영구중지)
	private String athnKey; // 인증키
	private String ctrlSeq; // 제어순번
	private String ipAddr; // 접속가능IP
	private String ipStat; // 아이피상태(Y 허용, N 중지, D 영구중지)

	/**
	 * getter / setter
	 */
	public String getCtrlId() {
		return ctrlId;
	}

	public void setCtrlId(String ctrlId) {
		this.ctrlId = ctrlId;
	}

	public String getRltnCorpInd() {
		return rltnCorpInd;
	}

	public void setRltnCorpInd(String rltnCorpInd) {
		this.rltnCorpInd = rltnCorpInd;
	}

	public String getRltnCorpId() {
		return rltnCorpId;
	}

	public void setRltnCorpId(String rltnCorpId) {
		this.rltnCorpId = rltnCorpId;
	}

	public String getCtrlStat() {
		return ctrlStat;
	}

	public void setCtrlStat(String ctrlStat) {
		this.ctrlStat = ctrlStat;
	}

	public String getAthnKey() {
		return athnKey;
	}

	public void setAthnKey(String athnKey) {
		this.athnKey = athnKey;
	}

	public String getCtrlSeq() {
		return ctrlSeq;
	}

	public void setCtrlSeq(String ctrlSeq) {
		this.ctrlSeq = ctrlSeq;
	}

	public String getIpAddr() {
		return ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public String getIpStat() {
		return ipStat;
	}

	public void setIpStat(String ipStat) {
		this.ipStat = ipStat;
	}
}
