/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.model;

/**
 * 공통코드 vo
 * @Class Name : CmnCdVO
 * @author lys
 * @since 2018.07.06
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class CmnCdVO {
	private String indCd;		// 마스터코드
	private String dtlCd;		// 상세코드
	private String dtlCdNm;		// 상세코드명
	private String useYn;		// 사용여부
	private String dpYn;		// 출력여부(Y/N)
	private String cdVal;		// 코드값
	private String sortOrd;		// 정렬순서
	private String prvUseCd1;	// 참조코드1
	private String prvUseCd2;	// 참조코드2
	private String prvUseCd3;	// 참조코드3
	
	
	public String getIndCd() {
		return indCd;
	}
	public void setIndCd(String indCd) {
		this.indCd = indCd;
	}
	public String getDtlCd() {
		return dtlCd;
	}
	public void setDtlCd(String dtlCd) {
		this.dtlCd = dtlCd;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getDtlCdNm() {
		return dtlCdNm;
	}
	public void setDtlCdNm(String dtlCdNm) {
		this.dtlCdNm = dtlCdNm;
	}
	public String getDpYn() {
		return dpYn;
	}
	public void setDpYn(String dpYn) {
		this.dpYn = dpYn;
	}
	public String getCdVal() {
		return cdVal;
	}
	public void setCdVal(String cdVal) {
		this.cdVal = cdVal;
	}
	public String getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(String sortOrd) {
		this.sortOrd = sortOrd;
	}
	public String getPrvUseCd1() {
		return prvUseCd1;
	}
	public void setPrvUseCd1(String prvUseCd1) {
		this.prvUseCd1 = prvUseCd1;
	}
	public String getPrvUseCd2() {
		return prvUseCd2;
	}
	public void setPrvUseCd2(String prvUseCd2) {
		this.prvUseCd2 = prvUseCd2;
	}
	public String getPrvUseCd3() {
		return prvUseCd3;
	}
	public void setPrvUseCd3(String prvUseCd3) {
		this.prvUseCd3 = prvUseCd3;
	}
}
