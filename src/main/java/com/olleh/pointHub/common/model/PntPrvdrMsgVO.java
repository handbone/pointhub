/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.model;

/**
 * 클립포인트 결과코드 vo
 * @Class Name : PntPrvdrMsgVO
 * @author lys
 * @since 2018.11.08
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.08   lys        최초생성
 * </pre>
 */
public class PntPrvdrMsgVO {
	
	private String prvdrId;		// 제공처ID (클립포인트, 카드사, 티머니 등..)
	private String msgSeq;		// 메시지 일련번호
	private String pntCd;		// 제공처 연동 코드값 (클립포인트, 카드사, 티머니 등..) 
	private String msgId;		// 메시지ID
	private String msg;			// 메시지명
	private String rfrnVal1;	// 참조값1 (포인트허브 매핑 메시지ID)
	private String rfrnVal2;	// 참조값2
	private String rfrnVal3;	// 참조값3
	
	
	// getter / setter
	public String getPrvdrId() {
		return prvdrId;
	}
	public void setPrvdrId(String prvdrId) {
		this.prvdrId = prvdrId;
	}
	public String getMsgSeq() {
		return msgSeq;
	}
	public void setMsgSeq(String msgSeq) {
		this.msgSeq = msgSeq;
	}
	public String getPntCd() {
		return pntCd;
	}
	public void setPntCd(String pntCd) {
		this.pntCd = pntCd;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getRfrnVal1() {
		return rfrnVal1;
	}
	public void setRfrnVal1(String rfrnVal1) {
		this.rfrnVal1 = rfrnVal1;
	}
	public String getRfrnVal2() {
		return rfrnVal2;
	}
	public void setRfrnVal2(String rfrnVal2) {
		this.rfrnVal2 = rfrnVal2;
	}
	public String getRfrnVal3() {
		return rfrnVal3;
	}
	public void setRfrnVal3(String rfrnVal3) {
		this.rfrnVal3 = rfrnVal3;
	}
}
