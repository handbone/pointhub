/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.model;

import java.util.List;
import java.util.Map;

import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 결과정보 VO (메소드 호출결과를 담는다.)
 * 
 * @Class Name : ResultVo
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * 2018.08.22   sci        VO데이타생성일('yyyymmdd'),일시 추가
 * </pre>
 */
@SuppressWarnings("rawtypes")
public class ResultVo {
    
	/** 결과 클래스 */
	private String sucYn;	// 성공여부
	private Object reqBody;	// 송신Body
	private String rstCd;	// 결과 코드
	private String rstMsg;	// 결과 메시지
	private String rstDate;	// 결과 발생일
	private String rstTime;	// 결과 발생시각
	private Object rstObj;	// 결과 Object
	private Object rstBody;	// 결과 Body Data
	private String caller;	// caller (caller에 따라 결과를 parsing 할때 사용)
	private String pinNo; 	    // 기프티쇼 핀번호
	private String phubCpnNo; 	// 포인트허브 쿠폰번호(vs.pinNo)
	private List list;		    // 리스트 반환
	private Map<String, Object> map;	// Map 반환
	
	public String getPhubCpnNo() {
		return phubCpnNo;
	}
	public void setPhubCpnNo(String phubCpnNo) {
		this.phubCpnNo = phubCpnNo;
	}
	
	/** Getter, Setter */
	public String getSucYn() {
		return StringUtil.nvl(sucYn, "N");
	}
	public void setSucYn(String sucYn) {
		this.sucYn = sucYn;
	}
	public Object getReqBody() {
		return reqBody;
	}
	public void setReqBody(Object resBody) {
		this.reqBody = resBody;
	}
	public String getRstCd() {
		return StringUtil.nvl(rstCd, "");
	}
	public void setRstCd(String rstCd) {
		this.rstCd = rstCd;
	}
	public String getRstMsg() {
		return StringUtil.nvl(rstMsg, "");
	}
	public void setRstMsg(String rstMsg) {
		this.rstMsg = rstMsg;
	}
	public Object getRstObj() {
		return rstObj;
	}
	public void setRstObj(Object rstObj) {
		this.rstObj = rstObj;
	}
	public Object getRstBody() {
		return rstBody;
	}
	public void setRstBody(Object rstBody) {
		this.rstBody = rstBody;
	}
	public String getCaller() {
		return StringUtil.nvl(caller, "");
	}
	public void setCaller(String caller) {
		this.caller = caller;
	}
	public String getRstDate() {
		return rstDate;
	}
	public void setRstDate(String rstDate) {
		this.rstDate = rstDate;
	}
	public String getRstTime() {
		return rstTime;
	}
	public void setRstTime(String rstTime) {
		this.rstTime = rstTime;
	}
	public String getPinNo() {
		return pinNo;
	}
	public void setPinNo(String pinNo) {
		this.pinNo = pinNo;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	public Map<String, Object> getMap() {
		return map;
	}
	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
	
	@Override
	public String toString() {
		return "ResultVo [sucYn=" + sucYn + ", reqBody=" + reqBody + ", rstCd=" + rstCd + ", rstMsg=" + rstMsg
				+ ", rstDate=" + rstDate + ", rstTime=" + rstTime + ", rstObj=" + rstObj + ", rstBody=" + rstBody
				+ ", caller=" + caller + ", pinNo=" + pinNo + ", phubCpnNo=" + phubCpnNo + ", list=" + list + ", map="
				+ map + "]";
	}
	

	
}