/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.model;

/**
 * PG사정보 vo
 * @Class Name : PgCmpnInfoVO
 * @author lys
 * @since 2018.08.31
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.31   lys        최초생성
 * </pre>
 */
public class PgCmpnInfoVO {

	private String pgCmpnId;		// PG사ID
	private String pgCmpnNm;		// PG사명
	private String pntAplyStrtDd;	// 적용시작일
	private String pntAplyEndDd;	// 적용종료일
	private String chrgrNm;			// 담당자명
	private String chrgrTel;		// 담당자연락처
	private String athnKey;			// 인증키
	
	
	/**
	 * getter / setter
	 */
	public String getPgCmpnId() {
		return pgCmpnId;
	}
	public void setPgCmpnId(String pgCmpnId) {
		this.pgCmpnId = pgCmpnId;
	}
	public String getPgCmpnNm() {
		return pgCmpnNm;
	}
	public void setPgCmpnNm(String pgCmpnNm) {
		this.pgCmpnNm = pgCmpnNm;
	}
	public String getPntAplyStrtDd() {
		return pntAplyStrtDd;
	}
	public void setPntAplyStrtDd(String pntAplyStrtDd) {
		this.pntAplyStrtDd = pntAplyStrtDd;
	}
	public String getPntAplyEndDd() {
		return pntAplyEndDd;
	}
	public void setPntAplyEndDd(String pntAplyEndDd) {
		this.pntAplyEndDd = pntAplyEndDd;
	}
	public String getChrgrNm() {
		return chrgrNm;
	}
	public void setChrgrNm(String chrgrNm) {
		this.chrgrNm = chrgrNm;
	}
	public String getChrgrTel() {
		return chrgrTel;
	}
	public void setChrgrTel(String chrgrTel) {
		this.chrgrTel = chrgrTel;
	}
	public String getAthnKey() {
		return athnKey;
	}
	public void setAthnKey(String athnKey) {
		this.athnKey = athnKey;
	}
}
