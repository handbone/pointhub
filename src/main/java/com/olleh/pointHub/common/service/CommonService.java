/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.exception.UserException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.ErrorInfoVO;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;


/**
 * 포인트허브 공통 service
 * @Class Name : CommonService
 * @author lys
 * @since 2018.08.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.05    lys        최초생성
 * </pre>
 */
@Service
public class CommonService {
	private Logger log = new Logger(this.getClass());

	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	MessageManage messageManage;

	
	
	/**
	 * <pre> 공통에러 처리 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. 에러 controller에서만 호출 한다.
	 *      2. error, errorCustom 메소드에서만 호출
	 *      </pre>
	 */
	public ModelAndView errorProc(Map<String, Object> params, HttpServletRequest request) throws Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "errorProc";
		log.debug(methodName, "Start!");
		log.debug(methodName, "params=" + JsonUtil.toJson(params));
		

		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd      = "001";											// step 코드 (익셉션 발생시 사용)
		String requestUri  = StringUtil.nvl(request.getRequestURI());
		String errCd       = StringUtil.nvl(params.get("eCode"));
		String eMsg        = StringUtil.nvl(params.get("eMsg"));
		SbankDealVO voDeal = SessionUtils.getSbankDealVO(request);
		ModelAndView mv    = new ModelAndView();
		
		
		try {
			/***************************************************************
			 * 2. 파라미터 셋팅
			 ***************************************************************/
			// eCode가 null이면 flashMap 조회
			stepCd = "002";
			if( "".equals(errCd) ) {
				Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
				log.debug("errorProc", "flashMap=" +StringUtil.nvl(JsonUtil.toJson(flashMap)));
				
				if( flashMap != null ) {
					errCd = StringUtil.nvl(flashMap.get("eCode"), "F999");
					eMsg  = StringUtil.nvl(flashMap.get("eMsg"));					
				}
			}
			
			// 세션초기화 전 세틀뱅크 전달용 파라미터를 셋팅 한다.
			stepCd = "003";
			mv.addObject("pg_tr_no" ,       StringUtil.nvl(voDeal.getPgTrNo()));
			mv.addObject("return_url_2" ,   StringUtil.nvl(voDeal.getReturnUrl2()));
			mv.addObject(Constant.RET_CODE, errCd);
			mv.addObject(Constant.RET_MSG , eMsg);
			
			// 에러정보 셋팅 (에러화면에서 사용)
			mv.addObject("eCode", errCd);
			mv.addObject("eMsg",  eMsg);
			
			// 세션 초기화 (404 에러는 세션초기화를 하지 않는다.)
			stepCd = "004";
			if( requestUri.indexOf("404") < 0 ) {
				stepCd = "005";
				Map<String, Object> tmpMap = new HashMap<String, Object>();			// temp 맵
				tmpMap.put("sessionProcTyp", "1");	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
				Map<String, Object> sessionMap = sessionMng(tmpMap, request);
				if( ("".equals(StringUtil.nvl(sessionMap))) || (!(Constant.SUCCESS).equals(sessionMap.get(Constant.SUCCESS_YN))) ) {
					// 세션소멸 실패
					log.error("error", "sessionMap=" + JsonUtil.toJson(sessionMap));
				}			
			}
		} catch (Exception e) {
			// 에러 발생
			log.printStackTracePH(methodName, e);
			log.debug(methodName, "[Exception]stepCd="+stepCd+", e="+StringUtil.nvl(e.getMessage()));
			mv.addObject(Constant.RET_CODE, messageManage.getMsgCd("SY_ERROR_900"));
			mv.addObject(Constant.RET_MSG,  StringUtil.nvl(e.getMessage()));
			mv.addObject("eCode", messageManage.getMsgCd("SY_ERROR_900"));
			mv.addObject("eMsg",  StringUtil.nvl(e.getMessage()));			
		}

		
		/***************************************************************
		 * 6. 리턴 
		 ***************************************************************/
		log.debug(methodName, "Complete!");
		return mv;
	}
	
	
	/**
	 * <pre> 로그인세션 관리 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. sessionProcTyp - 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
	 *      </pre>
	 */
	public Map<String, Object> sessionMng(Map<String, Object> params, HttpServletRequest request) throws Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "sessionMng";
		log.debug(methodName, "Start!");
		log.debug(methodName, "params=" + JsonUtil.toJson(params));
		

		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd                 = "001";											// step 코드 (익셉션 발생시 사용)
		String sessionProcTyp         = StringUtil.nvl(params.get("sessionProcTyp"));	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
		Map<String, Object> tmpMap    = new HashMap<String, Object>();					// temp Map
		Map<String, Object> returnMap = new HashMap<String, Object>();					// 리턴맵
		
		
		try {
			/***************************************************************
			 * 2. 로그인 유저 세션 처리
			 ***************************************************************/
			if( "1".equals(sessionProcTyp) ) {
				// 세션소멸 후 새로 생성 한다.
				stepCd = "002";
				HttpSession session = SessionUtils.getSession(request);
				session.invalidate();
				request.getSession(true);
				
			} else if( "2".equals(sessionProcTyp) ) {
				// 세션소멸 후 새로 생성 한다.
				stepCd = "002";
				HttpSession session = SessionUtils.getSession(request);
				session.invalidate();
				request.getSession(true);
				
			} else {
				// 정의되지 않은 세션처리 유형 입니다.
				stepCd = "007";
				throw new UserException(stepCd, messageManage.getMsgVOY("BZ_ERROR_151"));
			}
			

			/***************************************************************
			 * 5. 리턴코드 셋팅 : 정상
			 ***************************************************************/
			returnMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
			returnMap.put(Constant.RET_CODE,   Constant.SUCCESS_CODE);
			returnMap.put(Constant.RET_MSG,    Constant.SUCCESS_MSG);
			
		} catch (UserException e) {
			// 에러메시지 정의
			ErrorInfoVO errinfo = e.getErrorInfo();
			returnMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			returnMap.put(Constant.RET_CODE,   errinfo.getErrCd());
			returnMap.put(Constant.RET_MSG,    errinfo.getErrMsg());			
			
			log.debug(methodName, "[UserException]stepCd="+stepCd+", errCd="+errinfo.getErrCd()+", errMsg="+errinfo.getErrMsg());
			
		} catch (Exception e) {
			// 에러 발생
			log.printStackTracePH(methodName, e);
			log.debug(methodName, "[Exception]stepCd="+stepCd+", e="+StringUtil.nvl(e.getMessage()));
			returnMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			returnMap.put(Constant.RET_CODE,   messageManage.getMsgCd("SY_ERROR_900"));
			returnMap.put(Constant.RET_MSG,    StringUtil.nvl(e.getMessage()));
		}

		
		/***************************************************************
		 * 6. 리턴 
		 ***************************************************************/
		log.debug(methodName, "Complete!");
		return returnMap;
	}
}