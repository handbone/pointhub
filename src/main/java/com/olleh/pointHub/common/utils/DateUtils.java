/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.springframework.util.StringUtils;

/**
 * 일시 관련 정의 Utility
 * @Class Name : DateUtils
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class DateUtils {
	
	static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	static String DEFAULT_TIME_FORMAT = "HH:mm:ss";
	static String DEFAULT_DATE_TIME_FORMAT = DEFAULT_DATE_FORMAT +" "+ DEFAULT_TIME_FORMAT;
	
	
    /**
     * 현재 일자 (yyyyMMdd)
     *
     * @param  
     * @return String
     * @see
     */
	public static String defaultDate() {
		return dateFormatString(new Date(), "yyyyMMdd");
	}
	
	/**
	 * 현재 시간 (HHmmss)
     *
     * @param  
     * @return String
     * @see
	 */
	public static String defaultTime() {
		return dateFormatString(new Date(), "HHmmss");
	}
	
	/**
	 * 현재 시간 스탬프
	 * 
	 * @return
	 */
	public static String defaultTimestamp() {
		return dateFormatString(new Date(), "HHmmssSSS");
	}
	
	/**
	 * 현재 일자 시간
	 * 
	 * @return
	 */
	public static String defaultFullTimestamp() {
		return dateFormatString(new Date(), "yyyyMMddHHmmss");
	}
	
	/**
	 * String 일자 형식 변환 기본형 (yyyy-MM-dd)
     *
     * @param  String yyyyMMdd
     * @return String
     * @see
	 */
	public static String defaultDateFormat(String yyyyMMdd) {
		return dateFormat(yyyyMMdd, DEFAULT_DATE_FORMAT);
	}
	
	/**
	 * String 일자 형식 변환
     *
     * @param  String yyyyMMdd
     * @param  String format
     * @return String
     * @see
	 */
	public static String dateFormat(String yyyyMMdd, String format) {
		return dateFormat(stringFormatDate(yyyyMMdd + defaultTime()), format);
	}
	
	/**
	 * String 시간 형식 변환 기본형 (HH:mm:ss)
     *
     * @param  String HHmmss
     * @return String
     * @see
	 */
	public static String defaultTimeFormat(String HHmmss) {
		return timeFormat(HHmmss, DEFAULT_TIME_FORMAT);
	}
	
	/**
	 * String 시간 형식 변환
     *
     * @param  String HHmmss
     * @param  String format
     * @return String
     * @see
	 */
	public static String timeFormat(String HHmmss, String format) {
		return timeFormat(stringFormatDate(defaultDate() + HHmmss), format);
	}
	
	/**
	 * String 일시 형식 변환 기본형 (yyyy-MM-dd HH:mm:ss)
     *
     * @param  String yyyyMMddHHmmss
     * @return String
     * @see
	 */
	public static String defaultDateTimeFormat(String yyyyMMddHHmmss) {
		return dateTimeFormat(yyyyMMddHHmmss, DEFAULT_DATE_TIME_FORMAT);
	}
	
	/**
	 * String 일시 형식 변환
	 *
     * @param  String yyyyMMddHHmmss
     * @param  String format
     * @return String
     * @see
	 */
	public static String dateTimeFormat(String yyyyMMddHHmmss, String format) {
		return dateTimeFormat(stringFormatDate(yyyyMMddHHmmss), format);
	}
	
	
	/**
	 * Date 일자 형식 변환 기본형 (yyyy-MM-dd)
	 *
     * @param  Date date
     * @return String
     * @see
	 */
	public static String defaultDateFormat(Date date) {
		return dateFormatString(date, DEFAULT_DATE_FORMAT);
	}
	/**
	 * Date 일자 형식 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
     * @see
	 */
	public static String dateFormat(Date date, String format) {
		return dateFormatString(date, format);
	}
	
	/**
	 * Date 시간 형식 변환 기본형 (HH:mm:ss)
	 *
     * @param  Date date
     * @return String
     * @see
	 */
	public static String defaultTimeFormat(Date date) {
		return dateFormatString(date, DEFAULT_DATE_FORMAT);
	}
	
	/**
	 * Date 시간 형식 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
     * @see
	 */
	public static String timeFormat(Date date, String format) {
		return dateFormatString(date, format);
	}
	
	/**
	 * Date 일시 형식 변환 기본형 (yyyy-MM-dd HH:mm:ss)
	 *
     * @param  Date date
     * @return String
     * @see
	 */
	public static String defaultDateTimeFormat(Date date) {
		return dateFormatString(date, DEFAULT_DATE_TIME_FORMAT);
	}
	
	/**
	 * Date 일시 형식 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
     * @see
	 */
	public static String dateTimeFormat(Date date, String format) {
		return dateFormatString(date, format);
	}
	
	/**
	 * Date 일시 → String 일시 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
     * @see
	 */
	private static String dateFormatString(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}
	
	/**
	 * String 일시 → Date 일시 변환
	 *
     * @param  String yyyyMMddHHmmss
     * @return String
     * @see
	 */
	private static Date stringFormatDate(String yyyyMMddHHmmss) {
//		System.out.println("yyyyMMddHHmmss : "+ yyyyMMddHHmmss);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(
			Integer.parseInt(yyyyMMddHHmmss.substring(0, 4)),		// year
			Integer.parseInt(yyyyMMddHHmmss.substring(4, 6)) - 1,	// month
			Integer.parseInt(yyyyMMddHHmmss.substring(6, 8)),		// date
			Integer.parseInt(yyyyMMddHHmmss.substring(8, 10)),		// hour
			Integer.parseInt(yyyyMMddHHmmss.substring(10, 12)),		// minute
			Integer.parseInt(yyyyMMddHHmmss.substring(12, 14))		// second
		);
		
		Date date = new Date();
		date = calendar.getTime();
		
//		System.out.println("date : "+ date);
		return date;
	}
	
	/**
	 * 날짜를 파리미터로 넘겨준 형식대로 취득한다。
	 * 
	 * @param String pattern (YYMMDD날짜형식)
	 * @return String 취득한형식화된 날짜
	 * @see
	 */
    public static String getDateTimeByPattern(String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                pattern, Locale.KOREAN);
        String dateString = formatter.format(new Date());

        return dateString;
    }
    
    /**
     * 날짜에 포함 된 특수문자를 제거후 반환한다.
     * @param date
     * @return
     * @see
     */
    public static String getCleanDateFormat(final String date) {
    	String result = date;
    	if( result != null ) {
    		result = StringUtils.replace(result, "-", "");
    		result = StringUtils.replace(result, ".", "");
    		result = StringUtils.replace(result, "/", "");
    		result = StringUtils.replace(result, ":", "");
    		result = StringUtils.replace(result, " ", "");
    	}
    	return result;
    }
    
    /**
     * date 값에서 yyyyMMdd 값을 뽑아서 반환한다.
     * @param date
     * @return
     * @see
     */
    public static String getCleanYYYYMMDD(final String date) {
    	String result = "";
    	if( !StringUtils.isEmpty(date) ) {
    		result = DateUtils.getCleanDateFormat(date);
    		if( result.length() >= 8 ) {
    			result = result.substring(0, 8);
    		}
    	}
		return result;
    }
    
    /**
     * yyyyMMdd 형식의 문자열을 split 처리해서 반환한다. 
     * @param date
     * @return yyyy[0], MM[1], dd[2]
     * @see
     */
    public static String[] getSplitYYYYMMDD(String pDate) {
    	String   date   = pDate;
    	String[] result = new String[3];
    	if( date != null ) {
    		date = DateUtils.getCleanDateFormat(date);
    		if( date.length() >= 8 ) {
				result[0] = date.substring(0, 4);
				result[1] = date.substring(4, 6);
				result[2] = date.substring(6, 8);
			}
    	}
    	
    	return result;
    }    
	
    /**
     * 포인트허브 거래번호를 인자로 거래당일일자(yyyymmddhh24miss)와 하루 더한 일자를 리턴한다. 
     * @param  phub_tr_no
     * @return yyyymmddhh24miss[0],yyyymmddhh24miss[1]
     * @see
     */
    public static String[] getBetweenDates(String tr_no) {
    	String[] result = new String[2];
    	DateFormat df = new SimpleDateFormat("yyyyMMdd");
    	String sFromDate = "", sToDate = "";
    	
    	if( tr_no == null ) {
    		return result;
    	} else {
    		if(tr_no.length() < 16){
    			return result;
    		}
    	}
    	
    	sFromDate = "20"+ tr_no.substring(2,8);
    	
    	try {
    		
    		Date date = df.parse(sFromDate);
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(date);
    		cal.add(Calendar.DATE, 1);
    		sToDate = df.format(cal.getTime())+ "235959";
    		
    	} catch (ParseException e) {
    		sToDate = "";
    	}
    	
    	result[0] = sFromDate+"000000";
    	result[1] = sToDate;
    	
    	return result;
    }     
    
    
    /**
     * 현재일 기준(오늘 포함)으로 파라미터 수만큼 기간을 구함 
     * @param  interval
     * @return yyyymmddhh24miss[0],yyyymmddhh24miss[1]
     * @see
     */
    public static String[] getBetweenDatesByNum(int interval) {
    	StringBuffer sbStr = new StringBuffer();
    	String[] result = new String[2];
    	DateFormat df = new SimpleDateFormat("yyyyMMdd");
    	String sFromDate = "", sToDate = "";
    		
		Date date = new Date();
		sToDate = df.format(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, -interval+1);
		sFromDate = df.format(cal.getTime())+ "000000";
		sToDate   = sbStr.append(sToDate).append("235959").toString();
    	
    	result[0] = sFromDate;
    	result[1] = sToDate;
    	
    	return result;
    }
	
}
