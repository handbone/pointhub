/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.FrameworkServlet;

import com.olleh.pointHub.common.spring.filter.XssHtmlInputFilter;

/**
 * 시스템 관련 유틸리티
 * @Class Name : SystemUtils
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class SystemUtils {
	public static XssHtmlInputFilter xssHtmlInputFilter = new XssHtmlInputFilter(false);
		
	/**
	 * xss 필터 처리
	 * 
	 * @param html
	 * @return String
	 * @see
	 */
	public static String getCleanHtml(String html) {
		return xssHtmlInputFilter.filter(html);
	}
	
	/**
	 * 화면에서 입력되는 값 필터링 처리 후 반환한다.
	 * 
	 * @param str
	 * @return String
	 * @see
	 */
	public static String getCleanString(String pStr) {
		String str = pStr;
		if( !StringUtils.isEmpty(str) ) {
			str = xssHtmlInputFilter.filter(str);
			str = StringUtils.replace(str, "\"", "&quot;");
			str = StringUtils.replace(str, "'", "&apos;");
			str = StringUtils.replace(str, "<", "&lt;");
			str = StringUtils.replace(str, ">", "&gt;");
			str = StringUtils.replace(str, "(", "");
			str = StringUtils.replace(str, ")", "");
			str = StringUtils.replace(str, "=", "");
			str = StringUtils.replace(str, "%", "");
			str = StringUtils.replace(str, "-", "");
			str = StringUtils.replace(str, "--", "");
		}
		return str;
	}
	
	/**
	 * 화면에서 입력되는 값 필터링(DB) 처리 후 반환한다.
	 * @param str
	 * @return String
	 * @see
	 */
	public static String getCleanDBString(String pStr) {
		String str = pStr;
		if( !StringUtils.isEmpty(str) ) {
			str = SystemUtils.getCleanString(str);
			str = str.toLowerCase();
			
			str = StringUtils.replace(str, "select", "");
			str = StringUtils.replace(str, "from", "");
			str = StringUtils.replace(str, "all_tab_columns", "");
			str = StringUtils.replace(str, "all_tables", "");
			str = StringUtils.replace(str, "get_host_name", "");
			str = StringUtils.replace(str, "substr", "");
			str = StringUtils.replace(str, "when", "");
			str = StringUtils.replace(str, "else", "");
			str = StringUtils.replace(str, "dual", "");
			str = StringUtils.replace(str, "ascii", "");
			str = StringUtils.replace(str, "distinct", "");
			str = StringUtils.replace(str, "union", "");
			str = StringUtils.replace(str, "database_name", "");
		}
		return str;
	}
	
	/**
	 * contextPath 리턴
	 * 
	 * @return String
	 * @see
	 */
	public static String getContextPath() {
		HttpServletRequest request = getCurrentRequest();
		return request==null?null:request.getContextPath();
	}
	
	/**
	 * 현재 HttpServletRequest 반환
	 * 
	 * @return HttpServletRequest
	 * @see
	 */
	public static HttpServletRequest getCurrentRequest() {
		ServletRequestAttributes requestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
		return requestAttributes==null?null:requestAttributes.getRequest();
	}
	
	/**
	 * 현재 스키마 + 도메인 정보를 반환한다.
	 * 
	 * @param HttpServletRequest request
	 * @return String
	 * @see
	 */
	public static String getCurrentDomain(HttpServletRequest request) {
		return String.format("%s://%s", request.getScheme(), request.getServerName());
	}
	
	/**
	 * request 에서 device 정보를 찾아 반환한다.
	 * 
	 * @param HttpServletRequest request
	 * @return Device
	 * @see
	 */
	public static Device getCurrentDevice(HttpServletRequest request) {
		return DeviceUtils.getCurrentDevice(request);
	}
	
	/**
	 * request 에서 device 정보를 찾아서 pc/모바일 구분값을 반환한다.
	 * 
	 * @param HttpServletRequest request
	 * @return Device
	 * @see
	 *     1. 리턴값 구분
	 *        PC : PC
	 *        MB : 모바일 (태블릿 포함)
	 */
	public static String getCurrentDeviceInd(HttpServletRequest request) {
		Device device = DeviceUtils.getCurrentDevice(request);
		String deviceInd = "MB";
		
		// device구분 체크
		if( device.isNormal() ) deviceInd = "PC";
		
		return deviceInd;
	}	
	
	/**
	 * client pc ip address 반환
	 * 
	 * @param HttpServletRequest request 
	 * @return String IP Address
	 * @see
	 */
	public static String getIpAddress(HttpServletRequest request) {
		
		String clientIp = null;
		if (clientIp == null) clientIp = request.getHeader("Proxy-Client-IP");
		if (clientIp == null) clientIp = request.getHeader("WL-Proxy-Client-IP");
		if (clientIp == null) clientIp = request.getHeader("HTTP_CLIENT_IP");
		if (clientIp == null) clientIp = request.getHeader("HTTP_X_FORWARDED_FOR");
		if (clientIp == null) clientIp = request.getHeader("X-FORWARDED-FOR");
		if (clientIp == null) clientIp = request.getHeader("X-Forwarded-For");
		if (clientIp == null) clientIp = request.getRemoteAddr();

		if(clientIp != null){
			clientIp = clientIp.split(",")[0]; // 값이 , 구분자로 한개 이상이 넘어오는 경우가 있으므로 아래 처리함.
		} else {
			clientIp = "";
		}
        return clientIp;
    }
	
	/**
	 * bean 객체가 아닌 곳에서 bean 관련 메소드 호출이 필요한 경우에 bean 으로 접근하기 위한 WebApplicationContext 를 반환한다.
	 * 
	 * web.xml 에 정의한 <servlet-name>smcweb</servlet-name> 을 파라메터로 지정해줘야 bean 메소드에 접근할수 있다.
	 * 
	 * @param 
	 * @return WebApplicationContext
	 * @see
	 */
	public static WebApplicationContext getWebApplicationContext() {
		return WebApplicationContextUtils.getWebApplicationContext(SystemUtils.getCurrentRequest().getSession().getServletContext(), FrameworkServlet.SERVLET_CONTEXT_PREFIX + "smcweb");
	}
	
	/**
	 * bean 객체가 아닌 곳에서 bean 관련 메소드 호출이 필요한 경우에 bean 으로 접근하기 위한 WebApplicationContext 를 반환한다.
	 * 
	 * web.xml 에 정의한 <servlet-name>smcweb</servlet-name> 을 파라메터로 지정해줘야 bean 메소드에 접근할수 있다.
	 * 
	 * @param HttpServletRequest request
	 * @return WebApplicationContext
	 * @see
	 */
	public static WebApplicationContext getWebApplicationContext(HttpServletRequest request) {
		return WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext(), FrameworkServlet.SERVLET_CONTEXT_PREFIX + "smcweb");
	}
	
	/**
	 * beanName 에 해당하는 bean 오브젝트를 반환한다.
	 * 
	 * @param String bean name
	 * @return Object
	 * @throws Exception
	 * @see
	 */
	public static Object getBean(String beanName) throws Exception {
		return getWebApplicationContext().getBean(beanName);
	}
	
    
    /**
	 * request header 에서 user-agent 값을 통해 브라우저가 IE인 경우 버전정보를 반환한다.
	 * 
	 * @param HttpServletRequest request
	 * @return Map<String, Object> true : 8버전이하인경우 false : 그외
	 * @see
	 */
	public static Map<String, Object> getCheckIEversion(final HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		String mapKey = "user-agent";
		String userAgentValue = request.getHeader(mapKey);
		
		if( !StringUtils.isEmpty(userAgentValue) ) {
			Pattern pattern = Pattern.compile("MSIE ([0-9]{1,}[.0-9]{0,})");
			Matcher matcher = pattern.matcher(userAgentValue);
			if( matcher != null && matcher.find(1) ) {
				result.put("ieVer", matcher.group(1)); //IE 브라우저 버전
				
				pattern = Pattern.compile("Trident/([0-9]{1,}[.0-9]{0,})");
				matcher = pattern.matcher(userAgentValue);
				if( matcher != null && matcher.find(1) ) {
					result.put("trident", matcher.group(1)); //IE 호환성보기 인 경우 trident 값 체크 ( ie8 : 4, ie9 : 5, ie10 : 6 )
				}
			}
		}
		
		return result;
	}
	
	
	/**
	 * cmprIP 가 host server의 IP인지 리턴 한다. (ipv4)
	 * 
	 * @param cmprIP
	 * @return
	 */
	public static boolean isHostServerIP(String cmprIP) {

		boolean b = false;

		try {
			
			Enumeration<NetworkInterface> niEnum = NetworkInterface.getNetworkInterfaces();

			breakOut :
			while (niEnum.hasMoreElements()) {

				NetworkInterface ni = niEnum.nextElement();
				Enumeration<InetAddress> iaEnum = ni.getInetAddresses();

				while (iaEnum.hasMoreElements()) {
					
					InetAddress inetAddress = iaEnum.nextElement();

					if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress()
						&& inetAddress.isSiteLocalAddress()
						&& org.apache.commons.lang3.StringUtils.equals(cmprIP, inetAddress.getHostAddress().toString())) {
						
						b = true;
						break breakOut;
					}
				}
			}
		} catch (SocketException e) {
			b = false;
		}

		return b;
	}
	
	/**
	 * host server의 ip를 리턴 한다. (ipv4)
	 * @param 
	 * @return
	 */
	public static String getHostServerIP() {
		String hostAddr = "";

		try {
			Enumeration<NetworkInterface> nienum = NetworkInterface.getNetworkInterfaces();

			while ( nienum.hasMoreElements() ) {

				NetworkInterface ni         = nienum.nextElement();
				Enumeration<InetAddress> kk = ni.getInetAddresses();

				while (kk.hasMoreElements()) {
					InetAddress inetAddress = kk.nextElement();
					
					if (!inetAddress.isLoopbackAddress() && 
							!inetAddress.isLinkLocalAddress() && 
							inetAddress.isSiteLocalAddress()) {

						 hostAddr = inetAddress.getHostAddress().toString();
					}
				}
			}
		} catch (SocketException e) {
			hostAddr = "";
		}
		
		return hostAddr;
 	}	
	
	/**
	 * properties 파일에 정의된 code값과 mapping 되는 value 값을 반환한다.
	 * 
	 * @param code
	 * @param args
	 * @param defaultMessage
	 * @param locale
	 * @param applicationContext
	 * @return String
	 * @see
	 */
	public static String getMessage(final String code, final Object[] args, final String defaultMessage, final Locale locale, final ApplicationContext applicationContext) {
		return applicationContext.getMessage(code, args, defaultMessage, locale);
	}
	public static String getMessage(final String code, final ApplicationContext applicationContext) {
		return SystemUtils.getMessage(code, null, "", Locale.KOREA, applicationContext);
	}
	public static String getMessage(final String code, final Object[] args, final ApplicationContext applicationContext) {
		return SystemUtils.getMessage(code, args, "", Locale.KOREA, applicationContext);
	}
}
