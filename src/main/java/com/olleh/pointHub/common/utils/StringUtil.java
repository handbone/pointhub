/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.utils;

import org.springframework.util.StringUtils;

import com.olleh.pointHub.common.exception.UserException;
import com.olleh.pointHub.common.model.ErrorInfoVO;

/**
 * 공통 문자열 유틸리티 정의
 * @Class Name : StringUtil
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class StringUtil {
	
	
	/**
	 * 문자열 변수가 널인지 여부를 반환한다.
	 * 
	 * @param v 문자열 변수
	 * @return 널여부 널이면 true
	 * @see
	 */
	public static boolean isNull(String v)
	{
		return v == null || v.equals("");
	}
	
	/**
	 * 대상이 null 이거나 "" 인 경우, 지정한 기본값을 리턴한다.
	 * 
	 * @param String str 검사할 대상
	 * @param value 대상이 null 또는 "" 인 경우 리턴할 기본값
	 * @return String
	 * @see
	 */
	public static String nvl(Object obj, String value) {
		return StringUtils.isEmpty(obj) ? value:String.valueOf(obj);
	}
	
	/**
	 * 대상이 null 이거나 "" 인 경우, 지정한 기본값을 리턴한다.
	 * 
	 * @param String str 검사할 대상
	 * @param String value 대상이 null 또는 "" 인 경우 리턴할 기본값
	 * @return String
	 * @see
	 */
	public static String nvl(Object obj) {
		return nvl(obj, "");
	}
    
    /**
     * str 길이 만큼 마스킹 처리해서 반환한다.
     * @param str
     * @return
     * @see
     */
    public static String getMaskStr(final String str) {
    	//String result = "";
    	StringBuffer result = new StringBuffer();
    	if( !StringUtils.isEmpty(str) ) {
    		for( int i=0; i<str.length(); i++ ) {
    			result.append("*");
    		}
    	}
    	
    	return result.toString();
    }
    
    /**
     * idx 길이까지만 반환한다. (시작 index는 0)
     * @param String str
     * @param int idx
     * @return
     * @see
     */
    public static String getStrEndIdx(final String str, final int idx) {
    	String result = nvl(str);
    	
    	if( result.length() > idx ) {
    		result = result.substring(0, idx);
    	}
    	
    	return result;
    }    
    
    /**
     * 유저정의 예외를 한곳에서 출력하기 위해 예외 객체에 담았던 에러 관련 정보를 일정한 형식으로 출력
     * 
     * @param ex 예외객체
     * @param message 메세지취득위한 객체
     * @return String 형식화된 문자열
     * @see
     */
    public static String printUserSysMessage(UserException ex) {
    	StringBuffer sbMessage = new StringBuffer();
    	ErrorInfoVO errorInfo = ex.getErrorInfo();
    	sbMessage.append("[");
    	sbMessage.append(errorInfo.getClassName());
    	sbMessage.append(".");
    	sbMessage.append(errorInfo.getMethodName());
    	sbMessage.append("]");
    	sbMessage.append("(");
    	sbMessage.append(errorInfo.getErrCd());
    	sbMessage.append(")");
    	sbMessage.append(errorInfo.getErrMsg());

    	return sbMessage.toString();
    }    
}