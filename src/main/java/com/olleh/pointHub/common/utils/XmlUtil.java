package com.olleh.pointHub.common.utils;

import java.util.Map;

import org.json.JSONObject;
import org.json.XML;

/**
 * xml 유틸리티
 * @Class Name : XmlUtil
 * @author hwlee
 * @since 2019.01.29
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일			수 정 자		수정내용
 * ----------	--------	-----------------------------
 * 2019.01.29	hwlee		최초생성
 * </pre>
 */
public class XmlUtil{
	
	/**
	 * Map -> xml String 변환 후 리턴
	 * 
	 * @param map
	 * @return String
	 * @see
	 */
	public static String MapToXml(Map<String, Object> map){
		JSONObject jsonObj = new JSONObject(map);
		String xml = XML.toString(jsonObj);
		
		return xml;
	}
	
	/**
	 * xml String -> Map 변환 후 리턴
	 * 
	 * @param String
	 * @return Map
	 * @see
	 */
	public static Map<String, Object> XmlToMap(String xml){
		JSONObject jsonObj = XML.toJSONObject(xml);
		Map<String, Object> map = jsonObj.toMap();
		
		return map;
	}
}
