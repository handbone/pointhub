/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.olleh.pointHub.common.log.Logger;

/**
 * 금액관련 유틸리티
 * @Class Name : Amount
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class Amount {
	
	/**
	 * 포멧
	 */		
    static DecimalFormat dfNumber = new DecimalFormat("#,##0.####");
    static DecimalFormat dfNumberPoint1 = new DecimalFormat("#,##0.0");
    static DecimalFormat dfNumberPoint2 = new DecimalFormat("#,##0.00");
    static DecimalFormat dfNumberPoint3 = new DecimalFormat("#,##0.000");
    static DecimalFormat dfNumberPoint4 = new DecimalFormat("#,##0.0000");
    static DecimalFormat dfNumberPoint5 = new DecimalFormat("###0.##");
    static DecimalFormat dfNumberPoint6 = new DecimalFormat("###0");

    
	/**
	 * <pre>정수형 숫자를 #,###,### 타입의 문자열로 리턴 한다.</pre>
     *
     * @param  int iNumber
     * @return String
     * @see
	 */
    public static String num(int iNumber) {
        return dfNumber.format(iNumber);
    }
    
	/**
	 * <pre>정수형 숫자를 #,###,### 타입의 문자열로 리턴 한다.</pre>
     *
     * @param  int iNumber
     * @param  int i_point
     * @return String
     * @see
     *  1. i_point 로 소숫점 자리수 처리를 한다.
	 */    
    public static String num(int iNumber, int i_point) {
        return num((double) iNumber, i_point);
    }

    /**
     * <pre>long형 숫자를 #,###,### 타입의 문자열로 리턴 한다.</pre>
     *
     * @param  Long lNumber
     * @return String 
     * @see
     */
    public static String num(Long lNumber) {
        return dfNumber.format(lNumber);
    }
    
	/**
	 * <pre>long형 숫자를 #,###,### 타입의 문자열로 리턴 한다.</pre>
     *
     * @param  int iNumber
     * @param  int i_point
     * @return String
     * @see
     *  1. i_point 로 소숫점 자리수 처리를 한다.
	 */     
    public static String num(Long lNumber, int i_point) {
        return num((double) lNumber, i_point);
    }

    /**
     * <pre>실수형 숫자를 #,###,### 타입의 문자열로 리턴 한다.</pre>
     *
     * @param  double dNumber
     * @return String 
     * @see
     */
    public static String num(double dNumber) {
        return dfNumber.format(dNumber);
    }

    /**
     * <pre>실수형 숫자를 정의된 타입의(ex)#,###,###) 문자열로 리턴 한다.</pre>
     *
     * @param  double dNumber
     * @param  int    i_point (소수점 자리수)
     * @return String
     * @see
     *  1. i_point 로 소숫점 자리수 처리를 한다.
     */    
    public static String num(double dNumber, int i_point) {
        try {
	        switch (i_point) {
	            case 1:
	            return dfNumberPoint1.format(dNumber);
	            case 2:
	            return dfNumberPoint2.format(dNumber);
	            case 3:
	            return dfNumberPoint3.format(dNumber);
	            case 4:
	            return dfNumberPoint4.format(dNumber);
	            case 5:
	            return dfNumberPoint5.format(dNumber);
	            default :
	            return "";
	        }
        }catch(Exception e) {
        	return "";
        }
    }

    /**
     * <pre>숫자형 문자열을 #,###,### 타입의 문자열로 리턴 한다.</pre>
     * 
     * @param  String strNumber
     * @return String
     * @see
     */
    public static String num(String strNumber) {
        double dNumber = 0.;
        try {
            dNumber = Double.parseDouble(strNumber);
        } catch (Exception ne) {
            return "0";
        }
        return dfNumber.format(dNumber);
    }
    
    /**
     * <pre>숫자형 문자열을 정의된 타입의(ex)#,###,###) 문자열로 리턴 한다.</pre>
     *
     * @param  String strNumber
     * @param  int    i_point (소수점 자리수)
     * @return String
     * @see
     *  1. i_point 로 소숫점 자리수 처리를 한다.
     */    
    public static String num(String strNumber, int i_point) {
        double dNumber = 0.;
        try {
            dNumber = Double.parseDouble(strNumber);
        } catch (Exception ne) {
            return "0";
        }
        return num(dNumber, i_point);
    }

    /**
     * <pre>BigDecimal형을  #,###,### 타입의 문자열로 리턴 한다.</pre>
     * 
     * @param  BigDecimal bdNumber
     * @return String
     * @see
     */
    public static String num(BigDecimal bdNumber) {
        double dNumber = bdNumber.doubleValue();
        return num(dNumber);
    }
    
    /**
     * <pre>BigDecimal형을 정의된 타입의(ex)#,###,###) 문자열로 리턴 한다.</pre>
     *
     * @param  BigDecimal bdNumber
     * @param  int    i_point (소수점 자리수)
     * @return String
     * @see
     *  1. i_point 로 소숫점 자리수 처리를 한다.
     */ 
    public static String num(BigDecimal bdNumber, int i_point) {
        double dNumber = bdNumber.doubleValue();
        return num(dNumber, i_point);
    }
    
   /**
    * <pre>String 값을 #,###,###,##0.#0 형식으로 바꿈</pre>
    * 
    * @param String str
    * @return #,###,###,###.00
    * @see
    */
    public static String fFormat(String str) {
        if (str == null)
            return "";
        if (str.length() == 0)
            return "";

        DecimalFormat df = new DecimalFormat("#,###,###,##0.00");
        
        try {
            return df.format(Long.parseLong(str));
        } catch (NumberFormatException nfe) {
            try {
            	Double dVal = Double.valueOf(str);
                return df.format(dVal);
            } catch (Exception ee) {
                return "0";
            }
        } catch (Exception e) {
            return "0";
        }
    }

    /**
     * <pre>실수형 문자열을 정수형타입의 문자열로 리턴 한다.</pre>
     * 
     * @param String str
     * @return #,###,###,###.00
     * @see
     */
    public static String intFormat(String str) {
        if (str == null)
            return "";
        if (str.length() == 0)
            return "";

        try {
            return dfNumberPoint6.format(Double.parseDouble(str));
        } catch (NumberFormatException nfe) {
            try {
            	Double dVal = Double.valueOf(str);
                return dfNumberPoint6.format(dVal);
            } catch (Exception ee) {
                return "0";
            }
        } catch (Exception e) {
            return "0";
        }
    }
}