/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.olleh.pointHub.common.model.CertificationVO;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.model.ShareVO;

/**
 * 세션과 관련된 처리를 위한 유틸리티 정의
 * @Class Name : JsonUtil
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class SessionUtils {

	/**
	 * 인증 처리 결과를 get/set 할때 사용할 키값
	 */
	public static String CERTIFICATION_VO = "certificationVO";
	
	/**
	 * 세틀뱅크(PG) 거래정보를 get/set 할때 사용할 키값
	 */
	public static String SBANK_DEAL_VO = "sbankDealVO";	
	
	/**
	 * 공통적으로 사용되는 값들을 get/set 할때 사용할 키값
	 */
	public static String SHARE_VO = "shareVO";
	
	
	/**
	 * 현재 request 에서 session 정보를 취득한다.
	 * 
	 * @param request
	 * @return
	 * @see
	 */
	public static HttpSession getSession(final HttpServletRequest request) {
		return request==null?null:request.getSession();
	}
	
	/**
	 * session 에 key 에 해당하는 값이 존재하는지 확인한다.
	 * @return
	 * @see
	 */
	public static boolean isExist(final HttpServletRequest request, String key) {
		Object obj = getSession(request)==null?null:getSession(request).getAttribute(key);
		return obj == null ? false:true;
	}
	
	/**
	 * 인증 처리 결과 CertificationVO를 반환한다. 이전에 저장한 값이 있으면 그 값을 리턴하고, 없을 경우 생성해서 리턴한다.
	 * @param request
	 * @return
	 * @see
	 */
	public static CertificationVO getCertificationVO(final HttpServletRequest request) {
		CertificationVO vo = null;
		
		if( SessionUtils.isExist(request, SessionUtils.CERTIFICATION_VO) ) {
			vo = (CertificationVO)getSession(request).getAttribute(SessionUtils.CERTIFICATION_VO);
		} else {
			vo = new CertificationVO();
		}
		
		return vo;
	}
	
	/**
	 * 세틀뱅크(PG) 거래정보 SbankDealVO를 세션에 저장한다.
	 * @param request
	 * @param vo
	 * @see
	 */
	public static void setSbankDealVO(final HttpServletRequest request, SbankDealVO vo) {
		
		getSession(request).setAttribute(SessionUtils.SBANK_DEAL_VO, vo);
	}
	
	/**
	 * 세틀뱅크(PG) 거래정보 SbankDealVO를 세션에서 제거한다.
	 * @param request
	 * @param vo
	 * @see
	 */
	public static void removeSbankDealVO(final HttpServletRequest request) {

		getSession(request).removeAttribute(SessionUtils.SBANK_DEAL_VO);
	}
	
	
	
	/**
	 * 세틀뱅크(PG) 거래정보 SbankDealVO를 반환한다. 이전에 저장한 값이 있으면 그 값을 리턴하고, 없을 경우 생성해서 리턴한다.
	 * @param request
	 * @return
	 * @see
	 */
	public static SbankDealVO getSbankDealVO(final HttpServletRequest request) {
		SbankDealVO vo = null;
		
		if( SessionUtils.isExist(request, SessionUtils.SBANK_DEAL_VO) ) {
			vo = (SbankDealVO)getSession(request).getAttribute(SessionUtils.SBANK_DEAL_VO);
		} else {
			vo = new SbankDealVO();
		}
		
		return vo;
	}
	
	/**
	 * 인증 처리 결과 CertificationVO를 세션에 저장한다.
	 * @param request
	 * @param vo
	 * @see
	 */
	public static void setCertificationVO(final HttpServletRequest request, CertificationVO vo) {
		
		getSession(request).setAttribute(SessionUtils.CERTIFICATION_VO, vo);
	}
	
	/**
	 * 인증 처리 결과 CertificationVO를 세션에서 제거한다.
	 * @param request
	 * @param vo
	 * @see
	 */
	public static void removeCertificationVO(final HttpServletRequest request) {

		getSession(request).removeAttribute(SessionUtils.CERTIFICATION_VO);
	}	
	
	
	
	
	/**
	 * 공유되는 정보를 가지고 있는 ShareVO를 반환한다. 이전에 저장한 값이 있으면 그 값을 리턴하고, 없을 경우 생성해서 리턴한다.
	 * @param request
	 * @return
	 * @see
	 */
	public static ShareVO getShareVo(final HttpServletRequest request) {
		ShareVO vo = null;
		
		if( SessionUtils.isExist(request, SessionUtils.SHARE_VO) ) {
			vo = (ShareVO)getSession(request).getAttribute(SessionUtils.SHARE_VO);
		} else {
			vo = new ShareVO();
		}
		return vo;
	}

	/**
	 * 공유정보 ShareVO를 세션에 저장한다.
	 * @param request
	 * @param vo
	 * @see
	 */
	public static void setShareVo(final HttpServletRequest request, ShareVO vo) {
		getSession(request).setAttribute(SessionUtils.SHARE_VO, vo);
	}
	
	/**
	 * 공유정보 ShareVO를 세션에서 제거한다.
	 * @param request
	 * @see
	 */
	public static void removeShareVo(final HttpServletRequest request) {
		getSession(request).removeAttribute(SessionUtils.SHARE_VO);
	}
	
	/**
	 *  ShareVO 의 shareMap 에 key, value 값을 반환한다.
	 *  
	 * @param key
	 * @param request
	 * @return
	 * @see
	 */
	public static Object getShareMapValue(final String key, final HttpServletRequest request) {
		Map<String, Object> shareMap = SessionUtils.getShareVo(request).getShareMap();
		if( shareMap != null ) {
			return shareMap.get(key);
		}
		return null;
	}
	
	/**
	 * ShareVO 의 shareMap 에 key, value 값을 추가한다.
	 * 
	 * @param key
	 * @param value
	 * @param request
	 * @see
	 */
	public static void putShareMapValue(final String key, final Object value, final HttpServletRequest request) {
		ShareVO vo = SessionUtils.getShareVo(request);

		Map<String, Object> shareMap = vo.getShareMap();
		if( shareMap == null ) {
			shareMap = new HashMap<String, Object>();
		}
		
		shareMap.put(key, value);
		
		vo.setShareMap(shareMap);
		SessionUtils.setShareVo(request, vo);
	}
	
	/**
	 * 공유정보 shareMap을 세션에서 제거한다.
	 * @param request
	 * @see
	 */
	public static void removeShareMap(final HttpServletRequest request) {
		ShareVO vo = SessionUtils.getShareVo(request);
		vo.setShareMap(null);
		SessionUtils.setShareVo(request, vo);
	}
}