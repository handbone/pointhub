/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.utils;

import org.apache.commons.collections4.map.ListOrderedMap;
import org.apache.commons.lang3.StringUtils;

/**
 * map key값 소문자로 변환
 * @Class Name : LowerKeyMap
 * @author lys
 * @since 2018.07.16
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.16   lys        최초생성
 * </pre>
 */
public class LowerKeyMap<K, V> extends ListOrderedMap<K, V> {
	/** serialVersionUID */
	private static final long serialVersionUID = 1629908826215938208L;

	/**
     * <pre>key 에 대하여 소문자로 변환하여 super.put
     * (ListOrderedMap) 을 호출한다.</pre>
     * @param key
     *        - '_' 가 포함된 변수명
     * @param value
     *        - 명시된 key 에 대한 값 (변경 없음)
     * @return previous value associated with specified
     *         key, or null if there was no mapping for
     *         key
     * @see
     */
    @SuppressWarnings("unchecked")
	public V put(final K key, final V value) {
        return super.put((K)StringUtils.lowerCase((String) key), value);
    }
}
