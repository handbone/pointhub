/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

/**
 * json 유틸리티
 * @Class Name : JsonUtil
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class JsonUtil {
	
	public static ObjectMapper objectMapper = new ObjectMapper();
	
	// 생성자
    public JsonUtil()
    {
        // 생성자 Code
    }
    
	/**
	 * java object 를 json string 으로 변환 후 리턴
	 * 
	 * @param obj
	 * @return
	 * @see
	 */
	public static String toJson(Object obj) {
		String ret = null;
		try {
			ret = objectMapper.writeValueAsString(obj);
		} catch(Exception e) {
			ret = null;
		}
		
		return ret;
	}
	
	/**
	 * json string 을 java object 로 변환 후 리턴
	 * @param obj
	 * @return
	 * @see
	 */
	public static Object toJava(String json, Class<?> clazz) {
		Object ret = null;
		try {
			ret = objectMapper.readValue(json, clazz);
		} catch(Exception e) {
			ret = null;
		}
		
		return ret;
	}    
    
	/**
	 * <pre>Json String -> Map 형태 변환</pre>
     *
     * @param  String param
     * @return Map<String, Object>
     * @see
	 */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> JsonToMap(String param)
    {
        Gson gson = new Gson();

        return gson.fromJson(param, new HashMap<String,Object>().getClass());
    }
    
    /**
     * Json -> List 변환
     * @param param
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
	public static List<Map<String, Object>> JsonToList(String param) {
    	
    	Gson gson = new Gson();
    	
    	return gson.fromJson(param, new ArrayList<Map<String, Object>>().getClass());
    }
    
	/**
	 * <pre>Json String -> LinkedHashMap 형태 변환(들어온 순서대로)</pre>
     *
     * @param  String param
     * @return LinkedHashMap<String, Object>
     * @see
	 */
    @SuppressWarnings("unchecked")
    public static LinkedHashMap<String, Object> JsonToLinkedHashMap(String param)
    {
        Gson gson = new Gson();

        return gson.fromJson(param, new LinkedHashMap<String,Object>().getClass());
    }

	/**
	 * <pre>List -> Json String 변환</pre>
     *
     * @param  List<?> jqGridList
     * @return String
     * @see
	 */
    public static String ListToJson(List<?> jqGridList)
    {
        Gson gson = new Gson();

        return gson.toJson(jqGridList);
    }

	/**
	 * <pre>Json String 변환</pre>
     *
     * @param  String sData
     * @return String
     * @see
	 */
    public static String OneStringToJson(String sData)
    {
        Gson gson = new Gson();

        return gson.toJson(sData);
    }
    
	/**
	 * <pre>Json String 변환</pre>
     *
     * @param  HashMap<String, Object> map
     * @return String
     * @see
	 */
    public static String HashMapToJson(HashMap<String, Object> map)
    {
        Gson gson = new Gson();

        return gson.toJson(map);
    }
    
	/**
	 * <pre>Json String 변환</pre>
     *
     * @param  Map<String, Object> map
     * @return String
     * @see
	 */
    public static String MapToJson(Map<String, Object> map)
    {
        Gson gson = new Gson();

        return gson.toJson(map);
    }
}
