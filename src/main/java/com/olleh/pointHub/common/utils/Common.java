/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;

/**
 * 널여부 체크, Key Array 반환등 공통으로 사용하는 함수들을 제공한다.
 * @Class Name : Common
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class Common
{
	//private final static Logger log = new Logger(Common.class);

	/**
	 * <pre>문자열 변수가 널인지 여부를 반환한다.</pre>
	 * @param v 문자열 변수
	 * @return 널여부 널이면 true
	 * @see
	 */
	public static boolean isNull(String v)
	{
		return v == null || v.equals("");
	}
	public static boolean isNull(Object v)
	{
		return v == null;
	}	
	/**
	 * <pre>문자열 변수가 널이면 ""을 반환한다.</pre>
	 * @param v 문자열 변수
	 * @return 널이며 "" 반환, 아니면 원래 문자열 반환
	 * @see
	 */
	public static String Null(String v)
	{
		return isNull(v)?"":v;
	}

	public static Object Null(Object v)
	{
		return v == null ? "":v;
	}
	
	public static String Null(String v, String d)
	{
		return isNull(v)?d:v;
	}
	
	public static int len(String s)
	{
		return Null(s).length();
	}	
	
	/**
	 * <pre>map의 key값 Array를 반환한다.</pre>
     *
     * @param  hm map변수
     * @return keay값 array
     * @see
	 */
	public static Object[] getKeys(LinkedHashMap<String, ?> hm)
	{
		if(hm == null) return null;
		Set<String> set = hm.keySet();
		Object[] hmKeys = set.toArray();

		return hmKeys;
	}
	
	/**
	 * 사업자번호에 하이픈(-) 붙이기
	 * 
	 * @param compNo 사업자번호
	 * @return 하이픈(-)이 붙은 번호.
	 * @see
	 */
	public static String splitCompNo(String pCompNo)
	{
		String compNo = Null(pCompNo).replaceAll("-", "");
		if(compNo.length() == 10) 
			return compNo.substring(0, 3) + "-" + compNo.substring(3, 5) + "-" + compNo.substring(5, 10);
		return "";
	}

	/**
	 * 문자열을 바이트 기준으로 자른다.
	 * 
	 * @param String ss 문자열
	 * @param int sz 기준 길이
	 * @return String 자른 문자열
	 * @see
	 */
	public static String sliceByte(String ps, int pz)
	{
		String ss = ps;
		int    sz = pz;
		byte[] bss = ss.getBytes();
		byte[] tss = new byte[sz];
		if(bss.length > sz)
		{
			System.arraycopy(bss, 0, tss, 0, sz);
			ss = new String(tss);
			bss = null;
			tss = null;
		}
		return ss;
	}
	
	/**
	 * 숫자로된 금액을 문자로 변경하여 반환 한다.
	 * 
	 * @param Long money 숫자로된 금액
	 * @return String
	 * @see
	 */
	public static String money2Hangul(Long money) {
		String r = "";
		
		if(money > 0) {
			String[] han1 = {"", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구"};
			String[] han2 = {"", "십", "백", "천"};
			String[] han3 = {"", "만", "억", "조", "경", "해"};
			String sMoney = String.valueOf(money);
			
			StringBuffer result = new StringBuffer();
			int len = sMoney.length();
			for(int i=len-1; i>=0; i--) {
				int cMoney = Integer.valueOf(sMoney.substring(len-i-1, len-i));
				
				result.append(han1[cMoney]);
				
				if(cMoney > 0) {
					result.append(han2[i % 4]);
				}
				
				if(i % 4 == 0) {
					result.append(han3[i / 4]);
				}
			}
			
			r = result.toString();
		}
		
		return r;
	}
	
	/**
	 * 숫자로된 금액을 문자로 변경하여 반환 한다.
	 * 
	 * @param Long money 숫자로된 금액
	 * @return String
	 * @see
	 */	
	public static String money2HangulEx(Long money) {
		String r = "";
		
		if(money > 0) {
			String[] han1 = {"", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
			String[] han2 = {"", "십", "백", "천"};
			String[] han3 = {"", "만", "억", "조", "경", "해"};
			
			String sMoney = String.valueOf(money);
			
			StringBuffer result = new StringBuffer();
			int len = sMoney.length();
			for(int i=len-1; i>=0; i--) {
				int cMoney = Integer.valueOf(sMoney.substring(len-i-1, len-i));
				
				result.append(han1[cMoney]);
				
				if(cMoney > 0) {
					result.append(han2[i % 4]);
				}
				
				if(i % 4 == 0) {
					result.append(han3[i / 4]);
				}
			}
			
			r = result.toString();
		}
		
		return r;
	}
	
	/**
	 * 카멜표기법의 변수를 스네이크 표기법으로 변환 한다.
	 * 
	 * @param String c 카멜표기법 변수
	 * @return String
	 * @see
	 */
	public static String camel2snake(String c)
	{
		if(isNull(c)) return c;
		
		String rtn = "";
		StringBuffer r = new StringBuffer();
		char cc;
		for(int i=0;i<c.length();i++)
		{
			cc = c.charAt(i);
			if(cc >= 0x41 && cc <= 0x5A) {r.append("_");}
			r.append(cc);
		}
		
		rtn = r.toString();
		return rtn.toUpperCase();
	}
	
	/**
	 * 스네이크 표기법의 변수를 카멜표기법으로 변환 한다.
	 * 
	 * @param String s 스네이크 표기법 변수
	 * @return String
	 * @see
	 */
	public static String snake2camel(String s)
	{
		if(isNull(s)) return s;
		
		StringBuffer r = new StringBuffer();
		char cs;
		for(int i=0;i<s.length();i++)
		{
			cs = s.charAt(i);
			if(cs == 0x5F) {r.append((""+cs).toUpperCase());}
			else r.append((""+cs).toLowerCase());
		}
		
		return r.toString();
	}

	/**
	 * SHA-256 변환
	 * 
	 * @param String input
	 * @return String
	 * @see
	 */	
	public static String sha1(String pInput) {
		String input = pInput;
		
		try
		{
			byte[] salt = Common.getRandomString().getBytes();
			MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
			mDigest.reset();
			mDigest.update(salt);
			byte[] result = mDigest.digest(input.getBytes());
			StringBuffer sb = new StringBuffer();
			for(int i=0;i<result.length;i++)
			{
				sb.append(Integer.toString((result[i] % 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		}
		catch (NoSuchAlgorithmException e) {
			input = "";
		}
		return input;
	}
	
	/**
	 * String 을 숫자로 바꿔 Comma 처리
	 *  
	 * @param String str
	 * @return String
	 * @see
	 */
	public static String setComma(String str){
		int result = Integer.valueOf(str);
		DecimalFormat df = new DecimalFormat("#, ##0");
		return df.format(result);
	}
	
    /**
     * 랜덤 문자열을 생성한다.
     *
     * @param  
     * @return String
     * @see
     */
	public static String getRandomString() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
    /**
     * 요청된 길이의 랜덤 숫자를 생성한다.
     *
     * @param  int size
     * @return int
     * @see
     */	
	public static int getRandom(int size) {
	    int x = (int)(System.currentTimeMillis() % size);
	    return x;
	}
    
    /**
     * base64 인코딩 결과를 반환한다.
     * @param binaryData
     * @return
     * @see
     */
    public static byte[] base64Encode(byte[] binaryData) {
    	return Base64.encodeBase64(binaryData);
    }
    
    /**
     * base64 디코딩 결과를 반환한다.
     * @param base64Data
     * @return
     * @see
     */
    public static byte[] base64Decode(byte[] base64Data) {
    	return Base64.decodeBase64(base64Data);
    }
    
    /**
     * 절상, 절하, 반올림 처리 후 결과를 반환한다.
     * @param strMode CEIL:절상 FLOOR:절하 그외 반올림
     * @param nCalcVal 처리할 값(소수점 이하 데이터 포함)
     * @param nDigit 연산 기준 자릿수(-4:천단위 -3:백단위 -2:십단위, -1:원단위 0:소수점 1자리 1:소수점 2자리 ...)
     * @return
     * @see
     */
    public static String calcMath(String strMode, double pCalcVal, int pDigit) {
    	double nCalcVal = pCalcVal;
    	int nDigit      = pDigit;
    	
    	if( "CEIL".equals(strMode) ) { //절상
    		if( nDigit < 0 ) {
    			nDigit = -(nDigit);
    			nCalcVal = Math.ceil(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
    		} else {
    			nCalcVal = Math.ceil(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
    		}
    	} else if( "FLOOR".equals(strMode) ) { //절하
    		if( nDigit < 0 ) {
    			nDigit = -(nDigit);
    			nCalcVal = Math.floor(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
    		} else {
    			nCalcVal = Math.floor(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
    		}
    	} else {
    		if( nDigit < 0 ) {
    			nDigit = -(nDigit);
    			nCalcVal = Math.round(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
    		} else {
    			nCalcVal = Math.round(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
    		}
    	}
    	
    	DecimalFormat df = new DecimalFormat(nDigit<0?"#":"#.#");
    	return df.format(nCalcVal);
    	//return String.valueOf(nCalcVal);
    }
    
    /**
     * 고객명을 마스킹 처리해서 반환한다.
     * @param custNm
     * @return
     * @see
     *  1. 성만 빼고 나머지는 마스킹 처리
     */
    public static String getMaskCustNm(final String custNm) {
    	StringBuffer sbResult = new StringBuffer();
    	
    	if( !StringUtil.isNull(custNm) ) {
    		sbResult.append(custNm.substring(0, 1));
    		for( int i=1; i<custNm.length(); i++ ) {
    			sbResult.append("*");
    		}   		
    	}
    	
    	return sbResult.toString();
    }
    
    /**
     * 핸드폰 번호를 마스킹 처리해서 반환한다.
     * @param hpNo
     * @return
     * @see
     *  1. 끝 4자리만 마스킹 처리
     */
    public static String getMaskHpNo(final String hpNo) {
    	//String result = "";
    	StringBuffer sbResult = new StringBuffer();
    	
    	if( !StringUtil.isNull(hpNo) ) {
        	// 휴대번호 구분자가 - 인 경우
        	if( hpNo.indexOf("-") > 0 ) {
        		String[] strAry = hpNo.split("-", 3);
        		
        		for( int i=0; i<strAry.length; i++ ) {
        			if( i == (strAry.length - 1) ) {
        				sbResult.append(StringUtil.getMaskStr(strAry[i]));
        			} else {
        				sbResult.append(strAry[i])
        						.append("-");
        			}
        		}
        		
        	} else {
        		// 끝에 4자리만 마스킹 처리
        		sbResult.append(hpNo.substring(0, (hpNo.length()-4)))
        				.append(StringUtil.getMaskStr(hpNo.substring((hpNo.length()-4), hpNo.length())));
        	}    		
    	}

    	return sbResult.toString();
    }    
}
