/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.spring.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.service.CommonService;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;

/**
 * 세션을 관리 한다.
 * @Class Name : SessionInterceptor
 * @author lys
 * @since 2018.08.21
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   lys        최초생성
 * </pre>
 */
public class SessionInterceptor extends HandlerInterceptorAdapter {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CommonService commonService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//*.do url만 호출 된다.
		if( handler instanceof HandlerMethod ) { 
			// 최초페이지인 경우와 특수한 페이지의 경우 세션값 초기화 처리를 한다.
			// 최초페이지인(약관동의&본인인증 페이지) 경우 세션을 초기화 하고 들어간다.
			// 에러페이지와 최종처리 url의 경우에도 초기화 할지 확인 필요 - 2018.08.21. 이용수
			// TODO
			if( "/pg/identification.do".equals(request.getRequestURI()) ) {
				log.debug("preHandle", "--- SessionInterceptor.preHandle 수행 ---");
				
				// 세션 초기화
				Map<String, Object> tmpMap = new HashMap<String, Object>();			// temp 맵
				tmpMap.put("sessionProcTyp", "1");	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
				Map<String, Object> sessionMap = commonService.sessionMng(tmpMap, request);
				if( ("".equals(StringUtil.nvl(sessionMap))) || (!(Constant.SUCCESS).equals(sessionMap.get(Constant.SUCCESS_YN))) ) {
					// 세션소멸 실패
					log.error("preHandle", "sessionMap=" + JsonUtil.toJson(sessionMap));
				}
				log.debug("preHandle", "--- Session clear! ---");
			}
		}
		
		return super.preHandle(request, response, handler);
	}


	/**
	 * ajax 요청이 맞는지 체크후 결과를 반환한다.
	 * @param request
	 * @return
	 */
	private boolean isAjaxRequest(HttpServletRequest request) {
		String header = request.getHeader(Constant.AJAX_HEADER);
		if( header != null && header.equals("true") ) {
			return true;
		}
		return false;
 	}
}
