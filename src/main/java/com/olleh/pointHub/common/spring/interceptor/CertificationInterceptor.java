/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.spring.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.CertificationVO;
import com.olleh.pointHub.common.spring.annotation.CertificationCheck;
import com.olleh.pointHub.common.utils.SessionUtils;

/**
 * <pre>사용자가 본인인증을 실행한후 포인트조회/사용을 할 수 있는데,
 * 그 이후에 호출되는 action들의 경우 본인인증 성공 여부를 체크해야 한다.
 * 이 체크하는 부분을 공통으로 처리하기 위한 인터셉터를 정의한다.</pre>
 * 
 * @Class Name : CertificationInterceptor.java
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class CertificationInterceptor extends HandlerInterceptorAdapter {
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 공통 오류 처리를 위한 코드값
	 */
	int CERTIFICATION_FAILD = 979;
	/**
	 * 공통 오류 페이지 
	 */
	String redirectPath = "/error.do";
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//호출 되는 controller 의 method 에 정의된 @RefererCheck annotation 체크.
		if( handler instanceof HandlerMethod ) { 
			//log.debug("preHandle", "--- CertificationInterceptor 수행 ---");
			HandlerMethod handlerMethod = (HandlerMethod)handler;
			CertificationCheck certificationCheck = handlerMethod.getMethodAnnotation(CertificationCheck.class);
			
			if( certificationCheck != null && certificationCheck.required() == true ) {
				//log.debug("preHandle", "certificationCheck.required() == true");
				CertificationVO certificationVO = SessionUtils.getCertificationVO(request);
				
				//본인인증 최종 성공여부 값이 "Y" 가 아니면 본인인증을 실패한 것으로 판단한다. 공통 에러 페이지로 이동시킨다.
				if( !"Y".equals(certificationVO.getSuccYn()) ) {
					//eCode 값을 던지고 ajax 호출인 경우 프론트에서 처리하고, 그외의 경우 공통 에러페이지로 리다이렉트 처리한다.
					if( isAjaxRequest(request) ) {
						response.sendError(CERTIFICATION_FAILD);
					} else {
						//response.sendRedirect(redirectPath + "?eCode=" + CERTIFICATION_FAILD);
						response.sendRedirect(redirectPath);
					}
					log.debug("preHandle", "--- CertificationInterceptor Error! ---");
					return false;
				}
			}
		}
		
		return super.preHandle(request, response, handler);
	}
	
	/**
	 * ajax 요청이 맞는지 체크후 결과를 반환한다.
	 * @param request
	 * @return
	 */
	private boolean isAjaxRequest(HttpServletRequest request) {
		String header = request.getHeader(Constant.AJAX_HEADER);
		if( header != null && header.equals("true") ) {
			return true;
		}
		return false;
 	}
}
