/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.spring.advice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.exception.BaseException;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.model.SbankDealVO;
import com.olleh.pointHub.common.service.CommonService;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.SessionUtils;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;


/**
 * 공통 Advice
 * @Class Name : CommonAdvice
 * @author lys
 * @since 2018.08.23
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23   lys        최초생성
 * </pre>
 */
@ControllerAdvice
public class CommonAdvice implements ResponseBodyAdvice<Object> {
	private Logger log = new Logger(this.getClass());
	
	private final String COMM_MSG = "서비스 이용에 불편을 드려 죄송합니다.\n잠시 후 다시 이용하여 주시기 바랍니다.";
	
	@Autowired
	CommonService commonService;
	
	
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}
	
	
	/**
	 * <pre> server-front ajax통신시 서버에서 프론트로 리턴되는 메시지값을 변환한다. </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return String
	 * @throws Exception 
	 * @see <pre></pre>
	 */	
	@Override
	public Object beforeBodyWrite(Object pBody, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		// TODO Auto-generated method stub
		Object body = pBody;
		
		try {
			// type 확인
			String typeName = body.getClass().getName();
			//log.debug("postHandle", "typeName=" + typeName);		
			
			// declare		
			Map<String, Object> tmpBody = null;
			String retCode = "";
			
			// Ajax 통신일때만
			if( isAjaxRequest(request) ) {
				//log.debug("postHandle", "--- is Ajax ---");
				// 형변환
				if( "java.lang.String".equals(typeName) ) {
					tmpBody = JsonUtil.JsonToMap(String.valueOf(body));
				
				} else if( "java.util.HashMap".equals(typeName) ) {
					tmpBody = new HashMap<String, Object>((Map<String, Object>) body);
				}
				
				// ret_code\":\"00\",\"ret_msg
				retCode = StringUtil.nvl(tmpBody.get(Constant.RET_CODE));
				
				// 리턴코드별 에러 메시지 정의(I로 시작하는 에러코드는 사용자에게 그대로 출력한다. 
				if( !"00".equals(retCode) && !retCode.startsWith("I") ) {
					log.debug("postHandle", "--- beforeBodyWrite 수행 ---");
					tmpBody.put(Constant.RET_CODE, "-999");
					tmpBody.put(Constant.RET_MSG, "서비스 이용에 불편을 드려 죄송합니다.\n잠시 후 다시 이용하여 주시기 바랍니다.[errCode="+retCode+"]");
					
					log.info("postHandle", "beforeBodyWrite.bef="+JsonUtil.toJson(body));
					log.info("postHandle", "beforeBodyWrite.aft="+JsonUtil.toJson(tmpBody));					
					
					// 리턴값 형변환
					if( "java.lang.String".equals(typeName) ) {
						body = JsonUtil.MapToJson(tmpBody);
					
					} else if( "java.util.HashMap".equals(typeName) ) {
						body = tmpBody;
					}				
				}
				
				// 세션초기화
				// 포인트결제요청 성공만 세션을 초기화 한다. (해당 url은 front-server단 마지막 통신구간 이므로, 통신이 정상종료 되면 세션을 초기화 시킨다.)
				if( "00".equals(retCode) && 
						"/phub/std/pay.do".equals(StringUtil.nvl(request.getURI().getPath())) ) {
					
					// 일반서비스일때만 세션 초기화
					SbankDealVO voDeal = SessionUtils.getSbankDealVO(SystemUtils.getCurrentRequest());
					if( (Constant.SERVICE_ID_SVC_BASE).equals(voDeal.getServiceId()) ) {
						//
						Map<String, Object> tmpMap = new HashMap<String, Object>();			// temp 맵
						tmpMap.put("sessionProcTyp", "1");	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
						Map<String, Object> sessionMap = commonService.sessionMng(tmpMap, SystemUtils.getCurrentRequest());
						if( ("".equals(StringUtil.nvl(sessionMap))) || (!(Constant.SUCCESS).equals(sessionMap.get(Constant.SUCCESS_YN))) ) {
							// 세션소멸 실패
							log.error("preHandle", "sessionMap=" + JsonUtil.toJson(sessionMap));
						}						
					}
				}
			}			
		} catch (Exception e)  {
			log.error("postHandle", "[Exception]msg=" + StringUtil.nvl(e.getMessage()));
		}

		
		//log.debug("postHandle", "--- beforeBodyWrite 종료 ---");
		return body;
	}
	
	
	/**
	 * <pre> 익셉션 핸들링 </pre>
	 * 
	 * @param  <?>
	 * @return String
	 * @throws Exception 
	 * @see <pre></pre>
	 */		
    @ResponseStatus(HttpStatus.BAD_REQUEST)  
    @ExceptionHandler(value = BaseException.class)  
    public ModelAndView handleBaseException(BaseException e){
    	log.debug("BaseException", "Start!");
    	log.debug("BaseException", "getErrMsg=" + StringUtil.nvl(e.getErrMsg()));
    	log.debug("BaseException", "getMessage=" + StringUtil.nvl(e.getMessage()));
    	
    	// set ModelAndView
    	ModelAndView mv = new ModelAndView("comm/error");
    	
    	// ajax 통신여부 체크
    	if( isAjaxRequest(SystemUtils.getCurrentRequest()) ) {
    		mv.setViewName("jsonView");
    		mv.addObject(Constant.RET_CODE, StringUtil.nvl(e.getErrCd(), "F997"));
    		mv.addObject(Constant.RET_MSG,  COMM_MSG);    		
    	} else {
    		mv.addObject("eCode", StringUtil.nvl(e.getErrCd(), "F997"));
    		mv.addObject("eMsg",  COMM_MSG);
    	}
    	
    	// return 
    	//log.debug("Exception]", "mv=" + JsonUtil.toJson(mv));
    	return mv;
    }  
      
    @ExceptionHandler(value = Exception.class)  
    public ModelAndView handleException(Exception e) throws Exception {
    	log.printStackTracePH("handleException", e);
    	log.debug("Exception]", "getMessage=" + StringUtil.nvl(e.getMessage()));
    	
    	// set ModelAndView
    	ModelAndView mv = new ModelAndView("comm/error");
    	
    	// ajax 통신여부 체크
    	if( isAjaxRequest(SystemUtils.getCurrentRequest()) ) {
    		mv.setViewName("jsonView");
    		mv.addObject(Constant.RET_CODE, "F998");
    		mv.addObject(Constant.RET_MSG,  COMM_MSG);
    	} else {
    		mv.addObject("eCode", "F998");
    		mv.addObject("eMsg",  COMM_MSG);
    	}
    	// return 
    	//log.debug("Exception]", "mv=" + JsonUtil.toJson(mv));
    	return mv;
    }   

	/**
	 * ajax 요청이 맞는지 체크후 결과를 반환한다. (포인트허브내 Ajax처리 체크용)
	 * @param request
	 * @return
	 */
	private boolean isAjaxRequest(ServerHttpRequest request) {
		
		List<String> header = request.getHeaders().get(Constant.AJAX_HEADER);
		if( header != null && "true".equals(header.get(0)) ) {
			return true;
		}
		return false;
 	}
	
	/**
	 * ajax 요청이 맞는지 체크후 결과를 반환한다. (대외용)
	 * @param request
	 * @return
	 */
	private boolean isAjaxRequest(HttpServletRequest request) {
		String header   = request.getHeader(HttpHeaders.CONTENT_TYPE);
		String phHeader = request.getHeader(Constant.AJAX_HEADER);
		//log.debug("isAjaxRequest]", "header=" + header);
		//log.debug("isAjaxRequest]", "indexOf=" + header.indexOf("application1/json"));
		
		if( (header != null && (header.indexOf("application/json") != -1)) ||
			(phHeader != null && "true".equals(phHeader)) ) {
			return true;
		}
		return false;
 	}
}
