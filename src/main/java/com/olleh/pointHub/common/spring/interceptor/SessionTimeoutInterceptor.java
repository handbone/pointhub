/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.spring.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.spring.annotation.SessionTimeoutCheck;

/**
 * 세션타임아웃을 관리 한다.
 * @Class Name : SessionTimeoutInterceptor
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class SessionTimeoutInterceptor extends HandlerInterceptorAdapter {
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 공통 오류 처리를 위한 코드값
	 */
	int SESSION_TIMED_OUT = 901;
	/**
	 * 공통 오류 페이지 
	 */
	String redirectPath = "/error.do";
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//호출 되는 controller 의 method 에 정의된 @SessionTimeoutCheck annotation 체크.
		if( handler instanceof HandlerMethod ) { 
			//log.debug("preHandle", "--- SessionTimeoutInterceptor 수행 ---");
			HandlerMethod handlerMethod = (HandlerMethod)handler;
			SessionTimeoutCheck sessionTimeoutCheck = handlerMethod.getMethodAnnotation(SessionTimeoutCheck.class);
			
			//eCode 값을 체크해서 리다이렉션 순환 오류가 발생하지 않도록 한다.
			if( sessionTimeoutCheck != null && sessionTimeoutCheck.required() == true 
					&& request.getRequestedSessionId() != null 
					&& !request.isRequestedSessionIdValid() 
					&& StringUtils.isEmpty(request.getParameter("eCode")) ) {

				if( isAjaxRequest(request) ) {
					response.sendError(SESSION_TIMED_OUT);
				} else {
					response.sendRedirect(redirectPath + "?eCode=" + SESSION_TIMED_OUT);
				}
				log.debug("preHandle", "--- SessionTimeoutInterceptor Error! ---");
				return false;
			}
		}
		
		return super.preHandle(request, response, handler);
	}
	
	/**
	 * ajax 요청이 맞는지 체크후 결과를 반환한다.
	 * @param request
	 * @return
	 */
	private boolean isAjaxRequest(HttpServletRequest request) {
		String header = request.getHeader(Constant.AJAX_HEADER);
		if( header != null && header.equals("true") ) {
			return true;
		}
		return false;
 	}
}
