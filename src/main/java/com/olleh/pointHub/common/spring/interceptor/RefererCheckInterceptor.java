/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pointHub.common.spring.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.spring.annotation.RefererCheck;

/**
 * 사용자의 브라우저의 접근 경로를 체크해서 접근 허용 여부를 결정 할때 사용
 * @Class Name : RefererCheckInterceptor
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class RefererCheckInterceptor extends HandlerInterceptorAdapter {
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 공통 오류 처리를 위한 코드값
	 */
	int REFERER_CHECK = 980;
	/**
	 * 공통 오류 페이지 
	 */
	String redirectPath = "/error.do";
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//호출 되는 controller 의 method 에 정의된 @RefererCheck annotation 체크.
		if( handler instanceof HandlerMethod ) {
			//log.debug("preHandle", "--- RefererCheckInterceptor 수행 ---");
			HandlerMethod handlerMethod = (HandlerMethod)handler;
			RefererCheck refererCheck = handlerMethod.getMethodAnnotation(RefererCheck.class);
			
			if( refererCheck != null ) {
				String referer = request.getHeader("referer");
				//log.debug("referer : {}", referer);
				
				if( referer == null || referer.indexOf("localhost") == -1 ) {
					
					if( isAjaxRequest(request) ) {
						response.sendError(REFERER_CHECK);
					} else {
						log.debug("preHandle", "--- RefererCheckInterceptor Error! ---");
						response.sendRedirect(redirectPath + "?eCode=" + REFERER_CHECK);
					}
					return false;
				}
			}
		}
		
		return super.preHandle(request, response, handler);
	}
	
	/**
	 * ajax 요청이 맞는지 체크후 결과를 반환한다.
	 * @param request
	 * @return
	 */
	private boolean isAjaxRequest(HttpServletRequest request) {
		String header = request.getHeader(Constant.AJAX_HEADER);
		if( header != null && header.equals("true") ) {
			return true;
		}
		return false;
 	}
}
