/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pointHub.common.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.olleh.pointHub.common.components.AccsCtrnManage;
import com.olleh.pointHub.common.components.CodeManage;
import com.olleh.pointHub.common.components.Constant;
import com.olleh.pointHub.common.components.MessageManage;
import com.olleh.pointHub.common.components.PgCmpnInfoManage;
import com.olleh.pointHub.common.components.SysPrmtManage;
import com.olleh.pointHub.common.components.UrlManage;
import com.olleh.pointHub.common.log.Logger;
import com.olleh.pointHub.common.service.CommonService;
import com.olleh.pointHub.common.utils.JsonUtil;
import com.olleh.pointHub.common.utils.StringUtil;
import com.olleh.pointHub.common.utils.SystemUtils;


/**
 * 공통기능 처리 컨트롤러
 * @Class Name : CommonController
 * @author lys
 * @since 2018.08.17
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.17   lys        최초생성
 * </pre>
 */
@Controller
public class CommonController {
	private Logger log = new Logger(this.getClass());

	@Autowired
	CodeManage codeManage;
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	UrlManage urlManage;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	AccsCtrnManage acsCtrnManage;
	
	@Autowired
	PgCmpnInfoManage pgCmpnInfoManage;

	
	/**
	 * 404, 405, 500 에러 화면으로 이동한다.
	 * @param params
	 * @param request
	 * @return
	 * @throws Exception
	 */
	
	/**
	 * <pre>404, 405, 500 에러 화면</pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 * </pre>
	 */	
	@RequestMapping(value={"/error.do", "/error400.do", "/error401.do", "/error403.do", "/error404.do", "/error405.do", "/error500.do"})
	public ModelAndView error(@RequestParam Map<String,Object> params, HttpServletRequest request) throws Exception {
		log.debug("error", "params=" +StringUtil.nvl(JsonUtil.toJson(params)));
		
		// declare
		String requestUri  = StringUtil.nvl(request.getRequestURI());
		
		// ModelAndView
		ModelAndView mv = commonService.errorProc(params, request);
		mv.setViewName("comm/error");

		// 404 / 405 / 500 에러코드 추가 설정
		if( requestUri.indexOf("400") != -1 ) {
			mv.addObject(Constant.RET_CODE, "400");
			mv.addObject("eCode", "400");
			
		} else if( requestUri.indexOf("401") != -1 ) {
			mv.addObject(Constant.RET_CODE, "401");
			mv.addObject("eCode", "401");
			
		} else if( requestUri.indexOf("403") != -1 ) {
			mv.addObject(Constant.RET_CODE, "403");
			mv.addObject("eCode", "403");
			
		} else if( requestUri.indexOf("404") != -1 ) {
			mv.addObject(Constant.RET_CODE, "404");
			mv.addObject("eCode", "404");
			
		} else if( requestUri.indexOf("405") != -1 ) {
			mv.addObject(Constant.RET_CODE, "405");
			mv.addObject("eCode", "405");
			
		} else if( requestUri.indexOf("500") != -1 ) {
			mv.addObject(Constant.RET_CODE, "500");
			mv.addObject("eCode", "500");
			
		//} else if ( requestUri.indexOf("/error.do") != -1 ) {
			//mv.addObject("eCode", errCd);
		}
		
		// return
		log.debug("error", "mv=" + JsonUtil.toJson(mv));
		return mv;
	}
	
	
	@RequestMapping(value={"/error_custom.do"})
	public ModelAndView errorCustom(@RequestParam Map<String,Object> params, HttpServletRequest request) throws Exception {
		log.debug("errorCustom", "params=" +StringUtil.nvl(JsonUtil.toJson(params)));
		log.debug("errorCustom", "requestUri=" +StringUtil.nvl(request.getRequestURI()));
		log.debug("errorCustom", "refererUri=" +StringUtil.nvl(request.getHeader("referer")));
		
		// ModelAndView
		ModelAndView mv = commonService.errorProc(params, request);
		mv.setViewName("comm/error_custom");
		
		// \r\n html 태그로 변경
		Map mMap = mv.getModel();
		String eMsg = StringUtil.nvl(mMap.get("eMsg"));
		log.debug("errorCustom", "mMap=" +StringUtil.nvl(JsonUtil.toJson(mMap)));
		
		if( !"".equals(eMsg) ) {
			// html 태그로 변환
			eMsg = eMsg.replaceAll("\\n", "<br/>");
			
			// text 문구에서 [errCode 제거
			int indexOf = eMsg.indexOf("[errCode=");
			if( indexOf != -1 ) {
				//
				eMsg = eMsg.substring(0, indexOf);
			}
			
			mv.addObject("eMsg",  eMsg);
		}
		
		// return
		log.debug("error", "mv=" + JsonUtil.toJson(mv));
		return mv;
	}
}
